<!-- Latest Blog -->
  <div class="main-container col2-right-layout">
    <div class="main container">
      <div class="row">
        <section class="col-sm-9 wow bounceInUp animated">
        <div class="col-main">
          <div class="my-account">
            <div class="page-title">
              <h2>ซื้อ Bid Pack</h2>
            </div>
            <div class="my-wishlist">
              <div class="table-responsive">
                
                  <fieldset>
                    <input type="hidden" value="ROBdJO5tIbODPZHZ" name="form_key">
                    <table id="wishlist-table" class="clean-table linearize-table data-table">
                      <thead>
                        <tr class="first last">
                          <th class="customer-wishlist-item-image"></th>
                          <th class="customer-wishlist-item-info"></th>
                          <th class="customer-wishlist-item-quantity">จำนวน</th>
                          <th class="customer-wishlist-item-price">ราคา</th>
                          <th class="customer-wishlist-item-cart"></th>
                          <th class="customer-wishlist-item-remove"></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr id="item_31" class="first odd" style="
    /* padding-top: 20px; */
"> 
                          <td class="wishlist-cell0 customer-wishlist-item-image"><a title="Softwear Women's Designer" href="#/" class="product-image"> <img width="100" alt="Softwear Women's Designer" src="{!! asset('frontend/punbid/') !!}/products-images/bidpack.png"> </a></td>
                          <td class="wishlist-cell1 customer-wishlist-item-info"><h3 class="product-name"><a title="Softwear Women's Designer" href="#">Bid Pack x 100ea</a></h3>
                            <div class="description std">
                              <div class="inner">
โปรโมชั่น แถมฟรี 10ea</div>
                            </div>
                            </td>
                        <form method="post" action="{!!url('bidcheckout') !!}">
                          <td data-rwd-label="Quantity" class="wishlist-cell2 customer-wishlist-item-quantity">
                            <div class="cart-cell">
                              <div class="add-to-cart-alt">
                                <input type="text" value="1" name="qty" class="input-text qty validate-not-negative-number" pattern="\d*">
                              </div>
                            </div></td>
                          <td data-rwd-label="Price" class="wishlist-cell3 customer-wishlist-item-price"><div class="cart-cell">
                              <div class="price-box"> <span id="product-price-39" class="regular-price"> <span class="price">1,000 THB</span> </span> </div>
                            </div>
                            </td>
                          <td style="vertical-align: middle;">
                      <button class="button btn-add" title="Add All to Cart" type="submit"><span>ซื้อเลย</span></button>
                          </td>
                          <input type="hidden" name="price" value="1000">
                          <input type="hidden" name="name" value="1000 บาท : 100 Bid">
                          <input type="hidden" name="pid" value="1">
                          <input type="hidden" name="paytype" value="bidpack">
                          <input type="hidden" name="url" value="{!!url('') !!}/uploads/bidpacks/1000.svg ">
                          <input type="hidden" name="bids" id="bids" value="100"> 
                        </form>
                        </tr>
                        <tr id="item_33" class="odd">
                          <td class="wishlist-cell0 customer-wishlist-item-image"><a title="Bid Pack x 300ea" href="#" class="product-image"> <img width="100" alt="Bidpack" src="{!! asset('frontend/punbid/') !!}/products-images/bidpack.png"> </a></td>
                          <td class="wishlist-cell1 customer-wishlist-item-info"><h3 class="product-name"><a title="Softwear Women's Designer" href="#">Bid Pack x 200ea</a></h3>
                            <div class="description std">
                              <div class="inner">โปรโมชั่น แถมฟรี 20ea</div>
                            </div>
                            </td>
                        <form method="post" action="{!!url('bidcheckout') !!}">
                          <td data-rwd-label="Quantity" class="wishlist-cell2 customer-wishlist-item-quantity"><div class="cart-cell">
                              <div class="add-to-cart-alt">
                                <input type="text" value="1" name="qty" class="input-text qty validate-not-negative-number" pattern="\d*">
                              </div>
                            </div></td>
                          <td data-rwd-label="Price" class="wishlist-cell3 customer-wishlist-item-price"><div class="cart-cell">
                              <div class="price-box"> <span id="product-price-39" class="regular-price"> <span class="price">2,000 THB</span> </span> </div>
                            </div></td>
                          <td style="vertical-align: middle;">
                      <button class="button btn-add" title="Add All to Cart" type="button"><span>ซื้อเลย</span></button>
                          </td>
                          <input type="hidden" name="price" value="1000">
                          <input type="hidden" name="name" value="1000 บาท : 100 Bid">
                          <input type="hidden" name="pid" value="1">
                          <input type="hidden" name="paytype" value="bidpack">
                          <input type="hidden" name="url" value="{!!url('') !!}/uploads/bidpacks/1000.svg ">
                          <input type="hidden" name="bids" id="bids" value="100"> 
                        </form>
                        </tr>
                        <tr id="item_32" class="last even">
                          <td class="wishlist-cell0 customer-wishlist-item-image"><a title="Slim Fit Casual Shirt" href="#" class="product-image"> <img width="100" alt="Slim Fit Casual Shirt" src="{!! asset('frontend/punbid/') !!}/products-images/bidpack.png"> </a></td>
                          <td class="wishlist-cell1 customer-wishlist-item-info"><h3 class="product-name"><a title="Slim Fit Casual Shirt" href="#">Bid Pack x 300ea</a></h3>
                            <div class="description std">
                              <div class="inner">โปรโมชั่น แถมฟรี 30ea</div>
                            </div>
                            </td>
                          <form method="post" action="{!!url('bidcheckout') !!}">
                          <td data-rwd-label="Quantity" class="wishlist-cell2 customer-wishlist-item-quantity"><div class="cart-cell">
                              <div class="add-to-cart-alt">
                                <input type="text" value="1" name="qty" class="input-text qty validate-not-negative-number" pattern="\d*">
                              </div>
                            </div></td>
                          <td data-rwd-label="Price" class="wishlist-cell3 customer-wishlist-item-price"><div class="cart-cell">
                              <div class="price-box"> <span id="product-price-2" class="regular-price"> <span class="price">3,000 THB</span> </span> </div>
                            </div></td>
                          <td style="vertical-align: middle;">
                      <button class="button btn-add" title="Add All to Cart" type="button"><span>ซื้อเลย</span></button>
                          </td>
                          <input type="hidden" name="price" value="1000">
                          <input type="hidden" name="name" value="1000 บาท : 100 Bid">
                          <input type="hidden" name="pid" value="1">
                          <input type="hidden" name="paytype" value="bidpack">
                          <input type="hidden" name="url" value="{!!url('') !!}/uploads/bidpacks/1000.svg ">
                          <input type="hidden" name="bids" id="bids" value="100"> 
                        </form>
                        </tr>
                      </tbody>
                    </table>
                    
                  </fieldset>
              </div>
            </div>
            
          </div></div>
        </section>
       
        <aside class="col-right sidebar col-sm-3 wow bounceInUp animated">
         @if(Auth::check())
          <div class="block block-account">
            <div class="block-title">My Account</div>
            <div class="block-content">
              <ul>

                <li>
                                <a href="{!! URL::to('myaccount') !!}">
                                    <span
                                            class="livicon-evo vertical-adjust"
                                            data-options="name: user.svg; style: original; size: 18px; tryToSharpen: true;">
                                    </span>
                                    <span>{!! html_entity_decode(Lang::get('topheadder.7')) !!}</span>
                                </a>
                            </li>
                           
                            @if($userdet->group_id==1||$userdet->group_id==2)
                            <li><a href="{!! URL::to('dashboard') !!}"><i class="fa fa-download"></i> {!! html_entity_decode(Lang::get('topheadder.8')) !!}</a></li>
                            @endif
                            <li><a href="{!! URL::to('myaccount') !!}"><i class="fa fa-download"></i> {!! html_entity_decode(Lang::get('topheadder.9')) !!}</a></li>
                            <li><a href="{!! URL::to('myaccount') !!}"><i class="fa fa-cog"></i> {!! html_entity_decode(Lang::get('topheadder.10')) !!}</a></li>
                            <li>
                                <a href="{!!url('myaccount?v=wishlist') !!}">
                                    <i class="fa fa-heart">
                                        <span class="badge-red" id="current_wlists">{!!$countwlists!!}</span>
                                    </i>
                                    {!! html_entity_decode(Lang::get('topheadder.11')) !!}
                                </a>
                            </li>
                            <li>
                                <a href="{!!url('cart') !!}">
                                    <i class="fa fa-shopping-cart">
                                        <span class="badge-red"><?php echo Cart::instance('shop')->count();?></span>
                                    </i>
                                    {!! html_entity_decode(Lang::get('topheadder.12')) !!}
                                </a>
                            </li>
                       
                            <li class="last"<a href="{!! url('user/logout') !!}"><i class="fa fa-futbol-o"></i> {!! html_entity_decode(Lang::get('topheadder.13')) !!}</a></li>
                           
                
              </ul>
            </div>
          </div>
          @else
          <div class="block block-account">
            <div class="block-title">Login</div>
            <div class="block-content">
              <ul>

                <li>
                                <a href="{!! URL::to('user/login') !!}">
                                <span
                                            class="livicon-evo vertical-adjust"
                                            data-options="name: user.svg; style: original; size: 18px; tryToSharpen: true;">
                                    </span>
                                    <span>Login</span>
                </a>
              </li>
            </ul>
          </div>
          @endif
        </aside>

      </div>
    </div>
  </div>
  
  <!-- End Latest Blog --> 