@section('content')

<?php 

  if(Session::get('lang')===null||Session::get('lang')=='')
  {
    $lang = 'th';
  }else{
    $lang = Session::get('lang');
  }
?>
<div class="col-lg-12">
				<div id="rs_grid">
					<div class="row">
					<div class="woocommerce_wrapper rs_listview_div">
					    <ul class="dgm_listdata rs_grid">
@foreach ($rowData as $row)
<?php if($lang=='en'){
							   		 $row->name_th = $row->name_en;
								} ?>
	<li class="mix <?php
					if(isset($row->sale_price)&&$row->sale_price!="0.00")
					{
						echo ' offers';
					}if(isset($row->featured)&&$row->featured==1)
					{
						echo ' recommend';
					}if(isset($row->price)&&$row->price>18000)
					{
						echo ' price';
					}?>" data-value="<?php echo $row->id; ?>">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rs_toppadder30">
		<a href="{{ url('product-detail?view='.$row->id) }}">
			<h4 class="widget-title widget-title-bid">{{$row->name_th}}</h4>
		</a>
		<div class="rs_product_div">
			<a href="{{ url('product-detail?view='.$row->id) }}">
			<div class="rs_product_img">
				<img src="{{ url('') }}/uploads/products/<?php echo $row->featured_img; ?>" class="img-responsive" alt="">
			</div>
			</a>
		    <aside class="widget widget_authors">
		      @if(isset($row->sale_price)&&$row->sale_price!="0.00")
		      <div class="line_yellow">จากราคาปกติ <span class="discount_price">{{ number_format($row->price)}}THB</span>  ซื้อตอนนี้</div>
		      <div class="bid-buy-text2">
		      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding"> 
		      	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-padding">
		        	<div class="bid-button"> <a href="{{ url('product-detail?view='.$row->id) }}" class="rs_button rs_button_orange mg_buttonbuy">ซื้อเลย</a> </div>
		        </div>
		        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-8 no-padding">
		            <div class="bid-buy-text-details">
		            ซื้อด้วย <span class="bid-buy-sp-red">เงินสด <?php echo number_format($row->sale_price); ?></span>THB<br> 
		            </div>
		        </div>
		      </div>
		      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding"> 
		      	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-padding">
		        	<div class="bid-button"> <a href="{{ url('product-detail?view='.$row->id) }}" class="rs_button rs_button_blue mg_buttonbuy">ซื้อเลย</a> </div>
		        </div>
		        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-8 no-padding">
		            <div class="bid-buy-text-details">
		            ซื้อด้วย <span class="bid-buy-sp">S-Pay <?php echo number_format($row->sale_price*0.5); ?></span>S-Pay
		            </div>
		        </div>
		      </div>
		      </div>

		      @else
				<div class="line_yellow">{{ number_format($row->price)}} THB ซื้อตอนนี้</div>
		    
		      <div class="bid-buy-text2">
		      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding"> 
		      	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-padding">
		        	<div class="bid-button"> <a href="{{ url('product-detail?view='.$row->id) }}" class="rs_button rs_button_orange mg_buttonbuy">ซื้อเลย</a> </div>
		        </div>
		        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-8 no-padding">
		            <div class="bid-buy-text-details">
		            ซื้อด้วย <span class="bid-buy-sp-red">เงินสด <?php echo number_format($row->price); ?></span>THB<br> 
		            </div>
		        </div>
		      </div>
		      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding"> 
		      	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-padding">
		        	<div class="bid-button"> <a href="{{ url('product-detail?view='.$row->id) }}" class="rs_button rs_button_blue mg_buttonbuy">ซื้อเลย</a> </div>
		        </div>
		        <div class="col-lg-7 col-md-7 col-sm-8 col-xs-8 no-padding">
		            <div class="bid-buy-text-details">
		            ซื้อด้วย <span class="bid-buy-sp">S-Pay <?php echo number_format($row->price*0.5); ?></span>S-Pay
		            </div>
		        </div>
		      </div>
		      </div>
		        @endif
		    </aside>
		</div>
	</div>
	</li>
@endforeach
						</ul>
					</div>	
					</div>
			 	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="row">
					  <div class="rs_pagination rs_toppadder40 rs_bottompadder40 text-right">
					  {!! $pagination->render() !!}
					  </div>
					</div>
				</div>
				</div>
				
				
</div>
