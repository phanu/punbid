
<div class="container">
<div class="row">
@if(Session::has('messagetext'))
    
       {!! Session::get('messagetext') !!}
     
  @endif
  <ul class="parsley-error-list">
    @foreach($errors->all() as $error)
      <li>{!! $error !!}</li>
    @endforeach
  </ul>   
</div>
</div>

<?php 
use \App\Http\Controllers\BidtransrecordsController; 


if(isset($_GET['pwin'])&&$_GET['pwin']!="")
{   
      Cart::instance('winner')->destroy(); 

      $winner_prod = BidtransrecordsController::frontcheckyouwin($_GET['pwin']); 

    
      if($winner_prod!="nowin"&&$winner_prod!="lose"){

        Cart::instance('winner')->add(
          [
            'id' => $winner_prod->products_bid_id, 
            'name' => $winner_prod->name_th, 
            'qty' => 1, 
            'price' => $winner_prod->product_value_bid,
            'options' => [
                  'pic_url' => $winner_prod->featured_img,'products_code' => $winner_prod->products_code,  
                  'name_en' => $winner_prod->name_en,'paytype' => 'winner',
                  'shiping_cost' => $winner_prod->shiping_cost
                ]
          ]
        );

        $instance_name = 'winner';
      }else{
        $instance_name = 'no';
      }

}elseif(isset($_REQUEST["paytype"])&&($_REQUEST['paytype']=="spay")){
      Cart::instance('spay')->destroy(); 
      Cart::instance('spay')->add(
        [
          'id' => $_REQUEST['pid'], 
          'name' => $_REQUEST['name_th'], 
          'qty' => $_REQUEST['qty'], 
          'price' => $_REQUEST['pricespay'], 
          'options' => [
                  'pic_url' => $_REQUEST['url'],'products_code' => $_REQUEST['products_code'],  
                  'name_en' => $_REQUEST['name_en'],'paytype' => $_REQUEST['paytype'],
                  'color' => isset($_REQUEST['color'])  ? $_REQUEST['color'] : "0",
                  'shiping_cost' => ($_REQUEST['shiping_cost']/2)
                ]
        ]
      );
      $instance_name = 'spay';
}else
{
  if(Cart::instance('shop')->count()=="0")
  { 
    $instance_name = 'no';
  }else{
    $instance_name = 'shop';
  }
}


if($instance_name=='no')
{
  echo '<div class="container">
<div class="row">';
  echo '<div class="alert alert-danger  fade in block-inner">
            <i class="icon-cancel-circle"></i> '.Lang::get('checkout.1').'</div>';
  echo '</div></div>';
  
}else{

      $users_details = \App\Http\Controllers\UserscustomController::display(Session::get('uid'));
      $userdet = $users_details['row'];
?>
<div class="container">
 <h3>{!! html_entity_decode(Lang::get('checkout.2')) !!}</h3>
 <div class="rs_graybg rs_toppadder70 rs_bottompadder100">
  <div class="container">
   <div class="row">
    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

    </div>
    <div class="row">
     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="rs_checkout_form_section">
         <div class="portlet-body form">
          <!-- BEGIN FORM-->
          <form action="{!! url('orderswinner/savepublic') !!}" class="form-horizontal" method="post" id="order_punbidtheme">

            <div class="row">
             <div class="col-md-6">
              <div class="form-group">
               <label class="control-label col-md-3">{!! html_entity_decode(Lang::get('checkout.3')) !!}</label>
               <div class="col-md-9">
                <input type="text" class="form-control" name="firstname" value="{!!$userdet->first_name!!}" required>
                </div>
              </div>

          <div class="form-group">
           <label class="control-label col-md-3">{!! html_entity_decode(Lang::get('checkout.4')) !!}</label>
           <div class="col-md-9">
            <input type="text" class="form-control" name="lastname" value="{!!$userdet->last_name!!}" required>

          </div>
          </div>

            <div class="form-group">
             <label class="control-label col-md-3">{!! html_entity_decode(Lang::get('checkout.5')) !!}</label>
             <div class="col-md-9">
              <div class="radio-list">
               <label class="radio-inline">
                 <input type="radio" name="sex" value="0" <?php if($userdet->sex=="0") echo "checked" ?> required/>
                 {!! html_entity_decode(Lang::get('checkout.6')) !!}</label>
                 <label class="radio-inline">
                   <input type="radio" name="sex" value="1" <?php if($userdet->sex=="1") echo "checked" ?> required/>
                   {!! html_entity_decode(Lang::get('checkout.7')) !!}</label>
                 </div>
               </div>
             </div>

             <div class="form-group">
             <label class="control-label col-md-3">{!! html_entity_decode(Lang::get('checkout.8')) !!}</label>
             <div class="col-md-9">
               <input class="form-control" id="phone" name="phone" type="text" value="{!!$userdet->phone!!}" required/>

             </div>
           </div>
         </div>

         <div class="col-md-6">
         <div class="form-group">
       <label class="control-label col-md-3">{!! html_entity_decode(Lang::get('checkout.9')) !!}</label>
       <div class="col-md-9">
        <textarea class="form-control" name="address" id="address" rows="3" required> {!!$userdet->address!!}</textarea>
      </div>
      </div>

      <div class="form-group">
          <label class="col-sm-3 control-label">{!! html_entity_decode(Lang::get('checkout.10')) !!}</label>
          <div class="col-sm-9">
           <select class="form-control select2" id="xprovince" name="province" required></select>
          </div>
      </div>

      <div class="form-group">
          <label class="col-sm-3 control-label">{!! html_entity_decode(Lang::get('checkout.11')) !!}</label>
          <div class="col-sm-9">
           <select class="form-control select2" name="district" id="xdistrict" required></select>
          </div>
      </div>

      <div class="form-group">
          <label class="col-sm-3 control-label">{!! html_entity_decode(Lang::get('checkout.12')) !!}</label>
          <div class="col-sm-9">
           <select class="form-control select2" name="subdistrict" id="xsubdistrict" required></select>
          </div>
      </div>
        <div class="form-group">
                                                            <label class="col-sm-3 control-label">{!! html_entity_decode(Lang::get('checkout.13')) !!}</label>
                                                            <div class="col-sm-9">
                                                                    <input type="text" id="xzipcode" class="form-control" name="postcode" data-parsley-type="number" value="{!!$userdet->postcode!!}" placeholder="รหัสไปรษณีย์" name="postcode" required>
                                                            </div>
                                                    </div>
         </div>


         <!--/span-->
       </div>
       <!--/row-->



<!-- END FORM-->
</div>

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <div class="rs_checkout_order_section rs_toppadder20 rs_bottompadder20">
      <div class="rs_chkout_headings">
        <h4>{!! html_entity_decode(Lang::get('checkout.14')) !!}</h4>
      </div>
      <div class="woo-cart-table rs_woo_total_table">
        <table class="table">
          <thead>
            <tr>
              <th class="text-center">{!! html_entity_decode(Lang::get('checkout.15')) !!}</th>
              <th class="text-center">{!! html_entity_decode(Lang::get('checkout.16')) !!}</th>
              <th class="text-center">{!! html_entity_decode(Lang::get('checkout.17')) !!}</th>
            </tr>
          </thead>
          <tbody>
            <?php $sum_shipping_cost = number_format(0,2); 
            foreach(Cart::instance($instance_name)->content() as $row) :?>
            <tr>
              <td>
               <p>@if(CNF_LANG=='th') 
                      {!! $row->name !!} 
                      @else 
                      {!! $row->options->name_en !!} 
                      @endif</p>
             </td>
             <td class="text-center">
               <span style="text-align:center;">{!! $row->qty !!}</span>
             </td>

              <td class="text-center">
               <span>{!! number_format($row->price) !!}<small></small>
                      @if($row->options->paytype=='spay') 
                      Spay 
                      @else 
                      THB
                      @endif</span>
              </td>
            </tr>
            <input type="hidden" name="bulk_productCode[]" value="{!! $row->options->products_code !!}">
            <input type="hidden" name="bulk_quantityOrdered[]" value="{!! $row->qty !!}">
            <input type="hidden" name="bulk_priceEach[]" value="{!! $row->price !!}">
            <input type="hidden" name="bulk_shiping_cost[]" value="{!! $row->options->shiping_cost !!}">
            <input type="hidden" name="counter[]">
            @if(isset($row->options->color)&&$row->options->color!="")
            <input type="hidden" name="bulk_product_color[]" value="{!! $row->options->color !!}">
            @endif
            <?php $sum_shipping_cost = $sum_shipping_cost+($row->options->shiping_cost*$row->qty); endforeach; ?>
          </tbody>
          <tfoot>
            <tr>
              <td></td>
              <td>
               <p><span>{!! html_entity_decode(Lang::get('checkout.18')) !!}</span></p>
             </td>
             <td class="text-center">
              <span><?php echo Cart::instance($instance_name)->subtotal(); ?> @if($instance_name=='spay') Spay @else THB @endif</span>
            </td>
            </tr>
          <tr>
              <td></td>
              <td>
               <p><span>{!! html_entity_decode(Lang::get('checkout.19')) !!}</span></p>
             </td>
             <td class="text-center">
              <span><?php echo $sum_shipping_cost; ?> @if($instance_name=='spay') Spay @else THB @endif</span>
            </td>
          </tr>
          <tr>
              <td></td>
              <td>
               <p><span>{!! html_entity_decode(Lang::get('checkout.20')) !!}</span></p>
             </td>
             <td class="text-center">
              <span id="setpaymentfee">0 @if($instance_name=='spay') Spay @else THB @endif</span>
            </td>
          </tr>
            <tr>
              <td></td>
              <td>
               <p><span>{!! html_entity_decode(Lang::get('checkout.21')) !!}</span></p>
             </td>
             <td class="text-center">
              <span id="checkouttotal"><?php echo number_format(str_replace(',', '', Cart::instance($instance_name)->total())+$sum_shipping_cost,2);  ?> @if($instance_name=='spay') Spay @else THB @endif</span>
              <input type="hidden" name="numtotal" id="numtotal" value="<?php echo str_replace(',', '', Cart::instance($instance_name)->total())+$sum_shipping_cost; ?>">
            </td>
          </tr>
          </tfoot>
      </table>
    </div>
  </div>
</div>


<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <div class="rs_checkout_payment_section rs_toppadder20 rs_bottompadder20">
    @if($instance_name!='spay') 
    <div class="rs_chkout_headings">
      <h4>{!! html_entity_decode(Lang::get('checkout.22')) !!}</h4>
    </div>
    <div class="woocommerce-checkout-payment" style="margin-bottom:20px;">
      <ul class="payment_methods methods">
        <!--<li>
          <div class="rs_radiobox">
            <input type="radio" value="paypal" id="radio4" name="payment_type" required="">
            <label for="radio4"></label>
          </div>
          PayPal
          <img src="{!! asset('frontend') !!}/punbidtheme/images/paypal.png" class="pull-right" alt="">
          <div class="payment_box" data-period="paypal">
            <p>Make your payment directly into our bank account. Please use your Order ID as the payment reference. Your order won’t be shipped until the funds have cleared in our account.</p>
          </div>
        </li>-->
        <li>
          <div class="rs_radiobox">
            <input type="radio" value="credit_card" id="radio3" name="payment_type" required>
            <label for="radio3"></label>
          </div>
          {!! html_entity_decode(Lang::get('checkout.23')) !!}
          <img src="{!! asset('frontend') !!}/punbidtheme/images/paysbuy.jpg" class="pull-right" alt="">
          <div class="payment_box" data-period="credit_card">
            <p>{!! html_entity_decode(Lang::get('checkout.24')) !!}</p>
          </div>
        </li>
        <li>
          <div class="rs_radiobox">
            <input type="radio" value="dbt" id="radio1" name="payment_type" required>
            <label for="radio1"></label>
          </div>
          {!! html_entity_decode(Lang::get('checkout.25')) !!}
          <img src="{!! asset('frontend') !!}/punbidtheme/images/01-kbank.png" class="pull-right" alt="">
          <div class="payment_box" data-period="dbt">
            <p>{!! html_entity_decode(Lang::get('checkout.26')) !!}</p>
            <p>{!! html_entity_decode(Lang::get('checkout.27')) !!}</p>
            <p>{!! html_entity_decode(Lang::get('checkout.28')) !!}</p>
          </div>
        </li>
      </ul>
    </div>  
    @endif
    <div class="row">
     <div class="col-md-offset-3 col-md-9" style="
     text-align: right; float:right;
     ">
     @if($instance_name=="spay")
      <input type="hidden" name="payment_type" id="spay" value="spay"> 
      <input type="hidden" name="payment_fee" id="payment_fee" value="<?php echo str_replace(',', '', Cart::instance($instance_name)->total())+$sum_shipping_cost; ?>"> 
     @else
           <input type="hidden" name="payment_fee" id="payment_fee" value=""> 
     @endif
     <input type="hidden" name="orderNumber" id="orderNumber" value=""> 
     <input type="hidden" name="products_bid_id" id="products_bid_id" value="{!!$winner_prod->products_bid_id!!}">
     <input type="hidden" name="status" id="status" value="onhold">   
     <input type="hidden" name="customerNumber" id="customerNumber" value="<?php echo Session::get('uid'); ?>"> 
     <button type="submit" class="rs_button rs_button_orange rs_center_btn rs_toppadder20">{!! html_entity_decode(Lang::get('checkout.29')) !!}</button>


   </div>

 </form>
</div>
</form>
</div>
</div>
</div>
</div>

</div>	


</div>		
  </div></div></div>

<?php } ?>