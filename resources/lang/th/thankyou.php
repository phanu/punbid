<?php 
return array( 

1=>"คุณไม่มีสิทธิเข้าถึงหน้านี้",
2=>"รายการสั่งซื้อ",
3=>"Hi",
4=>", Your recent order on <span class='il'>Sibeza.com</span></a> has been completed. Your order details are shown below for your reference.</p>",
5=>"Order",
6=>"on",
7=>"Product",
8=>"Price",
9=>"Subtotal",
10=>"Payment Method",
11=>"Total",
12=>"Billing Information",
13=>"If you have any questions, simply respond to this email and we'll be happy to help. Thank you ",
14=>"Kind Regards,<br> Customer Service<br>service@sibeza.com",
15=>"Copyright © 2016 <span class='il'>Sibeza.com</span> All rights reserved.",
16=>"Bidpack : ",

);