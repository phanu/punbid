(function($, window){	
	
	eachCount(sesPid);
	getData(sesPid);
	
	function eachCount(p){
		if(p===0){
			var div = $('[data-bidtime]');
		}else{
			var div = $("#prod_"+p);
		}
		$('[data-bidtime]').each(function() {
			var $this = $(this);
			var finalDate = $this.attr('data-bidtime');
			var time = $this.attr("data-bidtime");
			var bid = $this.attr("data-bidnum");
			var price = $this.attr("data-bidprice");
			var closest = $this.closest(".frmproduct");
			var divsum = $(".prod-summary");
			
			console.log(time+", "+bid+", "+price);
			
			if(p===sesPid){
				$this.countdown(finalDate, function(event) {
					var time_format = '<span class="hours"><span class="value">%H</span></span><span class="minutes"><span class="value">%M</span></span><span class="seconds"><span class="value">%S</span></span>';
					if(event.offset.days > 0) {
						time_format = '<span data-value="%-d" class="days"><span class="value">%-dd</span></span> ' + time_format;
					}
					if(event.offset.weeks > 0) {
						time_format = '%-w week%!w ' + time_format;
					}
					closest.find("#deal-countdown").html(event.strftime(time_format));
				});
			}else{
				$this.countdown(finalDate);
			}
			
			price = accounting.formatMoney(price, "", 2, ",", ".");
			
			divsum.find(".amount-bid").html(price);
			//closest.find(".btn-bit").show();				
		});
	}
	
	$("[id='btn-bid']").on("click", function(){
		var pid = $(this).attr("data-pid");
		var pids = $("#prod_"+pid);
		var time = pids.attr("data-bidtime");
		var bid = pids.attr("data-bidnum");
		var price = pids.attr("data-bidprice");
		var cid = pids.attr("data-bidcus");
		var closest = pids.closest(".frmproduct");
		var divsum = $(".prod-summary");
		
		console.log(sesUid);
		
		if(cid===sesUid){
			alert("ท่านไม่สามารถทำรายการติดต่อกันได้ กรุณารองใหม่อีกครั้ง!!!");
		}else{
			//update
			$.ajax({
				method: "POST",
				dataType: "json",
				url: "data/databid.php",
				data: {p: pid, c: sesUid}
			}).done(function( data ) {
				console.log(data);
				pids.attr("data-bidtime", data.d);
				pids.attr("data-bidnum", data.n);
				pids.attr("data-bidprice", data.pr);
				pids.attr("data-bidpid", data.p);
				pids.attr("data-bidcus", data.c);
				
				divsum.find(".user-bid").html(data.u);
				
				eachCount(pid);
			});
		}
		
	});
	
	function getData(p){
		//LOAD DATA
		//update
		setInterval(function() {
			$.ajax({
				method: "POST",
				dataType: "json",
				url: "data/data.php?"+Date.parse(new Date()),
				data: {p: p}
			}).done(function( data ) {
				
				for(var i=0;i<data.length;i++){
					var obj = data[i];
					
					var pids = $("#prod_"+obj.p);
					var closest = pids.closest(".frmproduct");
					var divsum = $(".prod-summary");
					pids.attr("data-bidtime", obj.d);
					pids.attr("data-bidnum", obj.n);
					pids.attr("data-bidprice", obj.pr);
					pids.attr("data-bidpid", obj.p);
					pids.attr("data-bidcus", obj.c);
					
					divsum.find(".user-bid").html(obj.u);
					
					eachCount(obj.p);
				}
				
			});	
		}, 1000);
	}
	
})(jQuery);