<div class="modal fade rs_mypopup" id="login_popup" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header"  style="padding:0px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                  <div class="rs_popup_list">
                    <ul>
                      <li>
                        <div class="rs_popup_list_img"><img src="{!! asset('frontend') !!}/punbidtheme/images/af_1.jpg" alt="" class="img-responsive"></div>
                        <div class="rs_popup_list_data">
                          <h5>{!! html_entity_decode(Lang::get('login.1')) !!}</h5>
                          <p>{!! html_entity_decode(Lang::get('login.2')) !!}</p>
                        </div>
                      </li>
                      <li>
                        <div class="rs_popup_list_img"><img src="{!! asset('frontend') !!}/punbidtheme/images/af_2.jpg" alt="" class="img-responsive"></div>
                        <div class="rs_popup_list_data">
                          <h5>{!! html_entity_decode(Lang::get('login.3')) !!}</h5>
                          <p>{!! html_entity_decode(Lang::get('login.4')) !!}</p>
                        </div>
                      </li>
                      <li>
                        <div class="rs_popup_list_img"><img src="{!! asset('frontend') !!}/punbidtheme/images/af_3.jpg" alt="" class="img-responsive"></div>
                        <div class="rs_popup_list_data">
                          <h5>{!! html_entity_decode(Lang::get('login.5')) !!}</h5>
                          <p>{!! html_entity_decode(Lang::get('login.6')) !!}</p>
                        </div>
                      </li>
                      <li>
                        <div class="rs_popup_list_img"><img src="{!! asset('frontend') !!}/punbidtheme/images/af_6.jpg" alt="" class="img-responsive"></div>
                        <div class="rs_popup_list_data">
                          <h5>{!! html_entity_decode(Lang::get('login.7')) !!}</h5>
                          <p>{!! html_entity_decode(Lang::get('login.8')) !!}</p>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="rs_popup_form">
                     <div class="rs_popup_form_header"> 
                      <h4 class="text-uppercase">{!! html_entity_decode(Lang::get('login.9')) !!}</h4>
                    </div>
                    <form method="post" action="{!! url('user/signin') !!}" id="LoginAjax">
                      <div class="form-group"> <input type="text" name="username" class="form-control" id="tlogin" placeholder="{!! html_entity_decode(Lang::get('login.11')) !!}" required> </div>
                      <div class="form-group"> <input type="password" name="password" class="form-control" id="tpass" placeholder="{!! html_entity_decode(Lang::get('login.12')) !!}" required> <a href="{!! url('user/login?forgot=pass') !!}">{!! html_entity_decode(Lang::get('login.13')) !!}</a> </div>
                      <div class="form-group" style="margin-bottom:0px;"> <button type="submit" id="tsub" class="rs_button rs_button_orange">{!! html_entity_decode(Lang::get('login.14')) !!}</button>
                      <h4 style="    margin: 20px 0px 0px 0px;">{!! html_entity_decode(Lang::get('login.15')) !!}</h4>
                      </div>
                    </form>

                      <div class="form-group has-feedback text-center">
              <p class="text-muted text-center"><b> {!! html_entity_decode(Lang::get('login.10')) !!} </b>    </p>
              <div style="padding:15px 0; float:left;width: 100%;">
                <a href="{!! URL::to('user/socialize/facebook') !!}" class="btn btn-primary"><i class="icon-facebook" id="tfacebook"></i> {!! html_entity_decode(Lang::get('login.16')) !!} </a>
                <a href="{!! URL::to('user/socialize/google') !!}" class="btn btn-danger"><i class="icon-google"></i> {!! html_entity_decode(Lang::get('login.17')) !!} </a>
              <!--<a href="{!! URL::to('user/socialize/twitter') !!}" class="btn btn-info"><i class="icon-twitter"></i> Twitter </a>-->
              </div>
              <p style="margin:0px;">
                     {!! html_entity_decode(Lang::get('login.18')) !!} <a href="{!! url('user/register') !!}">{!! html_entity_decode(Lang::get('login.19')) !!} <i class="fa fa-angle-double-right"></i></a>
                       </p>
            </div>
                  </div>
                </div>
              </div>
            </div>
          </div>