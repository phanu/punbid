<?php 
return array( 
"1"			=> "You don't have permission to access to  this page",  
 "2"			=> "Order list",  
 "3"			=> "Hi",  
 "4"			=> ", Your recent order on Sibeza.com has been completed. Your order details are shown below for your reference.",  
 "5"			=> "Order",  
 "6"			=> "on",  
 "7"			=> "Product",  
 "8"			=> "Price",  
 "9"			=> "Subtotal",  
 "10"			=> "Payment Method",  
 "11"			=> "Total",  
 "12"			=> "Billing Information",  
 "13"			=> "If you have any questions, simply respond to this email and we'll be happy to help. Thank you ",  
 "14"			=> "Kind Regards, Customer Serviceservice@sibeza.com",  
 "15"			=> "Copyright © 2016 Sibeza.com All rights reserved.",  
 "16"			=> "Bidpack : ",  
 );