

		 {!! Form::open(array('url'=>'pages/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{!! $error !!}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Pages CMS Management</legend>
				{!! Form::hidden('pageID', $row['pageID']) !!}					
									  <div class="form-group  " >
										<label for="Title" class=" control-label col-md-4 text-left"> Title <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('title', $row['title'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  ) !!}
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Alias" class=" control-label col-md-4 text-left"> Alias <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('alias', $row['alias'],array('class'=>'form-control', 'placeholder'=>'',   ) !!}
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Note" class=" control-label col-md-4 text-left"> Note </label>
										<div class="col-md-6">
										  {!! Form::text('note', $row['note'],array('class'=>'form-control', 'placeholder'=>'',   ) !!}
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> {!! Form::hidden('created', $row['created']) !!}{!! Form::hidden('updated', $row['updated']) !!}					
									  <div class="form-group  " >
										<label for="Filename" class=" control-label col-md-4 text-left"> Filename <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('filename', $row['filename'],array('class'=>'form-control', 'placeholder'=>'',   ) !!}
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('status', $row['status'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  ) !!}
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Access" class=" control-label col-md-4 text-left"> Access <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='access' rows='5' id='access' class='form-control '  
				         required  >{!! $row['access'] !!}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Allow Guest" class=" control-label col-md-4 text-left"> Allow Guest </label>
										<div class="col-md-6">
										  <textarea name='allow_guest' rows='5' id='allow_guest' class='form-control '  
				           >{!! $row['allow_guest'] !!}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Template" class=" control-label col-md-4 text-left"> Template </label>
										<div class="col-md-6">
										  <textarea name='template' rows='5' id='template' class='form-control '  
				           >{!! $row['template'] !!}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Metakey" class=" control-label col-md-4 text-left"> Metakey </label>
										<div class="col-md-6">
										  <textarea name='metakey' rows='5' id='metakey' class='form-control '  
				           >{!! $row['metakey'] !!}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Metadesc" class=" control-label col-md-4 text-left"> Metadesc </label>
										<div class="col-md-6">
										  <textarea name='metadesc' rows='5' id='metadesc' class='form-control '  
				           >{!! $row['metadesc'] !!}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Default" class=" control-label col-md-4 text-left"> Default </label>
										<div class="col-md-6">
										  <textarea name='default' rows='5' id='default' class='form-control '  
				           >{!! $row['default'] !!}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Slide" class=" control-label col-md-4 text-left"> Slide </label>
										<div class="col-md-6">
										  <textarea name='slide' rows='5' id='slide' class='form-control '  
				           >{!! $row['slide'] !!}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Shop" class=" control-label col-md-4 text-left"> Shop </label>
										<div class="col-md-6">
										  <textarea name='shop' rows='5' id='shop' class='form-control '  
				           >{!! $row['shop'] !!}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {!! html_entity_decode(Lang::get('core.sb_apply') !!}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {!! html_entity_decode(Lang::get('core.sb_save') !!}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
