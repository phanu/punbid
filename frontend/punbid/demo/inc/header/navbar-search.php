<form class="navbar-search" method="get" action="/">
	<label class="sr-only screen-reader-text" for="search">ค้นหารายการประมูล:</label>
	<div class="input-group">
		<input type="text" id="search" class="form-control search-field" dir="ltr" value="" name="s" placeholder="กำลังหาทองคำแท่งอยู่ใช่ไหม?" />
		<div class="input-group-addon search-categories">
			<select name='product_cat' id='product_cat' class='postform resizeselect' >
				<option value='0' selected='selected'>รายการประมูลทั้งหมด</option>
				<option class="level-0" value="laptops-laptops-computers">ทองคำแท่ง</option>
				<option class="level-0" value="ultrabooks-laptops-computers">สร้อยทอง</option>
				<option class="level-0" value="mac-computers-laptops">กำไร</option>
				<option class="level-0" value="all-in-one-laptops-computers">แหวนทอง</option>
				<option class="level-0" value="servers">กรอบพระ</option>
				<option class="level-0" value="peripherals">พระแผง</option>
				<option class="level-0" value="gaming-laptops-computers">ของเด็ก</option>
				<option class="level-0" value="accessories-laptops-computers">แหวนหัวพลอย</option>
			
			</select>
		</div>
		<div class="input-group-btn">
			<input type="hidden" id="search-param" name="post_type" value="product" />
			<button type="submit" class="btn btn-secondary"><i class="ec ec-search"></i></button>
		</div>
	</div>
</form>