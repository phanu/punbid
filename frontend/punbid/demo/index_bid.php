<?php
	include($_SERVER['DOCUMENT_ROOT']."/demo/includes/config.php");
?>
<!DOCTYPE html>
<html lang="en-US" itemscope="itemscope" itemtype="http://schema.org/WebPage">
<head>
<? include("inc/head.php")?>
</head>

<body class="page home page-template-default">
<div id="page" class="hfeed site"> <a class="skip-link screen-reader-text" href="#site-navigation">Skip to navigation</a> <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
	
	<? include("inc/header.php")?>
	<!-- #masthead -->
	
	<div id="content" class="site-content" tabindex="-1">
		<div class="container">
			<div id="primary" class="content-area">
				<main id="main" class="site-main">
					<div class="home-v1-slider" > 
						<!-- ========================================== SECTION – HERO : END========================================= -->
						
						<div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
							<div class="item" style="background-image: url(assets/images/slider/banner-2.jpg);">
								<div class="container">
									<div class="row">
										<div class="col-md-offset-3 col-md-5"> 
											<!-- /.caption --> 
										</div>
									</div>
								</div>
								<!-- /.container --> 
							</div>
							<!-- /.item -->
							
							<div class="item" style="background-image: url(assets/images/slider/banner-1.jpg);">
								<div class="container">
									<div class="row">
										<div class="col-md-offset-3 col-md-5">
											<div class="caption vertical-center text-left">
												<div class="hero-subtitle-v2 fadeInDown-1"> GoGoldRun.com </div>
												<div class="hero-2 fadeInDown-2"> ทองคำราคาถูกที่สุดในโลก</strong> </div>
												<div class="hero-action-btn fadeInDown-3"> <a href="single-product.html" class="big le-button ">BID NOW</a> </div>
											</div>
											<!-- /.caption --> 
										</div>
									</div>
								</div>
								<!-- /.container --> 
							</div>
							<!-- /.item -->
							
							<div class="item" style="background-image: url(assets/images/slider/banner-3.jpg);">
								<div class="container">
									<div class="row">
										<div class="col-md-offset-3 col-md-5">
											<div class="caption vertical-center text-left">
												<div class="hero-subtitle-v2 fadeInLeft-1"> GoGoldRun </div>
												<div class="hero-2 fadeInRight-1"> ไปๆกันไปเอาทองกัน</strong> </div>
												<div class="hero-action-btn fadeInLeft-2"> <a href="single-product.html" class="big le-button ">BID NOW</a> </div>
											</div>
											<!-- /.caption --> 
										</div>
									</div>
								</div>
								<!-- /.container --> 
							</div>
							<!-- /.item --> 
							
						</div>
						<!-- /.owl-carousel --> 
						
						<!-- ========================================= SECTION – HERO : END ========================================= --> 
						
					</div>
					<!-- /.home-v1-slider -->
					
					<div class="home-v1-ads-block animate-in-view fadeIn animated" data-animation="fadeIn">
						<div class="ads-block row">
							<div class="ad col-xs-12 col-sm-4">
								<div class="media">
									<div class="media-left media-middle"> <img data-echo="assets/images/banner/cameras.jpg" src="assets/images/blank.gif" alt=""> </div>
									<div class="media-body media-middle">
										<div class="ad-text"> สร้อยคอทองคำหนัก 1 บาท</div>
										<div class="ad-action"> <a href="#">BID NOW</a> </div>
									</div>
								</div>
							</div>
							<div class="ad col-xs-12 col-sm-4">
								<div class="media">
									<div class="media-left media-middle"> <img data-echo="assets/images/banner/MobileDevicesv2-2.jpg" src="assets/images/blank.gif" alt=""> </div>
									<div class="media-body media-middle">
										<div class="ad-text"> สร้อยคอทองคำหนัก 2 บาท</div>
										<div class="ad-action"> <a href="#">BID NOW</a> </div>
									</div>
								</div>
							</div>
							<div class="ad col-xs-12 col-sm-4">
								<div class="media">
									<div class="media-left media-middle"> <img data-echo="assets/images/banner/DesktopPC.jpg" src="assets/images/blank.gif" alt=""> </div>
									<div class="media-body media-middle">
										<div class="ad-text"> สร้อยคอทองคำหนัก 1 บาท </div>
										<div class="ad-action"> <a href="#">BID NOW</a> </div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="home-v1-deals-and-tabs deals-and-tabs row animate-in-view fadeIn animated" data-animation="fadeIn">
						<div class="deals-block col-lg-4">
							<section class="section-onsale-product">
								<header>
									<h2 class="h1">ไกล้หมดเวลา</h2>
									<div class="savings"> <span class="savings-text">ประหยัด<span class="amount">99%</span></span> </div>
								</header>
								<!-- /header -->
								
								<div class="onsale-products">
									<div class="onsale-product"> <a href="shop.html">
										<div class="product-thumbnail"> <img class="wp-post-image" data-echo="assets/images/onsale-product.jpg" src="assets/images/onsale-product.jpg" alt=""></div>
										<h3>สร้อยคอทองคำหนัก 1 บาท</h3>
										</a> <span class="price"> <span class="electro-price"> <ins><span class="amount">8.00฿</span></ins> <del><span class="amount">24,000฿</span></del> </span> </span><!-- /.price -->
										
										<div class="deal-progress">
											<div class="deal-stock"> <span class="stock-sold">ประมูลทั้งหมด<strong> 80 ครั้ง</strong></span> <span class="stock-available">มีคนประมูลแล้ว <strong>26</strong></span> </div>
											<div class="progress"> <span class="progress-bar" style="width:26%"> 80 </span> </div>
										</div>
										<!-- /.deal-progress -->
										
										<!--<div class="deal-countdown-timer"
											data-time="2016-06-22 02:00:00"
											data-bid="80"
											data-price="8.00">
											<div class="marketing-text text-xs-center">จบวันนี้!</div>
											<div id="deal-countdown" class="countdown">	
												<span class="days"><span class="value">--</span><b>Days</b></span> 
												<span class="hours"><span class="value">--</span><b>Hours</b></span> 
												<span class="minutes"><span class="value">--</span><b>Mins</b></span> 
												<span class="seconds"><span class="value">--</span><b>Secs</b></span> 
											</div>
											<span class="deal-end-date" style="display:none">2016-10-22 02:00:00</span>
											<div style="width:100%; text-align:center; margin-top:10px" class="btn-bit"></div>
										</div>-->
										<!-- /.deal-countdown-timer --> 
									</div>
									<!-- /.onsale-product --> 
								</div>
								<!-- /.onsale-products --> 
							</section>
							<!-- /.section-onsale-product --> 
						</div>
						<!-- /.col -->
						
						<div class="tabs-block col-lg-8">
							<div class="products-carousel-tabs">
								<ul class="nav nav-inline">
									<li class="nav-item"><a class="nav-link active" href="#tab-products-1" data-toggle="tab">กำลังประมูล</a></li>
									<li class="nav-item"><a class="nav-link" href="#tab-products-2" data-toggle="tab">ไกล้ประมูล</a></li>
									<li class="nav-item"><a class="nav-link" href="#tab-products-3" data-toggle="tab">ปิดประมูล</a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="tab-products-1" role="tabpanel">
										<div class="woocommerce columns-3">
											<ul class="products columns-3">
												<?
													$sql = mysql_query("
														SELECT
															sc_prod_sale.psid
															, sc_prod_sale.pid
															, sc_prod_sale.ps_start_price
															, sc_prod_sale.ps_end_price
															, sc_prod_sale.ps_start_date
															, sc_prod_sale.ps_end_date
															, sc_prod.prod_title
															, sc_prod.prod_sku
															, sc_prod.prod_price
															, sc_prod_sale.ps_status
														FROM
															sc_prod_sale
															INNER JOIN sc_prod 
																ON (sc_prod_sale.pid = sc_prod.pid)
														WHERE (sc_prod_sale.ps_status ='0')
														ORDER BY sc_prod_sale.ps_end_date ASC
													");
													$i=0;
													while($row = mysql_fetch_assoc($sql)){
														$class = "";
														if($i==0){
															$class = "first";
														}
														if($i==2){
															$class = "last";
														}
														
														$pid = $row['pid'];
														$time = date("Y/m/d H:i:s", strtotime($row['ps_end_date']));
														$sprice = $row['ps_start_price'];
														$eprice = $row['ps_end_price'];
														$price = $row['prod_price'];
														
														//numbid
														$bidnum = countDesc("pdid","sc_prod_bid","psid='".$row['psid']."'");
														
														//get cus
														if($bidnum > 0){
															$cust = getDescArr("cr_cust.username,cr_cust.cid","sc_prod_bid,cr_cust","sc_prod_bid.cid=cr_cust.cid AND sc_prod_bid.psid='".$row['psid']."'", "sc_prod_bid.pdid DESC", "1");
															$bidcus = $cust['cid'];
														}else{
															$bidcus = 0;
														}
												?>
												<li class="product <?=$class?>">
													<div class="product-outer">
														<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"><?=$row['prod_sku']?></a></span> <a href="single-product.html">
															<h3><?=$row['prod_title']?></h3>
															<div class="product-thumbnail"> <img src="assets/images/products/5.jpg" data-echo="assets/images/products/5.jpg" class="img-responsive" alt=""> </div>
															</a>
															<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"><span class="amount-bid"><?=$eprice?></span>฿</span></ins> <del><span class="amount"><?=$price?>฿</span></del> <span class="amount"> </span> </span> </span> </div>
															<!-- /.price-add-to-cart -->
															
															<!--<div class="hover-area">
																<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link">BID</a> </div>
															</div>-->
															<div class="deal-countdown-timer" id="prod_<?=$pid?>"
																data-bidtime="<?=$time?>"
																data-bidnum="<?=$bidnum?>"
																data-bidprice="<?=$eprice?>"
																data-bidpid="<?=$i?>"
																data-bidcus="<?=$bidcus?>">
																<div class="marketing-text text-xs-center">จบภายใน</div>
																<div id="deal-countdown" class="countdown">	
																	<!--<span class="days"><span class="value">01</span><b>Days</b></span> 
																	<span class="hours"><span class="value">22</span><b>Hours</b></span> 
																	<span class="minutes"><span class="value">20</span><b>Mins</b></span> 
																	<span class="seconds"><span class="value">35</span><b>Secs</b></span>--> 
																</div>
																<span class="deal-end-date" style="display:none"></span>
																<div style="width:100%; text-align:center; margin-top:10px; font-size: 1.362em; font-weight:600" class="user-bid">
																	<?=($bidnum>0) ? $cust['username'] : "ให้ท่านคนแรก"?>
																</div>
																<div style="width:100%; text-align:center; margin-top:10px; display:none" class="btn-bit">
																	<button type="button" class="btn btn-primary" id="btn-bid" data-pid="<?=$pid?>">BID NOW</button>
																</div>
															</div>
														</div>
														<!-- /.product-inner --> 														
													</div>
													<!-- /.product-outer --> 
												</li>
												<!-- /.products -->
												<? 
														$i++;
													}
												?>
												
												<li class="product first">
													<div class="product-outer">
														<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag">สร้อยทอง</a></span> <a href="single-product.html">
															<h3>สร้อยคอทองคำหนัก 1 บาท</h3>
															<div class="product-thumbnail"> <img src="assets/images/products/3.jpg" data-echo="assets/images/products/3.jpg" class="img-responsive" alt=""> </div>
															</a>
															<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> 1.00฿</span></ins> <del><span class="amount">21,400฿</span></del> <span class="amount"> </span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
															<!-- /.price-add-to-cart -->
															
															<div class="hover-area">
																<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link"> BID</a> </div>
															</div>
														</div>
														<!-- /.product-inner --> 
													</div>
													<!-- /.product-outer --> 
												</li>
												<!-- /.products -->
												
												<li class="product ">
													<div class="product-outer">
														<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag">สร้อยทอง</a></span> <a href="single-product.html">
															<h3>สร้อยคอทองคำหนัก 1 บาท</h3>
															<div class="product-thumbnail"> <img src="assets/images/products/1.jpg" data-echo="assets/images/products/1.jpg" class="img-responsive" alt=""> </div>
															</a>
															<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount"> 1.00฿</span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
															<!-- /.price-add-to-cart -->
															
															<div class="hover-area">
																<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link"> BID</a> </div>
															</div>
														</div>
														<!-- /.product-inner --> 
													</div>
													<!-- /.product-outer --> 
												</li>
												<!-- /.products -->
												
												<li class="product last">
													<div class="product-outer">
														<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag">แหวนทองคำ 96.5%</a></span> <a href="single-product.html">
															<h3>แหวนทอง 1 บาท</h3>
															<div class="product-thumbnail"> <img src="assets/images/products/2.jpg" data-echo="assets/images/products/2.jpg" class="img-responsive" alt=""> </div>
															</a>
															<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount"> 1.00฿</span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
															<!-- /.price-add-to-cart -->
															
															<div class="hover-area">
																<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link"> BID</a> </div>
															</div>
														</div>
														<!-- /.product-inner --> 
													</div>
													<!-- /.product-outer --> 
												</li>
												<!-- /.products -->
											</ul>
										</div>
									</div>
									<div class="tab-pane" id="tab-products-2" role="tabpanel">
										<div class="woocommerce columns-3">
											<ul class="products columns-3">
												<li class="product first">
													<div class="product-outer">
														<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
															<h3>ทองคำแท่งหนัก1บาท</h3>
															<div class="product-thumbnail"> <img data-echo="assets/images/products/3.jpg" src="assets/images/blank.gif" alt=""> </div>
															</a>
															<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount">2.00฿</span></ins> <del><span class="amount">52,800฿</span></del> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
															<!-- /.price-add-to-cart -->
															
															<div class="hover-area">
																<div class="action-buttons"> <a href="#" rel="nofollow" class=" "> AUTOBID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
															</div>
														</div>
														<!-- /.product-inner --> 
													</div>
													<!-- /.product-outer --> 
												</li>
												<li class="product ">
													<div class="product-outer">
														<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
															<h3>ทองคำแท่งหนัก1บาท</h3>
															<div class="product-thumbnail"> <img data-echo="assets/images/products/5.jpg" src="assets/images/blank.gif" alt=""> </div>
															</a>
															<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount">2.00฿</span></ins> <del><span class="amount">52,800฿</span></del> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
															<!-- /.price-add-to-cart -->
															
															<div class="hover-area">
																<div class="action-buttons"> <a href="#" rel="nofollow" class=" "> AUTOBID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
															</div>
														</div>
														<!-- /.product-inner --> 
													</div>
													<!-- /.product-outer --> 
												</li>
												<li class="product last">
													<div class="product-outer">
														<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
															<h3>ทองคำแท่งหนัก1บาท</h3>
															<div class="product-thumbnail"> <img data-echo="assets/images/products/4.jpg" src="assets/images/blank.gif" alt=""> </div>
															</a>
															<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount">2.00฿</span></ins> <del><span class="amount">52,800฿</span></del> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
															<!-- /.price-add-to-cart -->
															
															<div class="hover-area">
																<div class="action-buttons"> <a href="#" rel="nofollow" class=" "> AUTOBID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
															</div>
														</div>
														<!-- /.product-inner --> 
													</div>
													<!-- /.product-outer --> 
												</li>
												<li class="product first">
													<div class="product-outer">
														<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
															<h3>ทองคำแท่งหนัก1บาท</h3>
															<div class="product-thumbnail"> <img data-echo="assets/images/products/1.jpg" src="assets/images/blank.gif" alt=""> </div>
															</a>
															<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount">2.00฿</span></ins> <del><span class="amount">52,800฿</span></del> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
															<!-- /.price-add-to-cart -->
															
															<div class="hover-area">
																<div class="action-buttons"> <a href="#" rel="nofollow" class=" "> AUTOBID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
															</div>
														</div>
														<!-- /.product-inner --> 
													</div>
													<!-- /.product-outer --> 
												</li>
												<li class="product ">
													<div class="product-outer">
														<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
															<h3>ทองคำแท่งหนัก1บาท</h3>
															<div class="product-thumbnail"> <img data-echo="assets/images/products/6.jpg" src="assets/images/blank.gif" alt=""> </div>
															</a>
															<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount">2.00฿</span></ins> <del><span class="amount">52,800฿</span></del> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
															<!-- /.price-add-to-cart -->
															
															<div class="hover-area">
																<div class="action-buttons"> <a href="#" rel="nofollow" class=" "> AUTOBID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
															</div>
														</div>
														<!-- /.product-inner --> 
													</div>
													<!-- /.product-outer --> 
												</li>
												<li class="product last">
													<div class="product-outer">
														<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
															<h3>ทองคำแท่งหนัก1บาท</h3>
															<div class="product-thumbnail"> <img data-echo="assets/images/products/2.jpg" src="assets/images/blank.gif" alt=""> </div>
															</a>
															<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount">2.00฿</span></ins> <del><span class="amount">52,800฿</span></del> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
															<!-- /.price-add-to-cart -->
															
															<div class="hover-area">
																<div class="action-buttons"> <a href="#" rel="nofollow" class=" "> AUTOBID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
															</div>
														</div>
														<!-- /.product-inner --> 
													</div>
													<!-- /.product-outer --> 
												</li>
											</ul>
										</div>
									</div>
									<div class="tab-pane" id="tab-products-3" role="tabpanel">
										<div class="woocommerce columns-3">
											<ul class="products columns-3">
												<li class="product first">
													<div class="product-outer">
														<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag">Audio Speakers</a></span> <a href="single-product.html">
															<h3>Wireless Audio System Multiroom 360</h3>
															<div class="product-thumbnail"> <img src="assets/images/blank.gif" data-echo="assets/images/products/1.jpg" class="img-responsive" alt=""> </div>
															</a>
															<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> 1.00฿</span></ins> <del><span class="amount">1.00฿</span></del> <span class="amount"> </span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
															<!-- /.price-add-to-cart -->
															
															<div class="hover-area">
																<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link"> BID</a> </div>
															</div>
														</div>
														<!-- /.product-inner --> 
													</div>
													<!-- /.product-outer --> 
												</li>
												<!-- /.products -->
												
												<li class="product ">
													<div class="product-outer">
														<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag">Laptops</a></span> <a href="single-product.html">
															<h3>Tablet Red EliteBook  Revolve 810 G2</h3>
															<div class="product-thumbnail"> <img src="assets/images/blank.gif" data-echo="assets/images/products/2.jpg" class="img-responsive" alt=""> </div>
															</a>
															<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> 1.00฿</span></ins> <del><span class="amount">1.00฿</span></del> <span class="amount"> </span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
															<!-- /.price-add-to-cart -->
															
															<div class="hover-area">
																<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link"> BID</a> </div>
															</div>
														</div>
														<!-- /.product-inner --> 
													</div>
													<!-- /.product-outer --> 
												</li>
												<!-- /.products -->
												
												<li class="product last">
													<div class="product-outer">
														<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag">Headphones</a></span> <a href="single-product.html">
															<h3>White Solo 2 Wireless</h3>
															<div class="product-thumbnail"> <img src="assets/images/blank.gif" data-echo="assets/images/products/3.jpg" class="img-responsive" alt=""> </div>
															</a>
															<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount"> 1.00฿</span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
															<!-- /.price-add-to-cart -->
															
															<div class="hover-area">
																<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link"> BID</a> </div>
															</div>
														</div>
														<!-- /.product-inner --> 
													</div>
													<!-- /.product-outer --> 
												</li>
												<!-- /.products -->
												
												<li class="product first">
													<div class="product-outer">
														<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
															<h3>Smartphone 6S 32GB LTE</h3>
															<div class="product-thumbnail"> <img src="assets/images/products/4.jpg" data-echo="assets/images/products/4.jpg" class="img-responsive" alt=""> </div>
															</a>
															<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> $1,215.00</span></ins> <del><span class="amount">$2,215.00</span></del> <span class="amount"> </span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
															<!-- /.price-add-to-cart -->
															
															<div class="hover-area">
																<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link"> BID</a> </div>
															</div>
														</div>
														<!-- /.product-inner --> 
													</div>
													<!-- /.product-outer --> 
												</li>
												<!-- /.products -->
												
												<li class="product ">
													<div class="product-outer">
														<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag">Cameras</a></span> <a href="single-product.html">
															<h3>Purple NX Mini F1 aparat  SMART NX</h3>
															<div class="product-thumbnail"> <img src="assets/images/blank.gif" data-echo="assets/images/products/5.jpg" class="img-responsive" alt=""> </div>
															</a>
															<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount"> 1.00฿</span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
															<!-- /.price-add-to-cart -->
															
															<div class="hover-area">
																<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link"> BID</a> </div>
															</div>
														</div>
														<!-- /.product-inner --> 
													</div>
													<!-- /.product-outer --> 
												</li>
												<!-- /.products -->
												
												<li class="product last">
													<div class="product-outer">
														<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag">Printers</a></span> <a href="single-product.html">
															<h3>Full Color LaserJet Pro  M452dn</h3>
															<div class="product-thumbnail"> <img src="assets/images/blank.gif" data-echo="assets/images/products/6.jpg" class="img-responsive" alt=""> </div>
															</a>
															<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount"> 1.00฿</span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
															<!-- /.price-add-to-cart -->
															
															<div class="hover-area">
																<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link"> BID</a> </div>
															</div>
														</div>
														<!-- /.product-inner --> 
													</div>
													<!-- /.product-outer --> 
												</li>
												<!-- /.products -->
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- /.tabs-block --> 
					</div>
					<!-- /.deals-and-tabs --> 
					
					<!-- ============================================================= 2-1-2 Product Grid ============================================================= -->
					<section class="products-2-1-2 animate-in-view fadeIn animated" data-animation="fadeIn">
						<h2 class="sr-only">Products Grid</h2>
						<div class="container">
							<ul class="nav nav-inline nav-justified">
								<li class="nav-item"><a href="shop.html" class="active nav-link">ประมูลทั้งหมด</a></li>
								<li class="nav-item"><a class="nav-link" href="shop.html">ทองคำแท่ง</a></li>
								<li class="nav-item"><a class="nav-link" href="shop.html">สร้อยทอง</a></li>
								<li class="nav-item"><a class="nav-link" href="shop.html">กำไร</a></li>
								<li class="nav-item"><a class="nav-link" href="shop.html">กรอบพระ</a></li>
								<li class="nav-item"><a class="nav-link" href="shop.html">ของเด็ก</a></li>
								<li class="nav-item"><a class="nav-link" href="shop.html">พระแผง</a></li>
								<li class="nav-item"><a class="nav-link" href="shop.html">แหวนหัวพลอย</a></li>
								<li class="nav-item"><a class="nav-link" href="shop.html">แหวนหัวพลอย</a></li>
							</ul>
							<div class="columns-2-1-2">
								<ul class="products exclude-auto-height">
									<li class="product">
										<div class="product-outer">
											<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
												<h3>ทองคำแท่งหนัก1บาท</h3>
												<div class="product-thumbnail"> <img data-echo="assets/images/2.jpg" src="assets/images/2.jpg" alt=""> </div>
												</a>
												<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount">2.00฿</span></ins> <del><span class="amount">52,800฿</span></del> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
												<!-- /.price-add-to-cart -->
												
												<div class="hover-area">
													<div class="action-buttons"> <a href="#" rel="nofollow" class=" "> AUTOBID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
									</li>
									<li class="product">
										<div class="product-outer">
											<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
												<h3>ทองคำแท่งหนัก1บาท</h3>
												<div class="product-thumbnail"> <img data-echo="assets/images/products/6-1.jpg" src="assets/images/products/6-1.jpg" alt=""> </div>
												</a>
												<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount">2.00฿</span></ins> <del><span class="amount">52,800฿</span></del> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
												<!-- /.price-add-to-cart -->
												
												<div class="hover-area">
													<div class="action-buttons"> <a href="#" rel="nofollow" class=" "> AUTOBID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
									</li>
								</ul>
								<ul class="products exclude-auto-height product-main-2-1-2">
									<li class="last product">
										<div class="product-outer">
											<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
												<h3>ทองคำแท่งหนัก1บาท</h3>
												<div class="product-thumbnail"> <img class="wp-post-image" data-echo="assets/images/1.jpg" src="assets/images/1.jpg" alt=""> </div>
												</a>
												<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount">2.00฿</span></ins> <del><span class="amount">52,800฿</span></del> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
												<!-- /.price-add-to-cart -->
												
												<div class="hover-area">
													<div class="action-buttons"> <a href="#" rel="nofollow" class=" "> AUTOBID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
									</li>
								</ul>
								<ul class="products exclude-auto-height">
									<li class="product">
										<div class="product-outer">
											<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
												<h3>ทองคำแท่งหนัก1บาท</h3>
												<div class="product-thumbnail"> <img class="wp-post-image" data-echo="assets/images/2.jpg" src="assets/images/2.jpg" alt=""> </div>
												</a>
												<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount">2.00฿</span></ins> <del><span class="amount">52,800฿</span></del> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
												<!-- /.price-add-to-cart -->
												
												<div class="hover-area">
													<div class="action-buttons"> <a href="#" rel="nofollow" class=" "> AUTOBID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
									</li>
									<li class="product">
										<div class="product-outer">
											<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
												<h3>ทองคำแท่งหนัก1บาท</h3>
												<div class="product-thumbnail"> <img class="wp-post-image" data-echo="assets/images/2.jpg" src="assets/images/2.jpg" alt=""> </div>
												</a>
												<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount">2.00฿</span></ins> <del><span class="amount">52,800฿</span></del> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
												<!-- /.price-add-to-cart -->
												
												<div class="hover-area">
													<div class="action-buttons"> <a href="#" rel="nofollow" class=" "> AUTOBID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
									</li>
								</ul>
							</div>
						</div>
					</section>
					<!-- ============================================================= 2-1-2 Product Grid : End============================================================= -->
					
					<section class="section-product-cards-carousel animate-in-view fadeIn animated" data-animation="fadeIn">
						<header>
							<h2 class="h1">รายการประมูล 20 อันดับ</h2>
							<ul class="nav nav-inline">
								<li class="nav-item active"><span class="nav-link">Top 20</span></li>
								<li class="nav-item"><a class="nav-link" href="product-category.html">ทองคำแท่ง</a></li>
								<li class="nav-item"><a class="nav-link" href="product-category.html">สร้อยทอง</a></li>
								<li class="nav-item"><a class="nav-link" href="product-category.html">แหวนทอง</a></li>
							</ul>
						</header>
						<div id="home-v1-product-cards-careousel">
							<div class="woocommerce columns-3 home-v1-product-cards-carousel product-cards-carousel owl-carousel">
								<ul class="products columns-3">
									<li class="product product-card first">
										<div class="product-outer">
											<div class="media product-inner"> <a class="media-left" href="single-product.html" title="Pendrive USB 3.0 Flash 64 GB"> <img class="media-object wp-post-image img-responsive" src="assets/images/product-cards/4.jpg" data-echo="assets/images/product-cards/4.jpg" alt=""> </a>
												<div class="media-body"> <span class="loop-product-categories"> <a href="product-category.html" rel="tag">แหวนทอง</a> </span> <a href="single-product.html">
													<h3>Widescreen 4K SUHD TV</h3>
													</a>
													<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount"> $800</span> </span> </span> <a href="cart.html" class="button add_to_cart_button">BID NOW</a> </div>
													<!-- /.price-add-to-cart -->
													
													<div class="hover-area">
														<div class="action-buttons"> <a href="#" class=" ">AUTO BID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
													</div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
										
									</li>
									<!-- /.products -->
									<li class="product product-card ">
										<div class="product-outer">
											<div class="media product-inner"> <a class="media-left" href="single-product.html" title="Pendrive USB 3.0 Flash 64 GB"> <img class="media-object wp-post-image img-responsive" src="assets/images/product-cards/6.jpg" data-echo="assets/images/product-cards/6.jpg" alt=""> </a>
												<div class="media-body"> <span class="loop-product-categories"> <a href="product-category.html" rel="tag">ทองคำแท่ง</a> </span> <a href="single-product.html">
													<h3>ทองคำแท่งหนัก 1 บาท</h3>
													</a>
													<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount">1.00฿</span> </span> </span> <a href="cart.html" class="button add_to_cart_button">BID NOW</a> </div>
													<!-- /.price-add-to-cart -->
													
													<div class="hover-area">
														<div class="action-buttons"> <a href="#" class=" ">AUTO BID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
													</div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
										
									</li>
									<!-- /.products -->
									<li class="product product-card last">
										<div class="product-outer">
											<div class="media product-inner"> <a class="media-left" href="single-product.html" title="Pendrive USB 3.0 Flash 64 GB"> <img class="media-object wp-post-image img-responsive" src="assets/images/product-cards/4.jpg" data-echo="assets/images/product-cards/4.jpg" alt=""> </a>
												<div class="media-body"> <span class="loop-product-categories"> <a href="product-category.html" rel="tag">Printers</a> </span> <a href="single-product.html">
													<h3>ทองคำแท่งหนัก 2 บาท</h3>
													</a>
													<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> 5.00฿</span></ins> <del><span class="amount">50,000฿</span></del> <span class="amount"> </span> </span> </span> <a href="cart.html" class="button add_to_cart_button">BID NOW</a> </div>
													<!-- /.price-add-to-cart -->
													
													<div class="hover-area">
														<div class="action-buttons"> <a href="#" class=" ">AUTO BID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
													</div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
										
									</li>
									<!-- /.products -->
									<li class="product product-card first">
										<div class="product-outer">
											<div class="media product-inner"> <a class="media-left" href="single-product.html" title="Pendrive USB 3.0 Flash 64 GB"> <img class="img-responsive media-object wp-post-image" src="assets/images/product-cards/5.jpg"" data-echo="assets/images/product-cards/5.jpg" alt=""> </a>
												<div class="media-body"> <span class="loop-product-categories"> <a href="product-category.html" rel="tag"> ทองคำแท่ง</a> </span> <a href="single-product.html">
													<h3>Notebook Purple G752VT-T7008T</h3>
													</a>
													<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> 5.00฿</span></ins> <del><span class="amount">50,000฿</span></del> <span class="amount"> </span> </span> </span> <a href="cart.html" class="button add_to_cart_button">BID NOW</a> </div>
													<!-- /.price-add-to-cart -->
													
													<div class="hover-area">
														<div class="action-buttons"> <a href="#" class=" ">AUTO BID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
													</div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
										
									</li>
									<!-- /.products -->
									<li class="product product-card ">
										<div class="product-outer">
											<div class="media product-inner"> <a class="media-left" href="single-product.html" title="Pendrive USB 3.0 Flash 64 GB"> <img class="img-responsive media-object wp-post-image" src="assets/images/product-cards/7.jpg"" data-echo="assets/images/product-cards/7.jpg" alt=""> </a>
												<div class="media-body"> <span class="loop-product-categories"> <a href="product-category.html" rel="tag">ของเด็ก</a> </span> <a href="single-product.html">
													<h3>ข้อเท้าเด็ก</h3>
													</a>
													<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> 5.00฿</span></ins> <del><span class="amount">50,000฿</span></del> <span class="amount"> </span> </span> </span> <a href="cart.html" class="button add_to_cart_button">BID NOW</a> </div>
													<!-- /.price-add-to-cart -->
													
													<div class="hover-area">
														<div class="action-buttons"> <a href="#" class=" ">AUTO BID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
													</div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
										
									</li>
									<!-- /.products -->
									<li class="product product-card last">
										<div class="product-outer">
											<div class="media product-inner"> <a class="media-left" href="single-product.html" title="กรอบพระ"> <img class="img-responsive media-object wp-post-image" src="assets/images/product-cards/6.jpg"" data-echo="assets/images/product-cards/6.jpg" alt=""> </a>
												<div class="media-body"> <span class="loop-product-categories"> <a href="product-category.html" rel="tag"> ทองคำแท่ง</a> </span> <a href="single-product.html">
													<h3>Tablet Thin EliteBook  Revolve 810 G6</h3>
													</a>
													<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount"> $500</span> </span> </span> <a href="cart.html" class="button add_to_cart_button">BID NOW</a> </div>
													<!-- /.price-add-to-cart -->
													
													<div class="hover-area">
														<div class="action-buttons"> <a href="#" class=" ">AUTO BID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
													</div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
										
									</li>
									<!-- /.products -->
								</ul>
								<ul class="products columns-3">
									<li class="product product-card first">
										<div class="product-outer">
											<div class="media product-inner"> <a class="media-left" href="single-product.html" title="Pendrive USB 3.0 Flash 64 GB"> <img class="img-responsive media-object wp-post-image" src="assets/images/blank.gif" data-echo="assets/images/product-cards/5.jpg" alt=""> </a>
												<div class="media-body"> <span class="loop-product-categories"> <a href="product-category.html" rel="tag">Headphone Cases</a> </span> <a href="single-product.html">
													<h3>Universal Headphones Case in Black</h3>
													</a>
													<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount"> $1500</span> </span> </span> <a href="cart.html" class="button add_to_cart_button">BID NOW</a> </div>
													<!-- /.price-add-to-cart -->
													
													<div class="hover-area">
														<div class="action-buttons"> <a href="#" class=" ">AUTO BID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
													</div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
										
									</li>
									<!-- /.products -->
									<li class="product product-card ">
										<div class="product-outer">
											<div class="media product-inner"> <a class="media-left" href="single-product.html" title="Pendrive USB 3.0 Flash 64 GB"> <img class="img-responsive media-object wp-post-image" src="assets/images/product-cards/4.jpg"" data-echo="assets/images/product-cards/4.jpg" alt=""> </a>
												<div class="media-body"> <span class="loop-product-categories"> <a href="product-category.html" rel="tag">Printers</a> </span> <a href="single-product.html">
													<h3>Full Color LaserJet Pro  M452dn</h3>
													</a>
													<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount"> $500</span> </span> </span> <a href="cart.html" class="button add_to_cart_button">BID NOW</a> </div>
													<!-- /.price-add-to-cart -->
													
													<div class="hover-area">
														<div class="action-buttons"> <a href="#" class=" ">AUTO BID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
													</div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
										
									</li>
									<!-- /.products -->
									<li class="product product-card last">
										<div class="product-outer">
											<div class="media product-inner"> <a class="media-left" href="single-product.html" title="Pendrive USB 3.0 Flash 64 GB"> <img class="img-responsive media-object wp-post-image" src="assets/images/product-cards/7.jpg" data-echo="assets/images/product-cards/7.jpg" alt=""> </a>
												<div class="media-body"> <span class="loop-product-categories"> <a href="product-category.html" rel="tag">แหวนทอง</a> </span> <a href="single-product.html">
													<h3>แหวนทอง 1 กรัม</h3>
													</a>
													<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount"> 1.00฿</span> </span> </span> <a href="cart.html" class="button add_to_cart_button">BID NOW</a> </div>
													<!-- /.price-add-to-cart -->
													
													<div class="hover-area">
														<div class="action-buttons"> <a href="#" class=" ">AUTO BID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
													</div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
										
									</li>
									<!-- /.products -->
									<li class="product product-card first">
										<div class="product-outer">
											<div class="media product-inner"> <a class="media-left" href="single-product.html" title="Pendrive USB 3.0 Flash 64 GB"> <img class="img-responsive media-object wp-post-image" src="assets/images/product-cards/6.jpg" data-echo="assets/images/product-cards/6.jpg" alt=""> </a>
												<div class="media-body"> <span class="loop-product-categories"> <a href="product-category.html" rel="tag"> ทองคำแท่ง</a> </span> <a href="single-product.html">
													<h3>Notebook Purple G752VT-T7008T</h3>
													</a>
													<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> 5.00฿</span></ins> <del><span class="amount">50,000฿</span></del> <span class="amount"> </span> </span> </span> <a href="cart.html" class="button add_to_cart_button">BID NOW</a> </div>
													<!-- /.price-add-to-cart -->
													
													<div class="hover-area">
														<div class="action-buttons"> <a href="#" class=" ">AUTO BID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
													</div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
										
									</li>
									<!-- /.products -->
									<li class="product product-card ">
										<div class="product-outer">
											<div class="media product-inner"> <a class="media-left" href="single-product.html" title="Pendrive USB 3.0 Flash 64 GB"> <img class="img-responsive media-object wp-post-image" src="assets/images/product-cards/5.jpg" data-echo="assets/images/product-cards/5.jpg" alt=""> </a>
												<div class="media-body"> <span class="loop-product-categories"> <a href="product-category.html" rel="tag">Peripherals</a> </span> <a href="single-product.html">
													<h3>External SSD USB 3.1  750 GB</h3>
													</a>
													<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> 5.00฿</span></ins> <del><span class="amount">50,000฿</span></del> <span class="amount"> </span> </span> </span> <a href="cart.html" class="button add_to_cart_button">BID NOW</a> </div>
													<!-- /.price-add-to-cart -->
													
													<div class="hover-area">
														<div class="action-buttons"> <a href="#" class=" ">AUTO BID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
													</div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
										
									</li>
									<!-- /.products -->
									<li class="product product-card last">
										<div class="product-outer">
											<div class="media product-inner"> <a class="media-left" href="single-product.html" title="Pendrive USB 3.0 Flash 64 GB"> <img class="img-responsive media-object wp-post-image" src="assets/images/product-cards/4.jpg" data-echo="assets/images/product-cards/4.jpg" alt=""> </a>
												<div class="media-body"> <span class="loop-product-categories"> <a href="product-category.html" rel="tag"> ทองคำแท่ง</a> </span> <a href="single-product.html">
													<h3>Tablet Thin EliteBook  Revolve 810 G6</h3>
													</a>
													<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> 5.00฿</span></ins> <del><span class="amount">50,000฿</span></del> <span class="amount"> </span> </span> </span> <a href="cart.html" class="button add_to_cart_button">BID NOW</a> </div>
													<!-- /.price-add-to-cart -->
													
													<div class="hover-area">
														<div class="action-buttons"> <a href="#" class=" ">AUTO BID</a> <a href="#" class="add-to-BID-link">BID</a> </div>
													</div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
										
									</li>
									<!-- /.products -->
								</ul>
							</div>
						</div>
						<!-- #home-v1-product-cards-careousel --> 
						
					</section>
					<div class="home-v1-banner-block animate-in-view fadeIn animated" data-animation="fadeIn">
						<div class="home-v1-fullbanner-ad fullbanner-ad" style="margin-bottom: 70px"> <a href="#"><img src="assets/images/banner/home-v1-banner.png" data-echo="assets/images/banner/home-v1-banner.png" class="img-responsive" alt=""></a> </div>
					</div>
					<!-- /.home-v1-banner-block -->
					
					<section class="home-v1-recently-viewed-products-carousel section-products-carousel animate-in-view fadeIn animated" data-animation="fadeIn">
						<header>
							<h2 class="h1">Recently Added</h2>
							<div class="owl-nav"> <a href="#products-carousel-prev" data-target="#recently-added-products-carousel" class="slider-prev"><i class="fa fa-angle-left"></i></a> <a href="#products-carousel-next" data-target="#recently-added-products-carousel" class="slider-next"><i class="fa fa-angle-right"></i></a> </div>
						</header>
						<div id="recently-added-products-carousel">
							<div class="woocommerce columns-6">
								<div class="products owl-carousel recently-added-products products-carousel columns-6">
									<div class="product">
										<div class="product-outer">
											<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
												<h3>Tablet Thin EliteBook  Revolve 810 G6</h3>
												<div class="product-thumbnail"> <img src="assets/images/blank.gif" data-echo="assets/images/product-category/2.jpg" class="img-responsive" alt=""> </div>
												</a>
												<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> 1.00฿</span></ins> <del><span class="amount">1.00฿</span></del> <span class="amount"> </span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
												<!-- /.price-add-to-cart -->
												
												<div class="hover-area">
													<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link"> BID</a> </div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
									</div>
									<!-- /.products -->
									
									<div class="product">
										<div class="product-outer">
											<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
												<h3>Notebook Purple G952VX-T7008T</h3>
												<div class="product-thumbnail"> <img src="assets/images/blank.gif" data-echo="assets/images/product-category/3.jpg" class="img-responsive" alt=""> </div>
												</a>
												<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount"> 1.00฿</span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
												<!-- /.price-add-to-cart -->
												
												<div class="hover-area">
													<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link"> BID</a> </div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
									</div>
									<!-- /.products -->
									
									<div class="product">
										<div class="product-outer">
											<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
												<h3>ทองคำแท่งหนัก1บาท</h3>
												<div class="product-thumbnail"> <img src="assets/images/blank.gif" data-echo="assets/images/product-category/1.jpg" class="img-responsive" alt=""> </div>
												</a>
												<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount"> 1.00฿</span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
												<!-- /.price-add-to-cart -->
												
												<div class="hover-area">
													<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link"> BID</a> </div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
									</div>
									<!-- /.products -->
									
									<div class="product">
										<div class="product-outer">
											<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
												<h3>Tablet Thin EliteBook  Revolve 810 G6</h3>
												<div class="product-thumbnail"> <img src="assets/images/blank.gif" data-echo="assets/images/product-category/2.jpg" class="img-responsive" alt=""> </div>
												</a>
												<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount"> 1.00฿</span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
												<!-- /.price-add-to-cart -->
												
												<div class="hover-area">
													<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link"> BID</a> </div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
									</div>
									<!-- /.products -->
									
									<div class="product">
										<div class="product-outer">
											<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
												<h3>Laptop Yoga 21 80JH0035GE  W8.1 (Copy)</h3>
												<div class="product-thumbnail"> <img src="assets/images/blank.gif" data-echo="assets/images/product-category/5.jpg" class="img-responsive" alt=""> </div>
												</a>
												<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount"> 1.00฿</span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
												<!-- /.price-add-to-cart -->
												
												<div class="hover-area">
													<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link"> BID</a> </div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
									</div>
									<!-- /.products -->
									
									<div class="product">
										<div class="product-outer">
											<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
												<h3>Notebook Purple G952VX-T7008T</h3>
												<div class="product-thumbnail"> <img src="assets/images/blank.gif" data-echo="assets/images/product-category/4.jpg" class="img-responsive" alt=""> </div>
												</a>
												<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount"> 1.00฿</span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
												<!-- /.price-add-to-cart -->
												
												<div class="hover-area">
													<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link"> BID</a> </div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
									</div>
									<!-- /.products -->
									
									<div class="product">
										<div class="product-outer">
											<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
												<h3>Smartphone 6S 128GB LTE</h3>
												<div class="product-thumbnail"> <img src="assets/images/blank.gif" data-echo="assets/images/product-category/6.jpg" class="img-responsive" alt=""> </div>
												</a>
												<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> </span></ins> <span class="amount"> $200.00</span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
												<!-- /.price-add-to-cart -->
												
												<div class="hover-area">
													<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link"> BID</a> </div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
									</div>
									<!-- /.products -->
									
									<div class="product">
										<div class="product-outer">
											<div class="product-inner"> <span class="loop-product-categories"><a href="product-category.html" rel="tag"> ทองคำแท่ง</a></span> <a href="single-product.html">
												<h3> ทองคำแท่งหนัก 2 บาท</h3>
												<div class="product-thumbnail"> <img src="assets/images/blank.gif" data-echo="assets/images/product-category/3.jpg" class="img-responsive" alt=""> </div>
												</a>
												<div class="price-add-to-cart"> <span class="price"> <span class="electro-price"> <ins><span class="amount"> 1.00฿</span></ins> <del><span class="amount">1.00฿</span></del> <span class="amount"> </span> </span> </span> <a rel="nofollow" href="single-product.html" class="button add_to_cart_button">BID NOW</a> </div>
												<!-- /.price-add-to-cart -->
												
												<div class="hover-area">
													<div class="action-buttons"> <a href="#" rel="nofollow" class=" ">AUTOBID</a> <a href="BID.html" class="add-to-BID-link">BID</a> </div>
												</div>
											</div>
											<!-- /.product-inner --> 
										</div>
										<!-- /.product-outer --> 
									</div>
									<!-- /.products --> 
									
								</div>
							</div>
						</div>
					</section>
				</main>
				<!-- #main --> 
			</div>
			<!-- #primary --> 
			
		</div>
		<!-- .container --> 
	</div>
	<!-- #content -->
	
	<? include("inc/footer.php")?>
	<!-- #colophon --> 
	
</div>
<!-- #page --> 

<script type="text/javascript" src="assets/js/jquery.min.js"></script> 
<script type="text/javascript" src="assets/js/tether.min.js"></script> 
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="assets/js/bootstrap-hover-dropdown.min.js"></script> 
<script type="text/javascript" src="assets/js/owl.carousel.min.js"></script> 
<script type="text/javascript" src="assets/js/echo.min.js"></script> 
<script type="text/javascript" src="assets/js/wow.min.js"></script> 
<script type="text/javascript" src="assets/js/jquery.easing.min.js"></script> 
<script type="text/javascript" src="assets/js/jquery.waypoints.min.js"></script> 
<script type="text/javascript" src="assets/js/electro.js"></script>
<script src="//cdn.rawgit.com/hilios/jQuery.countdown/2.1.0/dist/jquery.countdown.min.js"></script>
<script type="text/javascript" src="assets/js/custom.js"></script>

</body>
</html>
