<?php use \App\Http\Controllers\ProductdetailController; 
$data = ProductdetailController::display(); ?>
<style type="text/css">
.rs_graybg.rs_toppadder30.rs_bottompadder70 .container .row .col-lg-12.col-md-12.col-sm-12.col-xs-12 .rs_buy_product_buy_top_section .col-lg-7.col-md-6.col-sm-12.col-xs-12.no-padding .rs_single_product_slider .col-lg-9.col-md-12.col-sm-12.col-xs-12.no-padding.border-left .col-lg-8.col-md-8.col-sm-12.col-xs-12.no-padding {
  text-align: right;
  font-style: italic;
}
</style>
<?php 
  


  if(Session::get('lang')==null||Session::get('lang')=='')
  {
    $lang = 'th';
  }else{
    $lang = Session::get('lang');
  }


  $rowx = $data['row'];
  $row = $data['row'];
  
  $fields = $data['fields'];
  

  

?>
@if($lang=='th')
<div class="rs_graybg rs_toppadder30 rs_bottompadder70">
  <div class="container">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="rs_buy_product_buy_top_section">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
            <h4 class="widget-title widget-title-bid">
                {!! $row->name_th_1 !!}
            </h4>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12 no-padding">
        <div class="rs_single_product_slider"> 
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 no-padding">
            <div id="sync2" class="owl-carousel">
                  <?php 
                    $fields['featured_img']['format_as'] = $fields['gallery']['format_as'] = 'imgsmall'; 
                  ?>
                  {!! SiteHelpers::formatRows($row->featured_img,$fields['featured_img'],$row ,$row->name_th_1) !!} 
                  {!! SiteHelpers::formatRows($row->gallery,$fields['gallery'],$row ,$row->name_th_1) !!}
            </div>
            </div>
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 no-padding border-left">
            <div id="sync1" class="owl-carousel">
                  <?php 
                    $fields['featured_img']['format_as'] = $fields['gallery']['format_as'] = 'imgsmall'; 
                  ?>
                  {!! SiteHelpers::formatRows($row->featured_img,$fields['featured_img'],$row ,$row->name_th_1) !!} 
                  {!! SiteHelpers::formatRows($row->gallery,$fields['gallery'],$row ,$row->name_th_1) !!}
            </div>
            
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 no-padding">
            {!! html_entity_decode(Lang::get('productshop.1')) !!}
            </div>
            </div>
            
            
        </div>
      </div>    
   <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 no-padding">
      <aside class="widget widget_license_product_sell rs_bottompadder20">
        <div id="buypanel">
        <h4 class="h4title_product_sell"> {!! $row->name_th_1 !!} </h4>
<div class="text">{!! $row->title_th !!}</div>
          <form class="form-productbuy-details" name="form-productbuy-detailshop" method="post" action="{!!url('cart') !!}">
            <div class="form-group select-qty">
              <label for="qty" class="col-xs-4 no-padding">{!! html_entity_decode(Lang::get('productshop.2')) !!} </label>
              <div class="col-xs-8">
                <div class="rs_incre_decre_btns">
                  <button type="button" class="btn_small count_minus" id="breakDecrease"> <i class="fa fa-minus"></i> </button>
                  <button type="button" class="btn_big count_val" id="breakValue" disabled>1</button>
                   <input type="hidden" class="quantity_rows" name="qty" value="1">
                  <button type="button" class="btn_small count_plus" id="breakIncrease"> <i class="fa fa-plus"></i> </button>
                </div>
              </div>
            </div>
            @if(isset($row->color_th))
            <div class="form-group ">
              <label for="field-color" class="col-xs-4 no-padding">{!! html_entity_decode(Lang::get('productshop.3')) !!}</label>
                <div class="col-xs-6">
                <select id="productcolor" class="rs-custom-select" name="color">
                    <?php 
                    $fields['color_th']['format_as'] = 'dropdownlist'; 
                    ?>
                    {!! SiteHelpers::formatRows($row->color_th,$fields['color_th'],$row ,$row->color_id) !!} 
                </select>
                </div>
            </div>
            @endif
            
            <div class="form-group ">
              <label for="field-shipping" class="col-xs-4 no-padding">ราคา</label>
                @if(isset($row->sale_price)&&$row->sale_price!="0.00")
                <div class="col-xs-6">
                <span style="text-decoration: line-through; color: #ccc;">{!! number_format($row->price,2) !!}</span> {!! number_format($row->sale_price,2) !!}
                @else
                <div class="col-xs-6" >
                {!! number_format($row->price,2) !!}
                @endif THB
                </div>
            </div>

            <div class="form-group ">
              <label for="field-shipping" class="col-xs-4 no-padding">{!! html_entity_decode(Lang::get('productshop.4')) !!}</label>
                <div class="col-xs-6">
                @if(isset($row->sale_price)&&$row->sale_price!="0.00")
                <span style="text-decoration: line-through; color: #ccc;">{!! number_format($row->price*0.5,2) !!}</span> {!! number_format($row->sale_price*0.5,2) !!}
                @else
                {!! number_format($row->price*0.5,2) !!}
                @endif S-Pay
                </div>
            </div>
            <div class="form-group">
            <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 no-padding">
              <button type="submit" name="paytype" value="money" class="rs_button rs_button_orange" style="margin-bottom:10px;">
              {!! html_entity_decode(Lang::get('productshop.5')) !!}
              <?php 
              if(isset($row->sale_price)&&$row->sale_price!="0.00")
                { $pricemoney = $row->sale_price; echo number_format($row->sale_price); }
              else 
                { $pricemoney = $row->price; echo number_format($row->price); } ?> THB
              </button> 
              
            </div>
            <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 no-padding">
                <button type="button" id="spayclick" name="paytypes" value="spay" class="rs_button rs_button_blue">{!! html_entity_decode(Lang::get('productshop.5')) !!}
                <?php 
              if(isset($row->sale_price)&&$row->sale_price!="0.00")
                { $pricespay = $row->sale_price*0.5; echo number_format($row->sale_price*0.5); }
              else 
                { $pricespay = $row->price*0.5; echo number_format($row->price*0.5); } ?> S-Pay</button>
               
            </div>
                    <input type="hidden" name="url" value="{!! asset('uploads/products') !!}/{!! $row->featured_img !!}">
                    <input type="hidden" name="products_code" value="<?php echo $row->products_code; ?>">
                    <input type="hidden" name="name_en" value="<?php echo $rowx->name_en_1; ?>">
                    <input type="hidden" name="name_th" value="<?php echo $rowx->name_th_1; ?>">
                    <input type="hidden" name="pid" value="<?php echo $row->id; ?>">
                    <input type="hidden" name="pricemoney" value="{!! $pricemoney !!}">
                    <input type="hidden" name="pricespay" value="{!! $pricespay !!}">
                    <input type="hidden" name="cartaction" value="add">
                    <input type="hidden" name="shiping_cost" value="{!! $row->shiping_cost !!}">
            </div>
          </form>
          <div class="text-left"> {!! html_entity_decode(Lang::get('productshop.6')) !!} <a class="btn btn-border" href="tel:{!! html_entity_decode(Lang::get('productshop.7')) !!}"><i class="fa fa-phone-square"></i> {!! html_entity_decode(Lang::get('productshop.7')) !!}</a></div>
          <div class="text-left"><i class="fa fa-truck fa-lg"></i> {!! html_entity_decode(Lang::get('productshop.8')) !!}</div>
          <div class="text-left"><i class="fa fa-money fa-lg"></i> {!! html_entity_decode(Lang::get('productshop.9')) !!}</div>
                <img src="{!! asset('frontend') !!}/punbidtheme/images/banktransfer.png" alt="payment" />
</div>
      </aside>
    </div>
    </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="rs_buy_license_section">
        <h4 class="h4title_product_sell">{!!$row->name_th_1!!}</h4>
          {!! $row->descript_th !!}
      </div>
    </div>
  </div>
</div>
</div>


@else


<div class="rs_graybg rs_toppadder30 rs_bottompadder70">
  <div class="container">
    <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="rs_buy_product_buy_top_section">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
            <h4 class="widget-title widget-title-bid">
                {!! $row->name_en_1 !!}
            </h4>
      </div>
      <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12 no-padding">
        <div class="rs_single_product_slider"> 
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 no-padding">
            <div id="sync2" class="owl-carousel">
                  <?php 
                    $fields['featured_img']['format_as'] = $fields['gallery']['format_as'] = 'imgsmall'; 
                  ?>
                  {!! SiteHelpers::formatRows($row->featured_img,$fields['featured_img'],$row ,$row->name_en_1) !!} 
                  {!! SiteHelpers::formatRows($row->gallery,$fields['gallery'],$row ,$row->name_en_1) !!}
            </div>
            </div>
            <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12 no-padding border-left">
            <div id="sync1" class="owl-carousel">
                  <?php 
                    $fields['featured_img']['format_as'] = $fields['gallery']['format_as'] = 'imgsmall'; 
                  ?>
                  {!! SiteHelpers::formatRows($row->featured_img,$fields['featured_img'],$row ,$row->name_en_1) !!} 
                  {!! SiteHelpers::formatRows($row->gallery,$fields['gallery'],$row ,$row->name_en_1) !!}
            </div>
            
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 no-padding">
            {!! html_entity_decode(Lang::get('productshop.1')) !!}
            </div>
            </div>
            
            
        </div>
      </div>    
   <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 no-padding">
      <aside class="widget widget_license_product_sell rs_bottompadder20">
        <div id="buypanel">
        <h4 class="h4title_product_sell"> {!! $row->name_en_1 !!} </h4>
<div class="text">{!! $row->title_en !!}</div>
          <form class="form-productbuy-details" name="form-productbuy-detailshop" meenod="post" action="{!!url('cart') !!}">
            <div class="form-group select-qty">
              <label for="qty" class="col-xs-4 no-padding">{!! html_entity_decode(Lang::get('productshop.2')) !!} </label>
              <div class="col-xs-8">
                <div class="rs_incre_decre_btns">
                  <button type="button" class="btn_small count_minus" id="breakDecrease"> <i class="fa fa-minus"></i> </button>
                  <button type="button" class="btn_big count_val" id="breakValue" disabled>1</button>
                   <input type="hidden" class="quantity_rows" name="qty" value="1">
                  <button type="button" class="btn_small count_plus" id="breakIncrease"> <i class="fa fa-plus"></i> </button>
                </div>
              </div>
            </div>
            @if(isset($row->color_en))
            <div class="form-group ">
              <label for="field-color" class="col-xs-4 no-padding">{!! html_entity_decode(Lang::get('productshop.3')) !!}</label>
                <div class="col-xs-6">
                <select id="productcolor" class="rs-custom-select" name="color">
                    <?php 
                    $fields['color_en']['format_as'] = 'dropdownlist'; 
                    ?>
                    {!! SiteHelpers::formatRows($row->color_en,$fields['color_en'],$row ,$row->color_id) !!} 
                </select>
                </div>
            </div>
            @endif
            
            <div class="form-group ">
              <label for="field-shipping" class="col-xs-4 no-padding">ราคา</label>
                @if(isset($row->sale_price)&&$row->sale_price!="0.00")
                <div class="col-xs-6">
                <span style="text-decoration: line-enrough; color: #ccc;">{!! number_format($row->price,2) !!}</span> {!! number_format($row->sale_price,2) !!}
                @else
                <div class="col-xs-6" >
                {!! number_format($row->price,2) !!}
                @endif enB
                </div>
            </div>

            <div class="form-group ">
              <label for="field-shipping" class="col-xs-4 no-padding">{!! html_entity_decode(Lang::get('productshop.4')) !!}</label>
                <div class="col-xs-6">
                @if(isset($row->sale_price)&&$row->sale_price!="0.00")
                <span style="text-decoration: line-enrough; color: #ccc;">{!! number_format($row->price*0.5,2) !!}</span> {!! number_format($row->sale_price*0.5,2) !!}
                @else
                {!! number_format($row->price*0.5,2) !!}
                @endif S-Pay
                </div>
            </div>
            <div class="form-group">
            <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 no-padding">
              <button type="submit" name="paytype" value="money" class="rs_button rs_button_orange" style="margin-bottom:10px;">
              {!! html_entity_decode(Lang::get('productshop.5')) !!}
              <?php 
              if(isset($row->sale_price)&&$row->sale_price!="0.00")
                { $pricemoney = $row->sale_price; echo number_format($row->sale_price); }
              else 
                { $pricemoney = $row->price; echo number_format($row->price); } ?> enB
              </button> 
              
            </div>
            <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12 no-padding">
                <button type="button" id="spayclick" name="paytypes" value="spay" class="rs_button rs_button_blue">{!! html_entity_decode(Lang::get('productshop.5')) !!}
                <?php 
              if(isset($row->sale_price)&&$row->sale_price!="0.00")
                { $pricespay = $row->sale_price*0.5; echo number_format($row->sale_price*0.5); }
              else 
                { $pricespay = $row->price*0.5; echo number_format($row->price*0.5); } ?> S-Pay</button>
               
            </div>
                    <input type="hidden" name="url" value="{!! asset('uploads/products') !!}/{!! $row->featured_img !!}">
                    <input type="hidden" name="products_code" value="<?php echo $row->products_code; ?>">
                    <input type="hidden" name="name_en" value="<?php echo $rowx->name_en_1; ?>">
                    <input type="hidden" name="name_th" value="<?php echo $rowx->name_th_1; ?>">
                    <input type="hidden" name="pid" value="<?php echo $row->id; ?>">
                    <input type="hidden" name="pricemoney" value="{!! $pricemoney !!}">
                    <input type="hidden" name="pricespay" value="{!! $pricespay !!}">
                    <input type="hidden" name="cartaction" value="add">
                    <input type="hidden" name="shiping_cost" value="{!! $row->shiping_cost !!}">
            </div>
          </form>
          <div class="text-left"> {!! html_entity_decode(Lang::get('productshop.6')) !!} <a class="btn btn-border" href="tel:{!! html_entity_decode(Lang::get('productshop.7')) !!}"><i class="fa fa-phone-square"></i> {!! html_entity_decode(Lang::get('productshop.7')) !!}</a></div>
          <div class="text-left"><i class="fa fa-truck fa-lg"></i> {!! html_entity_decode(Lang::get('productshop.8')) !!}</div>
          <div class="text-left"><i class="fa fa-money fa-lg"></i> {!! html_entity_decode(Lang::get('productshop.9')) !!}</div>
                <img src="{!! asset('frontend') !!}/punbidtheme/images/banktransfer.png" alt="payment" />
</div>
      </aside>
    </div>
    </div>
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="rs_buy_license_section">
        <h4 class="h4title_product_sell">{!!$row->name_en_1!!}</h4>
          {!! $row->descript_en !!}
      </div>
    </div>
  </div>
</div>
</div>


@endif
