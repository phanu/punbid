<ul class="nav nav-pills pull-right">v
                                        <li class="active">
                                            <a href="#informwithdraw" data-toggle="tab" aria-expanded="true">{!! html_entity_decode(Lang::get('myacc-wihdraw.1')) !!}</a>
                                        </li>
                                        <li class="">
                                            <a href="#informwithdraw-hist" data-toggle="tab" aria-expanded="false">{!! html_entity_decode(Lang::get('myacc-wihdraw.2')) !!}</a>
                                        </li>
                                    </ul>
                                    <h4 class="header-title m-b-30">{!! html_entity_decode(Lang::get('myacc-wihdraw.3')) !!}</h4>
                                    <div class="tab-content">
                                        		<div id="informwithdraw" class="tab-pane active fade in">
													<div class="row">
												{!! Form::open(array('url'=>'payment/savewithdraw/', 'class'=>'form-horizontal ' ,'files' => true)) !!}
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group rs_bottompadder20">
																<label>{!! html_entity_decode(Lang::get('myacc-wihdraw.4')) !!}</label>
																<input type="number"  id="calbid" name="bid" class="form-control" placeholder="{!! html_entity_decode(Lang::get('myacc-wihdraw.5')) !!}">
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group rs_bottompadder20">
																<label>{!! html_entity_decode(Lang::get('myacc-wihdraw.6')) !!}</label>
																  <input type="text" class="form-control" name="accountname"
																  value="{!!$userdet->first_name!!} {!!$userdet->last_name!!}" placeholder="{!! html_entity_decode(Lang::get('myacc-wihdraw.7')) !!}" readonly="readonly" required>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group rs_bottompadder20">
																<label>{!! html_entity_decode(Lang::get('myacc-wihdraw.8')) !!}</label>
																<input type="text" name="accountnum" class="form-control" placeholder="{!! html_entity_decode(Lang::get('myacc-wihdraw.9')) !!}" required> 
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group rs_bottompadder20">
																<label>{!! html_entity_decode(Lang::get('myacc-wihdraw.10')) !!}</label>
																<input type="text" class="form-control" name="bank" placeholder="{!! html_entity_decode(Lang::get('myacc-wihdraw.11')) !!}" required>
															</div>
														</div>

													</div>
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group rs_bottompadder20">
																<label>{!! html_entity_decode(Lang::get('myacc-wihdraw.12')) !!}</label>
																<input type="number" name="amount" id="withdrawamount"  class="form-control" placeholder="{!! html_entity_decode(Lang::get('myacc-wihdraw.13')) !!}" readonly="readonly">
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group rs_bottompadder20">
																<label>{!! html_entity_decode(Lang::get('myacc-wihdraw.14')) !!}</label><br>
													<a href="" id="calculatebid" class="rs_button rs_button_blue" required>
																	{!! html_entity_decode(Lang::get('myacc-wihdraw.15')) !!}
																</a>
															</div>
														</div>
													</div>
													<div class="rs_btn_div rs_toppadder30">
													<input type="hidden" name="customerNumber" value="{!!$userdet->id!!}">
														<input type="hidden" name="status" value="request">
														<button type="submit" class="rs_button rs_button_orange">{!! html_entity_decode(Lang::get('myacc-wihdraw.16')) !!}</button>
													</div>
												{!! Form::close() !!}
													</div>

													<div id="informwithdraw-hist" class="tab-pane fade in">
													<div class="row">
														<div class="col-md-12">
		                                                
		                                                   <?php 
		                                                   	use \App\Http\Controllers\WithdrawsController;
		                                                    $data = WithdrawsController::display(); 
		                                                   	
		                                                    
		                                                    ?>

		                                                   <table id="datatable" class="table table-striped">
		                                                    <thead>
		                                                        <tr>
		                                                        @foreach ($data['tableGrid'] as $t)
		                                                            @if($t['view'] =='1')               
		                                               <?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
		                                                                @if(SiteHelpers::filterColumn($limited ))
		                                                                
		                                                                    <th><?php echo \SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())); ?></th>          
		                                                                @endif 
		                                                            @endif
		                                                        @endforeach
		                                                        </tr>
		                                                    </thead>

		                                                    <tbody>
		                                                        @foreach ($data['rowData'] as $row)
		                                                            <tr>                                    
		                                                             @foreach ($data['tableGrid'] as $field)
		                                                                 @if($field['view'] =='1')
		                                                                    <?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
		                                                                    @if(SiteHelpers::filterColumn($limited ))
		                                                                     <td>          
		                                                                        <p>{!! SiteHelpers::formatRows($row->{$field['field']},$field,$row) !!}</p> 
		                                                                     </td>
		                                                                    @endif  
		                                                                 @endif                  
		                                                             @endforeach              
		                                                            </tr>
		                                                        @endforeach
		                                                    </tbody>

		                                                    </table>
		                                                </div>
		                                                </div>
		                                                </div>
											</div>
