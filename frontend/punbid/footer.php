﻿
  <!-- our features -->
  <div class="our-features-box">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-xs-12 col-sm-6">
          <div class="feature-box first"> <span class="fa fa-truck"></span>
            <div class="content">
              <h3>ทองคำราคาดี ประมูลเลย</h3>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-12 col-sm-6">
          <div class="feature-box"> <span class="fa fa-headphones"></span>
            <div class="content">
              <h3>ไปๆกันไปเอาทองกัน</h3>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-12 col-sm-6">
          <div class="feature-box"> <span class="fa fa-share"></span>
            <div class="content">
              <h3>ทองไม่รู้ร้อนเลย</h3>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-12 col-sm-6">
          <div class="feature-box last"> <span class="fa fa-phone"></span>
            <div class="content">
              <h3>สายด่วน 096-695-6669</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  
</div>
<!-- End Footer --> 

<!-- mobile menu -->
<div id="mobile-menu">
  <ul>
    <li>
      <div class="mm-search">
        <form id="search1" name="search">
          <div class="input-group">
            <div class="input-group-btn">
              <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> </button>
            </div>
            <input type="text" class="form-control simple" placeholder="Search ..." name="srch-term" id="srch-term">
          </div>
        </form>
      </div>
    </li>
    <li><a href="index.php">Home</a>
          <ul>
        <li class="level1"><a href="../fashion/index.php"><span>Fashion Store</span></a> </li>
        <li class="level1"><a href="index.php"><span>Digital Store</span></a> </li>
        <li class="level1"><a href="../furniture/index.php"><span>Furniture Store</span></a> </li>
        <li class="level1"><a href="../jewellery/index.php"><span>Jewellery Store</span></a> </li>
      </ul>
    </li>
    <li><a href="#">Pages</a>
      <ul>
        <li><a href="grid.php">Grid</a> </li>
        <li> <a href="list.php">List</a> </li>
        <li> <a href="product_detail.php">Product Detail</a> </li>
        <li> <a href="shopping_cart.php">Shopping Cart</a> </li>
        <li><a href="checkout.php">Checkout</a> </li>
        <li> <a href="wishlist.php">Wishlist</a> </li>
        <li> <a href="dashboard.php">Dashboard</a> </li>
        <li> <a href="multiple_addresses.php">Multiple Addresses</a> </li>
        <li> <a href="about_us.php">About us</a> </li>
        <li><a href="blog.php">Blog</a>
          <ul>
            <li><a href="blog-detail.php">Blog Detail</a> </li>
          </ul>
        </li>
        <li><a href="contact_us.php">Contact us</a> </li>
        <li><a href="404error.php">404 Error Page</a> </li>
      </ul>
    </li>
    <li><a href="#">Women</a>
      <ul>
        <li> <a href="#" class="">Stylish Bag</a>
          <ul>
            <li> <a href="grid.php" class="">Clutch Handbags</a> </li>
            <li> <a href="grid.php" class="">Diaper Bags</a> </li>
            <li> <a href="grid.php" class="">Bags</a> </li>
            <li> <a href="grid.php" class="">Hobo handbags</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Material Bag</a>
          <ul>
            <li> <a href="grid.php">Beaded Handbags</a> </li>
            <li> <a href="grid.php">Fabric Handbags</a> </li>
            <li> <a href="grid.php">Handbags</a> </li>
            <li> <a href="grid.php">Leather Handbags</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Shoes</a>
          <ul>
            <li> <a href="grid.php">Flat Shoes</a> </li>
            <li> <a href="grid.php">Flat Sandals</a> </li>
            <li> <a href="grid.php">Boots</a> </li>
            <li> <a href="grid.php">Heels</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Jwellery</a>
          <ul>
            <li> <a href="grid.php">Bracelets</a> </li>
            <li> <a href="grid.php">Necklaces &amp; Pendent</a> </li>
            <li> <a href="grid.php">Pendants</a> </li>
            <li> <a href="grid.php">Pins &amp; Brooches</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Dresses</a>
          <ul>
            <li> <a href="grid.php">Casual Dresses</a> </li>
            <li> <a href="grid.php">Evening</a> </li>
            <li> <a href="grid.php">Designer</a> </li>
            <li> <a href="grid.php">Party</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Swimwear</a>
          <ul>
            <li> <a href="grid.php">Swimsuits</a> </li>
            <li> <a href="grid.php">Beach Clothing</a> </li>
            <li> <a href="grid.php">Clothing</a> </li>
            <li> <a href="grid.php">Bikinis</a> </li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="grid.php">Men</a>
      <ul>
        <li> <a href="grid.php" class="">Shoes</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.php">Sport Shoes</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Casual Shoes</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Leather Shoes</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">canvas shoes</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Dresses</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.php">Casual Dresses</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Evening</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Designer</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Party</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Jackets</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.php">Coats</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Formal Jackets</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Leather Jackets</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Blazers</a> </li>
          </ul>
        </li>
        <li> <a href="#.php">Watches</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.php">Fasttrack</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Casio</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Titan</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Tommy-Hilfiger</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Sunglasses</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.php">Ray Ban</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Fasttrack</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Police</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Oakley</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Accesories</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.php">Backpacks</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Wallets</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Laptops Bags</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Belts</a> </li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="grid.php">Electronics</a>
      <ul>
        <li> <a href="grid.php"><span>Mobiles</span></a>
          <ul>
            <li> <a href="grid.php"><span>Samsung</span></a> </li>
            <li> <a href="grid.php"><span>Nokia</span></a> </li>
            <li> <a href="grid.php"><span>IPhone</span></a> </li>
            <li> <a href="grid.php"><span>Sony</span></a> </li>
          </ul>
        </li>
        <li> <a href="grid.php" class=""><span>Accesories</span></a>
          <ul>
            <li> <a href="grid.php"><span>Mobile Memory Cards</span></a> </li>
            <li> <a href="grid.php"><span>Cases &amp; Covers</span></a> </li>
            <li> <a href="grid.php"><span>Mobile Headphones</span></a> </li>
            <li> <a href="grid.php"><span>Bluetooth Headsets</span></a> </li>
          </ul>
        </li>
        <li> <a href="grid.php"><span>Cameras</span></a>
          <ul>
            <li> <a href="grid.php"><span>Camcorders</span></a> </li>
            <li> <a href="grid.php"><span>Point &amp; Shoot</span></a> </li>
            <li> <a href="grid.php"><span>Digital SLR</span></a> </li>
            <li> <a href="grid.php"><span>Camera Accesories</span></a> </li>
          </ul>
        </li>
        <li> <a href="grid.php"><span>Audio &amp; Video</span></a>
          <ul>
            <li> <a href="grid.php"><span>MP3 Players</span></a> </li>
            <li> <a href="grid.php"><span>IPods</span></a> </li>
            <li> <a href="grid.php"><span>Speakers</span></a> </li>
            <li> <a href="grid.php"><span>Video Players</span></a> </li>
          </ul>
        </li>
        <li> <a href="grid.php"><span>Computer</span></a>
          <ul>
            <li> <a href="grid.php"><span>External Hard Disk</span></a> </li>
            <li> <a href="grid.php"><span>Pendrives</span></a> </li>
            <li> <a href="grid.php"><span>Headphones</span></a> </li>
            <li> <a href="grid.php"><span>PC Components</span></a> </li>
          </ul>
        </li>
        <li> <a href="grid.php"><span>Appliances</span></a>
          <ul>
            <li> <a href="grid.php"><span>Vaccum Cleaners</span></a> </li>
            <li> <a href="grid.php"><span>Indoor Lighting</span></a> </li>
            <li> <a href="grid.php"><span>Kitchen Tools</span></a> </li>
            <li> <a href="grid.php"><span>Water Purifier</span></a> </li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="grid.php">Furniture</a>
      <ul>
        <li> <a href="grid.php">Living Room</a>
          <ul>
            <li> <a href="grid.php">Racks &amp; Cabinets</a> </li>
            <li> <a href="grid.php">Sofas</a> </li>
            <li> <a href="grid.php">Chairs</a> </li>
            <li> <a href="grid.php">Tables</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php" class="">Dining &amp; Bar</a>
          <ul>
            <li> <a href="grid.php">Dining Table Sets</a> </li>
            <li> <a href="grid.php">Serving Trolleys</a> </li>
            <li> <a href="grid.php">Bar Counters</a> </li>
            <li> <a href="grid.php">Dining Cabinets</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Bedroom</a>
          <ul>
            <li> <a href="grid.php">Beds</a> </li>
            <li> <a href="grid.php">Chest of Drawers</a> </li>
            <li> <a href="grid.php">Wardrobes &amp; Almirahs</a> </li>
            <li> <a href="grid.php">Nightstands</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Kitchen</a>
          <ul>
            <li> <a href="grid.php">Kitchen Racks</a> </li>
            <li> <a href="grid.php">Kitchen Fillings</a> </li>
            <li> <a href="grid.php">Wall Units</a> </li>
            <li> <a href="grid.php">Benches &amp; Stools</a> </li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="grid.php">Kids</a> </li>
    <li><a href="contact-us.php">Contact Us</a> </li>
  </ul>
  <div class="top-links">
    <ul class="links">
      <li><a title="My Account" href="login.php">My Account</a> </li>
      <li><a title="Wishlist" href="wishlist.php">Wishlist</a> </li>
      <li><a title="Checkout" href="checkout.php">Checkout</a> </li>
      <li><a title="Blog" href="blog.php"><span>Blog</span></a> </li>
      <li class="last"><a title="Login" href="login.php"><span>Login</span></a> </li>
    </ul>
  </div>
</div>

<!-- JavaScript --> 
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/bootstrap.min.js"></script> 
<script type="text/javascript" src="js/revslider.js"></script> 
<script type="text/javascript" src="js/common.js"></script> 
 
<script type="text/javascript" src="js/owl.carousel.min.js"></script> 
<script type="text/javascript" src="js/jquery.mobile-menu.min.js"></script> 
<script type='text/javascript'>
jQuery(document).ready(function() {
	$('.bid-time').each(function() {
			var $this = $(this), finalDate = $(this).data('countdown');
			$this.countdown(finalDate, function(event) {
			$this.html(event.strftime('%H:%M:%S'));
			});
		});
	jQuery('#rev_slider_4').show().revolution({
	dottedOverlay: 'none',
	delay: 5000,
	startwidth: 915,
	startheight: 490,
	hideThumbs: 200,
	thumbWidth: 200,
	thumbHeight: 50,
	thumbAmount: 2,
	navigationType: 'thumb',
	navigationArrows: 'solo',
	navigationStyle: 'round',
	touchenabled: 'on',
	onHoverStop: 'on',
	swipe_velocity: 0.7,
	swipe_min_touches: 1,
	swipe_max_touches: 1,
	drag_block_vertical: false,
	spinner: 'spinner0',
	keyboardNavigation: 'off',
	navigationHAlign: 'center',
	navigationVAlign: 'bottom',
	navigationHOffset: 0,
	navigationVOffset: 20,
	soloArrowLeftHalign: 'left',
	soloArrowLeftValign: 'center',
	soloArrowLeftHOffset: 20,
	soloArrowLeftVOffset: 0,
	soloArrowRightHalign: 'right',
	soloArrowRightValign: 'center',
	soloArrowRightHOffset: 20,
	soloArrowRightVOffset: 0,
	shadow: 0,
	fullWidth: 'on',
	fullScreen: 'off',
	stopLoop: 'off',
	stopAfterLoops: -1,
	stopAtSlide: -1,
	shuffle: 'off',
	autoHeight: 'off',
	forceFullWidth: 'on',
	fullScreenAlignForce: 'off',
	minFullScreenHeight: 0,
	hideNavDelayOnMobile: 1500,
	hideThumbsOnMobile: 'off',
	hideBulletsOnMobile: 'off',
	hideArrowsOnMobile: 'off',
	hideThumbsUnderResolution: 0,
	hideSliderAtLimit: 0,
	hideCaptionAtLimit: 0,
	hideAllCaptionAtLilmit: 0,
	startWithSlide: 0,
	fullScreenOffsetContainer: ''
});
});
</script> 
<!-- Hot Deals Timer 1--> 
<script src="js/jquery.countdown.min.js"></script>
<script src="js/jquery.countdown.js"></script>

</body>
</html>