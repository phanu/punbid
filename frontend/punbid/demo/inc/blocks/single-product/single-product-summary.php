<!-- /.loop-product-categories -->
<?
$pid = $row['pid'];
$time = date("Y/m/d H:i:s", strtotime($row['ps_end_date']));
$sprice = $row['ps_start_price'];
$eprice = $row['ps_end_price'];
$price = $row['prod_price'];

//numbid
$bidnum = countDesc("pdid","sc_prod_bid","psid='".$row['psid']."'");

//get cus
if($bidnum > 0){
	$cust = getDescArr("cr_cust.username,cr_cust.cid","sc_prod_bid,cr_cust","sc_prod_bid.cid=cr_cust.cid AND sc_prod_bid.psid='".$row['psid']."'", "sc_prod_bid.pdid DESC", "1");
	$bidcus = $cust['cid'];
	$bidcususer = $cust['username'];
}else{
	$bidcus = 0;
}

//find percent
$rem = $price - $eprice;
$rpercent = number_format(($rem * 100) / $price,2);
?>

<div class="summary entry-summary prod-summary"> 
	
	<!-- /.loop-product-categories -->
	
	<h1 itemprop="name" class="product_title entry-title"><?=$row['prod_title']?></h1>
	<div class="woocommerce-product-rating">
		<div class="star-rating" title="Rated 4.33 out of 5"> <span style="width:86.6%"> <strong itemprop="ratingValue" class="rating">4.33</strong> out of <span itemprop="bestRating">5</span> based on <span itemprop="ratingCount" class="rating">3</span> customer ratings </span> </div>
		<a href="#reviews" class="woocommerce-review-link"></a> </div>
	<!-- .woocommerce-product-rating --> 
	
	<!-- .brand -->
	
	<div class="availability in-stock">ผู้ประมูลล่าสุด <span class="user-bid"><?=$bidcususer?></span></div>
	<!-- .availability -->
	
	<p><strong><span class="user-bid"><?=$bidcususer?></span> </strong>ประมูล<strong> <?=$row['prod_title']?></strong> ในราคา<strong> <span class="amount-bid"><?=$eprice?></span> บาท </strong>ประหยัดไปตั้ง <span class="rpercent"><?=$rpercent?></span>% จากราคาขาย!</p>
	
	<!-- .action-buttons -->
	
	<div itemprop="description" style="display:none">
		<ul>
			<li>ราคาปกติ 23,000 บาท</li>
			<li>ลดให้-21,000 บาท (93%)</li>
			<li>มูลค่า Bid ที่ใช้ไป-800.00 บาท</li>
			<li>ราคาปิดประมูล-2,000 บาท</li>
			<li>ประหยัด฿209.70</li>
		</ul>
	</div>
	<!-- .description -->
	
	<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
		<p class="price">
			<span class="electro-price">
				<ins><span class="amount"><span class="amount-bid"><?=$eprice?></span> บาท</span></ins>
				<del><span class="amount">ราคา <?=$price?></span></del>
			</span>
		</p>
		<meta itemprop="price" content="1215" />
		<meta itemprop="priceCurrency" content="THB" />
		<link itemprop="availability" href="http://schema.org/InStock" />
	</div>
	<!-- /itemprop --> 	
</div>
<!-- .woocommerce-product-rating --> 

<!-- .brand --> 

<!-- .availability --> 

<!-- .action-buttons --> 

<!-- .description --> 

<!-- /itemprop -->

<?php //require_once 'inc/blocks/single-product/variations-form.php'; ?>
<form class="frmproduct variations_form cart" method="post">
	<div class="deal-countdown-timer" id="prod_<?=$pid?>"
		data-bidtime="<?=$time?>"
		data-bidnum="<?=$bidnum?>"
		data-bidprice="<?=$eprice?>"
		data-bidpid="<?=$pid?>"
		data-bidcus="<?=$bidcus?>">
		<div class="marketing-text text-xs-center">การประมูลจะสิ้นสุดใน</div>
		<div id="deal-countdown" class="countdown text-xs-center">	
			<!--<span class="days"><span class="value">01</span><b>Days</b></span> 
			<span class="hours"><span class="value">22</span><b>Hours</b></span> 
			<span class="minutes"><span class="value">20</span><b>Mins</b></span> 
			<span class="seconds"><span class="value">35</span><b>Secs</b></span>-->
		</div>
	</div>
	<div class="single_variation_wrap text-xs-center">
		<div class="woocommerce-variation single_variation"></div>
		<div class="woocommerce-variation-add-to-cart variations_button">
			<button type="button" class="single_add_to_cart_button button" id="btn-bid" data-pid="<?=$pid?>">BID NOW</button>
			<input type="hidden" name="add-to-cart" value="2452" />
			<input type="hidden" name="product_id" value="2452" />
			<input type="hidden" name="variation_id" class="variation_id" value="0" />
			<button type="button" class="single_add_to_cart_button button" id="btn-autobid" data-pid="<?=$pid?>">AUTO BID</button>
		</div>
	</div>
</form>
</div>
<!-- .summary -->