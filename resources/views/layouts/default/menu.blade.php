  {{--*/ $menus = SiteHelpers::menus('top') /*--}}
<div class="rs_graybg toplogo">
  <div class="container">
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
        <div class="rs_index3_logo"> 
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6">
          <a href="{!! url('') !!}"><img src="{!! asset('frontend') !!}/punbidtheme/images/punbidthemelogo.svg" class="img-responsive" alt="logo"></a>
          </div>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-6">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#rs_index3_menu" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          </div>
        </div>
      </div>
      <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
        <div class="rs_index3_menu">
          <div class="collapse navbar-collapse" id="rs_index3_menu">
            <ul class="nav navbar-nav">
              @foreach ($menus as $menu)
              <li @if(Request::segment(1) == $menu['module']) class="active" @endif>
              <a 
              @if($menu['menu_type'] =='external')
                href="{!! URL::to($menu['url']) !!}" 
              @else
                href="{!! URL::to($menu['module']) !!}<?php if(Session::get('lang')=='en'&&$menu['module']!='home'&&$menu['module']!='shop'&&$menu['module']!='bidpackhome')echo '-en';?>" 
              @endif
              >
              @if(Session::get('lang')=='th'||Session::get('lang')==''||empty(Session::get('lang')))
                {!! $menu['menu_lang']['title']['th'] !!}
              @else
                {!! $menu['menu_name'] !!}
              @endif  
              </a>
              </li>
              @endforeach
              @if(Session::get('lang')=='th'||Session::get('lang')==''||empty(Session::get('lang')))
                        <li><a href="{!! url('home/lang/en') !!}"><img src="{!! asset('frontend') !!}/punbidtheme/images/en.svg" alt=""></a></li>
                        @else
                        <li><a href="{!! url('home/lang/th') !!}"><img src="{!! asset('frontend') !!}/punbidtheme/images/th.svg" alt=""></a></li>
              @endif
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>