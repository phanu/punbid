
@if($setting['form-method'] =='native')
	<div class="sbox">
		<div class="sbox-title">  
			<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
				<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
			</h4>
	</div>

	<div class="sbox-content"> 
@endif	
			{!! Form::open(array('url'=>'productsbid/save/'.SiteHelpers::encryptID($row['products_bid_id']), 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'productsbidFormAjax')) !!}
			<div class="col-md-12">
						<fieldset><legend> ข้อมูลการประมูล</legend>
				{!! Form::hidden('products_bid_id', $row['products_bid_id']) !!}					
									  <div class="form-group  " >
										<label for="SKU" class=" control-label col-md-4 text-left"> SKU <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('products_code', $row['products_code'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Name Th" class=" control-label col-md-4 text-left"> Name Th <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('name_th', $row['name_th'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Name En" class=" control-label col-md-4 text-left"> Name En <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('name_en', $row['name_en'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Title Th" class=" control-label col-md-4 text-left"> Title Th <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='title_th' rows='5' id='title_th' class='form-control '  
				         required  >{{ $row['title_th'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Title En" class=" control-label col-md-4 text-left"> Title En <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='title_en' rows='5' id='title_en' class='form-control '  
				         required  >{{ $row['title_en'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Descript Th" class=" control-label col-md-4 text-left"> Descript Th <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='descript_th' rows='5' id='editor' class='form-control editor '  
						required >{{ $row['descript_th'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Descript En" class=" control-label col-md-4 text-left"> Descript En <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='descript_en' rows='5' id='editor' class='form-control editor '  
						required >{{ $row['descript_en'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Category" class=" control-label col-md-4 text-left"> Category <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='category' rows='5' id='category' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Price" class=" control-label col-md-4 text-left"> Price <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('price', $row['price'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Brand" class=" control-label col-md-4 text-left"> Brand <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <select name='brand' rows='5' id='brand' class='select2 ' required  ></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Featured Img" class=" control-label col-md-4 text-left"> Featured Img </label>
										<div class="col-md-6">
										  <input  type='file' name='featured_img' id='featured_img' @if($row['featured_img'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['featured_img'],'/uploads/products/') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Gallery" class=" control-label col-md-4 text-left"> Gallery </label>
										<div class="col-md-6">
										  
					<a href="javascript:void(0)" class="btn btn-xs btn-primary pull-right" onclick="addMoreFiles('gallery')"><i class="fa fa-plus"></i></a>
					<div class="galleryUpl">	
					 	<input  type='file' name='gallery[]'  />			
					</div>
					<ul class="uploadedLists " >
					<?php $cr= 0; 
					$row['gallery'] = explode(",",$row['gallery']);
					?>
					@foreach($row['gallery'] as $files)
						@if(file_exists('./uploads/products/'.$files) && $files !='')
						<li id="cr-<?php echo $cr;?>" class="">							
							<a href="{{ url('/uploads/products//'.$files) }}" target="_blank" >{{ $files }}</a> 
							<span class="pull-right removeMultiFiles" rel="cr-<?php echo $cr;?>" url="/uploads/products/{{$files}}">
							<i class="fa fa-trash-o  btn btn-xs btn-danger"></i></span>
							<input type="hidden" name="currgallery[]" value="{{ $files }}"/>
							<?php ++$cr;?>
						</li>
						@endif
					
					@endforeach
					</ul>
					 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Shiping Cost" class=" control-label col-md-4 text-left"> Shiping Cost <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('shiping_cost', $row['shiping_cost'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Bid Start" class=" control-label col-md-4 text-left"> Bid Start </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('bid_start', $row['bid_start'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Bid End" class=" control-label col-md-4 text-left"> Bid End </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('bid_end', $row['bid_end'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Active" class=" control-label col-md-4 text-left"> Active </label>
										<div class="col-md-6">
										  <?php $active = explode(",",$row['active']); ?>
					 <label class='checked checkbox-inline'>   
					<input type='checkbox' name='active[]' value ='1'   class='' 
					@if(in_array('1',$active))checked @endif 
					 />  </label>  
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Completed" class=" control-label col-md-4 text-left"> Completed </label>
										<div class="col-md-6">
										  <?php $completed = explode(",",$row['completed']); ?>
					 <label class='checked checkbox-inline'>   
					<input type='checkbox' name='completed[]' value ='1'   class='' 
					@if(in_array('1',$completed))checked @endif 
					 /> completed </label>  
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Boots" class=" control-label col-md-4 text-left"> Boots <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('boots', $row['boots'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Complete bid Profit" class=" control-label col-md-4 text-left"> Complete bid Profit <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('complete_val', $row['complete_val'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Comment" class=" control-label col-md-4 text-left"> Comment </label>
										<div class="col-md-6">
										  <textarea name='comment' rows='5' id='comment' class='form-control '  
				           >{{ $row['comment'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Botwin" class=" control-label col-md-4 text-left"> Botwin </label>
										<div class="col-md-6">
										  <?php $botwin = explode(",",$row['botwin']); ?>
					 <label class='checked checkbox-inline'>   
					<input type='checkbox' name='botwin[]' value ='1'   class='' 
					@if(in_array('1',$botwin))checked @endif 
					 />  </label>  
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
												
								
						
			<div style="clear:both"></div>	
							
			<div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">	
					<button type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
					<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
				</div>			
			</div> 		 
			{!! Form::close() !!}


@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

	
</div>	
			 
<script type="text/javascript">
$(document).ready(function() { 
	
		$("#category").jCombo("{!! url('productsbid/comboselect?filter=product_categories:id:name_th|id') !!}",
		{  selected_value : '{{ $row["category"] }}' });
		
		$("#brand").jCombo("{!! url('productsbid/comboselect?filter=brand:id:name_th') !!}",
		{  selected_value : '{{ $row["brand"] }}' });
		 
	
	$('.editor').summernote();
	$('.previewImage').fancybox();	
	$('.tips').tooltip();	
	$(".select2").select2({ width:"98%"});	
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'}); 
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});			
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("productsbid/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});
				
	var form = $('#productsbidFormAjax'); 
	form.parsley();
	form.submit(function(){
		
		if(form.parsley('isValid') == true){			
			var options = { 
				dataType:      'json', 
				beforeSubmit :  showRequest,
				success:       showResponse  
			}  
			$(this).ajaxSubmit(options); 
			return false;
						
		} else {
			return false;
		}		
	
	});

});

function showRequest()
{
	$('.ajaxLoading').show();		
}  
function showResponse(data)  {		
	
	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		notyMessage(data.message);	
		$('#sximo-modal').modal('hide');	
	} else {
		notyMessageError(data.message);	
		$('.ajaxLoading').hide();
		return false;
	}	
}			 

</script>		 