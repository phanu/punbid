<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Products Bid Id', (isset($fields['products_bid_id']['language'])? $fields['products_bid_id']['language'] : array())) }}</td>
						<td>{{ $row->products_bid_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Name Th', (isset($fields['name_th']['language'])? $fields['name_th']['language'] : array())) }}</td>
						<td>{{ $row->name_th}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Name En', (isset($fields['name_en']['language'])? $fields['name_en']['language'] : array())) }}</td>
						<td>{{ $row->name_en}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Title Th', (isset($fields['title_th']['language'])? $fields['title_th']['language'] : array())) }}</td>
						<td>{{ $row->title_th}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Title En', (isset($fields['title_en']['language'])? $fields['title_en']['language'] : array())) }}</td>
						<td>{{ $row->title_en}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Descript Th', (isset($fields['descript_th']['language'])? $fields['descript_th']['language'] : array())) }}</td>
						<td>{{ $row->descript_th}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Descript En', (isset($fields['descript_en']['language'])? $fields['descript_en']['language'] : array())) }}</td>
						<td>{{ $row->descript_en}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Color', (isset($fields['color']['language'])? $fields['color']['language'] : array())) }}</td>
						<td>{{ $row->color}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Price', (isset($fields['price']['language'])? $fields['price']['language'] : array())) }}</td>
						<td>{{ $row->price}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Create Date', (isset($fields['create_date']['language'])? $fields['create_date']['language'] : array())) }}</td>
						<td>{{ $row->create_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Category', (isset($fields['category']['language'])? $fields['category']['language'] : array())) }}</td>
						<td>{{ $row->category}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Featured', (isset($fields['featured']['language'])? $fields['featured']['language'] : array())) }}</td>
						<td>{{ $row->featured}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Brand', (isset($fields['brand']['language'])? $fields['brand']['language'] : array())) }}</td>
						<td>{{ $row->brand}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Featured Img', (isset($fields['featured_img']['language'])? $fields['featured_img']['language'] : array())) }}</td>
						<td>{!! SiteHelpers::formatRows($row->featured_img,$fields['featured_img'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Gallery', (isset($fields['gallery']['language'])? $fields['gallery']['language'] : array())) }}</td>
						<td>{{ $row->gallery}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Active', (isset($fields['active']['language'])? $fields['active']['language'] : array())) }}</td>
						<td>{{ $row->active}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Products Code', (isset($fields['products_code']['language'])? $fields['products_code']['language'] : array())) }}</td>
						<td>{{ $row->products_code}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Shiping Cost', (isset($fields['shiping_cost']['language'])? $fields['shiping_cost']['language'] : array())) }}</td>
						<td>{{ $row->shiping_cost}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tag', (isset($fields['tag']['language'])? $fields['tag']['language'] : array())) }}</td>
						<td>{{ $row->tag}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Bid Start', (isset($fields['bid_start']['language'])? $fields['bid_start']['language'] : array())) }}</td>
						<td>{{ date('',strtotime($row->bid_start)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Bid End', (isset($fields['bid_end']['language'])? $fields['bid_end']['language'] : array())) }}</td>
						<td>{{ date('',strtotime($row->bid_end)) }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Winner Id', (isset($fields['winner_id']['language'])? $fields['winner_id']['language'] : array())) }}</td>
						<td>{{ $row->winner_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Boots', (isset($fields['boots']['language'])? $fields['boots']['language'] : array())) }}</td>
						<td>{{ $row->boots}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Comment', (isset($fields['comment']['language'])? $fields['comment']['language'] : array())) }}</td>
						<td>{{ $row->comment}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	