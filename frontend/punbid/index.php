﻿ <?php include('header.php'); ?>
 <!-- Slider -->
  <div id="magik-slideshow" class="magik-slideshow">
    <div class="container">
      <div class="row">
        <div class="col-md-9">
          <div id='rev_slider_4_wrapper' class='rev_slider_wrapper fullwidthbanner-container'>
            <div id='rev_slider_4' class='rev_slider fullwidthabanner'>
              <ul>
                <li data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='images/slide-img1.jpg'><img src='images/slide-img1.jpg' alt="slide-img" data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' />
                  <div class="info">
                    <div class='tp-caption ExtraLargeTitle sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2;max-width:auto;max-height:auto;white-space:nowrap;'><span>Stylish Women</span> </div>
                    <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;max-width:auto;max-height:auto;white-space:nowrap;'><span>Perfect Jewellery</span> </div>
                    <div class='tp-caption Title sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1450' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'>In augue urna, nunc, tincidunt, augue, augue facilisis facilisis.</div>
                    <div class='tp-caption sfb  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'><a href='#' class="buy-btn">Shopping Now</a> </div>
                  </div>
                </li>
                <li data-transition='random' data-slotamount='7' data-masterspeed='1000' data-thumb='images/slide-img2.jpg'><img src='images/slide-img2.jpg' alt="slide-img" data-bgposition='left top' data-bgfit='cover' data-bgrepeat='no-repeat' />
                  <div class="info">
                    <div class='tp-caption ExtraLargeTitle sft slide2  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1100' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:2;max-width:auto;max-height:auto;white-space:nowrap;padding-right:0px'><span>Special Offer</span> </div>
                    <div class='tp-caption LargeTitle sfl  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1300' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:3;max-width:auto;max-height:auto;white-space:nowrap;'>accessories</div>
                    <div class='tp-caption Title sft  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Power2.easeInOut' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</div>
                    <div class='tp-caption sfb  tp-resizeme ' data-endspeed='500' data-speed='500' data-start='1500' data-easing='Linear.easeNone' data-splitin='none' data-splitout='none' data-elementdelay='0.1' data-endelementdelay='0.1' style='z-index:4;max-width:auto;max-height:auto;white-space:nowrap;'><a href='#' class="buy-btn">Buy Now</a> </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="col-md-3 hot-deal">
          <ul class="products-grid">
            <li class="right-space two-height item">
              	<?php include('hot.php'); ?>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  
  <!-- end Slider -->
  <div class="content-page">
    <div class="container"> 
      <!-- featured category fashion -->
      <div class="category-product">
        <div class="navbar nav-menu">
          <div class="navbar-collapse">
            <ul class="nav navbar-nav">
              <li>
                <div class="new_title">
                  <h2><strong>กำลัง</strong>ประมูล</h2>
                </div>
              </li>
              <li class="active"><a data-toggle="tab" href="#tab-1">แหวนทอง</a> </li>
              <li class="divider"></li>
              <li><a data-toggle="tab" href="#tab-2">สร้อยคอ</a> </li>
              <li class="divider"></li>
              <li><a data-toggle="tab" href="#tab-3">สร้อยข้อมือ</a> </li>
              <li class="divider"></li>
              <li><a data-toggle="tab" href="#tab-4">กำไล</a> </li>
              <li class="divider"></li>
            </ul>
          </div>
          <!-- /.navbar-collapse --> 
          
        </div>
        <div class="product-bestseller">
          <div class="product-bestseller-content">
            <div class="product-bestseller-list">
              <div class="tab-container"> 
                <!-- tab product -->
                <div class="tab-panel active" id="tab-1">
                  <div class="category-products">
                    <ul class="products-grid">
                      <?php $i=1; $end=4; $new='new'; include('actural_list.php'); ?>
                    </ul>
                  </div>
                </div>
                <!-- tab product -->
                <div class="tab-panel" id="tab-2">
                  <div class="category-products">
                    <ul class="products-grid">
                      <?php $i=4; $end=7; include('actural_list.php'); ?>
                    </ul>
                  </div>
                </div>
                <div class="tab-panel" id="tab-3">
                  <div class="category-products">
                    <ul class="products-grid">
                      <?php $i=1; $end=4; $new='new'; include('actural_list.php'); ?>
                    </ul>
                  </div>
                </div>
                <div class="tab-panel" id="tab-4">
                  <div class="category-products">
                    <ul class="products-grid">
                      <?php $i=4; $end=7; $new='new'; include('actural_list.php'); ?>
                    </ul>
                  </div>
                </div>
                <div class="tab-panel" id="tab-5">
                  <div class="category-products">
                    <ul class="products-grid">
					<?php $i=1; $end=4; $new='new'; include('actural_list.php'); ?>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
  
  <!-- bestsell Slider -->
  <section class="bestsell-pro">
    <div class="container">
      <div class="slider-items-products">
        <div class="bestsell-block">
          <div id="bestsell-slider" class="product-flexslider hidden-buttons">
            <div class="home-block-inner">
              <div class="block-title">
            
                  <em>Gogoldrun.com</em></h2>
              </div>
              <div class="pretext"></div>
              <a href="grid.php" class="view_more_bnt">สมัครสมาชิก</a> </div>
            <div class="slider-items slider-width-col4 products-grid block-content">
              <?php $i=rand(1,4); $end=$i+3; include('actural_list2.php'); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <!-- End Bestsell Slider --> 
  <div class="bottom-banner-section">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6">
          <div class="bottom-banner-img1"> <a href="#"> <img src="images/ads-01.jpg" alt="bottom banner">
            <div class="bottom-img-info1">
              <h3>ซื้อบิด</h3>
              <span class="line"></span> </div>
            </a></div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="bottom-banner-img1"> <a href="#"> <img src="images/ads-02.jpg" alt="bottom banner">
            <div class="bottom-img-info1">
              <h3>ผู้ชนะประมูล</h3>
              <span class="line"></span> </div>
            </a></div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="bottom-banner-img1"> <a href="#"> <img src="images/ads-03.jpg" alt="bottom banner"><span class="banner-overly"></span>
            <div class="bottom-img-info1">
              <h3>ผู้แพ้อย่าเสียใจ</h3>
              <span class="line"></span> </div>
            </a></div>
        </div>
        
      </div>
    </div>
  </div>
  <!-- featured Slider -->
  <section class="featured-pro">
    <div class="container">
      <div class="slider-items-products">
        <div class="featured-block">
          <div id="featured-slider" class="product-flexslider hidden-buttons">
            <div class="home-block-inner">
              <div class="block-title">
                      <em>ปิดประมูลแล้ว</em></h2>
              </div>
              <div class="pretext">ผู้ชนะประมูล</div>
              <a href="grid.php" class="view_more_bnt">ดูทั้งหมด</a> </div>
            <div class="slider-items slider-width-col4 products-grid block-content">
<?php $i=rand(1,4); $end=$i+3; include('actural_list2.php'); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End featured Slider --> 
  
  <!-- Special Product Slider -->
  
  <!-- End Special Product Slider --> 
  
  <!-- bottom banner section -->
  <div class="bottom-banner-section">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6">
          <div class="bottom-banner-img1"> <a href="#"> <img src="images/ads-01.jpg" alt="bottom banner">
            <div class="bottom-img-info1">
            
              <span class="line"></span> </div>
            </a></div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="bottom-banner-img1"> <a href="#"> <img src="images/ads-02.jpg" alt="bottom banner">
            <div class="bottom-img-info1">

              <span class="line"></span> </div>
            </a></div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="bottom-banner-img1"> <a href="#"> <img src="images/ads-01.jpg" alt="bottom banner"><span class="banner-overly"></span>
            <div class="bottom-img-info1">
         
              <span class="line"></span> </div>
            </a></div>
        </div>
        
      </div>
    </div>
  </div>
  
  <!-- Latest Blog -->
  <div class="main-container col2-right-layout">
    <div class="main container">
      <div class="row">
        <section class="col-sm-9 wow bounceInUp animated">
        <div class="col-main">
          <div class="my-account">
            <div class="page-title">
              <h2>ซื้อ Bid Pack</h2>
            </div>
            <div class="my-wishlist">
              <div class="table-responsive">
                <form method="post" action="#/wishlist/index/update/wishlist_id/1/" id="wishlist-view-form">
                  <fieldset>
                    <input type="hidden" value="ROBdJO5tIbODPZHZ" name="form_key">
                    <table id="wishlist-table" class="clean-table linearize-table data-table">
                      <thead>
                        <tr class="first last">
                          <th class="customer-wishlist-item-image"></th>
                          <th class="customer-wishlist-item-info"></th>
                          <th class="customer-wishlist-item-quantity">จำนวน</th>
                          <th class="customer-wishlist-item-price">ราคา</th>
                          <th class="customer-wishlist-item-cart"></th>
                          <th class="customer-wishlist-item-remove"></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr id="item_31" class="first odd" style="
    /* padding-top: 20px; */
">
                          <td class="wishlist-cell0 customer-wishlist-item-image"><a title="Softwear Women's Designer" href="#/" class="product-image"> <img width="100" alt="Softwear Women's Designer" src="products-images/bidpack.png"> </a></td>
                          <td class="wishlist-cell1 customer-wishlist-item-info"><h3 class="product-name"><a title="Softwear Women's Designer" href="#">Bid Pack x 100ea</a></h3>
                            <div class="description std">
                              <div class="inner">
โปรโมชั่น แถมฟรี 10ea</div>
                            </div>
                            </td>
                          <td data-rwd-label="Quantity" class="wishlist-cell2 customer-wishlist-item-quantity"><div class="cart-cell">
                              <div class="add-to-cart-alt">
                                <input type="text" value="1" name="qty[31]" class="input-text qty validate-not-negative-number" pattern="\d*">
                              </div>
                            </div></td>
                          <td data-rwd-label="Price" class="wishlist-cell3 customer-wishlist-item-price"><div class="cart-cell">
                              <div class="price-box"> <span id="product-price-39" class="regular-price"> <span class="price">1,000 THB</span> </span> </div>
                            </div></td>
                          
                          
                        </tr>
                        <tr id="item_33" class="odd">
                          <td class="wishlist-cell0 customer-wishlist-item-image"><a title="Bid Pack x 300ea" href="#" class="product-image"> <img width="100" alt="Bidpack" src="products-images/bidpack.png"> </a></td>
                          <td class="wishlist-cell1 customer-wishlist-item-info"><h3 class="product-name"><a title="Softwear Women's Designer" href="#">Bid Pack x 200ea</a></h3>
                            <div class="description std">
                              <div class="inner">โปรโมชั่น แถมฟรี 20ea</div>
                            </div>
                            </td>
                          <td data-rwd-label="Quantity" class="wishlist-cell2 customer-wishlist-item-quantity"><div class="cart-cell">
                              <div class="add-to-cart-alt">
                                <input type="text" value="1" name="qty[31]" class="input-text qty validate-not-negative-number" pattern="\d*">
                              </div>
                            </div></td>
                          <td data-rwd-label="Price" class="wishlist-cell3 customer-wishlist-item-price"><div class="cart-cell">
                              <div class="price-box"> <span id="product-price-39" class="regular-price"> <span class="price">2,000 THB</span> </span> </div>
                            </div></td>
                          
                          
                        </tr>
                        <tr id="item_32" class="last even">
                          <td class="wishlist-cell0 customer-wishlist-item-image"><a title="Slim Fit Casual Shirt" href="#" class="product-image"> <img width="100" alt="Slim Fit Casual Shirt" src="products-images/bidpack.png"> </a></td>
                          <td class="wishlist-cell1 customer-wishlist-item-info"><h3 class="product-name"><a title="Slim Fit Casual Shirt" href="#">Bid Pack x 300ea</a></h3>
                            <div class="description std">
                              <div class="inner">โปรโมชั่น แถมฟรี 30ea</div>
                            </div>
                            </td>
                          <td data-rwd-label="Quantity" class="wishlist-cell2 customer-wishlist-item-quantity"><div class="cart-cell">
                              <div class="add-to-cart-alt">
                                <input type="text" value="1" name="qty[32]" class="input-text qty validate-not-negative-number" pattern="\d*">
                              </div>
                            </div></td>
                          <td data-rwd-label="Price" class="wishlist-cell3 customer-wishlist-item-price"><div class="cart-cell">
                              <div class="price-box"> <span id="product-price-2" class="regular-price"> <span class="price">3,000 THB</span> </span> </div>
                            </div></td>
                          
                          
                        </tr>
                      </tbody>
                    </table>
                    <div class="buttons-set buttons-set2">
                      
                      <button class="button btn-add" onclick="addAllWItemsToCart()" title="Add All to Cart" type="button"><span>ซื้อเลย</span></button>
                      
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
            
          </div></div>
        </section>
        <aside class="col-right sidebar col-sm-3 wow bounceInUp animated">
          <div class="block block-account">
            <div class="block-title">My Account</div>
            <div class="block-content">
              <ul>
                <li><a href="#">Account Dashboard</a></li>
                <li><a href="#">Account Information</a></li>
                <li><a href="#">Address Book</a></li>
                <li><a href="#">My Orders</a></li>
                <li><a href="#">Billing Agreements</a></li>
                <li><a href="#">Recurring Profiles</a></li>
                <li><a href="#">My Product Reviews</a></li>
                <li><a href="#">My Tags</a></li>
                <li class="current"><a href="#">My Wishlist</a></li></li>
                <li><a href="#">My Downloadable</a></li>
                <li class="last"><a href="#">Newsletter Subscriptions</a></li>
              </ul>
            </div>
          </div>
          
        </aside>
      </div>
    </div>
  </div>
  
  <!-- End Latest Blog --> 
  
  <!-- Brand Logo -->
  
  <?php include('footer.php'); ?>