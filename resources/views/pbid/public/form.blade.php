

		 {!! Form::open(array('url'=>'pbid/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> pbid</legend>
									
									  <div class="form-group  " >
										<label for="Products Bid Id" class=" control-label col-md-4 text-left"> Products Bid Id </label>
										<div class="col-md-6">
										  {!! Form::text('products_bid_id', $row['products_bid_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Name Th" class=" control-label col-md-4 text-left"> Name Th </label>
										<div class="col-md-6">
										  {!! Form::text('name_th', $row['name_th'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Name En" class=" control-label col-md-4 text-left"> Name En </label>
										<div class="col-md-6">
										  {!! Form::text('name_en', $row['name_en'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Title Th" class=" control-label col-md-4 text-left"> Title Th </label>
										<div class="col-md-6">
										  <textarea name='title_th' rows='5' id='title_th' class='form-control '  
				           >{{ $row['title_th'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Title En" class=" control-label col-md-4 text-left"> Title En </label>
										<div class="col-md-6">
										  <textarea name='title_en' rows='5' id='title_en' class='form-control '  
				           >{{ $row['title_en'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Descript Th" class=" control-label col-md-4 text-left"> Descript Th </label>
										<div class="col-md-6">
										  <textarea name='descript_th' rows='5' id='descript_th' class='form-control '  
				           >{{ $row['descript_th'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Descript En" class=" control-label col-md-4 text-left"> Descript En </label>
										<div class="col-md-6">
										  <textarea name='descript_en' rows='5' id='descript_en' class='form-control '  
				           >{{ $row['descript_en'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Color" class=" control-label col-md-4 text-left"> Color </label>
										<div class="col-md-6">
										  {!! Form::text('color', $row['color'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Price" class=" control-label col-md-4 text-left"> Price </label>
										<div class="col-md-6">
										  {!! Form::text('price', $row['price'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Create Date" class=" control-label col-md-4 text-left"> Create Date </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('create_date', $row['create_date'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Category" class=" control-label col-md-4 text-left"> Category </label>
										<div class="col-md-6">
										  {!! Form::text('category', $row['category'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Featured" class=" control-label col-md-4 text-left"> Featured </label>
										<div class="col-md-6">
										  {!! Form::text('featured', $row['featured'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Brand" class=" control-label col-md-4 text-left"> Brand </label>
										<div class="col-md-6">
										  {!! Form::text('brand', $row['brand'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Featured Img" class=" control-label col-md-4 text-left"> Featured Img </label>
										<div class="col-md-6">
										  {!! Form::text('featured_img', $row['featured_img'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Gallery" class=" control-label col-md-4 text-left"> Gallery </label>
										<div class="col-md-6">
										  <textarea name='gallery' rows='5' id='gallery' class='form-control '  
				           >{{ $row['gallery'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Active" class=" control-label col-md-4 text-left"> Active </label>
										<div class="col-md-6">
										  {!! Form::text('active', $row['active'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Products Code" class=" control-label col-md-4 text-left"> Products Code </label>
										<div class="col-md-6">
										  {!! Form::text('products_code', $row['products_code'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Shiping Cost" class=" control-label col-md-4 text-left"> Shiping Cost </label>
										<div class="col-md-6">
										  {!! Form::text('shiping_cost', $row['shiping_cost'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Tag" class=" control-label col-md-4 text-left"> Tag </label>
										<div class="col-md-6">
										  {!! Form::text('tag', $row['tag'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Bid Start" class=" control-label col-md-4 text-left"> Bid Start </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('bid_start', $row['bid_start'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Bid End" class=" control-label col-md-4 text-left"> Bid End </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('bid_end', $row['bid_end'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Winner Id" class=" control-label col-md-4 text-left"> Winner Id </label>
										<div class="col-md-6">
										  {!! Form::text('winner_id', $row['winner_id'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Boots" class=" control-label col-md-4 text-left"> Boots </label>
										<div class="col-md-6">
										  {!! Form::text('boots', $row['boots'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Comment" class=" control-label col-md-4 text-left"> Comment </label>
										<div class="col-md-6">
										  <textarea name='comment' rows='5' id='comment' class='form-control '  
				           >{{ $row['comment'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Completed" class=" control-label col-md-4 text-left"> Completed </label>
										<div class="col-md-6">
										  {!! Form::text('completed', $row['completed'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
