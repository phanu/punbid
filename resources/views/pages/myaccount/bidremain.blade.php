
                                    <h4 class="header-title m-b-30 myheadbid">{!! html_entity_decode(Lang::get('myacc-bidremain.1')) !!}</h4>

                                    <div class="tab-content">
                                        <div id="bidremain" class="tab-pane fade active in">
                                            <div class="row">
                                                <div class="col-md-12">
                                                
                                                <?php use \App\Http\Controllers\BidremindController; 
                                                    $data = BidremindController::display("all"); ?>

                                                   <table id="datatable" class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                        @foreach ($data['tableGrid'] as $t)
                                                            @if($t['view'] =='1')               
                                                                <?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
                                                                @if(SiteHelpers::filterColumn($limited ))
                                                                
                                                                    <th><?php echo \SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())); ?></th>          
                                                                @endif 
                                                            @endif
                                                        @endforeach
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @foreach ($data['rowData'] as $row)
                                                            <tr>                                    
                                                             @foreach ($data['tableGrid'] as $field)
                                                                 @if($field['view'] =='1')
                                                                    <?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
                                                                    @if(SiteHelpers::filterColumn($limited ))
                                                                     <td>
                                                                        @if($field['field'] =='get')
                                                                            @if($row->get<3)
                                                                            
                                                                                {!! html_entity_decode(Lang::get('myacc-bidremain.2')) !!} 
                                                                            
                                                                                @if($row->get==0)
                                                                                <a href="#" class="getitdone" id="bidremind{!!$row->products_bid_id!!}" data-id="{!!$row->products_bid_id!!}">{!! html_entity_decode(Lang::get('myacc-bidremain.3')) !!}</a>
                                                                                @endif
                                                                            @else
                                                                            
                                                                                {!! html_entity_decode(Lang::get('myacc-bidremain.4')) !!} 
                                                                                @if($row->get==3)
                                                                                <a href="{!! url('winnercheckout?pwin='.$row->products_bid_id) !!}">{!! html_entity_decode(Lang::get('myacc-bidremain.5')) !!}</a>
                                                                                @endif
                                                                            @endif
                                                                        @else            
                                                                        <p>{!! SiteHelpers::formatRows($row->{$field['field']},$field,$row) !!}</p> 
                                                                        @endif
                                                                     </td>
                                                                    @endif  
                                                                 @endif                  
                                                             @endforeach              
                                                            </tr>
                                                        @endforeach
                                                    </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                        
