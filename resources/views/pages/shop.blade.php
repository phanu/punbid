<?php 
  if(Session::get('lang')===null||Session::get('lang')=='')
  {
    $lang = 'th';
  }else{
    $lang = Session::get('lang');
  }
  ?>

<div class="rs_graybg rs_product_found rs_bottompadder30">
		<div class="container">
			<div class="row">
        		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="rs_filte rs_product4column">
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
								<div class="rs_sort"><i class="glyphicon glyphicon-option-vertical" aria-hidden="true"></i> {!! html_entity_decode(Lang::get('shop.1')) !!}</div>
							</div>
							<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
								<ul class="rs_sorting">
									<li><a href="#" class="filter" data-filter="all">{!! html_entity_decode(Lang::get('shop.2')) !!}</a></li>
									<li><a href="#" class="filter" data-filter=".price">{!! html_entity_decode(Lang::get('shop.3')) !!}</a></li>
									<li><a href="#" class="filter" data-filter=".offers">{!! html_entity_decode(Lang::get('shop.4')) !!}</a></li>
									<li><a href="#" class="filter" data-filter=".recommend">{!! html_entity_decode(Lang::get('shop.5')) !!}</a></li>
								<!--	<li><a href="#" class="filter" data-filter=".bestsellers">Best Sellers</a></li>-->
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
            <!--<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 col-lg-pull-9 col-md-pull-8 col-sm-pull-8 col-xs-pull-0">-->
            	<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
						<div class="rs_sidebar_wrapper">
							<aside class="widget widget_categories">
								<h4 class="widget-title">{!! html_entity_decode(Lang::get('shop.6')) !!}</h4>
									<?php use \App\Http\Controllers\CatproductController; ?>

									<?php echo CatproductController::display(); ?>
							</aside>
							
							<aside class="widget widget_file_formate">
								<h4 class="widget-title">{!! html_entity_decode(Lang::get('shop.7')) !!}</h4>
								<?php use \App\Http\Controllers\BrandController; ?>

								<?php echo BrandController::display(); ?>
								
							</aside>
							<aside class="widget widget_advertisement">
								<h4 class="widget-title">{!! html_entity_decode(Lang::get('shop.8')) !!}</h4>
								<img src="{!! asset('frontend') !!}/punbidtheme/images/banner/banner27.jpg" alt="" class="img-responsive">
								<img src="{!! asset('frontend') !!}/punbidtheme/images/banner/banner21.svg" alt="" class="img-responsive">
							</aside>
						</div>
				</div>
				<!--<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 col-lg-push-3 col-md-push-4 col-sm-push-4 col-xs-push-0">-->
                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                	<div class="ajaxLoading"></div>
                	<div class="{!!$modulepage!!}">
					<?php use \App\Http\Controllers\ProductsController; ?>

								<?php echo ProductsController::getdisplay(); ?>

					</div>
					
				</div>
			</div>
		</div>
	</div>