<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class withdraws extends Sximo  {
	
	protected $table = 'withdraws';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT withdraws.* FROM withdraws  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE withdraws.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
