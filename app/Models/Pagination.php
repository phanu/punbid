<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class pagination extends Sximo  {
	
	protected $table = 'brand';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT brand.* FROM brand  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE brand.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
