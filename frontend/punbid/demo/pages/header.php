<div class="top-bar">
	<div class="container">
		<nav>
			<ul id="menu-top-bar-left" class="nav nav-inline pull-left animate-dropdown flip">
				<li class="menu-item animate-dropdown"><a title="Welcome to Worldwide Electronics Store" href="#">Welcome to GoGoldRun.com</a></li>
			</ul>
		</nav>
		<nav>
			<ul id="menu-top-bar-right" class="nav nav-inline pull-right animate-dropdown flip">
				<li class="menu-item animate-dropdown"><a title="Store Locator" href="#"><i class="ec ec-map-pointer"></i>GoGoldRun คืออะไร?</a></li>
				<li class="menu-item animate-dropdown"><a title="Track Your Order" href="track-your-order.html"><i class="ec ec-transport"></i>ผู้ชนะประมูล</a></li>
				<li class="menu-item animate-dropdown"><a title="Shop" href="shop.html"><i class="ec ec-shopping-bag"></i>BID PACKคืออะไร?</a></li>
				<li class="menu-item animate-dropdown"><a title="My Account" href="my-account.html"><i class="ec ec-user"></i>บัญชีของฉัน</a></li>
			</ul>
		</nav>
	</div>
</div>
<!-- /.top-bar -->

<header id="masthead" class="site-header header-v1">
	<div class="container">
		<div class="row"> 
			
			<!-- ============================================================= Header Logo ============================================================= -->
			<div class="header-logo"> <a href="index.php?page=home" class="header-logo-link"> <img alt="logo" src="assets/images/logo.svg" width="175.748px" height="42.52px"/> </div>
			<!-- ============================================================= Header Logo : End============================================================= -->
			
			<form class="navbar-search" method="get" action="/">
				<label class="sr-only screen-reader-text" for="search">Search for:</label>
				<div class="input-group">
					<input type="text" id="search" class="form-control search-field" dir="ltr" value="" name="s" placeholder="Search for products" />
					<div class="input-group-addon search-categories">
						<select name='product_cat' id='product_cat' class='postform resizeselect' >
							<option value='0' selected='selected'>สินค้าประมูลทั้งหมด</option>
							<option class="level-0" value="laptops-laptops-computers">แหวนทอง</option>
							<option class="level-0" value="ultrabooks-laptops-computers">สร้อยทอง</option>
							<option class="level-0" value="mac-computers-laptops">ข้อมือ</option>
							<option class="level-0" value="all-in-one-laptops-computers">เด็ก</option>
							<option class="level-0" value="servers">แหวนทอง90%</option>
							<option class="level-0" value="peripherals">ทองแท่ง</option>
							<option class="level-0" value="gaming-laptops-computers">แหวนทองหัวพลอ</option>
							<option class="level-0" value="accessories-laptops-computers">พระแผง</option>
						</select>
					</div>
					<div class="input-group-btn">
						<input type="hidden" id="search-param" name="post_type" value="product" />
						<button type="submit" class="btn btn-secondary"><i class="ec ec-search"></i></button>
					</div>
				</div>
			</form>
			<ul class="navbar-mini-cart navbar-nav animate-dropdown nav pull-right flip">
				<li class="nav-item dropdown"> <a href="cart.html" class="nav-link" data-toggle="dropdown"> <i class="fa fa-circle-o" aria-hidden="true"></i> <span class="cart-items-count count">4</span> <span class="cart-items-total-price total-price"><span class="amount">589 BID</span></span> </a>
					<ul class="dropdown-menu dropdown-menu-mini-cart">
						<li>
							<div class="widget_shopping_cart_content">
								<ul class="cart_list product_list_widget ">
									<li class="mini_cart_item"> <a title="Remove this item" class="remove" href="#">×</a> <a href="single-product.html"> <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="assets/images/products/mini-cart1.jpg" alt="">White lumia 9001&nbsp; </a> <span class="quantity">2 × <span class="amount">£150.00</span></span> </li>
									<li class="mini_cart_item"> <a title="Remove this item" class="remove" href="#">×</a> <a href="single-product.html"> <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="assets/images/products/mini-cart2.jpg" alt="">PlayStation 4&nbsp; </a> <span class="quantity">1 × <span class="amount">£399.99</span></span> </li>
									<li class="mini_cart_item"> <a data-product_sku="" data-product_id="34" title="Remove this item" class="remove" href="#">×</a> <a href="single-product.html"> <img class="attachment-shop_thumbnail size-shop_thumbnail wp-post-image" src="assets/images/products/mini-cart3.jpg" alt="">POV Action Cam HDR-AS100V&nbsp; </a> <span class="quantity">1 × <span class="amount">£269.99</span></span> </li>
								</ul>
								<!-- end product list -->
								
								<p class="total"><strong>Subtotal:</strong> <span class="amount">£969.98</span></p>
								<p class="buttons"> <a class="button wc-forward" href="cart.html">View Cart</a> <a class="button checkout wc-forward" href="checkout.html">Checkout</a> </p>
							</div>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		<!-- /.row -->
		
		<div class="row">
			<div class="col-xs-12 col-lg-3">
				<nav>
					<ul class="list-group vertical-menu yamm make-absolute">
						<li class="list-group-item"><span><i class="fa fa-list-ul"></i> สินค้าประมูลทั้งหมด </span></li>
						<li class="highlight menu-item animate-dropdown"><a title="Value of the Day" href="home-v2.html">ทองคำแท่ง</a></li>
						<li class="highlight menu-item animate-dropdown"><a title="Top 100 Offers" href="home-v3.html">สร้อยทอง</a></li>
						<li class="highlight menu-item animate-dropdown"><a title="New Arrivals" href="home-v3-full-color-background.html">กำไรทอง</a></li>
						<li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown"> <a title="Computers &amp; Accessories" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">แหวนทอง</a>
							<ul role="menu" class=" dropdown-menu">
								<li class="menu-item animate-dropdown menu-item-object-static_block">
									<div class="yamm-content">
										<div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
											<div class="col-sm-12">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_single_image wpb_content_element vc_align_left">
															<figure class="wpb_wrapper vc_figure">
																<div class="vc_single_image-wrapper vc_box_border_grey"> <img src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt=""> </div>
															</figure>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<ul>
																	<li class="nav-title">แหวนทองคำ 96.5% </li>
																	<li><a href="#">แหวนทองคำ 96.5% ทั้งหมด</a></li>
																	<li><a href="#">แหวน 1 กรัม</a></li>
																	<li><a href="#">แหวน 1/2 สตางค์</a></li>
																	<li><a href="#">แหวน 1 สตางค์ </a></li>
																	<li><a href="#">แหวน 1 บาท</a></li>
																	<li class="nav-divider"></li>
																	<li><a href="#"><span class="nav-text">แหวนทองทั้งหมด</span><span class="nav-subtext">ที่ไกล้เปิดประมูล</span></a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<ul>
																	<li class="nav-title">แหวนหัวพลอย</li>
																	<li><a href="#">แหวนหัวพลอยทั้งหมด</a></li>
																	<li><a href="#">แหวน 1 กรัม</a></li>
																	<li><a href="#">แหวน 1/2 สตางค์</a></li>
																	<li><a href="#">แหวน 1 สตางค์ </a></li>
																	<li><a href="#">แหวน 1 บาท</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</li>
						<li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown"> <a title="Computers &amp; Accessories" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">แหวนทอง</a>
							<ul role="menu" class=" dropdown-menu">
								<li class="menu-item animate-dropdown menu-item-object-static_block">
									<div class="yamm-content">
										<div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
											<div class="col-sm-12">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_single_image wpb_content_element vc_align_left">
															<figure class="wpb_wrapper vc_figure">
																<div class="vc_single_image-wrapper vc_box_border_grey"> <img src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt=""> </div>
															</figure>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<ul>
																	<li class="nav-title">แหวนทองคำ 96.5% </li>
																	<li><a href="#">แหวนทองคำ 96.5% ทั้งหมด</a></li>
																	<li><a href="#">แหวน 1 กรัม</a></li>
																	<li><a href="#">แหวน 1/2 สตางค์</a></li>
																	<li><a href="#">แหวน 1 สตางค์ </a></li>
																	<li><a href="#">แหวน 1 บาท</a></li>
																	<li class="nav-divider"></li>
																	<li><a href="#"><span class="nav-text">แหวนทองทั้งหมด</span><span class="nav-subtext">ที่ไกล้เปิดประมูล</span></a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<ul>
																	<li class="nav-title">แหวนหัวพลอย</li>
																	<li><a href="#">แหวนหัวพลอยทั้งหมด</a></li>
																	<li><a href="#">แหวน 1 กรัม</a></li>
																	<li><a href="#">แหวน 1/2 สตางค์</a></li>
																	<li><a href="#">แหวน 1 สตางค์ </a></li>
																	<li><a href="#">แหวน 1 บาท</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</li>
						<li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown"> <a title="Computers &amp; Accessories" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">แหวนทอง</a>
							<ul role="menu" class=" dropdown-menu">
								<li class="menu-item animate-dropdown menu-item-object-static_block">
									<div class="yamm-content">
										<div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
											<div class="col-sm-12">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_single_image wpb_content_element vc_align_left">
															<figure class="wpb_wrapper vc_figure">
																<div class="vc_single_image-wrapper vc_box_border_grey"> <img src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt=""> </div>
															</figure>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<ul>
																	<li class="nav-title">แหวนทองคำ 96.5% </li>
																	<li><a href="#">แหวนทองคำ 96.5% ทั้งหมด</a></li>
																	<li><a href="#">แหวน 1 กรัม</a></li>
																	<li><a href="#">แหวน 1/2 สตางค์</a></li>
																	<li><a href="#">แหวน 1 สตางค์ </a></li>
																	<li><a href="#">แหวน 1 บาท</a></li>
																	<li class="nav-divider"></li>
																	<li><a href="#"><span class="nav-text">แหวนทองทั้งหมด</span><span class="nav-subtext">ที่ไกล้เปิดประมูล</span></a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<ul>
																	<li class="nav-title">แหวนหัวพลอย</li>
																	<li><a href="#">แหวนหัวพลอยทั้งหมด</a></li>
																	<li><a href="#">แหวน 1 กรัม</a></li>
																	<li><a href="#">แหวน 1/2 สตางค์</a></li>
																	<li><a href="#">แหวน 1 สตางค์ </a></li>
																	<li><a href="#">แหวน 1 บาท</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</li>
						<li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown"> <a title="Computers &amp; Accessories" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">แหวนทอง</a>
							<ul role="menu" class=" dropdown-menu">
								<li class="menu-item animate-dropdown menu-item-object-static_block">
									<div class="yamm-content">
										<div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
											<div class="col-sm-12">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_single_image wpb_content_element vc_align_left">
															<figure class="wpb_wrapper vc_figure">
																<div class="vc_single_image-wrapper vc_box_border_grey"> <img src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt=""> </div>
															</figure>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<ul>
																	<li class="nav-title">แหวนทองคำ 96.5% </li>
																	<li><a href="#">แหวนทองคำ 96.5% ทั้งหมด</a></li>
																	<li><a href="#">แหวน 1 กรัม</a></li>
																	<li><a href="#">แหวน 1/2 สตางค์</a></li>
																	<li><a href="#">แหวน 1 สตางค์ </a></li>
																	<li><a href="#">แหวน 1 บาท</a></li>
																	<li class="nav-divider"></li>
																	<li><a href="#"><span class="nav-text">แหวนทองทั้งหมด</span><span class="nav-subtext">ที่ไกล้เปิดประมูล</span></a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<ul>
																	<li class="nav-title">แหวนหัวพลอย</li>
																	<li><a href="#">แหวนหัวพลอยทั้งหมด</a></li>
																	<li><a href="#">แหวน 1 กรัม</a></li>
																	<li><a href="#">แหวน 1/2 สตางค์</a></li>
																	<li><a href="#">แหวน 1 สตางค์ </a></li>
																	<li><a href="#">แหวน 1 บาท</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</li>
						<li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown"> <a title="Computers &amp; Accessories" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">แหวนทอง</a>
							<ul role="menu" class=" dropdown-menu">
								<li class="menu-item animate-dropdown menu-item-object-static_block">
									<div class="yamm-content">
										<div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
											<div class="col-sm-12">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_single_image wpb_content_element vc_align_left">
															<figure class="wpb_wrapper vc_figure">
																<div class="vc_single_image-wrapper vc_box_border_grey"> <img src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt=""> </div>
															</figure>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<ul>
																	<li class="nav-title">แหวนทองคำ 96.5% </li>
																	<li><a href="#">แหวนทองคำ 96.5% ทั้งหมด</a></li>
																	<li><a href="#">แหวน 1 กรัม</a></li>
																	<li><a href="#">แหวน 1/2 สตางค์</a></li>
																	<li><a href="#">แหวน 1 สตางค์ </a></li>
																	<li><a href="#">แหวน 1 บาท</a></li>
																	<li class="nav-divider"></li>
																	<li><a href="#"><span class="nav-text">แหวนทองทั้งหมด</span><span class="nav-subtext">ที่ไกล้เปิดประมูล</span></a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<ul>
																	<li class="nav-title">แหวนหัวพลอย</li>
																	<li><a href="#">แหวนหัวพลอยทั้งหมด</a></li>
																	<li><a href="#">แหวน 1 กรัม</a></li>
																	<li><a href="#">แหวน 1/2 สตางค์</a></li>
																	<li><a href="#">แหวน 1 สตางค์ </a></li>
																	<li><a href="#">แหวน 1 บาท</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</li>
						<li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown"> <a title="Computers &amp; Accessories" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">แหวนทอง</a>
							<ul role="menu" class=" dropdown-menu">
								<li class="menu-item animate-dropdown menu-item-object-static_block">
									<div class="yamm-content">
										<div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
											<div class="col-sm-12">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_single_image wpb_content_element vc_align_left">
															<figure class="wpb_wrapper vc_figure">
																<div class="vc_single_image-wrapper vc_box_border_grey"> <img src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt=""> </div>
															</figure>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<ul>
																	<li class="nav-title">แหวนทองคำ 96.5% </li>
																	<li><a href="#">แหวนทองคำ 96.5% ทั้งหมด</a></li>
																	<li><a href="#">แหวน 1 กรัม</a></li>
																	<li><a href="#">แหวน 1/2 สตางค์</a></li>
																	<li><a href="#">แหวน 1 สตางค์ </a></li>
																	<li><a href="#">แหวน 1 บาท</a></li>
																	<li class="nav-divider"></li>
																	<li><a href="#"><span class="nav-text">แหวนทองทั้งหมด</span><span class="nav-subtext">ที่ไกล้เปิดประมูล</span></a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<ul>
																	<li class="nav-title">แหวนหัวพลอย</li>
																	<li><a href="#">แหวนหัวพลอยทั้งหมด</a></li>
																	<li><a href="#">แหวน 1 กรัม</a></li>
																	<li><a href="#">แหวน 1/2 สตางค์</a></li>
																	<li><a href="#">แหวน 1 สตางค์ </a></li>
																	<li><a href="#">แหวน 1 บาท</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</li>
						<li class="yamm-tfw menu-item menu-item-has-children animate-dropdown dropdown"> <a title="Computers &amp; Accessories" data-hover="dropdown" href="product-category.html" data-toggle="dropdown" class="dropdown-toggle" aria-haspopup="true">แหวนทอง</a>
							<ul role="menu" class=" dropdown-menu">
								<li class="menu-item animate-dropdown menu-item-object-static_block">
									<div class="yamm-content">
										<div class="row bg-yamm-content bg-yamm-content-bottom bg-yamm-content-right">
											<div class="col-sm-12">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_single_image wpb_content_element vc_align_left">
															<figure class="wpb_wrapper vc_figure">
																<div class="vc_single_image-wrapper vc_box_border_grey"> <img src="assets/images/megamenu-2.png" class="vc_single_image-img attachment-full" alt=""> </div>
															</figure>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<ul>
																	<li class="nav-title">แหวนทองคำ 96.5% </li>
																	<li><a href="#">แหวนทองคำ 96.5% ทั้งหมด</a></li>
																	<li><a href="#">แหวน 1 กรัม</a></li>
																	<li><a href="#">แหวน 1/2 สตางค์</a></li>
																	<li><a href="#">แหวน 1 สตางค์ </a></li>
																	<li><a href="#">แหวน 1 บาท</a></li>
																	<li class="nav-divider"></li>
																	<li><a href="#"><span class="nav-text">แหวนทองทั้งหมด</span><span class="nav-subtext">ที่ไกล้เปิดประมูล</span></a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="vc_column-inner ">
													<div class="wpb_wrapper">
														<div class="wpb_text_column wpb_content_element ">
															<div class="wpb_wrapper">
																<ul>
																	<li class="nav-title">แหวนหัวพลอย</li>
																	<li><a href="#">แหวนหัวพลอยทั้งหมด</a></li>
																	<li><a href="#">แหวน 1 กรัม</a></li>
																	<li><a href="#">แหวน 1/2 สตางค์</a></li>
																	<li><a href="#">แหวน 1 สตางค์ </a></li>
																	<li><a href="#">แหวน 1 บาท</a></li>
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</nav>
			</div>
			<div class="col-xs-12 col-lg-9">
				<nav>
					<ul id="menu-secondary-nav" class="secondary-nav">
						<li class="highlight menu-item"><a href="home-v2.html">ประมูล</a></li>
						<li class="menu-item"><a href="home-v3.html">สินค้าพวกนี้กำลังจะสิ้นสุดลง</a></li>
						<li class="menu-item"><a href="home-v3-full-color-background.html">สินค้าไกล้ประมูล</a></li>
						<li class="menu-item"><a href="blog-v1.html">ซื้อสิทธิประมูล</a></li>
						<li class="pull-right menu-item"><a href="blog-v2.html">ช่วยเหลือ</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</header>