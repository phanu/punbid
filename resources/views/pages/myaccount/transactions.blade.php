
                                    <ul class="nav nav-pills pull-right myheadbidbg">
                                        <li class="active">
                                            <a href="#mytransbid" data-toggle="tab" aria-expanded="true">{!! html_entity_decode(Lang::get('myacc-trans.1')) !!}</a>
                                        </li>
                                        <li class="">
                                            <a href="#mytransbidwin" data-toggle="tab" aria-expanded="false">{!! html_entity_decode(Lang::get('myacc-trans.2')) !!}</a>
                                        </li>
                                        <li class="">
                                            <a href="#mytransbidlose" data-toggle="tab" aria-expanded="false">{!! html_entity_decode(Lang::get('myacc-trans.3')) !!}</a>
                                        </li>
                                    </ul>
                                    <h4 class="header-title m-b-30">{!! html_entity_decode(Lang::get('myacc-trans.4')) !!}</h4>

                                    <div class="tab-content">
                                        <div id="mytransbid" class="tab-pane fade active in">
                                            <div class="row">
                                                <div class="col-md-12">
                                                
                                                <?php use \App\Http\Controllers\BidtransController; 
                                                        $data = BidtransController::displaymyaccount(); ?>

                                                   <table id="datatable" class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                        @foreach ($data['tableGrid'] as $t)
                                                            @if($t['view'] =='1')               
                                                                <?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
                                                                @if(SiteHelpers::filterColumn($limited ))
                                                                
                                                                    <th><?php echo \SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())); ?></th>          
                                                                @endif 
                                                            @endif
                                                        @endforeach
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @foreach ($data['rowData'] as $row)
                                                            <tr>                                    
                                                             @foreach ($data['tableGrid'] as $field)
                                                                 @if($field['view'] =='1')
                                                                    <?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
                                                                    @if(SiteHelpers::filterColumn($limited ))
                                                                     <td>                    
                                                                        <p>{!! SiteHelpers::formatRows($row->{$field['field']},$field,$row) !!}</p>                      
                                                                     </td>
                                                                    @endif  
                                                                 @endif                  
                                                             @endforeach              
                                                            </tr>
                                                        @endforeach
                                                    </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="mytransbidwin" class="tab-pane fade in">
                                            <div class="row">
                                                <div class="col-md-12">
                                                
                                                   <?php use \App\Http\Controllers\BidremindController; 
                                                    $data = BidremindController::display("win"); ?>

                                                   <table id="datatable" class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                        @foreach ($data['tableGrid'] as $t)
                                                            @if($t['view'] =='1')               
                                                                <?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
                                                                @if(SiteHelpers::filterColumn($limited ))
                                                                
                                                                    <th><?php echo \SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())); ?></th>          
                                                                @endif 
                                                            @endif
                                                        @endforeach
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @foreach ($data['rowData'] as $row)
                                                            <tr>                                    
                                                             @foreach ($data['tableGrid'] as $field)
                                                                 @if($field['view'] =='1')
                                                                    <?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
                                                                    @if(SiteHelpers::filterColumn($limited ))
                                                                     <td>
                                                                        @if($field['field'] =='get')
                                                                            @if($row->get<3)
                                                                            
                                                                                {!! html_entity_decode(Lang::get('myacc-trans.5')) !!} 
                                                                            
                                                                                @if($row->get==0)
                                                                                <a href="" id="getitdone" data-id="{!!$row->products_bid_id!!}">{!! html_entity_decode(Lang::get('myacc-trans.6')) !!}</a>
                                                                                @endif
                                                                            @else
                                                                            
                                                                                {!! html_entity_decode(Lang::get('myacc-trans.7')) !!} 
                                                                                @if($row->get==3)
                                                                                <a href="{!! url('winnercheckout?pwin='.$row->products_bid_id) !!}">{!! html_entity_decode(Lang::get('myacc-trans.8')) !!}</a>
                                                                                @endif
                                                                            @endif
                                                                        @else            
                                                                        <p>{!! SiteHelpers::formatRows($row->{$field['field']},$field,$row) !!}</p> 
                                                                        @endif
                                                                     </td>
                                                                    @endif  
                                                                 @endif                  
                                                             @endforeach              
                                                            </tr>
                                                        @endforeach
                                                    </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="mytransbidlose" class="tab-pane fade in">
                                            <div class="row">
                                                <div class="col-md-12">
                                                
                                                <?php 
                                                    $data = BidremindController::display("lose"); ?>

                                                   <table id="datatable" class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                        @foreach ($data['tableGrid'] as $t)
                                                            @if($t['view'] =='1')               
                                                                <?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
                                                                @if(SiteHelpers::filterColumn($limited ))
                                                                
                                                                    <th><?php echo \SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())); ?></th>          
                                                                @endif 
                                                            @endif
                                                        @endforeach
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @foreach ($data['rowData'] as $row)
                                                            <tr>                                    
                                                             @foreach ($data['tableGrid'] as $field)
                                                                 @if($field['view'] =='1')
                                                                    <?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
                                                                    @if(SiteHelpers::filterColumn($limited ))
                                                                     <td>
                                                                        @if($field['field'] =='get')
                                                                            @if($row->get<3)
                                                                            
                                                                                {!! html_entity_decode(Lang::get('myacc-trans.5')) !!} 
                                                                            
                                                                                @if($row->get==0)
                                                                                <a href="" id="getitdone" data-id="{!!$row->products_bid_id!!}">{!! html_entity_decode(Lang::get('myacc-trans.6')) !!}</a>
                                                                                @endif
                                                                            @else
                                                                            
                                                                                {!! html_entity_decode(Lang::get('myacc-trans.7')) !!} 
                                                                                @if($row->get==3)
                                                                                <a href="{!! url('winnercheckout?pwin='.$row->products_bid_id) !!}">{!! html_entity_decode(Lang::get('myacc-trans.8')) !!}</a>
                                                                                @endif
                                                                            @endif
                                                                        @else            
                                                                        <p>{!! SiteHelpers::formatRows($row->{$field['field']},$field,$row) !!}</p> 
                                                                        @endif
                                                                     </td>
                                                                    @endif  
                                                                 @endif                  
                                                             @endforeach              
                                                            </tr>
                                                        @endforeach
                                                    </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        
