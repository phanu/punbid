<ul class="nav nav-pills pull-right">
                                        <li class="active">
                                            <a href="#myinformpayment" data-toggle="tab" aria-expanded="true">{!! html_entity_decode(Lang::get('myacc-informpay.1')) !!}</a>
                                        </li>
                                        <li class="">
                                            <a href="#myinformpayment-hist" data-toggle="tab" aria-expanded="false">{!! html_entity_decode(Lang::get('myacc-informpay.2')) !!}</a>
                                        </li>
                                    </ul>
                                    <h4 class="header-title m-b-30">{!! html_entity_decode(Lang::get('myacc-informpay.3')) !!}</h4>
						{!! Form::open(array('url'=>'payment/savepublic/', 'class'=>'form-horizontal ' ,'files' => true)) !!}
											<div class="tab-content">
                                        		<div id="myinformpayment" class="tab-pane active fade in">
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group rs_bottompadder10">
																<label>{!! html_entity_decode(Lang::get('myacc-informpay.4')) !!}</label>
																<select class="form-control" name="orderNumber" required="required">
																	<option value="">{!! html_entity_decode(Lang::get('myacc-informpay.5')) !!}</option>
																	<?php use \App\Http\Controllers\PaymentController; 
																	$orderdata = PaymentController::userorderonhold(); 
																	?>
																	@foreach ($orderdata as $value)
															<option value="{!!$value->orderNumber!!}">{!! html_entity_decode(Lang::get('myacc-informpay.6')) !!} {!!$value->orderNumber!!} {!! html_entity_decode(Lang::get('myacc-informpay.7')) !!} {!!number_format($value->payment_fee,2) !!}THB
															</option>
																	@endforeach
																</select>
															</div>
														</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group rs_bottompadder20">
																<label>{!! html_entity_decode(Lang::get('myacc-informpay.8')) !!}</label>
																<input type="number" class="form-control" name="amount" placeholder="0.00 THB" required>
															</div>
														</div>
													</div>
													<div class="row">
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															
                                                    <div class="card-box">

                                                     <h5 class="header-title m-t-0 m-b-30">{!! html_entity_decode(Lang::get('myacc-informpay.9')) !!}</h5>

                                                     <input type="file" class="dropify" name="avatar" data-default-file="" required/>

                                                    </div>
                                                	</div>
														<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
															<div class="form-group rs_bottompadder20">
																<label>{!! html_entity_decode(Lang::get('myacc-informpay.10')) !!}</label>
                                                                <div class=" datepicker"> 
                                                                    <input type="text" name="paymentDate" class="form-control" placeholder="dd/mm/yyyy" id="datepickerpay" value="" required>
                                                                </div>
															</div>
														</div>
													</div>
														
													<div class="rs_btn_div rs_toppadder30">
														<input type="hidden" name="customerNumber" value="{!!$userdet->id!!}">
														<input type="hidden" name="status" value="verify">
														<button type="submit" class="rs_button rs_button_orange">{!! html_entity_decode(Lang::get('myacc-informpay.11')) !!}</button>
													</div>
												</div>
												<div id="myinformpayment-hist" class="tab-pane fade in">
													<div class="row">
		                                                <div class="col-md-12">
		                                                
		                                                   <?php 
		                                                    $data = PaymentController::display(); 
		                                                   
		                                                    
		                                                    ?>

		                                                   <table id="datatable" class="table table-striped">
		                                                    <thead>
		                                                        <tr>
		                                                        @foreach ($data['tableGrid'] as $t)
		                                                            @if($t['view'] =='1')               
		                                               <?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
		                                                                @if(SiteHelpers::filterColumn($limited ))
		                                                                
		                                                                    <th><?php echo \SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())); ?></th>          
		                                                                @endif 
		                                                            @endif
		                                                        @endforeach
		                                                        </tr>
		                                                    </thead>

		                                                    <tbody>
		                                                        @foreach ($data['rowData'] as $row)
		                                                            <tr>                                    
		                                                             @foreach ($data['tableGrid'] as $field)
		                                                                 @if($field['view'] =='1')
		                                                                    <?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
		                                                                    @if(SiteHelpers::filterColumn($limited ))
		                                                                     <td>          
		                                                                        <p>{!! SiteHelpers::formatRows($row->{$field['field']},$field,$row) !!}</p> 
		                                                                     </td>
		                                                                    @endif  
		                                                                 @endif                  
		                                                             @endforeach              
		                                                            </tr>
		                                                        @endforeach
		                                                    </tbody>

		                                                    </table>
		                                                </div>
		                                            </div>
												</div>
											</div>
					{!! Form::close() !!}

