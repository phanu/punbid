<div class="rs_purchase_table">    
    <table class="table">
        <thead>
			<tr>
				<th> No </th>
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')				
						<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
							
							<th>{{ $t['label'] }}</th>	

						@endif 
					@endif
				@endforeach
				
			  </tr>
        </thead>

        <tbody>        						
            @foreach ($rowData as $row)
                <tr>
				<td> {{ ++$i }}</td>									
				 @foreach ($tableGrid as $field)
				 	 
					 @if($field['view'] =='1')
					 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
					 	@if(SiteHelpers::filterColumn($limited ))
						 <td>
						 	@if($field['field'] =='get')
						 		@if($row->get<3)
						 		
								 	แพ้ 
								
									@if($row->get==0)
								 	<a href="" id="getitdone" data-id="{{$row->products_bid_id}}">รับสิทธิ</a>
									@endif
								@else
								
								 	ชนะ 
								 	@if($row->get==3)
									<a href="{{ url('checkout?pwin='.$row->products_bid_id) }}">Checkout winner</a>
									@endif
								@endif
					 		@else			 
						 	<p>{!! SiteHelpers::formatRows($row->{$field['field']},$field,$row) !!}</p>	
						 	@endif
						 </td>
						 
					  @endif	
					@endif	

				 @endforeach
				 <td>
				  		 
						 
					<!--<a href="?view={{ $row->id }}" oncl> <i class="fa fa-search"></i></a>--> 				
					
				</td>				 
                </tr>
				
            @endforeach
              
        </tbody>        

    </table>  
<div class="text-center"> {!! $pagination->render() !!}</div>
</div>  
