<div class="rs_contact_map">
		 <iframe src="https://www.google.com/maps/d/u/1/embed?mid=1twZg82DdJAYJ2bcFpf10cbcEmS4" width="100%" height="480"></iframe>
</div>
	<div class="rs_graybg rs_toppadder100 rs_bottompadder100">
		<div class="container">
			<div class="row">
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="rs_contact_form">
						<div class="rs_submitform">
							
								<div class="form-group">
									<input type="text" class="form-control" id="ur_name" placeholder="Enter your name">
								</div>
							
								<div class="form-group">
									<input type="email" class="form-control" id="ur_mail" placeholder="Enter your mail address">
								</div>
							
								<div class="form-group">
									<input type="text" class="form-control" id="sub" placeholder="Enter your subject">
								</div>
							
								<textarea class="form-control" rows="10" id="msg" placeholder="Enter your message"></textarea>
							
								<div class="rs_btn_div rs_toppadder30">
									<a href="javascript:;" id="send_btn" class="rs_button rs_button_orange">send</a>
									<p id="err"></p>
								</div>
							
						</div>
					</div>
				</div>
				<div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 pull-right">
					<div class="rs_contact_section">
						<div class="rs_contact_heading rs_green_heading rs_bottompadder50">
							<h3>ติดต่อเรา</h3>
						</div>
						<div class="rs_contact_info">
							<div class="rs_contact_info_img">
								<img src="{!! asset('frontend') !!}/punbidtheme/images/cnt_1.png" alt="" class="img-responsive">
							</div>
							<div class="rs_contact_data">
								<span>
350/53 ถ.พระราม 3 ซ.71 แขวงช่องนนทรี เขตยานนาวา กรุงเทพฯ 10120</span>
							</div>
						</div>
						<div class="rs_contact_info">
							<div class="rs_contact_info_img">
								<img src="{!! asset('frontend') !!}/punbidtheme/images/cnt_2.png" alt="" class="img-responsive">
							</div>
							<div class="rs_contact_data">
								<p> 02-xxx-xxxx</p>
							</div>
						</div>
						<div class="rs_contact_info">
							<div class="rs_contact_info_img">
								<img src="{!! asset('frontend') !!}/punbidtheme/images/cnt_3.png" alt="" class="img-responsive">
							</div>
							<div class="rs_contact_data">
								<p><a href="#">support@punbidtheme.com</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>