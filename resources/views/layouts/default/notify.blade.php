<script type="text/javascript">
$(document).ready(function() {
@if(Auth::check())
$.fn.setuserreadnotify = function() { 
       $.ajax({
            url: '{!!url('notification') !!}/saveuserreadnotify',
            method: "POST",
            data: { },
            dataType: "json",
            success: function(data) {
                    $('.noti-dot').hide();
                },
        });
}

$.fn.saveusernotify = function(vtitle,vnoticlass,vicon) { 
       $.ajax({
            url: '{!!url('notification') !!}/saveusernotify',
            method: "POST",
            data: { title : vtitle, noticlass : vnoticlass, icon : vicon },
            dataType: "json",
            success: function(data) {
                            
                },
        });
}

$.fn.getusernotify = function() { 
       $.ajax({
            url: '{!!url('notification') !!}/load',
            method: "GET",
            data: { },
            dataType: "json",
            success: function(data) {
                         
                         var htmlnoti = '';
                         var act = '';

                         if(data.total > 0)
                         $.each(data, function () {
                           $.each(this, function () {

                                if(this.is_read > 0)
                                {
                                    act = '';
                                }else
                                {
                                    act = ' active';
                                }
                                htmlnoti += '<li class="list-group-item'+act+'"><a href="#" class="user-list-item"> <div class="'+ this.noticlass +'"> <i class="'+ this.icon +'"></i> </div> <div class="user-desc"> <span class="name">'+ this.title +'</span> <span class="timemenu">'+ this.date +'</span> </div> </a> </li>';
                           });
                        });

                     if(data.is_read > 0)
                     {
                        $('.noti-dot').show();
                     }
                     $('#notilists').html(htmlnoti);

                     $('#notiunread').html(data.is_read);


                },
        });


       
    }


$( document ).ajaxSuccess(function( event,request, settings ) {
    if(settings.type==="POST")
    {
        $('.notification-box').getusernotify();
    }

});

@if(isset($_REQUEST["cartaction"]))

    @if($_REQUEST["cartaction"]=="add")
    
$('.notification-box').saveusernotify('{!! html_entity_decode(Lang::get('script-text.addcart')) !!}','icon bg-success','zmdi zmdi-plus-1');
    
    @endif

@endif

$('.notification-box').getusernotify();

            $('.right-bar-toggle').on('click', function(e) {

                $('#rnotify').toggleClass('right-bar-enabled');
                if(e.currentTarget.id==="rshownoti")
                {
                    $('.notification-box').setuserreadnotify();
                }
            });

@endif


//scarch 
var engine = new Bloodhound({
                remote: {
                    url: 'find?q=%QUERY%',
                    wildcard: '%QUERY%'
                },
                datumTokenizer: Bloodhound.tokenizers.whitespace('q'),
                queryTokenizer: Bloodhound.tokenizers.whitespace
            });

            // right side-bar toggle

            $(".search-input").typeahead({
                hint: true,
                highlight: true,
                minLength: 1,
            }, {
                source: engine.ttAdapter(),

                // This will be appended to "tt-dataset-" to form the class name of the suggestion menu.
                name: 'productlists',

                // the key from the array we want to display (name,id,email,etc...)
                templates: {
                    empty: [
                        '<div class="list-group search-results-dropdown"><div class="list-group-item">{!! html_entity_decode(Lang::get('script-text.nodat')) !!}</div></div>'
                    ],
                    header: [
                        '<div class="list-group search-results-dropdown">'
                    ],
                    suggestion: function (data) {

                        var ptype = '';

                        if(data.type==='pbid')
                        {
                            ptype = '<span class="label label-success">{!! html_entity_decode(Lang::get('script-text.sshop')) !!} </span>';
                            linkp = 'bidding?view='+data.id;
                        }else
                        {
                            ptype = '<span class="label label-danger">{!! html_entity_decode(Lang::get('script-text.sbid')) !!}</span>';
                            linkp = 'product-detail?view='+data.id;
                        }

                        return '<a href="'+linkp+'" class="list-group-item"><img src="uploads/products/' + data.featured_img + '"><div class="pp_des"><span class="pp_search_name">' + data.name_th + '</span><span class="pp_search_title">' + data.title_th + '</span><span class="pp_search_cat"><span class="label label-primary">{!! html_entity_decode(Lang::get('script-text.category')) !!} '+ data.category_th +' </span>'+ ptype +'</span></div></a>';
                    }
                }
            });
});
</script>