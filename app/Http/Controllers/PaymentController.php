<?php 
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect; 
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
class PaymentController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'payment';
	static $per_page	= '10';
	
	public function __construct() 
	{
		parent::__construct();
		$this->model = new Payment();
		
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'			=> 	$this->info['title'],
			'pageNote'			=>  $this->info['note'],
			'pageModule'		=> 'payment',
			'pageUrl'			=>  url('payment'),
			'return' 			=> 	self::returnUrl()	
		);		
				
	} 
	
	public function getIndex()
	{
		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
				
		$this->data['access']		= $this->access;	
		return view('payment.index',$this->data);
	}	

	public function postData( Request $request)
	{ 
		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : $this->info['setting']['orderby']); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : $this->info['setting']['ordertype']);
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = '';	
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		} 

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : $this->info['setting']['perpage'] ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('payment/data');
		
		$this->data['param']		= $params;
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		$this->data['setting'] 		= $this->info['setting'];
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('payment.table',$this->data);

	}

			
	function getUpdate(Request $request, $id = null)
	{

		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] 		=  $row;
		} else {
			$this->data['row'] 		= $this->model->getColumnTable('payments'); 
		}
		$this->data['setting'] 		= $this->info['setting'];
		$this->data['fields'] 		=  \AjaxHelpers::fieldLang($this->info['config']['forms']);
		
		$this->data['id'] = $id;

		return view('payment.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
			
			$this->data['id'] = $id;
			$this->data['access']		= $this->access;
			$this->data['setting'] 		= $this->info['setting'];
			$this->data['fields'] 		= \AjaxHelpers::fieldLang($this->info['config']['grid']);
			$this->data['subgrid']		= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
			return view('payment.view',$this->data);

		} else {

			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_error')
			));		
		}		
	}	


	function postCopy( Request $request)
	{
		
	    foreach(\DB::select("SHOW COLUMNS FROM payments ") as $column)
        {
			if( $column->Field != 'paymentId')
				$columns[] = $column->Field;
        }
		if(count($request->input('ids')) >=1)
		{

			$toCopy = implode(",",$request->input('ids'));
			
					
			$sql = "INSERT INTO payments (".implode(",", $columns).") ";
			$sql .= " SELECT ".implode(",", $columns)." FROM payments WHERE paymentId IN (".$toCopy.")";
			\DB::select($sql);
			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success')
			));		

		} else {
			return response()->json(array(
				'status'=>'success',
				'message'=> 'Please select row to copy'
			));	
		}

	
	}		

	function postSave( Request $request, $id =0)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {

			$data = $this->validatePost('payments');
			
			$id = $this->model->insertRow($data , $request->input('paymentId'));

			if($request->input('status')=='paymented')
			{
				if($request->input('payment-for')=='bid'){
					\DB::table('bid_orders')->where('orderNumber', '=', $request->input('orderNumber'))->update(array('status' => 'success'));

					$bidsget = \DB::table('bid_orders')->where('orderNumber', '=', $request->input('orderNumber'))->value('bids');



					\DB::table('tb_users')->whereId($request->input('customerNumber'))->increment('bid', $bidsget);	
					\DB::table('tb_notification')->insert([
											'userid' => $request->input('customerNumber'), 'title' => \Lang::get('core.youhadgot').$bidsget.' Bid', 'noticlass' => 'icon bg-success', 'icon' => 'zmdi zmdi-plus-circle'
											]);
				}else{

					\DB::table('orders')->where('orderNumber', '=', $request->input('orderNumber'))->update(array('status' => 'process'));

					$orderdata = \DB::table('bid_orders')->where('orderNumber', '=', $request->input('orderNumber'))->first();
					$orderdata['userdet'] = User::find($request->input('customerNumber'));
					$to = $orderdata['userdet']->email;
						$subject = \Lang::get('core.orderl').$request->input('orderNumber').\Lang::get('core.orderalreadypay');
						$message = view('user.emails.thankyou', $orderdata)->render();
							$headers  = 'MIME-Version: 1.0' . "\r\n";
							$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
							$headers .= 'From: '.CNF_APPNAME.' <'.CNF_EMAIL.'>' . "\r\n";
								mail($to, $subject, $message, $headers);

					\DB::table('tb_notification')->insert([
											'userid' => $request->input('customerNumber'), 'title' => \Lang::get('core.orderidadd').$request->input('orderNumber')	.\Lang::get('core.orderalreadypay'), 'noticlass' => 'icon bg-success', 'icon' => 'zmdi zmdi-plus-circle'
											]);
				}
			}

			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success')
				));	
			
		} else {

			$message = $this->validateListError(  $validator->getMessageBag()->toArray() );
			return response()->json(array(
				'message'	=> $message,
				'status'	=> 'error'
			));	
		}	
	
	}	

	public function postDelete( Request $request)
	{

		if($this->access['is_remove'] ==0) {   
			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_restric')
			));
			die;

		}		
		// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));
			
			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success_delete')
			));
		} else {
			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_error')
			));

		} 		

	}

	public static function display( )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Payment();
		$info = $model::makeInfo('payment');

		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']
			
		);

		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('payment.public.view',$data);
			} 

		} else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=> 10000,
				'sort'		=> 'paymentId' ,
				'order'		=> 'desc',
				'params'	=> ' and customerNumber = '.\Session::get('uid'),
				'global'	=> 1 
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];	

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit']; 
			$data['pagination'] = $pagination;
			return $data;		

		}


	}

	public static function userorderonhold( )
	{
		
		$yourid = \Session::get('uid');

		$orderdata = \DB::table('orders')->where('customerNumber', '=', $yourid)->where('status', '=', 'onhold')->where('payment_type', '=', 'dbt')->get();

		return $orderdata;
	}

	public static function userbidorderonhold( )
	{
		
		$yourid = \Session::get('uid');

		$orderdata = \DB::table('bid_orders')->where('customerNumber', '=', $yourid)->where('status', '=', 'onhold')->where('payment_type', '=', 'dbt')->get();

		return $orderdata;
	}

	/*function postSavepublic( Request $request)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('payments');		
			 $this->model->insertRow($data , $request->input('paymentId'));
			return  Redirect::back()->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success');
		} else {

			return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
			->withErrors($validator)->withInput();

		}	
	
	}*/

	//public static  function postSavepublic( $id = null, $bid = null)
	//{		
		//$rules = $this->validateForm();
		//$validator = Validator::make($request->all(), $rules);	
		//if ($validator->passes()) {
			//$data = $this->validatePost('tb_users');		
			//$model = Userscustom();
	//			$data =  $this->model->insertRow($arrayName = array('bid' => $bid ), $id);
	//		return  $data;
		/*} else {

			return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
			->withErrors($validator)->withInput();

		}	*/
	//}		

	public function postSavepublic( Request $request, $id =0)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {

			$data = $this->validatePost('payments');

			if(!is_null(Input::file('slip')))
			{
				$file = $request->file('slip'); 
				$destinationPath = './uploads/users/';
				$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension(); //if you need extension of the file
				$rand = rand(1000,1000);
				$newfilename2 = strtotime(date('Y-m-d H:i:s')).'-'.$rand.'x'.\Session::get('uid').'payment.'.$extension;
				$uploadSuccess = $request->file('slip')->move($destinationPath, $newfilename2);				 
				if( $uploadSuccess ) {
				    $data['slip'] = $newfilename2; 
				} 
				
			}	
			
			$data['paymentDate'] = Carbon::createFromFormat('d/m/Y', $request->input('paymentDate'));	

			$data['payment-for'] = substr($request->input('orderNumbersep'),0,3); 

			$data['orderNumber'] = substr($request->input('orderNumbersep'),3);

			$id = $this->model->insertRow($data , $request->input('paymentId'));


			if($data['payment-for']=="shp"){
			\DB::table('orders')->where('orderNumber', '=', $data['orderNumber'])->update(array('status' => $data['status']));
			}else
			{
			\DB::table('bid_orders')->where('orderNumber', '=', $data['orderNumber'])->update(array('status' => $data['status']));
			}
			
			return Redirect::back()->with('message', \SiteHelpers::alert('success','Inform payment already'));

		} else {
			return Redirect::back()->with('message','The following errors occurred')->with('message', \SiteHelpers::alert('error','The following errors occurred')
			)->withErrors($validator)->withInput();
		}	
	
	}	

	public function postSavewithdraw( Request $request)
	{
	
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {

			$data = $this->validatePost('withdraws');

			\DB::table('withdraws')->insert([
														'amount' => $request->amount,
														'bid' => $request->bid, 
														'accountname' => $request->accountname, 
														'accountnum' => $request->accountnum, 
														'bank' => $request->bank, 
														'customerNumber' => $request->customerNumber, 
														'status' =>  $request->status

											]);
				
			

			return Redirect::back()->with('message', \SiteHelpers::alert('success','Request withdraw already'));

		} else {
			return Redirect::back()->with('message','The following errors occurred')->with('message', \SiteHelpers::alert('error','The following errors occurred')
			)->withErrors($validator)->withInput();
		}		
	
	}



}