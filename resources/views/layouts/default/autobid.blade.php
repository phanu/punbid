<script type="text/javascript">
$(document).ready(function() {

$(document).on('click','.delcustom', function() {
  var id = $(this).data('id');
  $.ajax({
        url: "{!! url('bidtransrecords/autobidsave') !!}",
        method: "POST",
        data: { delid : id },
        dataType: "html",
        success: function(data) {

                //console.log(data);
                var data = jQuery.parseJSON(data);
                if(data.message==="success")
                {
                               var cvaluebid = parseFloat(data.current_bid);  
                                $( "#current_bid" ).html(commaSeparateNumber(cvaluebid));

                                swal({
                                              title: "ลบ autobid แล้ว!",
                                              text: "ระบบได้ทำการลบ autobid ในสินค้ารายการนี้",
                                              type: "success",
                                              showCancelButton: false,
                                              confirmButtonColor: "#DD6B55",
                                              closeOnConfirm: true,
                                              confirmButtonText: 'ตกลง',
                                              closeOnCancel: false,
                                              function (isConfirm) {
                                            }
                                  });

                                //console.log(data);
@if($pages=="pages.bidding")
       $("#removeautobt")
        .prev('div.row')
        .remove()
        .end();
@endif


                }else if(data.message==="fail")
                {
                                          swal({
                                          title: 'ไม่สามารถทำรายการได้',
                                          confirmButtonText: 'ตกลง',
                                          type: "error",
                                          text:'ไม่พบข้อมูลในระบบ'},
                                          function (isConfirm) {
                                            if (isConfirm) {

                                            }
                                          });
                }

            },
      });

});
//autobid
$('.autobid-button').on("click", function() {

      var bidadd = 0;

      if($(this).data('bidtype')==="custombid")
      {
          bidadd = $(this).siblings('input').val();
      }else
      {
          bidadd = $(this).data('add');
      }

      $.ajax({
        url: "{!! url('bidtransrecords/autobidsave') !!}",
        method: "POST",
        data: { products_bid_id : $(this).data('id'), addbid : bidadd, boots : $(this).data('boots') },
        dataType: "html",
        success: function(data) {

                //console.log(data);
                var data = jQuery.parseJSON(data);
                if(data.message==="success")
                {
                               var cvaluebid = parseFloat(data.current_bid);  
                                $( "#current_bid" ).html(commaSeparateNumber(cvaluebid));

                                swal({
                                              title: "เพิ่ม autobid แล้ว!",
                                              text: "ระบบได้ทำการเพิ่ม autobid ในสินค้ารายการนี้ \nจำนวน Autobid : "+data.autobidlist.bid_remain+"\n จำนวน Bid ต่อครั้ง : "+data.autobidlist.boots,
                                              type: "success",
                                              showCancelButton: false,
                                              confirmButtonColor: "#DD6B55",
                                              closeOnConfirm: true,
                                              confirmButtonText: 'ตกลง',
                                              closeOnCancel: false,
                                              function (isConfirm) {
                                            }
                                  });

                                //console.log(data);
@if($pages=="pages.bidding")
 if(data.autobidlist.bid_remain>0) {

                                  var bidremaining = parseFloat(data.autobidlist.bid_remain);

                                  var bidtake = bidremaining/parseFloat(data.autobidlist.boots);

                                  var bidautoid = data.autobidlist.id;
       $("#removeautobt")
        .prev('div.row')
        .remove()
        .end();
      $( "#autobidd" ).after('<div class="row no-margin"> <div class="col-md-5"> <p class="text-muted">'+bidremaining+'</p> </div> <div class="col-md-4"> <p class="text-muted">'+bidtake+'</p> </div> <div class="col-md-3"> <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5 delcustom" data-id="'+bidautoid+'"> <i class="fa fa-remove"></i> </button> </div>'); }

@endif


}else if(data.message==="fail")
                {
                                          swal({
                                          title: 'ไม่สามารถทำรายการได้',
                                          confirmButtonText: 'ตกลง',
                                          type: "error",
                                          text:'กรุณาตรวจสอบ Bid คงเหลือ'},
                                          function (isConfirm) {
                                            if (isConfirm) {

                                            }
                                          });
                }

            },
      });
    }); 
@else
    $('.biddingpusher').click(function(){
     swal({
      title: 'กรุณาเข้าสู่ระบบเพิ่มร่วมสนุก',
      confirmButtonText: 'สมัครสมาชิก',
      text:'สมัครเป็นสมาชิกกับเรา'},
      function (isConfirm) {
        if (isConfirm) {
          window.location.href = "{!!url('user/register') !!}";
        }
      })
    
     });
$('.autobid-button').on("click", function() {
      swal({
      title: 'กรุณาเข้าสู่ระบบเพิ่มร่วมสนุก',
      confirmButtonText: 'สมัครสมาชิก',
      text:'สมัครเป็นสมาชิกกับเรา'},
      function (isConfirm) {
        if (isConfirm) {
          window.location.href = "{!!url('user/register') !!}";
        }
      })
    
     });

@endif

});
</script>