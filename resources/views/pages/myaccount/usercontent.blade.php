                                    <ul class="nav nav-pills pull-right">
                                        <li class="active">
                                            <a href="#myaccount-info" data-toggle="tab" aria-expanded="true">{!! html_entity_decode(Lang::get('myacc-user.1')) !!}</a>
                                        </li>
                                        <li class="">
                                            <a href="#myaccount-changeprofile" data-toggle="tab" aria-expanded="false">{!! html_entity_decode(Lang::get('myacc-user.2')) !!}</a>
                                        </li>
                                        <li class="">
                                            <a href="#myaccount-verify" data-toggle="tab" aria-expanded="false">{!! html_entity_decode(Lang::get('myacc-user.3')) !!}</a>
                                        </li>
                                        <li class="">
                                            <a href="#myaccount-changepass" data-toggle="tab" aria-expanded="false">{!! html_entity_decode(Lang::get('myacc-user.4')) !!}</a>
                                        </li>
                                    </ul>
                                    <h4 class="header-title m-b-30">{!! html_entity_decode(Lang::get('myacc-user.5')) !!}</h4>

                                    <div class="tab-content">
                                        <div id="myaccount-info" class="tab-pane fade active in">
                                            <div class="row">
                                                <div class="col-md-12">
                                                   <div class="rs_user_dashboard_tab_info">
                                            <div class="rs_user_dashboard_tab_info_form">
                                                <form class="form-horizontal rs_loginform rs_bottompadder20">
                                                 <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label class="col-sm-5 col-xs-5 control-label">{!! html_entity_decode(Lang::get('myacc-user.6')) !!}</label>
                                                        <div class="col-sm-7 col-xs-7">
                                                            <div class="row">
                                                                <input type="text" class="form-control" placeholder="{!!$userdet->username!!}" disabled>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 col-xs-5 control-label">{!! html_entity_decode(Lang::get('myacc-user.7')) !!}</label>
                                                        <div class="col-sm-7 col-xs-7">
                                                            <div class="row">
                                                                <input type="password" class="form-control" placeholder="********" disabled>
                                                                <span><a href="#myaccount-changepass" data-toggle="tab">{!! html_entity_decode(Lang::get('myacc-user.8')) !!}</a></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-sm-5 col-xs-5 control-label">{!! html_entity_decode(Lang::get('myacc-user.9')) !!}</label>
                                                        <div class="col-sm-7 col-xs-7">
                                                                <div class="row">
                                                                    <input type="text" class="form-control" value="{!!$userdet->first_name!!}" placeholder="{!! html_entity_decode(Lang::get('myacc-user.10')) !!}" disabled>
                                                                </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                            <label class="col-sm-5 col-xs-5 control-label">{!! html_entity_decode(Lang::get('myacc-user.11')) !!}</label>
                                                            <div class="col-sm-7 col-xs-7">
                                                                <div class="row">
                                                                    <input type="text" class="form-control" value="{!!$userdet->last_name!!}" placeholder="{!! html_entity_decode(Lang::get('myacc-user.12')) !!}" disabled>
                                                                </div>
                                                            </div>
                                                     </div>
                                                        <div class="form-group">
                                                            <label class="col-sm-5 col-xs-5 control-label">{!! html_entity_decode(Lang::get('myacc-user.13')) !!} <i class="glyphicon glyphicon-calendar fa fa-calendar"></i> : </label>
                                                            <div class="col-sm-7 col-xs-7">
                                                                <div class="row datepicker"> 
                                                                    <input type="date" class="form-control" name="birthdate" value="{!!$userdet->birth_date!!}" placeholder="Birthday" disabled>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <div class="form-group">
                                                            <label class="col-sm-5 col-xs-5 control-label">{!! html_entity_decode(Lang::get('myacc-user.14')) !!}</label>
                                                            <div class="col-sm-7 col-xs-7">
                                                                <div class="row">
                                                                    <input type="radio" name="gender" value="0" <?php if ($userdet->sex == 0) echo 'checked'; ?> disabled> {!! html_entity_decode(Lang::get('myacc-user.15')) !!}<br>
                                                                    <input type="radio" name="gender" value="1" <?php if ($userdet->sex == 1) echo 'checked'; ?> disabled>
                                                                    {!! html_entity_decode(Lang::get('myacc-user.16')) !!}<br>
                                                                </div>

                                                            </div>
                                                    </div>
                                                    <div class="form-group">
                                                            <label class="col-sm-5 col-xs-5 control-label">{!! html_entity_decode(Lang::get('myacc-user.17')) !!}</label>
                                                            <div class="col-sm-7 col-xs-7">
                                                                <div class="row">
                                                                    <input type="text" class="form-control" value="{!!$userdet->phone!!}" placeholder="Phone" disabled>
                                                                </div>
                                                            </div>
                                                    </div>
                                                        <label class="col-sm-5 col-xs-5 control-label">{!! html_entity_decode(Lang::get('myacc-user.18')) !!}</label>
                                                            <div class="col-sm-7 col-xs-7">
                                                                <div class="row">
                                                                @if(trim($userdet->verified)=="verified")
                                                                <span class="label label-primary">{!! html_entity_decode(Lang::get('myacc-user.19')) !!}</span>
                                                                @elseif(trim($userdet->verified)=="inform")
                                                                <span class="label label-danger">{!! html_entity_decode(Lang::get('myacc-user.20')) !!}</span>
                                                                @else
                                                                <span class="label label-danger">{!! html_entity_decode(Lang::get('myacc-user.21')) !!}</span>
                                                                @endif
                                                                </div>
                                                            </div>
                                                           
                                                    
                                                    <p><a href="#myaccount-changeprofile" data-toggle="tab">{!! html_entity_decode(Lang::get('myacc-user.22')) !!}</a></p>
                                                    </div>
                                                   
                                                </form>
                                            </div>
                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="myaccount-changeprofile" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="rs_user_dashboard_tab_info">
                                            <div class="rs_user_dashboard_tab_info_form">
                                                <div class="row">
                                
                                                {!! Form::open(array('url'=>'user/saveprofilefront', 'class'=>'form-horizontal ' ,'files' => true)) !!}

                                                
                                                <div class="col-md-12">

                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>{!! html_entity_decode(Lang::get('myacc-user.9')) !!}<sup>*</sup></label>
                                                                <input type="text" class="form-control" placeholder="{!! html_entity_decode(Lang::get('myacc-user.10')) !!}First Name" name="first_name" value="{!!$userdet->first_name!!}" required>
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                            <div class="form-group">
                                                                <label>{!! html_entity_decode(Lang::get('myacc-user.11')) !!}<sup>*</sup></label>
                                                                <input type="text" class="form-control" name="last_name" placeholder="{!! html_entity_decode(Lang::get('myacc-user.12')) !!}" value="{!!$userdet->last_name!!}" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                       
                                                        <div class="form-group">
                                                            <label class="col-sm-3 control-label">{!! html_entity_decode(Lang::get('myacc-user.23')) !!}</label>
                                                            <div class="col-sm-9">
                                                                <div class="row datepicker">
                                                                    <input type="email" class="form-control" placeholder="{!! html_entity_decode(Lang::get('myacc-user.24')) !!}" name="email" value="{!!$userdet->email!!}" parsley-type="email" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   
                                                    
                                                    
                                                    <div class="form-group">
                                                                <label class="col-sm-3 control-label">{!! html_entity_decode(Lang::get('myacc-user.25')) !!} <i class="glyphicon glyphicon-calendar fa fa-calendar"></i> : </label>
                                                                <div class="col-sm-9 no-padding">
                                                                    <input type="text" name="birth_date" class="form-control" placeholder="dd/mm/yyyy" id="datepicker" value="{!!date("d/m/Y", strtotime($userdet->birth_date)) !!}" required="">                           
                                                                </div>
                                                    </div>
                                                        
                                                    <div class="form-group">
                                                            <label class="col-sm-3 control-label">{!! html_entity_decode(Lang::get('myacc-user.14')) !!}</label>
                                                            <div class="col-sm-4">
                                                                <div class="row">
                                                                    <input type="radio" name="sex" value="0" <?php if ($userdet->sex == 0) echo 'checked'; ?> > {!! html_entity_decode(Lang::get('myacc-user.15')) !!}<br>
                                                                    <input type="radio" name="sex" value="1" <?php if ($userdet->sex == 1) echo 'checked'; ?> >
                                                                    {!! html_entity_decode(Lang::get('myacc-user.16')) !!}<br>
                                                                </div>

                                                            </div>
                                                    </div>
                                                    <div class="form-group">
                                                            <label class="col-sm-3 control-label">{!! html_entity_decode(Lang::get('myacc-user.17')) !!}</label>
                                                            <div class="col-sm-9">
                                                                <div class="row">
                                                                    <input type="text" class="form-control" data-parsley-type="number" value="{!!$userdet->phone!!}" placeholder="Phone" name="phone" required>
                                                                </div>
                                                            </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="col-sm-3 control-label">{!! html_entity_decode(Lang::get('myacc-user.25')) !!}</label>
                                                        <div class="col-sm-9">
                                                                <div class="row">
                                                                    <textarea id="textarea" class="form-control" maxlength="225" name="address" rows="2" name="address" placeholder="{!! html_entity_decode(Lang::get('myacc-user.26')) !!}"></textarea>
                                                                </div>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                            <label class="col-sm-3 control-label">{!! html_entity_decode(Lang::get('myacc-user.27')) !!}</label>
                                                            <div class="col-sm-9">
                                                                <div class="row">
                                                                <select class="form-control select2" id="xprovince" name="province" required></select>
                                                                </div>
                                                            </div>
                                                    </div>

                                                    <div class="form-group">
                                                            <label class="col-sm-3 control-label">{!! html_entity_decode(Lang::get('myacc-user.28')) !!}</label>
                                                            <div class="col-sm-9">
                                                                <div class="row">
                                                                    <select class="form-control select2" name="district" id="xdistrict" required></select>
                                                                </div>
                                                            </div>
                                                    </div>

                                                    <div class="form-group">
                                                            <label class="col-sm-3 control-label">{!! html_entity_decode(Lang::get('myacc-user.29')) !!}</label>
                                                            <div class="col-sm-9">
                                                                <div class="row">
                                                                <select class="form-control select2" name="subdistrict" id="xsubdistrict" required></select>
                                                                </div>
                                                            </div>
                                                    </div>

                                                    <div class="form-group">
                                                            <label class="col-sm-3 control-label">{!! html_entity_decode(Lang::get('myacc-user.30')) !!}</label>
                                                            <div class="col-sm-9">
                                                                <div class="row">
                                                                    <input type="text" id="xzipcode" class="form-control" name="postcode" data-parsley-type="number" value="{!!$userdet->postcode!!}" placeholder="{!! html_entity_decode(Lang::get('myacc-user.31')) !!}" name="postcode" required>
                                                                </div>
                                                            </div>
                                                    </div>

                                                </div>
                                                   
                                                    <div class="rs_btn_div rs_toppadder30">
                                                        <button type="submit" class="rs_button rs_button_orange">{!! html_entity_decode(Lang::get('myacc-user.32')) !!}</button>
                                                    </div>
                                                {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div id="myaccount-verify" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-md-12">
                                                               
                                                        {!! Form::open(array('url'=>'user/saveprofileverify', 'class'=>'form-horizontal ' ,'files' => true)) !!}
                                                        <div class="col-md-12">
                                                            <div class="card-box">

                                                                <h5 class="header-title m-t-0 m-b-30">{!! html_entity_decode(Lang::get('myacc-user.33')) !!}</h5>

                                                                <input type="file" class="dropify" name="pic_personid" data-default-file="{!!url('') !!}/uploads/users/{!!$userdet->pic_personid!!}" @if(trim($userdet->verified)=="verified") disabled="disabled" @endif required/>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="card-box">

                                                                <h5 class="header-title m-t-0 m-b-30">{!! html_entity_decode(Lang::get('myacc-user.34')) !!}</h5>

                                                                <input type="file" class="dropify" name="pic_bookbank" data-default-file="{!!url('') !!}/uploads/users/{!!$userdet->pic_bookbank!!}"  @if(trim($userdet->verified)=="verified") disabled="disabled" @endif required/>
                                                            </div>
                                                        </div>
                                                         <div class="col-md-12">
                                                         <div class="rs_btn_div rs_toppadder30">
                                                        <button type="submit" class="rs_button rs_button_orange">{!! html_entity_decode(Lang::get('myacc-user.35')) !!}</button>
                                                        </div>
                                                        </div>
                                                        {!! Form::close() !!}
                                                    
                                                </div>
                                            </div>
                                        </div>
                                        <div id="myaccount-changepass" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="rs_user_dashboard_tab_info">
                                            <div class="rs_user_dashboard_tab_info_form">
                                {!! Form::open(array('url'=>'user/savepasswordfront/', 'class'=>'form-horizontal ' ,'files' => true)) !!}
                                                    

                                                    <div class="form-group">
                                                        <label for="hori-pass1" class="col-sm-4 col-xs-4 control-label">{!! html_entity_decode(Lang::get('myacc-user.36')) !!}</label>
                                                        <div class="col-sm-7 col-xs-8">
                                                            <input id="hori-pass1" type="password" name="password" placeholder="{!! html_entity_decode(Lang::get('myacc-user.37')) !!}" required
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="hori-pass2" class="col-sm-4 col-xs-4 control-label">{!! html_entity_decode(Lang::get('myacc-user.38')) !!}
                                                            </label>
                                                        <div class="col-sm-7 col-xs-8">
                                                            <input data-parsley-equalto="#hori-pass1" name="password_confirmation" type="password" required
                                                                   placeholder="{!! html_entity_decode(Lang::get('myacc-user.39')) !!}" class="form-control" id="hori-pass2">
                                                        </div>
                                                    </div>

                                                    <div class="rs_btn_div rs_toppadder30">
                                                        <button type="submit" class="rs_button rs_button_orange">{!! html_entity_decode(Lang::get('myacc-user.40')) !!}</button>
                                                    </div>
                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>