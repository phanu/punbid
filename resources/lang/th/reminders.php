<?php 
return array( 
"password"			=> "การตั้ง Password ความยาว 8-16 ตัวอักษร ประกอบไปด้วยตัวอักษรภาษาอังกฤษและตัวเลขอย่างน้อยอย่างละ 1 ตัวอักษร อักษรภาษาอังกฤษตัวใหญ่และเล็กมีผลต่อการใช้งาน (Case Sensitive) ตัวอย่างเช่น “passw0rd”, “12345678asdf” เป็นต้น",  
 "user"			=> "We can't find a user with that e-mail address.",  
 "token"			=> "This password reset token is invalid.",  
 "sent"			=> "Password reminder sent!",  
 );