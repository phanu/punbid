<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class bidremind extends Sximo  {
	
	protected $table = 'bid_to_spay';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT bid_to_spay.* FROM bid_to_spay  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE bid_to_spay.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
