
<?php 
/*
if(Cart::instance('shop')->count()=="0"&&Cart::instance('spay')->count()=="0")
{
  $cart_message = "ขณะนี้ไม่มีสินค้าในตะกร้า";
}
*/
if(isset($_REQUEST['paytype'])&&$_REQUEST['paytype']=="bidpack")
{

      $_REQUEST['paytype']=="bidpack";

      if($_REQUEST['price']=="0")
        {
            $_REQUEST['price']=$_REQUEST['bids']*2;

            $_REQUEST['name']=$_REQUEST['name'].$_REQUEST['bids'].' Bid';
        }

      Cart::instance('bidpack')->destroy();
      Cart::instance('bidpack')->add(
        [
          'id' => $_REQUEST['pid'], 
          'name' => $_REQUEST['name'], 
          'qty' => $_REQUEST['qty'], 
          'price' => $_REQUEST['price'], 
          'options' => [
                  'pic_url' => $_REQUEST['url'],
                  'paytype' => $_REQUEST['paytype'],
                  'bids' => $_REQUEST['bids']
                ]
        ]
      );

}



?>
<div class="container">
<div class="container">
<div class="row">
@if(Session::has('messagetext'))
    
       {!! Session::get('messagetext') !!}
     
  @endif
  <ul class="parsley-error-list">
    @foreach($errors->all() as $error)
      <li>{!! $error !!}</li>
    @endforeach
  </ul>   
</div>
</div>
</div>

<!-- END FORM-->




<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
<form action="{!! url('bidorders/savepublic') !!}" class="form-horizontal" method="post" id="order_punbidtheme">
  <div class="rs_checkout_order_section rs_toppadder20 rs_bottompadder20">
    <div class="rs_chkout_headings">
      <h4>{!! html_entity_decode(Lang::get('bidcheckout.1')) !!}</h4>
    </div>
    <div class="woo-cart-table rs_woo_total_table">
      <table class="table">
        <thead>
          <tr>
            <th class="text-center">{!! html_entity_decode(Lang::get('bidcheckout.2')) !!}</th>
            <th class="text-center">{!! html_entity_decode(Lang::get('bidcheckout.3')) !!}</th>
            <th class="text-center">{!! html_entity_decode(Lang::get('bidcheckout.4')) !!}</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach(Cart::instance('bidpack')->content() as $row) :?>
          <tr>
            <td>
             <p>{!! $row->name !!}</p>
           </td>
           <td>
              <span style="text-align:center;">{!! $row->qty !!}</span>
           </td>

            <td class="text-center">
             <span>{!! number_format($row->price) !!} THB<small></small>
             </span>
            </td>
          </tr>
          <input type="hidden" name="bidpack_id" value="{!! $row->id !!}">
          <input type="hidden" name="qty" value="{!! $row->qty !!}">
          <input type="hidden" name="price" value="{!! $row->price !!}">

     <input type="hidden" name="bids" id="bids" value="{!! $row->options->bids !!}"> 
          <?php endforeach; ?>
        </tbody>
      <tfoot>
        <tr>
          <td></td>
          <td>
           <p><span>รวม</span></p>
         </td>
         <td class="text-center">
          <span><?php echo Cart::instance('bidpack')->subtotal(); ?> THB</span>
        </td>
      </tr>
      <tr>
          <td></td>
          <td>
           <p><span>{!! html_entity_decode(Lang::get('bidcheckout.5')) !!}</span></p>
         </td>
         <td class="text-center">
          <span id="setpaymentfee">0 THB</span>
        </td>
      </tr>
        <tr>
          <td></td>
          <td>
           <p><span>{!! html_entity_decode(Lang::get('bidcheckout.6')) !!}</span></p>
         </td>
         <td class="text-center">
          <span id="checkouttotal"><?php echo number_format(str_replace(',', '', Cart::instance('bidpack')->subtotal()),2);  ?> THB</span>
    <input type="hidden" name="numtotal" id="numtotal" value="<?php echo str_replace(',', '', Cart::instance('bidpack')->subtotal()); ?>">
        </td>
      </tr>
      
    </tfoot>
  </table>
</div>
</div>
</div>

<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
  <div class="rs_checkout_payment_section rs_toppadder20 rs_bottompadder20">
    <div class="rs_chkout_headings">
      <h4>{!! html_entity_decode(Lang::get('bidcheckout.7')) !!}</h4>
    </div>
    <div class="woocommerce-checkout-payment" style="margin-bottom:20px;">
      <ul class="payment_methods methods">
        <li>
          <div class="rs_radiobox">
            <input type="radio" value="credit_card" id="radio3" name="payment_type" required>
            <label for="radio3"></label>
          </div>
          {!! html_entity_decode(Lang::get('bidcheckout.8')) !!}
          <img src="{!! asset('frontend') !!}/punbidtheme/images/paysbuy.jpg" class="pull-right" alt="">
          <div class="payment_box" data-period="credit_card">
            <p>{!! html_entity_decode(Lang::get('bidcheckout.9')) !!}</p>
          </div>
        </li>
        <li>
          <div class="rs_radiobox">
            <input type="radio" value="dbt" id="radio1" name="payment_type" required>
            <label for="radio1"></label>
          </div>
          {!! html_entity_decode(Lang::get('bidcheckout.10')) !!}
          <img src="{!! asset('frontend') !!}/punbidtheme/images/01-kbank.png" class="pull-right" alt="">
          <div class="payment_box" data-period="dbt">
            <p>{!! html_entity_decode(Lang::get('bidcheckout.11')) !!}</p>
            <p>{!! html_entity_decode(Lang::get('bidcheckout.12')) !!}</p>
            <p>{!! html_entity_decode(Lang::get('bidcheckout.13')) !!}</p>
          </div>
        </li>
      </ul>
    </div>

    <div class="row">
     <div class="col-md-offset-3 col-md-9" style="
     text-align: right; float:right;
     ">
    <input type="hidden" name="orderNumber" id="orderNumber" value=""> 
     <input type="hidden" name="payment_fee" id="payment_fee" value=""> 
     <input type="hidden" name="status" id="status" value="onhole"> 
     <input type="hidden" name="customerNumber" id="customerNumber" value="<?php echo Session::get('uid'); ?>"> 
     <button type="submit" class="rs_button rs_button_orange rs_center_btn rs_toppadder20">{!! html_entity_decode(Lang::get('bidcheckout.14')) !!}</button>
   </div>
 </form>
</div>
</form>
</div>
</div>
</div>
</div>

</div>	


</div>		
  </div></div></div>
