@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Name Th', (isset($fields['name_th']['language'])? $fields['name_th']['language'] : array())) }}</td>
						<td>{{ $row->name_th}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Name En', (isset($fields['name_en']['language'])? $fields['name_en']['language'] : array())) }}</td>
						<td>{{ $row->name_en}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Title Th', (isset($fields['title_th']['language'])? $fields['title_th']['language'] : array())) }}</td>
						<td>{{ $row->title_th}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Title En', (isset($fields['title_en']['language'])? $fields['title_en']['language'] : array())) }}</td>
						<td>{{ $row->title_en}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Descript Th', (isset($fields['descript_th']['language'])? $fields['descript_th']['language'] : array())) }}</td>
						<td>{{ $row->descript_th}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Descript En', (isset($fields['descript_en']['language'])? $fields['descript_en']['language'] : array())) }}</td>
						<td>{{ $row->descript_en}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Price', (isset($fields['price']['language'])? $fields['price']['language'] : array())) }}</td>
						<td>{{ $row->price}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sale Price', (isset($fields['sale_price']['language'])? $fields['sale_price']['language'] : array())) }}</td>
						<td>{{ $row->sale_price}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Category', (isset($fields['category']['language'])? $fields['category']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->category,'category','1:product_categories:id:name_th') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Featured', (isset($fields['featured']['language'])? $fields['featured']['language'] : array())) }}</td>
						<td>{!! SiteHelpers::formatRows($row->featured,$fields['featured'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Brand', (isset($fields['brand']['language'])? $fields['brand']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->brand,'brand','1:brand:id:name_th') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Featured Img', (isset($fields['featured_img']['language'])? $fields['featured_img']['language'] : array())) }}</td>
						<td>{!! SiteHelpers::formatRows($row->featured_img,$fields['featured_img'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Gallery', (isset($fields['gallery']['language'])? $fields['gallery']['language'] : array())) }}</td>
						<td>{!! SiteHelpers::formatRows($row->gallery,$fields['gallery'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Active', (isset($fields['active']['language'])? $fields['active']['language'] : array())) }}</td>
						<td>{!! SiteHelpers::formatRows($row->active,$fields['active'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Products Code', (isset($fields['products_code']['language'])? $fields['products_code']['language'] : array())) }}</td>
						<td>{{ $row->products_code}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Shiping Cost', (isset($fields['shiping_cost']['language'])? $fields['shiping_cost']['language'] : array())) }}</td>
						<td>{{ $row->shiping_cost}} </td>
						
					</tr>
				
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	