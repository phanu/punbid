<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'App\User',
		'secret' => '',
	],

	'google' => [
	    'client_id' => '647011571442-dit2b1180o55cqg2rto6aru3akob9tu7.apps.googleusercontent.com',
	    'client_secret' => 'qGpU1SqSWx1-NgzpN8M5OY_E',
	    'redirect' => 'https://www.sibeza.com/user/autosocial/google',
	],

	'twitter' => [
	    'client_id' => '',
	    'client_id' => 'q2NR24fPB2VtayTOMa6NDAG9s',
	    'client_secret' => 'deLBI0nVkllV1aAOrohk0X9nDJY1tognRQO2myJsGis9GnmBCY',
	    'redirect' => 'http://sximobuilder.com/sximodemo/sximo5/user/twitter',
	],

	'facebook' => [
	    'client_id' => '1825385364387104',
	    'client_secret' => 'd5d6d47df14628ee803b164b793a1362',
	    'redirect' => 'https://www.sibeza.com/user/autosocial/facebook',
	],		

];
