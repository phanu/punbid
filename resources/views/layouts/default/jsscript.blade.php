
  <!-- our features -->
  <div class="our-features-box">
    <div class="container">
      <div class="row">
        <div class="col-lg-3 col-xs-12 col-sm-6">
          <div class="feature-box first"> <span class="fa fa-truck"></span>
            <div class="content">
              <h3>ทองคำราคาดี ประมูลเลย</h3>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-12 col-sm-6">
          <div class="feature-box"> <span class="fa fa-headphones"></span>
            <div class="content">
              <h3>ไปๆกันไปเอาทองกัน</h3>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-12 col-sm-6">
          <div class="feature-box"> <span class="fa fa-share"></span>
            <div class="content">
              <h3>ทองไม่รู้ร้อนเลย</h3>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-12 col-sm-6">
          <div class="feature-box last"> <span class="fa fa-phone"></span>
            <div class="content">
              <h3>สายด่วน 096-695-6669</h3>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer -->
  
</div>
<!-- End Footer --> 

<!-- mobile menu -->
<div id="mobile-menu">
  <ul>
    <li>
      <div class="mm-search">
        <form id="search1" name="search">
          <div class="input-group">
            <div class="input-group-btn">
              <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> </button>
            </div>
            <input type="text" class="form-control simple" placeholder="Search ..." name="srch-term" id="srch-term">
          </div>
        </form>
      </div>
    </li>
    <li><a href="index.php">Home</a>
          <ul>
        <li class="level1"><a href="../fashion/index.php"><span>Fashion Store</span></a> </li>
        <li class="level1"><a href="index.php"><span>Digital Store</span></a> </li>
        <li class="level1"><a href="../furniture/index.php"><span>Furniture Store</span></a> </li>
        <li class="level1"><a href="../jewellery/index.php"><span>Jewellery Store</span></a> </li>
      </ul>
    </li>
    <li><a href="#">Pages</a>
      <ul>
        <li><a href="grid.php">Grid</a> </li>
        <li> <a href="list.php">List</a> </li>
        <li> <a href="product_detail.php">Product Detail</a> </li>
        <li> <a href="shopping_cart.php">Shopping Cart</a> </li>
        <li><a href="checkout.php">Checkout</a> </li>
        <li> <a href="wishlist.php">Wishlist</a> </li>
        <li> <a href="dashboard.php">Dashboard</a> </li>
        <li> <a href="multiple_addresses.php">Multiple Addresses</a> </li>
        <li> <a href="about_us.php">About us</a> </li>
        <li><a href="blog.php">Blog</a>
          <ul>
            <li><a href="blog-detail.php">Blog Detail</a> </li>
          </ul>
        </li>
        <li><a href="contact_us.php">Contact us</a> </li>
        <li><a href="404error.php">404 Error Page</a> </li>
      </ul>
    </li>
    <li><a href="#">Women</a>
      <ul>
        <li> <a href="#" class="">Stylish Bag</a>
          <ul>
            <li> <a href="grid.php" class="">Clutch Handbags</a> </li>
            <li> <a href="grid.php" class="">Diaper Bags</a> </li>
            <li> <a href="grid.php" class="">Bags</a> </li>
            <li> <a href="grid.php" class="">Hobo handbags</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Material Bag</a>
          <ul>
            <li> <a href="grid.php">Beaded Handbags</a> </li>
            <li> <a href="grid.php">Fabric Handbags</a> </li>
            <li> <a href="grid.php">Handbags</a> </li>
            <li> <a href="grid.php">Leather Handbags</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Shoes</a>
          <ul>
            <li> <a href="grid.php">Flat Shoes</a> </li>
            <li> <a href="grid.php">Flat Sandals</a> </li>
            <li> <a href="grid.php">Boots</a> </li>
            <li> <a href="grid.php">Heels</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Jwellery</a>
          <ul>
            <li> <a href="grid.php">Bracelets</a> </li>
            <li> <a href="grid.php">Necklaces &amp; Pendent</a> </li>
            <li> <a href="grid.php">Pendants</a> </li>
            <li> <a href="grid.php">Pins &amp; Brooches</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Dresses</a>
          <ul>
            <li> <a href="grid.php">Casual Dresses</a> </li>
            <li> <a href="grid.php">Evening</a> </li>
            <li> <a href="grid.php">Designer</a> </li>
            <li> <a href="grid.php">Party</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Swimwear</a>
          <ul>
            <li> <a href="grid.php">Swimsuits</a> </li>
            <li> <a href="grid.php">Beach Clothing</a> </li>
            <li> <a href="grid.php">Clothing</a> </li>
            <li> <a href="grid.php">Bikinis</a> </li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="grid.php">Men</a>
      <ul>
        <li> <a href="grid.php" class="">Shoes</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.php">Sport Shoes</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Casual Shoes</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Leather Shoes</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">canvas shoes</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Dresses</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.php">Casual Dresses</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Evening</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Designer</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Party</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Jackets</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.php">Coats</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Formal Jackets</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Leather Jackets</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Blazers</a> </li>
          </ul>
        </li>
        <li> <a href="#.php">Watches</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.php">Fasttrack</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Casio</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Titan</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Tommy-Hilfiger</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Sunglasses</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.php">Ray Ban</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Fasttrack</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Police</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Oakley</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Accesories</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.php">Backpacks</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Wallets</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Laptops Bags</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.php">Belts</a> </li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="grid.php">Electronics</a>
      <ul>
        <li> <a href="grid.php"><span>Mobiles</span></a>
          <ul>
            <li> <a href="grid.php"><span>Samsung</span></a> </li>
            <li> <a href="grid.php"><span>Nokia</span></a> </li>
            <li> <a href="grid.php"><span>IPhone</span></a> </li>
            <li> <a href="grid.php"><span>Sony</span></a> </li>
          </ul>
        </li>
        <li> <a href="grid.php" class=""><span>Accesories</span></a>
          <ul>
            <li> <a href="grid.php"><span>Mobile Memory Cards</span></a> </li>
            <li> <a href="grid.php"><span>Cases &amp; Covers</span></a> </li>
            <li> <a href="grid.php"><span>Mobile Headphones</span></a> </li>
            <li> <a href="grid.php"><span>Bluetooth Headsets</span></a> </li>
          </ul>
        </li>
        <li> <a href="grid.php"><span>Cameras</span></a>
          <ul>
            <li> <a href="grid.php"><span>Camcorders</span></a> </li>
            <li> <a href="grid.php"><span>Point &amp; Shoot</span></a> </li>
            <li> <a href="grid.php"><span>Digital SLR</span></a> </li>
            <li> <a href="grid.php"><span>Camera Accesories</span></a> </li>
          </ul>
        </li>
        <li> <a href="grid.php"><span>Audio &amp; Video</span></a>
          <ul>
            <li> <a href="grid.php"><span>MP3 Players</span></a> </li>
            <li> <a href="grid.php"><span>IPods</span></a> </li>
            <li> <a href="grid.php"><span>Speakers</span></a> </li>
            <li> <a href="grid.php"><span>Video Players</span></a> </li>
          </ul>
        </li>
        <li> <a href="grid.php"><span>Computer</span></a>
          <ul>
            <li> <a href="grid.php"><span>External Hard Disk</span></a> </li>
            <li> <a href="grid.php"><span>Pendrives</span></a> </li>
            <li> <a href="grid.php"><span>Headphones</span></a> </li>
            <li> <a href="grid.php"><span>PC Components</span></a> </li>
          </ul>
        </li>
        <li> <a href="grid.php"><span>Appliances</span></a>
          <ul>
            <li> <a href="grid.php"><span>Vaccum Cleaners</span></a> </li>
            <li> <a href="grid.php"><span>Indoor Lighting</span></a> </li>
            <li> <a href="grid.php"><span>Kitchen Tools</span></a> </li>
            <li> <a href="grid.php"><span>Water Purifier</span></a> </li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="grid.php">Furniture</a>
      <ul>
        <li> <a href="grid.php">Living Room</a>
          <ul>
            <li> <a href="grid.php">Racks &amp; Cabinets</a> </li>
            <li> <a href="grid.php">Sofas</a> </li>
            <li> <a href="grid.php">Chairs</a> </li>
            <li> <a href="grid.php">Tables</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php" class="">Dining &amp; Bar</a>
          <ul>
            <li> <a href="grid.php">Dining Table Sets</a> </li>
            <li> <a href="grid.php">Serving Trolleys</a> </li>
            <li> <a href="grid.php">Bar Counters</a> </li>
            <li> <a href="grid.php">Dining Cabinets</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Bedroom</a>
          <ul>
            <li> <a href="grid.php">Beds</a> </li>
            <li> <a href="grid.php">Chest of Drawers</a> </li>
            <li> <a href="grid.php">Wardrobes &amp; Almirahs</a> </li>
            <li> <a href="grid.php">Nightstands</a> </li>
          </ul>
        </li>
        <li> <a href="grid.php">Kitchen</a>
          <ul>
            <li> <a href="grid.php">Kitchen Racks</a> </li>
            <li> <a href="grid.php">Kitchen Fillings</a> </li>
            <li> <a href="grid.php">Wall Units</a> </li>
            <li> <a href="grid.php">Benches &amp; Stools</a> </li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="grid.php">Kids</a> </li>
    <li><a href="contact-us.php">Contact Us</a> </li>
  </ul>
  <div class="top-links">
    <ul class="links">
      <li><a title="My Account" href="login.php">My Account</a> </li>
      <li><a title="Wishlist" href="wishlist.php">Wishlist</a> </li>
      <li><a title="Checkout" href="checkout.php">Checkout</a> </li>
      <li><a title="Blog" href="blog.php"><span>Blog</span></a> </li>
      <li class="last"><a title="Login" href="login.php"><span>Login</span></a> </li>
    </ul>
  </div>
</div>

<!-- JavaScript --> 
<script type="text/javascript" src="{!! asset('frontend/punbid/') !!}/js/jquery.min.js"></script> 
<script type="text/javascript" src="{!! asset('frontend/punbid/') !!}/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="{!! asset('frontend/punbid/') !!}/js/revslider.js"></script> 
<script type="text/javascript" src="{!! asset('frontend/punbid/') !!}/js/common.js"></script> 
 
<script type="text/javascript" src="{!! asset('frontend/punbid/') !!}/js/owl.carousel.min.js"></script> 
<script type="text/javascript" src="{!! asset('frontend/punbid/') !!}/js/jquery.mobile-menu.min.js"></script> 
<script type='text/javascript'>
jQuery(document).ready(function() {
  $('.bid-time').each(function() {
      var $this = $(this), finalDate = $(this).data('countdown');
      $this.countdown(finalDate, function(event) {
      $this.html(event.strftime('%H:%M:%S'));
      });
    });
  jQuery('#rev_slider_4').show().revolution({
  dottedOverlay: 'none',
  delay: 5000,
  startwidth: 915,
  startheight: 490,
  hideThumbs: 200,
  thumbWidth: 200,
  thumbHeight: 50,
  thumbAmount: 2,
  navigationType: 'thumb',
  navigationArrows: 'solo',
  navigationStyle: 'round',
  touchenabled: 'on',
  onHoverStop: 'on',
  swipe_velocity: 0.7,
  swipe_min_touches: 1,
  swipe_max_touches: 1,
  drag_block_vertical: false,
  spinner: 'spinner0',
  keyboardNavigation: 'off',
  navigationHAlign: 'center',
  navigationVAlign: 'bottom',
  navigationHOffset: 0,
  navigationVOffset: 20,
  soloArrowLeftHalign: 'left',
  soloArrowLeftValign: 'center',
  soloArrowLeftHOffset: 20,
  soloArrowLeftVOffset: 0,
  soloArrowRightHalign: 'right',
  soloArrowRightValign: 'center',
  soloArrowRightHOffset: 20,
  soloArrowRightVOffset: 0,
  shadow: 0,
  fullWidth: 'on',
  fullScreen: 'off',
  stopLoop: 'off',
  stopAfterLoops: -1,
  stopAtSlide: -1,
  shuffle: 'off',
  autoHeight: 'off',
  forceFullWidth: 'on',
  fullScreenAlignForce: 'off',
  minFullScreenHeight: 0,
  hideNavDelayOnMobile: 1500,
  hideThumbsOnMobile: 'off',
  hideBulletsOnMobile: 'off',
  hideArrowsOnMobile: 'off',
  hideThumbsUnderResolution: 0,
  hideSliderAtLimit: 0,
  hideCaptionAtLimit: 0,
  hideAllCaptionAtLilmit: 0,
  startWithSlide: 0,
  fullScreenOffsetContainer: ''
});
});
</script> 
<!-- Hot Deals Timer 1--> 
<script src="{!! asset('frontend/punbid/') !!}/js/jquery.countdown.min.js"></script>
<script src="{!! asset('frontend/punbid/') !!}/js/jquery.countdown.js"></script>

<!-- Validation js (Parsleyjs) -->

<script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/fileuploads/js/dropify.min.js"></script>

<script src="{!! asset('frontend') !!}/punbidtheme/js/jquery.thprovinceset.1.0.js"></script>
<script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>

<!-- Typeahead.js Bundle -->
<script src="{!! asset('frontend') !!}/punbidtheme/js/typeahead.bundle.min.js"></script>
<!-- script -->
<script src="{!! asset('frontend') !!}/punbidtheme/js/app.main.js"></script>

<script>
var resizefunc = [];
</script>

    <!--<script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/js/bootstrap.min.js"></script>
 jQuery  -
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/js/jquery.min.js"></script>
        
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/js/detect.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/js/fastclick.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/js/jquery.slimscroll.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/js/jquery.blockUI.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/js/waves.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/js/jquery.nicescroll.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/js/jquery.scrollTo.min.js"></script>
-->
        <!-- Sweet Alert js -->
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/pages/jquery.sweet-alert.init.js"></script>
        <!-- Tour page js -->
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/hopscotch/js/hopscotch.min.js"></script>
        <!-- App js -
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/js/jquery.core.js"></script>-->
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/js/jquery.app.js"></script>
        <script src="https://js.pusher.com/3.2/pusher.min.js"></script>

        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/toastr/toastr.min.js"></script>
        <script src="https://js.pusher.com/3.2/pusher.min.js"></script>
@if($pages=="pages.myaccount")
    @include('layouts/default/myaccscript')
@endif

@include('layouts/default/notify')
@include('layouts/default/mainfun')
@include('layouts/default/tour')

<script type="text/javascript">
$(document).ready(function() {
              

$(document).on('click','#spayclick', function() {
  $('form[name=form-productbuy-detailshop]').attr('action','{!! url('checkout') !!}');
  $('form[name=form-productbuy-detailshop]').append('<input type="hidden" name="paytype" value="spay" />');
  $('form[name=form-productbuy-detailshop]').submit();
});

<?php if(isset($_GET['page'])=="") { $p=1; }else{ $p=$_GET['page']; } ?> 

 // reloadDatafront('#',!!}');  
    
  var sync1 = $("#sync1");
  var sync2 = $("#sync2");
 
  sync1.owlCarousel({
    singleItem : true,
    slideSpeed : 1000,
    navigation: false,
    pagination:true,
    afterAction : syncPosition,
    responsiveRefreshRate : 200,
  });
 
  sync2.owlCarousel({
    items : 4,
    itemsDesktop      : [1199,4],
    itemsDesktopSmall     : [979,4],
    itemsTablet       : [768,4],
    itemsMobile       : [479,0],
    pagination:false,
    responsiveRefreshRate : 100,
    afterInit : function(el){
      el.find(".owl-item").eq(0).addClass("synced");
    el.find(".owl-item").addClass("itemfull");
    el.find(".owl-wrapper").addClass("itemfull");
    }
  });
 
  function syncPosition(el){
    var current = this.currentItem;
    $("#sync2")
      .find(".owl-item")
      .removeClass("synced")
      .eq(current)
      .addClass("synced")
    if($("#sync2").data("owlCarousel") !== undefined){
      center(current)
    }
  }
 
  $("#sync2").on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).data("owlItem");
    sync1.trigger("owl.goTo",number);
  });
 
  function center(number){
    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
    var num = number;
    var found = false;
    for(var i in sync2visible){
      if(num === sync2visible[i]){
        var found = true;
      }
    }
 
    if(found===false){
      if(num>sync2visible[sync2visible.length-1]){
        sync2.trigger("owl.goTo", num - sync2visible.length+2)
      }else{
        if(num - 1 === -1){
          num = 0;
        }
        sync2.trigger("owl.goTo", num);
      }
    } else if(num === sync2visible[sync2visible.length-1]){
      sync2.trigger("owl.goTo", sync2visible[1])
    } else if(num === sync2visible[0]){
      sync2.trigger("owl.goTo", num-1)
    }
    
  }
   
});


</script> 
<style type="text/css">
  /*.is-complete {
      color: red;
      -webkit-animation-name: blinker;
      -webkit-animation-iteration-count: infinite;
      -webkit-animation-timing-function: cubic-bezier(1.0,0,0,1.0);
      -webkit-animation-duration: 1s;
    }
    @-webkit-keyframes blinker {
      from { opacity: 1.0; }
      to { opacity: 0.0; }
    }
    .disabledbutton {
      pointer-events: none;
      opacity: 0.4;
    }*/
</style>
<script type="text/javascript">
$(document).ready(function()
{
toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-center",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
  function timeToSeconds(time){
      
      try{
        time = time.split(/:/);
        return time[0] * 3600 + time[1] * 60 + time[2] * 1;
      }catch(e){
        newt = "00:00:00";
        time = newt.split(/:/);
        return time[0] * 3600 + time[1] * 60 + time[2] * 1;
      }
  }
   //ajaxTime.php is called every second to get time from server'
(function doStuff() {
  $.ajax({
                              url: "{!! url('bidtransrecords/bidstatus') !!}",
                              method: "GET",
                              dataType: "json",
                              success: function(results) {
// console.log(results);
                                    @if(Auth::check())
                                      var uid = {!! Session::get('uid') !!};
                                    @endif
                                        $.each(results.b, function(i, item) {

                                             $('#bid-time_bidding_'+item.pid).countdown(item.t, function(event) {
                                                  $(this).html(event.strftime('%H:%M:%S'));
                                                  
                                              }).on('finish.countdown', function(data) {  
                                                 // $('#bid-time_bidding_'+item.pid).addClass('is-complete');
                                                  $('#bid-'+item.pid).prop('disabled', true);
                                                  $('#bid-'+item.pid).parent('.bid-button-index').css("cursor","not-allowed");
                                                  $('.endhide_bidding_'+item.pid).addClass('disabledbutton');
                                              });

                                             if(timeToSeconds($('#bid-time_bidding_'+item.pid).html())<timeToSeconds("00:00:11"))
                                             {
                                                $("#bid-time_bidding_"+item.pid).addClass("lasttime");
                                             }else{
                                                $("#bid-time_bidding_"+item.pid).removeClass("lasttime");
                                             }

													                           @if(Auth::check())
                                                      if($('#bid-time_bidding_'+item.pid).html()==="00:00:00")
                                                      {

                                                          $('#bid-'+item.pid).prop('disabled', true);
                                                          $('#bid-'+item.pid).parent('.bid-button-index').css("cursor","not-allowed");
                                                          $('.endhide_bidding_'+item.pid).addClass('disabledbutton');

                                                      }else{

                                                        $('#bid-'+item.pid).prop('disabled', false);
                                                        $('#bid-'+item.pid).parent('.bid-button-index').css("cursor","");
                                                        $('.endhide_bidding_'+item.pid).removeClass('disabledbutton');
                                                        $("#bid-user_"+item.pid).removeClass("ubid");

                                                      }

                                                      if(item.ui===uid){
                                                        
                                                        $('#bid-'+item.pid).prop('disabled', true);
                                                        $('#bid-'+item.pid).parent('.bid-button-index').css("cursor","not-allowed");
                                                        $("#bid-user_"+item.pid).addClass("ubid");

                                                      }
                                                     @endif

                                              $("#product_value_bid_bidding_"+item.pid).html(item.v);

                                              if(item.u===null)
                                              {
                                                $("#xuser_bidding_"+item.pid).html('----');
                                              }else{
                                                $("#xuser_bidding_"+item.pid).html(item.u);
                                              }
                                        
                                        });
                                    }
       });
  setTimeout(doStuff,1000);
}());
  
@if($pages=="pages.bidding")
(function doStuff2() {
  $.ajax({

                                  url: "{!! url('bidtransrecords/transactable') !!}?view="+{{$_GET['view']}},
                                  method: "GET",
                                  dataType: "html",
                                  success: function(results){
                                          console.log(results);
                                          $("#bidd_{{$_GET['view']}}").html(results);
                                        }
           });
  setTimeout(doStuff2,1000);
}());
    

    @endif

});
  

$(document).on('click','#spayclick', function() {
  $('form[name=form-productbuy-detailshop]').attr('action','{!! url('checkout') !!}');
  $('form[name=form-productbuy-detailshop]').append('<input type="hidden" name="paytype" value="spay" />');
  $('form[name=form-productbuy-detailshop]').submit();
});

<?php if(isset($_GET['page'])=="") { $p=1; }else{ $p=$_GET['page']; } ?> 


</script>

<script type="text/javascript">
  $(document).ready(function() {


@if(Auth::check())

$(document).on('click','.getitdone', function() {

   var pbidid = $(this).data('id');
   $.ajax({
                            url: "{!! url('bidtransrecords/checkyouwin') !!}",
                            method: "POST",
                            data: { pbidid : pbidid },
                            dataType: "html",
                            success: function(ress) {
                                    var message = jQuery.parseJSON(ress);
                                    if(message.winner==='win')
                                    {
                                        swal({
                                            title: "{!! html_entity_decode(Lang::get('script-text.bidendpopup1')) !!}",
                                            text: "{!! html_entity_decode(Lang::get('script-text.bidendpopup2')) !!}",
                                            type: "success",
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "{!! html_entity_decode(Lang::get('script-text.bidendpopup3')) !!}",
                                            closeOnConfirm: false
                                        }, function (isConfirm) {
                                            if (isConfirm) {
                                                swal({
                                                        title: "{!! html_entity_decode(Lang::get('script-text.bidendpopupwc1')) !!}",
                                                        text: "{!! html_entity_decode(Lang::get('script-text.bidendpopupwc2')) !!}",
                                                        type: "success",
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "{!! html_entity_decode(Lang::get('script-text.bidendpopupwc3')) !!}",
                                                        closeOnConfirm: false
                                                    }, function (isConfirm) {
                                                        if (isConfirm) {
                                                          window.location.href = "{!!url('winnercheckout?pwin=') !!}"+pbidid;
                                                        }
                                                    });
                                            } 
                                        });
                                    }
                                    else if(message.winner==='lose')
                                    {
                                        swal({
                                            title: "{!! html_entity_decode(Lang::get('script-text.bidendpopuplose1')) !!}",
                                            text: "{!! html_entity_decode(Lang::get('script-text.bidendpopuplose2')) !!}",
                                            type: "info",
                                            showCancelButton: true,
                                            confirmButtonColor: "#438afe",
                                            confirmButtonText: "S-Pay",
                                            cancelButtonColor: "#fe5722",
                                            cancelButtonText: "Bid",
                                            closeOnConfirm: false,
                                            closeOnCancel: false
                                        }, function (isConfirm) {
                                            if (isConfirm) {

                                                swal({
                                                        title: "{!! html_entity_decode(Lang::get('script-text.bidendpopuplosespay1')) !!}",
                                                        text: "{!! html_entity_decode(Lang::get('script-text.bidendpopuplosespay2')) !!}",
                                                        type: "success",
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "{!! html_entity_decode(Lang::get('script-text.bidendpopuplosespay3')) !!}",
                                                        closeOnConfirm: true
                                                    }, function (isConfirm) {

                                                           $.ajax
                                                                ({ 
                                                                    url: "{!! url('bidtransrecords/returnbidandspay') !!}",
                                                                    method: "POST",
                                                                    data: {"pbidid": pbidid, "selector" : "spay" },
                                                                    type: 'html',
                                                                    success: function(result)
                                                                    {
                                                                       var retbid = jQuery.parseJSON(result);
                                                                       //console.log(retbid);
                                                                       if(retbid.message==="success")
                                                                       {
                                              var cvaluebid = parseFloat(retbid.current_bid);  
                                              $( "#current_spay" ).html(commaSeparateNumber(cvaluebid));
                                               $("#bidremind"+pbidid).hide();
$('.notification-box').saveusernotify('{!! html_entity_decode(Lang::get('script-text.bidendpopuplosespay4')) !!} '+ retbid.spaygain +' {!! html_entity_decode(Lang::get('script-text.bidendpopuplosespay5')) !!}','icon bg-success','zmdi zmdi-plus-circle');
                                                                          

                                                                       }else if(retbid.message==="fail")
                                                                       {
                                                                          swal({
                                                                            title: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo1')) !!}',
                                                                            confirmButtonText: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo2')) !!}',
                                                                            text:'{!! html_entity_decode(Lang::get('script-text.bidendcannotdo3')) !!}'},
                                                                            function (isConfirm) {
                                                                              if (isConfirm) {
                                                                                
                                                                              }
                                                                            })
                                                                       }
                                                                       
                                                                    }

                                                                });
                                                           // window.location.href = "{!!url('user/register') !!}";
                                                        
                                                    });

                                            } else {

                                                swal({
                                                        title: "{!! html_entity_decode(Lang::get('script-text.bidendpopuplosebid1')) !!}",
                                                        text: "{!! html_entity_decode(Lang::get('script-text.bidendpopuplosebid2')) !!}",
                                                        type: "success",
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "{!! html_entity_decode(Lang::get('script-text.bidendpopuplosebid3')) !!}",
                                                        closeOnConfirm: true
                                                    }, function (isConfirm) {

                                                           $.ajax
                                                                ({ 
                                                                    url: "{!! url('bidtransrecords/returnbidandspay') !!}",
                                                                    method: "POST",
                                                                    data: {"pbidid": pbidid, "selector" : "bid" },
                                                                    type: 'html',
                                                                    success: function(result)
                                                                    {
                                                                       var retbid = jQuery.parseJSON(result);
                                                                       //console.log(retbid);
                                                                       if(retbid.message==="success")
                                                                       {
                                                                          var cvaluebid = parseFloat(retbid.current_bid);  
                                                                          $( "#current_bid" ).html(commaSeparateNumber(cvaluebid));
                                                                           $("#bidremind"+pbidid).hide();
$('.notification-box').saveusernotify('{!! html_entity_decode(Lang::get('script-text.bidendpopuplosebid4')) !!}'+ retbid.bidgain +'{!! html_entity_decode(Lang::get('script-text.bidendpopuplosebid5')) !!}','icon bg-success','zmdi zmdi-plus-circle');

                                                                       }else if(retbid.message==="fail")
                                                                       {
                                                                          swal({
                                                                            title: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo1')) !!}',
                                                                            confirmButtonText: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo2')) !!}',
                                                                            text:'{!! html_entity_decode(Lang::get('script-text.bidendcannotdo3')) !!}'},
                                                                            function (isConfirm) {
                                                                              if (isConfirm) {
                                                                                
                                                                              }
                                                                            })
                                                                       }
                                                                       
                                                                    }

                                                                });
                                                           // window.location.href = "{!!url('user/register') !!}";
                                                        
                                                    });
                                            }
                                        });
                                    }
                                }
                            });
  


   return false;

});


                          
@if(isset($userdet)&&$userdet->bid>0)
$('.biddingpusher').click(function(){


 var cid = $(this).parent().data('id');

 var productval = $(this).parent().data('productval');

 var boots = $(this).parent().data('boots');

 $(this).parent('.bid-button-index').css("cursor","not-allowed");

 var customer = $(this).parent().data('customer');

$(this).prop('disabled', true);

    $.ajax
    ({ 
        url: "{!! url('bidtransrecords/savepublic') !!}",
        data: {"customerNumber": customer, "products_bid_id" : cid, "bid" : boots, "product_value" : productval },
        type: 'post',
        success: function(result)
        {
           var res = jQuery.parseJSON(result);

           $('#bid-'+res.pid).prop('disabled', false);

           $('#bid-'+res.pid).parent('.bid-button-index').css("cursor","");

           if(res.mess==="success")
           {

              if(typeof res.clickdisable !== "undefined")
              {
                swal({
                  title: "{!! html_entity_decode(Lang::get('script-text.cannotbid')) !!}",
                  text: "{!! html_entity_decode(Lang::get('script-text.bidendcannotdo6')) !!}",
                  type: "warning",
                  showCancelButton: false,
                  confirmButtonClass: 'btn-warning waves-effect waves-light',
                  confirmButtonText: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo2')) !!}'
                });

                 $('#bid-'+res.pid).prop('disabled', false);
 
                 $('#bid-'+res.pid).parent('.bid-button-index').css("cursor","");

                return false;
              }

              var cvaluebid = parseFloat(res.current_bid);  
              $( "#current_bid" ).html(commaSeparateNumber(cvaluebid));

              var product_value_bid = parseFloat(res.product_value_bid);

              toastr["success"]("Bid ของคุณเหลือ "+commaSeparateNumber(cvaluebid)+" Bid");
             
           }else if(res.mess==="nobidegn"){

                                        swal({
                                            title: "{!! html_entity_decode(Lang::get('script-text.cannotbid')) !!}",
                                            text: "{!! html_entity_decode(Lang::get('script-text.biddone')) !!}",
                                            type: "error",
                                            showCancelButton: true,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "{!! html_entity_decode(Lang::get('script-text.bidsold')) !!}",
                                            cancelButtonText: "{!! html_entity_decode(Lang::get('script-text.bidsold2')) !!}",
                                            closeOnConfirm: true,
                                            closeOnCancel: false
                                        }, function (isConfirm) {
                                            if (isConfirm) {
                                                window.location.href = "{!!url('bidcheckout') !!}";
                                            }
                                        });



           }else if(res.mess==="fail")
           {
                                        swal({
                                          title: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo1')) !!}',
                                          confirmButtonText: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo2')) !!}',
                                          text:'{!! html_entity_decode(Lang::get('script-text.bidendcannotdo4')) !!}'},
                                          function (isConfirm) {
                                            if (isConfirm) {

                                            }
                                          });
          }
        }
    });
});
@else

$('.biddingpusher').click(function(){
swal({
      title: '{!! html_entity_decode(Lang::get('script-text.cannotbid')) !!}',
      confirmButtonText: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo1')) !!}',
      text:'{!! html_entity_decode(Lang::get('script-text.biddone')) !!}',
      showCancelButton: true,
      type:'error'},
      function (isConfirm) {
        if (isConfirm) {
          window.location.href = "{!!url('bidpackhome') !!}";
        }
      })
    

});
@endif
$(document).on('click','#addtowishlist', function() {
  var id = $(this).data('id');
  $.ajax({
        url: "{!! url('bidtransrecords/addwlists') !!}",
        method: "POST",
        data: { wid : id },
        dataType: "html",
        success: function(data) {

                //console.log(data);
                var data = jQuery.parseJSON(data);
                if(data.message==="success")
                {
                    $( ".fa-heart-o" ).css({ 'color': "red" });
                    $( "#current_wlists" ).html(data.rewishlists);

                }else if(data.message==="delsuccess")
                {

                     $( ".fa-heart-o" ).css({ 'color': "" });
                     $( "#current_wlists" ).html(data.rewishlists);                

                }else if(data.message==="fail")
                {
                                          swal({
                                          title: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo1')) !!}',
                                          confirmButtonText: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo2')) !!}',
                                          type: "error",
                                          text:'{!! html_entity_decode(Lang::get('script-text.bidendcannotdo5')) !!}'},
                                          function (isConfirm) {
                                            if (isConfirm) {

                                            }
                                          });
                }

            },
      });
});

$(document).on('click','.delcustom', function() {
  var id = $(this).data('id');
  $.ajax({
        url: "{!! url('bidtransrecords/autobidsave') !!}",
        method: "POST",
        data: { delid : id },
        dataType: "html",
        success: function(data) {

                //console.log(data);
                var data = jQuery.parseJSON(data);
                if(data.message==="success")
                {
                               var cvaluebid = parseFloat(data.current_bid);  
                                $( "#current_bid" ).html(commaSeparateNumber(cvaluebid));

                                swal({
                                              title: "{!! html_entity_decode(Lang::get('script-text.delautobid')) !!}",
                                              text: "{!! html_entity_decode(Lang::get('script-text.notidelautobid')) !!}",
                                              type: "success",
                                              showCancelButton: false,
                                              confirmButtonColor: "#DD6B55",
                                              closeOnConfirm: true,
                                              confirmButtonText: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo2')) !!}',
                                              closeOnCancel: false,
                                              function (isConfirm) {
                                            }
                                  });

                                //console.log(data);
@if($pages=="pages.bidding")
       $("#removeautobt")
        .prev('div.row')
        .remove()
        .end();
@endif

toastr["success"]("Bid ของคุณเหลือ "+commaSeparateNumber(cvaluebid)+" Bid");

                }else if(data.message==="fail")
                {
                                          swal({
                                          title: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo1')) !!}',
                                          confirmButtonText: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo2')) !!}',
                                          type: "error",
                                          text:'{!! html_entity_decode(Lang::get('script-text.cannotfound')) !!}'},
                                          function (isConfirm) {
                                            if (isConfirm) {

                                            }
                                          });
                }

            },
      });

});
//autobid
$('.autobid-button').on("click", function() {

      var bidadd = 0;

      if($(this).data('bidtype')==="custombid")
      {
          bidadd = $(this).siblings('input').val();
      }else
      {
          bidadd = $(this).data('add');
      }

      $.ajax({
        url: "{!! url('bidtransrecords/autobidsave') !!}",
        method: "POST",
        data: { products_bid_id : $(this).data('id'), addbid : bidadd, boots : $(this).data('boots') },
        dataType: "html",
        success: function(data) {

                //console.log(data);
                var data = jQuery.parseJSON(data);
                if(data.message==="success")
                {
                               var cvaluebid = parseFloat(data.current_bid);  
                                $( "#current_bid" ).html(commaSeparateNumber(cvaluebid));

                                swal({
                                              title: "{!! html_entity_decode(Lang::get('script-text.addabid')) !!}",
                                              text: "{!! html_entity_decode(Lang::get('script-text.addabid2')) !!}"+data.autobidlist.bid_remain+"{!! html_entity_decode(Lang::get('script-text.addabid3')) !!}"+data.autobidlist.boots,
                                              type: "success",
                                              showCancelButton: false,
                                              confirmButtonColor: "#DD6B55",
                                              closeOnConfirm: true,
                                              confirmButtonText: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo2')) !!}',
                                              closeOnCancel: false,
                                              function (isConfirm) {
                                            }
                                  });

                                //console.log(data);
@if($pages=="pages.bidding")
                                if(data.autobidlist.bid_remain>0) {

                                  var bidremaining = parseFloat(data.autobidlist.bid_remain);

                                  var bidtake = bidremaining/parseFloat(data.autobidlist.boots);

                                  var bidautoid = data.autobidlist.id;
       $("#removeautobt")
        .prev('div.row')
        .remove()
        .end();
      $( "#autobidd" ).after('<div class="row no-margin"> <div class="col-md-5"> <p class="text-muted">'+bidremaining+'</p> </div> <div class="col-md-4"> <p class="text-muted">'+bidtake+'</p> </div> <div class="col-md-3"> <button class="btn btn-icon waves-effect waves-light btn-danger m-b-5 delcustom" data-id="'+bidautoid+'"> <i class="fa fa-remove"></i> </button> </div>'); }

@endif


                }else if(data.message==="fail")
                {
                                          swal({
                                          title: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo1')) !!}',
                                          confirmButtonText: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo2')) !!}',
                                          type: "error",
                                          text:'{!! html_entity_decode(Lang::get('script-text.bidsolda')) !!}'},
                                          function (isConfirm) {
                                            if (isConfirm) {

                                            }
                                          });
                }

            },
      });
    }); 
@else
    $('.biddingpusher').click(function(){
     swal({
      title: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo5')) !!}',
      confirmButtonText: '{!! html_entity_decode(Lang::get('script-text.regisbutt')) !!}',
      text:'{!! html_entity_decode(Lang::get('script-text.regisbutt2')) !!}'},
      function (isConfirm) {
        if (isConfirm) {
          window.location.href = "{!!url('user/register') !!}";
        }
      })
    
     });
$('.autobid-button').on("click", function() {
      swal({
      title: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo5')) !!}',
      confirmButtonText: '{!! html_entity_decode(Lang::get('script-text.regisbutt')) !!}',
      text:'{!! html_entity_decode(Lang::get('script-text.regisbutt2')) !!}'},
      function (isConfirm) {
        if (isConfirm) {
          window.location.href = "{!!url('user/register') !!}";
        }
      })
    
     });

@endif







    // Enable pusher logging - don't include this in production
    //Pusher.logToConsole = true;

    var pusher = new Pusher('8b5cebf10f40a2986078', {
      cluster: 'ap1',
      encrypted: true
    });

        var channel = pusher.subscribe('bidmain');
        var channelall = pusher.subscribe('bidmain');
          channelall.bind_all(function(chnal) {
                
                channel.bind(chnal, function(data) {

                    if(typeof data.bidnewtime !== "undefined")
                    {
                        /*$('#bid-time_'+chnal).countdown(new Date(data.bidnewtime.date) ).on('finish.countdown', function(data) {  
                          $('.bid-end-date_'+chnal).html('{!! html_entity_decode(Lang::get('script-text.closebid')) !!}');
                          $('.endhide_'+chnal).hide();
                        });*/
                    }

                    if(data.bidcompleted) 
                    {
                      $("#bid-time_bidding_"+data.pbidid).html('00:00:00');
                      $('.bid-end-date_bidding_'+data.pbidid).html('{!! html_entity_decode(Lang::get('script-text.closebid')) !!}');
                      $('.endhide_bidding_'+data.pbidid).hide();

                        @if(Auth::check())
                          $.ajax({
                            url: "{!! url('bidtransrecords/checkyouwin') !!}",
                            method: "POST",
                            data: { pbidid : data.pbidid },
                            dataType: "html",
                            success: function(ress) {
                                    var message = jQuery.parseJSON(ress);
                                    if(message.winner==='win')
                                    {
                                        swal({
                                            title: "{!! html_entity_decode(Lang::get('script-text.bidendpopuplose1')) !!}",
                                            text: "{!! html_entity_decode(Lang::get('script-text.bidendpopupwc2')) !!}",
                                            type: "success",
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "{!! html_entity_decode(Lang::get('script-text.bidendpopupwc1')) !!}",
                                            closeOnConfirm: false
                                        }, function (isConfirm) {
                                            if (isConfirm) {
                                                swal({
                                                        title: "{!! html_entity_decode(Lang::get('script-text.bidendpopupwc1')) !!}",
                                                        text: "{!! html_entity_decode(Lang::get('script-text.bidendpopupwc2')) !!} {!! html_entity_decode(Lang::get('script-text.getproducts')) !!}",
                                                        type: "success",
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "{!! html_entity_decode(Lang::get('script-text.gocheck')) !!}",
                                                        closeOnConfirm: false
                                                    }, function (isConfirm) {
                                                        if (isConfirm) {
                                                          window.location.href = "{!!url('winnercheckout?pwin=') !!}"+data.pbidid;
                                                          $('.notification-box').saveusernotify('{!! html_entity_decode(Lang::get('script-text.youwinbid')) !!}','icon bg-success','zmdi zmdi-star-circle');
                                                        }
                                                    });
                                            } 
                                        });
                                    }
                                    else if(message.winner==='lose')
                                    {
                                        swal({
                                            title: "{!! html_entity_decode(Lang::get('script-text.bidendpopuplose1')) !!}",
                                            text: "{!! html_entity_decode(Lang::get('script-text.bidendpopuplose2')) !!}",
                                            type: "info",
                                            showCancelButton: true,
                                            confirmButtonColor: "#438afe",
                                            confirmButtonText: "{!! html_entity_decode(Lang::get('script-text.spaytext')) !!}",
                                            cancelButtonColor: "#fe5722",
                                            cancelButtonText: "{!! html_entity_decode(Lang::get('script-text.bidtext')) !!}",
                                            closeOnConfirm: false,
                                            closeOnCancel: false
                                        }, function (isConfirm) {
                                            if (isConfirm) {

                                                swal({
                                                        title: "{!! html_entity_decode(Lang::get('script-text.bidendpopuplosespay1')) !!}",
                                                        text: "{!! html_entity_decode(Lang::get('script-text.bidendpopuplosespay2')) !!}",
                                                        type: "success",
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "{!! html_entity_decode(Lang::get('script-text.bidendpopuplosespay3')) !!}",
                                                        closeOnConfirm: true
                                                    }, function (isConfirm) {

                                                           $.ajax
                                                                ({ 
                                                                    url: "{!! url('bidtransrecords/returnbidandspay') !!}",
                                                                    method: "POST",
                                                                    data: {"pbidid": message.pbidid, "selector" : "spay" },
                                                                    type: 'html',
                                                                    success: function(result)
                                                                    {
                                                                       var retbid = jQuery.parseJSON(result);
                                                                       //console.log(retbid);
                                                                       if(retbid.message==="success")
                                                                       {
                                                                          var cvaluebid = parseFloat(retbid.current_bid);  
                                                                          $( "#current_spay" ).html(commaSeparateNumber(cvaluebid));
$('.notification-box').saveusernotify('คุณได้รับ '+ retbid.spaygain +' S-Pay','icon bg-success','zmdi zmdi-plus-circle');

                                                                       }else if(retbid.message==="fail")
                                                                       {
                                                                          swal({
                                                                            title: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo1')) !!}',
                                                                            confirmButtonText: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo2')) !!}',
                                                                            text:'{!! html_entity_decode(Lang::get('script-text.bidendcannotdo3')) !!}'},
                                                                            function (isConfirm) {
                                                                              if (isConfirm) {
                                                                                
                                                                              }
                                                                            })
                                                                       }
                                                                       
                                                                    }

                                                                });
                                                           // window.location.href = "{!!url('user/register') !!}";
                                                        
                                                    });

                                            } else {

                                                swal({
                                                        title: "{!! html_entity_decode(Lang::get('script-text.bidendcannotdo1')) !!}",
                                                        text: "{!! html_entity_decode(Lang::get('script-text.bidendpopuplosebid2')) !!}",
                                                        type: "success",
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "{!! html_entity_decode(Lang::get('script-text.bidendpopuplosebid3')) !!}",
                                                        closeOnConfirm: true
                                                    }, function (isConfirm) {

                                                           $.ajax
                                                                ({ 
                                                                    url: "{!! url('bidtransrecords/returnbidandspay') !!}",
                                                                    method: "POST",
                                                                    data: {"pbidid": message.pbidid, "selector" : "bid" },
                                                                    type: 'html',
                                                                    success: function(result)
                                                                    {
                                                                       var retbid = jQuery.parseJSON(result);
                                                                       //console.log(retbid);
                                                                       if(retbid.message==="success")
                                                                       {
                                                                          var cvaluebid = parseFloat(retbid.current_bid);  
                                                                          $( "#current_bid" ).html(commaSeparateNumber(cvaluebid));
$('.notification-box').saveusernotify('{!! html_entity_decode(Lang::get('script-text.bidendpopuplosebid4')) !!}'+ retbid.bidgain +' {!! html_entity_decode(Lang::get('script-text.bidtext')) !!}','icon bg-success','zmdi zmdi-plus-circle');
                                                                       }else if(retbid.message==="fail")
                                                                       {
                                                                          swal({
                                                                            title: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo1')) !!}',
                                                                            confirmButtonText: '{!! html_entity_decode(Lang::get('script-text.bidendcannotdo2')) !!}',
                                                                            text:'{!! html_entity_decode(Lang::get('script-text.bidendcannotdo3')) !!}'},
                                                                            function (isConfirm) {
                                                                              if (isConfirm) {
                                                                                
                                                                              }
                                                                            })
                                                                       }
                                                                       
                                                                    }

                                                                });
                                                           // window.location.href = "{!!url('user/register') !!}";
                                                        
                                                    });
                                            }
                                        });
                                    }
                                }
                            });
                        @endif
                    }
                    //console.log(data.bidnewtime.date);
                    $( "#product_value_bid_"+chnal ).html( data.product_value_bid );
                    $( "#xuser_"+chnal ).html( data.biduser );
                });

          });




});
</script>