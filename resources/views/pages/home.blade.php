
      <!-- featured category fashion -->
      <div class="category-product">
        <div class="navbar nav-menu">
          <div class="navbar-collapse">
            <ul class="nav navbar-nav">
              <li>
                <div class="new_title">
                  <h2><strong>กำลัง</strong> ประมูล</h2>
                </div>
              </li>
              <li class="active"><a data-toggle="tab" href="#tab-1">แหวนทอง </a> </li>
              <li class="divider"></li>
              <li><a data-toggle="tab" href="#tab-2">สร้อยคอ</a> </li>
              <li class="divider"></li>
              <li><a data-toggle="tab" href="#tab-3">สร้อยข้อมือ</a> </li>
              <li class="divider"></li>
              <li><a data-toggle="tab" href="#tab-4">กำไล</a> </li>
            </ul>
          </div>
          <!-- /.navbar-collapse --> 

        </div>
        <div class="product-bestseller">
          <div class="product-bestseller-content">
            <div class="product-bestseller-list">
              <div class="tab-container"> 
                <!-- tab product -->
                <div class="tab-panel active" id="tab-1">
                  <div class="category-products">
                    <ul class="products-grid">
                      @include('pages/auction')
                    </ul>
                  </div>
                </div>
                <!-- tab product -->
                <div class="tab-panel" id="tab-2">
                  <div class="category-products">
                    <ul class="products-grid">
                      @include('pages/auction')
                    </ul>
                  </div>
                </div>
                <div class="tab-panel" id="tab-3">
                  <div class="category-products">
                    <ul class="products-grid">
                     @include('pages/auction')
                    </ul>
                  </div>
                </div>
                <div class="tab-panel" id="tab-4">
                  <div class="category-products">
                    <ul class="products-grid">
                      @include('pages/auction')
                    </ul>
                  </div>
                </div>
                <div class="tab-panel" id="tab-5">
                  <div class="category-products">
                    <ul class="products-grid">
                      @include('pages/auction')
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>

  
  <!-- bestsell Slider -->
  <section class="bestsell-pro">
    <div class="container">
      <div class="slider-items-products">
        <div class="bestsell-block">
          <div id="bestsell-slider" class="product-flexslider hidden-buttons">
            <div class="home-block-inner">
              <div class="block-title">
                <h2><em> GOGOLDRUN.COM</em></h2>
              </div>
              <div class="pretext"></div>
              <a href="{{url('user/register')}}" class="view_more_bnt">สมัครสมาชิก</a> </div>
            <div class="slider-items slider-width-col4 products-grid block-content">
              @include('pages/auction_spacial')
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <!-- End Bestsell Slider --> 
  <div class="bottom-banner-section">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6">
          <div class="bottom-banner-img1"> <a href="#"> <img src="{!! asset('frontend/punbid/') !!}/images/ads-01.jpg" alt="bottom banner">
            <div class="bottom-img-info1">
              <h3>ซื้อบิด</h3>
              <span class="line"></span> </div>
            </a></div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="bottom-banner-img1"> <a href="#"> <img src="{!! asset('frontend/punbid/') !!}/images/ads-02.jpg" alt="bottom banner">
            <div class="bottom-img-info1">
              <h3>ผู้ชนะประมูล</h3>
              <span class="line"></span> </div>
            </a></div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="bottom-banner-img1"> <a href="#"> <img src="{!! asset('frontend/punbid/') !!}/images/ads-03.jpg" alt="bottom banner"><span class="banner-overly"></span>
            <div class="bottom-img-info1">
              <h3>ผู้แพ้อย่าเสียใจ</h3>
              <span class="line"></span> </div>
            </a></div>
        </div>
        
      </div>
    </div>
  </div>
  <!-- featured Slider -->
  <section class="featured-pro">
    <div class="container">
      <div class="slider-items-products">
        <div class="featured-block">
          <div id="featured-slider" class="product-flexslider hidden-buttons">
            <div class="home-block-inner">
              <div class="block-title">
                <h2><em>ปิดประมูลแล้ว</em></h2>
              </div>
              <div class="pretext">ผู้ชนะประมูล</div>
              <a href="grid.php" class="view_more_bnt">ดูทั้งหมด</a> </div>
            <div class="slider-items slider-width-col4 products-grid block-content">
@include('pages/auction_spacial')
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- End featured Slider --> 
  
  <!-- bottom banner section -->
  <div class="bottom-banner-section">
    <div class="container">
      <div class="row">
        <div class="col-md-4 col-sm-6">
          <div class="bottom-banner-img1"> <a href="#"> <img src="{!! asset('frontend/punbid/') !!}/images/ads-01.jpg" alt="bottom banner">
            <div class="bottom-img-info1">
              <h3>Handbags</h3>
              <span class="line"></span> </div>
            </a></div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="bottom-banner-img1"> <a href="#"> <img src="{!! asset('frontend/punbid/') !!}/images/ads-02.jpg" alt="bottom banner">
            <div class="bottom-img-info1">
              <h3>Electronics</h3>
              <span class="line"></span> </div>
            </a></div>
        </div>
        <div class="col-md-4 col-sm-6">
          <div class="bottom-banner-img1"> <a href="#"> <img src="{!! asset('frontend/punbid/') !!}/images/ads-01.jpg" alt="bottom banner"><span class="banner-overly"></span>
            <div class="bottom-img-info1">
              <h3>Men Shoes</h3>
              <span class="line"></span> </div>
            </a></div>
        </div>
        
      </div>
    </div>
  </div>