@extends('layouts.app')

@section('content')
<div class="page-content row">
	 
	 
 	<div class="page-content-wrapper m-t">   

<div class="sbox "> 
	<div class="sbox-title"> 

	 <h3> {!! $pageTitle !!} <small>{!! $pageNote !!}</small></h3>

	 	<div class="sbox-tools">
	   		<a href="{!! URL::to('pages?return='.$return) !!}" class="tips btn btn-xs btn-white pull-right" title="{!! html_entity_decode(Lang::get('core.btn_back') !!}"><i class="fa fa-arrow-circle-left"></i>&nbsp;{!! html_entity_decode(Lang::get('core.btn_back') !!}</a>
			
			@if($access['is_add'] ==1)
	   		<a href="{!! URL::to('pages/update/'.$id.'?return='.$return) !!}" class="tips btn btn-xs btn-white pull-right" title="{!! html_entity_decode(Lang::get('core.btn_edit') !!}"><i class="fa fa-edit"></i>&nbsp;{!! html_entity_decode(Lang::get('core.btn_edit') !!}</a>
			@endif 
		</div>
	</div>
	<div class="sbox-content" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Title', (isset($fields['title']['language'])? $fields['title']['language'] : array())) !!}</td>
						<td>{!! $row->title!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Slug / Alias', (isset($fields['alias']['language'])? $fields['alias']['language'] : array())) !!}</td>
						<td>{!! $row->alias!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Filename', (isset($fields['filename']['language'])? $fields['filename']['language'] : array())) !!}</td>
						<td>{!! $row->filename!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Status', (isset($fields['status']['language'])? $fields['status']['language'] : array())) !!}</td>
						<td>{!! $row->status!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Allow Guest', (isset($fields['allow_guest']['language'])? $fields['allow_guest']['language'] : array())) !!}</td>
						<td>{!! $row->allow_guest!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Template', (isset($fields['template']['language'])? $fields['template']['language'] : array())) !!}</td>
						<td>{!! $row->template!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Metakey', (isset($fields['metakey']['language'])? $fields['metakey']['language'] : array())) !!}</td>
						<td>{!! $row->metakey!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Metadesc', (isset($fields['metadesc']['language'])? $fields['metadesc']['language'] : array())) !!}</td>
						<td>{!! $row->metadesc!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Slide?', (isset($fields['slide']['language'])? $fields['slide']['language'] : array())) !!}</td>
						<td>{!! $row->slide!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Homepage ?', (isset($fields['default']['language'])? $fields['default']['language'] : array())) !!}</td>
						<td>{!! $row->default!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Shop', (isset($fields['shop']['language'])? $fields['shop']['language'] : array())) !!}</td>
						<td>{!! $row->shop!!} </td>
						
					</tr>
				
			</tbody>	
		</table>   
	
	</div>
</div>	

	</div>
</div>
	  
@stop