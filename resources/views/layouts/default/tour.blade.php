<script type="text/javascript">
  $(document).ready(function() {
                var placementRight = 'right';
                var placementLeft = 'left';

                // Define the tour!
                var tourlogin = {
                    id: "my-login",
                    i18n: {
                        nextBtn: "{!! html_entity_decode(Lang::get('script-text.nextBtn')) !!}",
                        prevBtn: "{!! html_entity_decode(Lang::get('script-text.prevBtn')) !!}",
                        doneBtn: "{!! html_entity_decode(Lang::get('script-text.doneBtn')) !!}"
                     },

                    steps: [
                        {
                            target: "loginbar",
                            title: "{!! html_entity_decode(Lang::get('tourlog.1')) !!}",
                            content: "{!! html_entity_decode(Lang::get('tourlog.2')) !!}",
                            placement: placementLeft,
                            yOffset: 10
                        },
                        {
                            target: 'loginbar',
                            title: "",
                            content: "",
                            placement: 'bottom',
                            zindex: 999,
                            onShow: function() {
                                    $element = $( "#loginbar" );
                                    $element.trigger( "click" );

                                    hopscotch.endTour(true); 

                                    var tourP2 = {
                                    id: "tutp2",
                                    steps: [
                                      {
                                        title: "{!! html_entity_decode(Lang::get('tourlog.3')) !!}",
                                        content: "{!! html_entity_decode(Lang::get('tourlog.4')) !!}",
                                        target: 'tlogin',
                                        placement: "left",
                                        nextOnTargetClick: true,
                                        delay: 1000
                                      },
                                      {
                                        title: "{!! html_entity_decode(Lang::get('tourlog.5')) !!}",
                                        content: "{!! html_entity_decode(Lang::get('tourlog.6')) !!}",
                                        target: 'tpass',
                                        placement: "left",
                                        nextOnTargetClick: true
                                      },
                                      {
                                        title: "{!! html_entity_decode(Lang::get('tourlog.7')) !!}",
                                        content: "{!! html_entity_decode(Lang::get('tourlog.8')) !!}",
                                        target: 'tsub',
                                        placement: "bottom",
                                        nextOnTargetClick: true
                                      },
                                      {
                                        title: "{!! html_entity_decode(Lang::get('tourlog.9')) !!}",
                                        content: "{!! html_entity_decode(Lang::get('tourlog.10')) !!}",
                                        target: 'tfacebook',
                                        placement: "top",
                                        nextOnTargetClick: true
                                      }
                                    ],
                    showPrevButton: true,
                    nextOnTargetClick: true
                                  }
                                  // Start the nested tour
                                  hopscotch.startTour(tourP2);

                            },
                        }
                        ],
                    showPrevButton: true,
                    nextOnTargetClick: true
                };  

                // Define the register!
                var tourregister = {
                    id: "my-regis",
                    i18n: {
                       nextBtn: "{!! html_entity_decode(Lang::get('script-text.nextBtn')) !!}",
                        prevBtn: "{!! html_entity_decode(Lang::get('script-text.prevBtn')) !!}",
                        doneBtn: "{!! html_entity_decode(Lang::get('script-text.doneBtn')) !!}"
                     },
                    steps: [
                        {
                            target: "registerbar",
                            title: "{!! html_entity_decode(Lang::get('tourlog.11')) !!}",
                            content: "{!! html_entity_decode(Lang::get('tourlog.12')) !!}",
                            placement: placementLeft,
                            yOffset: 10
                        },
                        {
                            target: 'registerbar',
                            title: "",
                            content: "",
                            placement: 'bottom',
                            zindex: 999,
                            onShow: function() {
                                    $element = $( "#registerbar" );
                                    $element.trigger( "click" );
                                    
                                    hopscotch.endTour(true); 

                                    var tourP3 = {
                                    id: "tutp3",
                                    steps: [
                                      {
                                        title: "{!! html_entity_decode(Lang::get('tourlog.13')) !!}",
                                        content: "{!! html_entity_decode(Lang::get('tourlog.14')) !!}",
                                        target: 'rname',
                                        placement: "left",
                                        delay: 1000,
                                        nextOnTargetClick: true
                                      },
                                      {
                                        title: "{!! html_entity_decode(Lang::get('tourlog.15')) !!}",
                                        content: "",
                                        target: 'rdate',
                                        placement: "left",
                                        nextOnTargetClick: true
                                      },
                                      {
                                        title: "{!! html_entity_decode(Lang::get('tourlog.16')) !!}",
                                        content: "{!! html_entity_decode(Lang::get('tourlog.17')) !!}",
                                        target: 'rpassword',
                                        placement: "left",
                                        nextOnTargetClick: true
                                      },
                                      {
                                        title: "{!! html_entity_decode(Lang::get('tourlog.18')) !!}",
                                        content: "{!! html_entity_decode(Lang::get('tourlog.19')) !!}",
                                        target: 'rsub',
                                        placement: "bottom",
                                        nextOnTargetClick: true
                                      },
                                      {
                                        title: "{!! html_entity_decode(Lang::get('tourlog.20')) !!}",
                                        content: "{!! html_entity_decode(Lang::get('tourlog.21')) !!}",
                                        target: 'ridfacebook',
                                        placement: "top",
                                        nextOnTargetClick: true
                                      }
                                    ],
                    showPrevButton: true,
                    nextOnTargetClick: true
                                  }
                                  // Start the nested tour
                                  hopscotch.startTour(tourP3);
                            },
                        }
                        ],
                    showPrevButton: true
                }; 

                // Define the tour!
                var tourbidpack = {
                    id: "my-buybid",
                    i18n: {
                        nextBtn: "{!! html_entity_decode(Lang::get('script-text.nextBtn')) !!}",
                        prevBtn: "{!! html_entity_decode(Lang::get('script-text.prevBtn')) !!}",
                        doneBtn: "{!! html_entity_decode(Lang::get('script-text.doneBtn')) !!}"
                     },
                    steps: [
                                  {
                                    title: "{!! html_entity_decode(Lang::get('tourlog.22')) !!}",
                                    content: "{!! html_entity_decode(Lang::get('tourlog.23')) !!}",
                                    target: "howbuybid",
                                    placement: "bottom",
                                    multipage: true,
                                    onNext: function() {
                                      window.location = "{!!url('bidpackhome') !!}"

                                    }
                                  },
                                  {
                                    title: "{!! html_entity_decode(Lang::get('tourlog.24')) !!}",
                                    content: "{!! html_entity_decode(Lang::get('tourlog.25')) !!}",
                                    target: document.querySelector("#buybid"),
                                    placement: "bottom",
                                    nextOnTargetClick: true
                                  }
                            ],
                    showPrevButton: true,
                    nextOnTargetClick: true
                  };


                
                    if (hopscotch.getState() === "my-buybid:1") {
                        try{

                             var qbid = document.querySelectorAll(".rs_incre_decre_btns");
                             var bbid = document.querySelectorAll(".buybid_width");
                            
                             var tourbidpack2 = {
                              id: "my-buybid2",
                                i18n: {
                                    nextBtn: "{!! html_entity_decode(Lang::get('script-text.nextBtn')) !!}",
                                    prevBtn: "{!! html_entity_decode(Lang::get('script-text.prevBtn')) !!}",
                                    doneBtn: "{!! html_entity_decode(Lang::get('script-text.doneBtn')) !!}"
                                 },
                              steps: [
                                            {
                                              title: "{!! html_entity_decode(Lang::get('tourlog.26')) !!}",
                                              content: "{!! html_entity_decode(Lang::get('tourlog.27')) !!}",
                                              target: "buybid",
                                              placement: "top",
                                              nextOnTargetClick: true
                                            },
                                            {
                                              title: "{!! html_entity_decode(Lang::get('tourlog.28')) !!}",
                                              content: "{!! html_entity_decode(Lang::get('tourlog.29')) !!}",
                                              target: qbid[1],
                                              placement: "right",
                                              xOffset: "center",
                                              arrowOffset: "top",
                                              nextOnTargetClick: true
                                            },
                                            {
                                              title: "{!! html_entity_decode(Lang::get('tourlog.30')) !!}",
                                              content: "{!! html_entity_decode(Lang::get('tourlog.31')) !!}",
                                              target: bbid[1],
                                              placement: "bottom",
                                              xOffset: "center",
                                              arrowOffset: "center",
                                              nextOnTargetClick: true
                                            }
                              ],
                    showPrevButton: true,
                    nextOnTargetClick: true

                            };

                            hopscotch.startTour(tourbidpack2);

                        }catch(e){
                            hopscotch.endTour(true); 
                        }
                    }
                




    /*var calloutMgr = hopscotch.getCalloutManager();
    calloutMgr.createCallout({
      id: 'tlogin1',
      target: 'tlogin',
      placement: 'bottom',
      title: 'Now you can share images &amp; files!',
      content: 'Share a project you\'re proud of, a photo from a recent event, or an interesting presentation.'
    });*/

                // Start the tour!
    $(document).on('click',("#howtologin"), function() {
          hopscotch.endTour(true); 
          hopscotch.startTour(tourlogin);
        
    });
    $(document).on('click',("#howtoregister"), function() {
          hopscotch.endTour(true); 
          hopscotch.startTour(tourregister);
        
    });
    $(document).on('click',("#howtobidpack"), function() {
          hopscotch.endTour(true); 
          hopscotch.startTour(tourbidpack);
          
    });

});
</script>