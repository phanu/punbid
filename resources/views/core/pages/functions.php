<?php
/**
 * Theme sprecific functions and definitions
 */

/* Theme setup section
------------------------------------------------------------------- */

// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) $content_width = 1170; /* pixels */

// Add theme specific actions and filters
// Attention! Function were add theme specific actions and filters handlers must have priority 1
if ( !function_exists( 'kingler_theme_theme_setup' ) ) {
	add_action( 'kingler_theme_action_before_init_theme', 'kingler_theme_theme_setup', 1 );
	function kingler_theme_theme_setup() {

		// Register theme menus
		add_filter( 'kingler_theme_filter_add_theme_menus',		'kingler_theme_add_theme_menus' );

		// Register theme sidebars
		add_filter( 'kingler_theme_filter_add_theme_sidebars',	'kingler_theme_add_theme_sidebars' );

		// Set options for importer
		add_filter( 'kingler_theme_filter_importer_options',		'kingler_theme_set_importer_options' );

		// Add theme specified classes into the body
		add_filter( 'body_class', 'kingler_theme_body_classes' );

		// Set list of the theme required plugins
		kingler_theme_storage_set('required_plugins', array( /*
			'booked',
			'buddypress',		// Attention! This slug used to install both BuddyPress and bbPress
			'calcfields',*/
			'essgrids',
//			'instagram_feed',
//			'instagram_widget',
//			'learndash',
//			'mailchimp',
//			'mega_main_menu',
//			'responsive_poll',
			'revslider',
			'tribe_events',
//			'trx_donations',
			'trx_utils',
			'visual_composer',
			'woocommerce',
			'html5_jquery_audio_player'
			)
		);
		
	}
}




// Add/Remove theme nav menus
if ( !function_exists( 'kingler_theme_add_theme_menus' ) ) {
	//add_filter( 'kingler_theme_filter_add_theme_menus', 'kingler_theme_add_theme_menus' );
	function kingler_theme_add_theme_menus($menus) {
		//For example:
		//$menus['menu_footer'] = esc_html__('Footer Menu', 'kingler-theme');
		//if (isset($menus['menu_panel'])) unset($menus['menu_panel']);
		return $menus;
	}
}


// Add theme specific widgetized areas
if ( !function_exists( 'kingler_theme_add_theme_sidebars' ) ) {
	//add_filter( 'kingler_theme_filter_add_theme_sidebars',	'kingler_theme_add_theme_sidebars' );
	function kingler_theme_add_theme_sidebars($sidebars=array()) {
		if (is_array($sidebars)) {
			$theme_sidebars = array(
				'sidebar_main'		=> esc_html__( 'Main Sidebar', 'kingler-theme' ),
//				'sidebar_outer'		=> esc_html__( 'Outer Sidebar', 'kingler-theme' ),
				'sidebar_footer'	=> esc_html__( 'Footer Sidebar', 'kingler-theme' )
			);
			if (function_exists('kingler_theme_exists_woocommerce') && kingler_theme_exists_woocommerce()) {
				$theme_sidebars['sidebar_cart']  = esc_html__( 'WooCommerce Cart Sidebar', 'kingler-theme' );
			}
			$sidebars = array_merge($theme_sidebars, $sidebars);
		}
		return $sidebars;
	}
}


// Add theme specified classes into the body
if ( !function_exists('kingler_theme_body_classes') ) {
	//add_filter( 'body_class', 'kingler_theme_body_classes' );
	function kingler_theme_body_classes( $classes ) {

		$classes[] = 'kingler_theme_body';
		$classes[] = 'body_style_' . trim(kingler_theme_get_custom_option('body_style'));
		$classes[] = 'body_' . (kingler_theme_get_custom_option('body_filled')=='yes' ? 'filled' : 'transparent');
		$classes[] = 'theme_skin_' . trim(kingler_theme_get_custom_option('theme_skin'));
		$classes[] = 'article_style_' . trim(kingler_theme_get_custom_option('article_style'));
		
		$blog_style = kingler_theme_get_custom_option(is_singular() && !kingler_theme_storage_get('blog_streampage') ? 'single_style' : 'blog_style');
		$classes[] = 'layout_' . trim($blog_style);
		$classes[] = 'template_' . trim(kingler_theme_get_template_name($blog_style));
		
		$body_scheme = kingler_theme_get_custom_option('body_scheme');
		if (empty($body_scheme)  || kingler_theme_is_inherit_option($body_scheme)) $body_scheme = 'original';
		$classes[] = 'scheme_' . $body_scheme;

		$top_panel_position = kingler_theme_get_custom_option('top_panel_position');
		if (!kingler_theme_param_is_off($top_panel_position)) {
			$classes[] = 'top_panel_show';
			$classes[] = 'top_panel_' . trim($top_panel_position);
		} else 
			$classes[] = 'top_panel_hide';
		$classes[] = kingler_theme_get_sidebar_class();

		if (kingler_theme_get_custom_option('show_video_bg')=='yes' && (kingler_theme_get_custom_option('video_bg_youtube_code')!='' || kingler_theme_get_custom_option('video_bg_url')!=''))
			$classes[] = 'video_bg_show';

		if (kingler_theme_get_theme_option('page_preloader')!='')
			$classes[] = 'preloader';

		return $classes;
	}
}


// Set theme specific importer options
if ( !function_exists( 'kingler_theme_set_importer_options' ) ) {
	//add_filter( 'kingler_theme_filter_importer_options',	'kingler_theme_set_importer_options' );
	function kingler_theme_set_importer_options($options=array()) {
		if (is_array($options)) {
			$options['debug'] = kingler_theme_get_theme_option('debug_mode')=='yes';
			$options['domain_dev'] = 'kingler.themerex.net';
			$options['domain_demo'] = 'kingler.themerex.net';
			$options['menus'] = array(
				'menu-main'	  => esc_html__('Main menu', 'kingler-theme'),
				'menu-user'	  => esc_html__('User menu', 'kingler-theme'),
				'menu-footer' => esc_html__('Footer menu', 'kingler-theme'),
				'menu-outer'  => esc_html__('Main menu', 'kingler-theme')
			);
			$options['file_with_attachments'] = array(				// Array with names of the attachments
	//			'http://some.cloud.net/theme_name/uploads.zip',		// Name of the remote file with attachments
	//			'demo/uploads.zip',									// Name of the local file with attachments
				'http://kingler.themerex.net/wp-content/imports/uploads.001',
				'http://kingler.themerex.net/wp-content/imports/uploads.002',
				'http://kingler.themerex.net/wp-content/imports/uploads.003',
				'http://kingler.themerex.net/wp-content/imports/uploads.004',
				'http://kingler.themerex.net/wp-content/imports/uploads.005',
				'http://kingler.themerex.net/wp-content/imports/uploads.006',
				'http://kingler.themerex.net/wp-content/imports/uploads.007'
			);
			$options['attachments_by_parts'] = true;				// Files above are parts of single file - large media archive. They are must be concatenated in one file before unpacking
		}
	
	}
}


/* Include framework core files
------------------------------------------------------------------- */
// If now is WP Heartbeat call - skip loading theme core files (to reduce server and DB uploads)
// Remove comments below only if your theme not work with own post types and/or taxonomies
//if (!isset($_POST['action']) || $_POST['action']!="heartbeat") {
	get_template_part('fw/loader');
//}


function bbp_save_extra_fields( $post_id, $post, $update ) {

		if(!isset($_POST['b_phonenumber'])||$_POST['b_phonenumber']!="")
		{
		update_post_meta( $post_id, '_bbp_topic_id', $post_id );
		update_post_meta( $post_id, '_bbp_last_active_id', $post_id );
		update_post_meta( $post_id, '_bbp_last_active_time', get_post_field( 'post_date', $post_id, 'db' ));
		}
}

add_action ( 'wp_insert_post', 'bbp_save_extra_fields', 10, 1 );
add_action ( 'bbp_edit_topic', 'bbp_save_extra_fields', 10, 1 );

function custom_bbp_show_lead_topic( $show_lead ) {
  echo '111';
}
 


add_filter('bbp_template_after_replies_loop', 'custom_bbp_show_lead_topic' );

add_action('topicmeta','hello_world');

function hello_world() {echo "<li>hello world</li>";}
?>