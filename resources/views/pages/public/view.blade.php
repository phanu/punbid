<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {!! $pageTitle !!} <small> {!! $pageNote !!} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Title', (isset($fields['title']['language'])? $fields['title']['language'] : array())) !!}</td>
						<td>{!! $row->title!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Slug / Alias', (isset($fields['alias']['language'])? $fields['alias']['language'] : array())) !!}</td>
						<td>{!! $row->alias!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Filename', (isset($fields['filename']['language'])? $fields['filename']['language'] : array())) !!}</td>
						<td>{!! $row->filename!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Status', (isset($fields['status']['language'])? $fields['status']['language'] : array())) !!}</td>
						<td>{!! $row->status!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Allow Guest', (isset($fields['allow_guest']['language'])? $fields['allow_guest']['language'] : array())) !!}</td>
						<td>{!! $row->allow_guest!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Template', (isset($fields['template']['language'])? $fields['template']['language'] : array())) !!}</td>
						<td>{!! $row->template!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Metakey', (isset($fields['metakey']['language'])? $fields['metakey']['language'] : array())) !!}</td>
						<td>{!! $row->metakey!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Metadesc', (isset($fields['metadesc']['language'])? $fields['metadesc']['language'] : array())) !!}</td>
						<td>{!! $row->metadesc!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Slide?', (isset($fields['slide']['language'])? $fields['slide']['language'] : array())) !!}</td>
						<td>{!! $row->slide!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Homepage ?', (isset($fields['default']['language'])? $fields['default']['language'] : array())) !!}</td>
						<td>{!! $row->default!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Shop', (isset($fields['shop']['language'])? $fields['shop']['language'] : array())) !!}</td>
						<td>{!! $row->shop!!} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	