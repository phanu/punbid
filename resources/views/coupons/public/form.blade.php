

		 {!! Form::open(array('url'=>'coupons/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{!! $error !!}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> coupons</legend>
									
									  <div class="form-group  " >
										<label for="Id" class=" control-label col-md-4 text-left"> Id </label>
										<div class="col-md-6">
										  {!! Form::text('id', $row['id'],array('class'=>'form-control', 'placeholder'=>'',   ) !!}
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Coupon Name" class=" control-label col-md-4 text-left"> Coupon Name </label>
										<div class="col-md-6">
										  {!! Form::text('coupon_name', $row['coupon_name'],array('class'=>'form-control', 'placeholder'=>'',   ) !!}
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Coupon Code" class=" control-label col-md-4 text-left"> Coupon Code </label>
										<div class="col-md-6">
										  {!! Form::text('coupon_code', $row['coupon_code'],array('class'=>'form-control', 'placeholder'=>'',   ) !!}
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Discount Type" class=" control-label col-md-4 text-left"> Discount Type </label>
										<div class="col-md-6">
										  {!! Form::text('discount_type', $row['discount_type'],array('class'=>'form-control', 'placeholder'=>'',   ) !!}
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Discount Num" class=" control-label col-md-4 text-left"> Discount Num </label>
										<div class="col-md-6">
										  {!! Form::text('discount_num', $row['discount_num'],array('class'=>'form-control', 'placeholder'=>'',   ) !!}
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {!! html_entity_decode(Lang::get('core.sb_apply') !!}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {!! html_entity_decode(Lang::get('core.sb_save') !!}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
