<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class bidtrans extends Sximo  {
	
	protected $table = 'v_bid_tran';
	protected $primaryKey = 'products_bid_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT v_bid_tran.* FROM v_bid_tran  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE v_bid_tran.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
