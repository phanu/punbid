﻿<!DOCTYPE html>
<html lang="en">
 <?php include('header.php'); ?>

<body class="product-page">
<div id="page"> 

  <section class="main-container col1-layout">
    <div class="main">
      <div class="container">
        <div class="row">
          <div class="col-main">
            <div class="product-view">
              <div class="product-essential">
                <form action="#" method="post" id="product_addtocart_form">
                  <input name="form_key" value="6UbXroakyQlbfQzK" type="hidden">
                  <div class="product-img-box col-lg-4 col-sm-5 col-xs-12">
                    <div class="new-label new-top-left"> New </div>
                    <div class="product-image">
                      <div class="product-full"> <img id="product-zoom" src="products-images/product-2.jpg" data-zoom-image="products-images/product1.jpg" alt="product-image"/> </div>
                      <div class="more-views">
                        <div class="slider-items-products">
                          <div id="gallery_01" class="product-flexslider hidden-buttons product-img-thumb">
                            <div class="slider-items slider-width-col4 block-content">
                              <div class="more-views-items"> <a href="#" data-image="products-images/product-3.jpg" data-zoom-image="products-images/product-3.jpg"> <img id="product-zoom"  src="products-images/product-3.jpg" alt="product-image"/> </a></div>
                              <div class="more-views-items"> <a href="#" data-image="products-images/product-4.jpg" data-zoom-image="products-images/product-4.jpg"> <img id="product-zoom"  src="products-images/product-4.jpg" alt="product-image"/> </a></div>
                              <div class="more-views-items"> <a href="#" data-image="products-images/product-5.jpg" data-zoom-image="products-images/product-5.jpg"> <img id="product-zoom"  src="products-images/product-5.jpg" alt="product-image"/> </a></div>
                              <div class="more-views-items"> <a href="#" data-image="products-images/product-6.jpg" data-zoom-image="products-images/product-6.jpg"> <img id="product-zoom"  src="products-images/product-6.jpg" alt="product-image"/> </a> </div>
                              <div class="more-views-items"> <a href="#" data-image="products-images/product-7.jpg" data-zoom-image="products-images/product-7.jpg"> <img id="product-zoom"  src="products-images/product-7.jpg" alt="product-image" /> </a></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- end: more-images --> 
                  </div>
                  <div class="product-shop col-lg-8 col-sm-7 col-xs-12">
                    <div class="product-next-prev"> <a class="product-next" href="#"><span></span></a> <a class="product-prev" href="#"><span></span></a> </div>
                       
                        <div class="product-name">
                      <h1>สร้อยคอ 2 บาท</h1>
                    </div>
                    <div class="item-info">
                            <div class="info-inner">
                               <?php
								$r=rand(1,22);
								$d=strtotime("+".$r." hour");
								$time=date("Y/m/d H:i:s", $d);
							  ?>
							  <div class="bid-time" data-countdown="<?php echo $time; ?>" style="    font-size: 36px;
    color: #a00101; font-family: roboto-bold;">00:00:00</div>
                              <div class="item-content">
                                
                                <div class="item-price">
                                  <div class="price-box">
                                    <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> <?php echo number_format(rand(100,500)); ?>.10 THB </span> </p>
                                    <p class="old-price"> <span class="price-label">ราคาปกติ</span> <span class="price"> 
									<?php echo number_format(rand(14000,30000)); ?>.00THB </span> </p>
                                  </div>
                                </div>
                         
                              </div>
                            </div>
                          </div>  
                          
                
                    
                    
                    <div class="short-description">
                      <h2>ประวัติการประมูล</h2>
                      <p>ทองเต็มตัว </p>
                    </div>
                    <div class="add-to-box">
                      <div class="add-to-cart">
                         <button onClick="productAddToCartForm.submit(this)" class="button btn-cart" title="Add to Cart" type="button">ออโต้บิด</span></button>
                 
                        <div class="pull-left">
                          <div class="custom pull-left">
                            <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="reduced items-count" type="button"><i class="fa fa-minus">&nbsp;</i></button>
                            <input type="text" class="input-text qty" title="Qty" value="1" maxlength="12" id="qty" name="qty">
                            <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="increase items-count" type="button"><i class="fa fa-plus">&nbsp;</i></button>
                          </div>
                        </div>
                        <button onClick="productAddToCartForm.submit(this)" class="button btn-cart" title="Add to Cart" type="button">ประมูลเลย</span></button>
                      </div>
                      <div class="email-addto-box">
                        <ul class="add-to-links">
                          <li> <a class="link-wishlist" href="wishlist.html"><span>Add to Wishlist</span></a></li>
                            </ul>
                       
                      </div>
                    </div>
                    <div class="social">
                      <ul class="link">
                        <li class="fb"><a href="#"></a></li>
                        <li class="tw"><a href="#"></a></li>
                        <li class="googleplus"><a href="#"></a></li>
                        <li class="rss"><a href="#"></a></li>
                        <li class="pintrest"><a href="#"></a></li>
                        <li class="linkedin"><a href="#"></a></li>
                        <li class="youtube"><a href="#"></a></li>
                      </ul>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
      
        </div>
      </div>
    </div>
  </section>
  <!-- Main Container End --> 
  
  <!-- Related Products Slider -->
  
  <div class="container">
  
   <!-- Related Slider -->
   
  <div class="related-pro">

      <div class="slider-items-products">
        <div class="related-block">
          <div id="related-products-slider" class="product-flexslider hidden-buttons">
            <div class="home-block-inner">
              <div class="block-title">
             
                  <em>รายการที่กำลังประมูล</em>
              </div>
    <a href="grid.html" class="view_more_bnt">ดูทั้งหมด</a>  
              
            </div>
           
            <div class="slider-items slider-width-col4 products-grid block-content">
              <?php $i=rand(1,4); $end=$i+3; include('actural_list2.php'); ?>
            </div>
          </div>
        </div>
      </div>

  </div>
  <!-- End related products Slider --> 
  
 
  
    
  </div>

  <!-- Footer -->
  <?php include('footer.php'); ?>
</div>
<div id="mobile-menu">
  <ul>
    <li>
      <div class="mm-search">
        <form id="search1" name="search">
          <div class="input-group">
            <div class="input-group-btn">
              <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> </button>
            </div>
            <input type="text" class="form-control simple" placeholder="Search ..." name="srch-term" id="srch-term">
          </div>
        </form>
      </div>
    </li>
    <li><a href="index.html">Home</a>
          <ul>
        <li class="level1"><a href="../fashion/index.html"><span>Fashion Store</span></a> </li>
        <li class="level1"><a href="index.html"><span>Digital Store</span></a> </li>
        <li class="level1"><a href="../furniture/index.html"><span>Furniture Store</span></a> </li>
        <li class="level1"><a href="../jewellery/index.html"><span>Jewellery Store</span></a> </li>
      </ul>
    </li>
    <li><a href="#">Pages</a>
      <ul>
        <li><a href="grid.html">Grid</a> </li>
        <li> <a href="list.html">List</a> </li>
        <li> <a href="product_detail.html">Product Detail</a> </li>
        <li> <a href="shopping_cart.html">Shopping Cart</a> </li>
        <li><a href="checkout.html">Checkout</a> </li>
        <li> <a href="wishlist.html">Wishlist</a> </li>
        <li> <a href="dashboard.html">Dashboard</a> </li>
        <li> <a href="multiple_addresses.html">Multiple Addresses</a> </li>
        <li> <a href="about_us.html">About us</a> </li>
        <li><a href="blog.html">Blog</a>
          <ul>
            <li><a href="blog-detail.html">Blog Detail</a> </li>
          </ul>
        </li>
        <li><a href="contact_us.html">Contact us</a> </li>
        <li><a href="404error.html">404 Error Page</a> </li>
      </ul>
    </li>
    <li><a href="#">Women</a>
      <ul>
        <li> <a href="#" class="">Stylish Bag</a>
          <ul>
            <li> <a href="grid.html" class="">Clutch Handbags</a> </li>
            <li> <a href="grid.html" class="">Diaper Bags</a> </li>
            <li> <a href="grid.html" class="">Bags</a> </li>
            <li> <a href="grid.html" class="">Hobo handbags</a> </li>
          </ul>
        </li>
        <li> <a href="grid.html">Material Bag</a>
          <ul>
            <li> <a href="grid.html">Beaded Handbags</a> </li>
            <li> <a href="grid.html">Fabric Handbags</a> </li>
            <li> <a href="grid.html">Handbags</a> </li>
            <li> <a href="grid.html">Leather Handbags</a> </li>
          </ul>
        </li>
        <li> <a href="grid.html">Shoes</a>
          <ul>
            <li> <a href="grid.html">Flat Shoes</a> </li>
            <li> <a href="grid.html">Flat Sandals</a> </li>
            <li> <a href="grid.html">Boots</a> </li>
            <li> <a href="grid.html">Heels</a> </li>
          </ul>
        </li>
        <li> <a href="grid.html">Jwellery</a>
          <ul>
            <li> <a href="grid.html">Bracelets</a> </li>
            <li> <a href="grid.html">Necklaces &amp; Pendent</a> </li>
            <li> <a href="grid.html">Pendants</a> </li>
            <li> <a href="grid.html">Pins &amp; Brooches</a> </li>
          </ul>
        </li>
        <li> <a href="grid.html">Dresses</a>
          <ul>
            <li> <a href="grid.html">Casual Dresses</a> </li>
            <li> <a href="grid.html">Evening</a> </li>
            <li> <a href="grid.html">Designer</a> </li>
            <li> <a href="grid.html">Party</a> </li>
          </ul>
        </li>
        <li> <a href="grid.html">Swimwear</a>
          <ul>
            <li> <a href="grid.html">Swimsuits</a> </li>
            <li> <a href="grid.html">Beach Clothing</a> </li>
            <li> <a href="grid.html">Clothing</a> </li>
            <li> <a href="grid.html">Bikinis</a> </li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="grid.html">Men</a>
      <ul>
        <li> <a href="grid.html" class="">Shoes</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.html">Sport Shoes</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Casual Shoes</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Leather Shoes</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">canvas shoes</a> </li>
          </ul>
        </li>
        <li> <a href="grid.html">Dresses</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.html">Casual Dresses</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Evening</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Designer</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Party</a> </li>
          </ul>
        </li>
        <li> <a href="grid.html">Jackets</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.html">Coats</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Formal Jackets</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Leather Jackets</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Blazers</a> </li>
          </ul>
        </li>
        <li> <a href="#.html">Watches</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.html">Fasttrack</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Casio</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Titan</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Tommy-Hilfiger</a> </li>
          </ul>
        </li>
        <li> <a href="grid.html">Sunglasses</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.html">Ray Ban</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Fasttrack</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Police</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Oakley</a> </li>
          </ul>
        </li>
        <li> <a href="grid.html">Accesories</a>
          <ul class="level1">
            <li class="level2 nav-6-1-1"><a href="grid.html">Backpacks</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Wallets</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Laptops Bags</a> </li>
            <li class="level2 nav-6-1-1"><a href="grid.html">Belts</a> </li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="grid.html">Electronics</a>
      <ul>
        <li> <a href="grid.html"><span>Mobiles</span></a>
          <ul>
            <li> <a href="grid.html"><span>Samsung</span></a> </li>
            <li> <a href="grid.html"><span>Nokia</span></a> </li>
            <li> <a href="grid.html"><span>IPhone</span></a> </li>
            <li> <a href="grid.html"><span>Sony</span></a> </li>
          </ul>
        </li>
        <li> <a href="grid.html" class=""><span>Accesories</span></a>
          <ul>
            <li> <a href="grid.html"><span>Mobile Memory Cards</span></a> </li>
            <li> <a href="grid.html"><span>Cases &amp; Covers</span></a> </li>
            <li> <a href="grid.html"><span>Mobile Headphones</span></a> </li>
            <li> <a href="grid.html"><span>Bluetooth Headsets</span></a> </li>
          </ul>
        </li>
        <li> <a href="grid.html"><span>Cameras</span></a>
          <ul>
            <li> <a href="grid.html"><span>Camcorders</span></a> </li>
            <li> <a href="grid.html"><span>Point &amp; Shoot</span></a> </li>
            <li> <a href="grid.html"><span>Digital SLR</span></a> </li>
            <li> <a href="grid.html"><span>Camera Accesories</span></a> </li>
          </ul>
        </li>
        <li> <a href="grid.html"><span>Audio &amp; Video</span></a>
          <ul>
            <li> <a href="grid.html"><span>MP3 Players</span></a> </li>
            <li> <a href="grid.html"><span>IPods</span></a> </li>
            <li> <a href="grid.html"><span>Speakers</span></a> </li>
            <li> <a href="grid.html"><span>Video Players</span></a> </li>
          </ul>
        </li>
        <li> <a href="grid.html"><span>Computer</span></a>
          <ul>
            <li> <a href="grid.html"><span>External Hard Disk</span></a> </li>
            <li> <a href="grid.html"><span>Pendrives</span></a> </li>
            <li> <a href="grid.html"><span>Headphones</span></a> </li>
            <li> <a href="grid.html"><span>PC Components</span></a> </li>
          </ul>
        </li>
        <li> <a href="grid.html"><span>Appliances</span></a>
          <ul>
            <li> <a href="grid.html"><span>Vaccum Cleaners</span></a> </li>
            <li> <a href="grid.html"><span>Indoor Lighting</span></a> </li>
            <li> <a href="grid.html"><span>Kitchen Tools</span></a> </li>
            <li> <a href="grid.html"><span>Water Purifier</span></a> </li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="grid.html">Furniture</a>
      <ul>
        <li> <a href="grid.html">Living Room</a>
          <ul>
            <li> <a href="grid.html">Racks &amp; Cabinets</a> </li>
            <li> <a href="grid.html">Sofas</a> </li>
            <li> <a href="grid.html">Chairs</a> </li>
            <li> <a href="grid.html">Tables</a> </li>
          </ul>
        </li>
        <li> <a href="grid.html" class="">Dining &amp; Bar</a>
          <ul>
            <li> <a href="grid.html">Dining Table Sets</a> </li>
            <li> <a href="grid.html">Serving Trolleys</a> </li>
            <li> <a href="grid.html">Bar Counters</a> </li>
            <li> <a href="grid.html">Dining Cabinets</a> </li>
          </ul>
        </li>
        <li> <a href="grid.html">Bedroom</a>
          <ul>
            <li> <a href="grid.html">Beds</a> </li>
            <li> <a href="grid.html">Chest of Drawers</a> </li>
            <li> <a href="grid.html">Wardrobes &amp; Almirahs</a> </li>
            <li> <a href="grid.html">Nightstands</a> </li>
          </ul>
        </li>
        <li> <a href="grid.html">Kitchen</a>
          <ul>
            <li> <a href="grid.html">Kitchen Racks</a> </li>
            <li> <a href="grid.html">Kitchen Fillings</a> </li>
            <li> <a href="grid.html">Wall Units</a> </li>
            <li> <a href="grid.html">Benches &amp; Stools</a> </li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="grid.html">Kids</a> </li>
    <li><a href="contact-us.html">Contact Us</a> </li>
  </ul>
  <div class="top-links">
    <ul class="links">
      <li><a title="My Account" href="login.html">My Account</a> </li>
      <li><a title="Wishlist" href="wishlist.html">Wishlist</a> </li>
      <li><a title="Checkout" href="checkout.html">Checkout</a> </li>
      <li><a title="Blog" href="blog.html"><span>Blog</span></a> </li>
      <li class="last"><a title="Login" href="login.html"><span>Login</span></a> </li>
    </ul>
  </div>
</div>

<!-- End Footer --> 

<!-- JavaScript --> 
<script type="text/javascript" src="js/jquery.min.js"></script> 
<script type="text/javascript" src="js/bootstrap.min.js"></script>  
<script type="text/javascript" src="js/common.js"></script> 
<script type="text/javascript" src="js/jquery.flexslider.js"></script> 
<script type="text/javascript" src="js/owl.carousel.min.js"></script> 
<script type="text/javascript" src="js/jquery.mobile-menu.min.js"></script> 
<script type="text/javascript" src="js/cloud-zoom.js"></script>
</body>
</html>