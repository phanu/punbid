﻿<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="x-ua-compatible" content="ie=edge">
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons Icon -->
<link rel="icon" href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico" type="image/x-icon" />
<title>โกโกล์ดรัน ไปๆกันไปเอาทองกัน</title>

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS Style -->
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/font-awesome.css" media="all">
<link rel="stylesheet" type="text/css" href="css/simple-line-icons.css" media="all">
<link rel="stylesheet" type="text/css" href="css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="css/owl.theme.css">
<link rel="stylesheet" type="text/css" href="css/jquery.bxslider.css">
<link rel="stylesheet" type="text/css" href="css/jquery.mobile-menu.css">
<link rel="stylesheet" type="text/css" href="css/style.css" media="all">
<link rel="stylesheet" type="text/css" href="css/revslider.css">
<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:700,600,800,400' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,600,500,700,800' rel='stylesheet' type='text/css'>
</head>

<body class="cms-index-index cms-home-page">
<div id="page"> 
  <!-- Header -->
  <header>
    <div class="header-container">
      
      <div class="container">
        <div class="row">
          
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 logo-block"> 
            <!-- Header Logo -->
            <div class="logo"> <a title="gogoldrun" href="index.php"><img alt="gogoldrun" src="images/logo.png"> </a> </div>
            <!-- End Header Logo --> 
          </div>
          
        </div>
      </div>
    </div>
  </header>
  <!-- end header --> 
  
  <!-- Navigation -->
  
  <nav>
    <div class="container">
      <div class="mm-toggle-wrap">
      <div class="search-box">
              <form action="cat" method="POST" id="search_mini_form" name="Categories">
                <input type="text" placeholder="Search entire store here..." value="Search" maxlength="70" name="search" id="search">
                <button type="button" class="search-btn-bg"><span class="glyphicon glyphicon-search"></span>&nbsp;</button>
              </form>
            </div>
      </div>
      
      <div class="nav-inner"> 
        <!-- BEGIN NAV -->
        <ul id="nav" class="hidden-xs">
           <li class="level2 nav-10-3"> <a href="#"> <span>ประมูล</span> </a> </li>
          
          
          <li class="mega-menu"> <a class="level-top" href="grid.php"><span>รายการประมูล</span></a>
            <div class="level0-wrapper dropdown-6col">
              <div class="container">
                <div class="level0-wrapper2">
                  <div class="nav-block nav-block-center"> 
                    <!--mega menu-->
                    <ul class="level0">
                      <li class="level3 nav-6-1 parent item"> <a href="#"><span>แหวนทอง</span></a>
                        <ul class="level1">
                          <li class="level2 nav-6-1-1"> <a href="#"><span>แหวน 1/2 ส</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>แหวน 1 ส</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>แหวน 2 ส</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>แหวน 1 บ</span></a> </li>
                        </ul>
                      </li>
                      <li class="level3 nav-6-1 parent item"> <a href="#"><span>สร้อยคอ</span></a>
                        <ul class="level1">
                          <li class="level2 nav-6-1-1"> <a href="#"><span>คอ 1/2 ส</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>คอ 1 ส</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>คอ 2 ส</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>คอ 3 ส</span></a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#"><span>คอ 1 </span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>คอ 6 ส</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>คอ 2 บ</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>คอ 3 บ</span></a> </li>
                            <li class="level2 nav-6-1-1"> <a href="#"><span>คอ 4 บ</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>คอ 5 บ</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>คอ 10 บ</span></a> </li>
                         
                        </ul>
                      </li>
                      <li class="level3 nav-6-1 parent item"> <a href="#"><span>สร้อยข้อมือ</span></a>
                        <ul class="level1">
                          <li class="level2 nav-6-1-1"> <a href="#"><span>สร้อยข้อมือ 1 ส</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>สร้อยข้อมือ 2 ส</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>สร้อยข้อมือ 1 บ</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>สร้อยข้อมือ 2 บ</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>สร้อยข้อมือ 3 บ</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>สร้อยข้อมือ 5 บ</span></a> </li>
                        </ul>
                      </li>
                      <li class="level3 nav-6-1 parent item"> <a href="#"><span>กำไล</span></a>
                        <ul class="level1">
                          <li class="level2 nav-6-1-1"> <a href="#"><span>กำไลข้อมือ 2 ส</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>ำไลข้อมือ 1 บ</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>ำไลข้อมือ 2 บ</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>ำไลข้ออือ 3 บ</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>ำไลข้อมือ 5 บ</span></a> </li>
                        </ul>
                      </li>
                      <li class="level3 nav-6-1 parent item"> <a href="#"><span>ทองคำแท่ง</span></a>
                        <ul class="level1">
                          <li class="level2 nav-6-1-1"> <a href="#"><span>ทองแคำท่ง 2 ส</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>ทองแคำท่ง 1 บ</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>ทองแคำท่ง 2 บ</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>ทองแคำท่ง 5 บ</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>ทองแคำท่ง 10 บ</span></a> </li>
                           <li class="level2 nav-6-1-1"> <a href="#"><span>พระประจำวันเกิด</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>พระโสธร</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>หลวงพ่อทวด</span></a> </li>
                        </ul>
                      </li>
                      <li class="level3 nav-6-1 parent item"> <a href="#"><span>อื่นๆ</span></a>
                        <ul class="level1">
                          <li class="level2 nav-6-1-1"> <a href="#"><span>แหวนทองหัวพลอย ทอง 90% แบบต่างๆๆ ราคาตามน้ำหนัก</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>สร้อยคอเด็ก 1/2 ส </span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>สร้อยคอเด็ก 1 ส</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>สร้อยคอเด็ก 2 ส</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>สร้อยคอเด็ก 1 บ</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>สร้อยข้อมือเด็ก 1/2 ส </span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>สร้อยข้อมือเด็ก 1 ส</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>สร้อยข้อมือเด็ก 2 ส</span></a> </li>
                          <li class="level2 nav-6-1-1"> <a href="#"><span>สร้อยข้อมือเด็ก 1 บ</span></a> </li>
                        </ul>
                      </li>
                    </ul>
                    <!--level0--> 
                  </div>
                  <!--nav-block nav-block-center--> 
                  <!--nav-block nav-block-center-->
                  <div class="nav-add">
                    <div class="push_item">
                      <div class="push_img"> <a href="#"><img src="images/menu-banner1.png" alt="sunglass"> </a> </div>
                    </div>
                    <div class="push_item push_item_last">
                      <div class="push_img"> <a href="#"><img src="images/menu-banner2.png" alt="shoes"> </a> </div>
                    </div>
                  </div>
                </div>
                <!--level0-wrapper2--> 
              </div>
              <!--container--> 
            </div>
            <!--level0-wrapper dropdown-6col--> 
            <!--mega menu--> 
          </li>     
          <li class="level2 nav-10-3"> <a href="#"> <span>ซื้อบิด</span> </a> </li>
           <li class="level2 nav-10-3"> <a href="#"> <span>ผู้ชนะประมูล</span> </a> </li>

          <li class="level2 nav-10-3"> <a href="#"> <span>โกโกล์ดรันคืออะไร?</span> </a> </li>

          <li class="level2 nav-10-3"> <a href="#"> <span>ติดต่อเรา</span> </a> </li>


       
        </ul>
        <!--nav--> 
      </div>
    </div>
  </nav>
  <!-- end nav -->