<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class productsbid extends Sximo  {
	
	protected $table = 'products_bid';
	protected $primaryKey = 'products_bid_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT products_bid.* FROM products_bid  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE products_bid.products_bid_id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
