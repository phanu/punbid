<?php // Add some items in your Controller.

Session::forget('discount');
Session::forget('discount_id');
$discount = 0;
$dis_total = 0;
$discount_type = '';
if(isset($_REQUEST["cartaction"]))
{


	if($_REQUEST["cartaction"]=="add")
	{

		if(isset($_REQUEST["paytype"]))
		{

			//$color= isset($_REQUEST['color'])?$_REQUEST['color']:0;
			if($_REQUEST['paytype']=="money")
			{
				Cart::instance('shop')->add(
					[
						'id' => $_REQUEST['pid'], 
						'name' => $_REQUEST['name_th'], 
						'qty' => $_REQUEST['qty'], 
						'price' => $_REQUEST['pricemoney'], 
						'options' => [
										'pic_url' => $_REQUEST['url'],'products_code' => $_REQUEST['products_code'],	
										'name_en' => $_REQUEST['name_en'],'paytype' => $_REQUEST['paytype'],
										'color' => isset($_REQUEST['color'])  ? $_REQUEST['color'] : "0",
										'shiping_cost' => $_REQUEST['shiping_cost']
									]
					]
				);
			}
			/*elseif($_REQUEST['paytype']=="spay")
			{
			Cart::instance('spay')->add(
				[
					'id' => $_REQUEST['pid'], 
					'name' => $_REQUEST['name_th'], 
					'qty' => $_REQUEST['qty'], 
					'price' => $_REQUEST['pricespay'], 
					'options' => [
									'pic_url' => $_REQUEST['url'],'products_code' => $_REQUEST['products_code'],	
									'name_en' => $_REQUEST['name_en'],'paytype' => $_REQUEST['paytype'],
									'color' => isset($_REQUEST['color'])  ? $_REQUEST['color'] : "0",
									'shiping_cost' => $_REQUEST['shiping_cost']
								]
				]
			);*/

		}

			

		
		$cart_message = Lang::get('cart.1');
	}elseif ($_REQUEST["cartaction"]=="update") {	
			
		//	print_r($_REQUEST);

			$i=0;
			if(isset($_REQUEST['qmoney'])){
				foreach ($_REQUEST['rowIdmoney'] as $key => $value) {
					# code...
					Cart::instance('shop')->update($value, $_REQUEST['qmoney'][$i]);
					$i++;
				}
			}

			/*$i=0;
			if(isset($_REQUEST['qspay'])){
				foreach ($_REQUEST['rowIdspay'] as $key => $value) {
					# code...
					Cart::instance('spay')->update($value, $_REQUEST['qspay'][$i]);
					$i++;
				}
			}
			*/
			$cart_message = Lang::get('cart.2');

	}elseif ($_REQUEST["cartaction"]=="delete") {	
			
			if(isset($_REQUEST['cartspayid'])){

					Cart::instance('spay')->remove($_REQUEST['cartspayid']);
			}

			if(isset($_REQUEST['cartshopid'])){

					Cart::instance('shop')->remove($_REQUEST['cartshopid']);

			}
			
	}


	
			
}

if(isset($_GET['message'])&&$_GET['message']=='del')
{
$cart_message = Lang::get('cart.3');
}

if(isset($_GET['code'])&&$_GET['code']!='')
{

			

		  $usecoup = \App\Http\Controllers\CouponsController::frontcheckcoupon($_GET['code']); 


	      if($usecoup=="nocoup"||$usecoup=="used"){

	      		$cart_message = Lang::get('cart.17');

	      }else{

	      		$discount = $usecoup->discount_num;
	      		$discount_type = $usecoup->discount_type;
	      		$dis_id = $usecoup->id;

	      		$cart_message = Lang::get('cart.18');
	    }
}


if(Cart::instance('shop')->count()=="0"&&Cart::instance('spay')->count()=="0")
{
	$cart_message = Lang::get('cart.4');
}

$sum_shipping_cost = number_format(0,2);

?>
<div class="container">
	<div class="rs_graybg rs_toppadder10 rs_bottompadder20">
		<div class="container">
			<div class="row">
				@if(isset($cart_message))
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="rs_message_div">
					
						<div class="alert alert-success">
							<p><i class="fa fa-check-square-o"></i>{!! $cart_message !!}</p>
						</div>
					</div>
				</div>
				@endif
				<form method="post" action="{!!url('cart') !!}">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="woo-cart-table rs_bottompadder30">
						<table class="table">
							<tr>
								<th></th>
								<th>{!! html_entity_decode(Lang::get('cart.5')) !!}</th>
								<th>{!! html_entity_decode(Lang::get('cart.6')) !!}</th>
								<th>{!! html_entity_decode(Lang::get('cart.7')) !!}</th>
								<th></th>
							</tr>
							<?php foreach(Cart::instance('shop')->content() as $row) :?>
							<tr>
								<td></td>
								<td>
									<img src="{!! $row->options->pic_url !!} " alt="">
									<p>@if(CNF_LANG=='th') 
										{!! $row->name !!} 
										@else 
										{!! $row->options->name_en !!} 
										@endif
									</p>
								</td>
								<td class="text-center">
									<div class="rs_incre_decre_btns">
										<button type="button" class="btn_small count_minus" id="breakDecrease">
										  <i class="fa fa-minus"></i>
										</button>
										<button type="button" class="btn_big count_val" id="breakValue" disabled>
										{!! $row->qty !!} 
										</button>
										<input type="hidden" class="quantity_rows" name="qmoney[]" value="{!! $row->qty !!}">
										<button type="button" class="btn_small count_plus" id="breakIncrease">
										  <i class="fa fa-plus"></i>
										</button>
									</div>
								</td>
								<td class="text-center">
									<span>{!! number_format($row->price) !!}<small></small>
										@if($row->options->paytype=='spay') 
										S-Pay 
										@else 
										THB
										@endif</span>
								</td>
								<td class="text-center">
								<a class="deletecartshop" id="{!! $row->rowId !!}"><i class="fa fa-times"></i></a>
								</td>
								<input type="hidden" name="rowIdmoney[]" value="{!! $row->rowId !!}">
							</tr>
							<?php $sum_shipping_cost = $sum_shipping_cost+($row->options->shiping_cost*$row->qty); endforeach; ?>
							<?php foreach(Cart::instance('spay')->content() as $row) :?>
							<tr>
								<td></td>
								<td>
									<img src="{!! $row->options->pic_url !!} " alt="">
									<p>@if(CNF_LANG=='th') 
										{!! $row->name !!} 
										@else 
										{!! $row->options->name_en !!} 
										@endif
									</p>
								</td>
								<td class="text-center">
									<div class="rs_incre_decre_btns">
										<button type="button" class="btn_small count_minus" id="breakDecrease">
										  <i class="fa fa-minus"></i>
										</button>
										<button type="button" class="btn_big count_val" id="breakValue" disabled>{!! $row->qty !!} 
										</button>
										<input type="hidden" class="quantity_rows" name="qspay[]" value="{!! $row->qty !!}">
										<button type="button" class="btn_small count_plus" id="breakIncrease">
										  <i class="fa fa-plus"></i>
										</button>
									</div>
								</td>
								<td class="text-center">
									<span>{!! number_format($row->price) !!}<small></small>
										@if($row->options->paytype=='spay') 
										S-Pay 
										@else 
										THB
										@endif</span>
								</td>
								<td class="text-center">
								<a class="deletecartspay" id="{!! $row->rowId !!}"><i class="fa fa-times"></i></a>
								</td>
								<input type="hidden" name="rowIdspay[]" value="{!! $row->rowId !!}">
							</tr>
							<?php endforeach; ?>
							<tr>
								<tfoot>
								<div class="rs_updatecart">
									<input type="hidden" name="cartaction" value="update">
                                	<button type="submit" class="rs_button rs_button_orange" data-text="Update Cart">
                                	<span>{!! html_entity_decode(Lang::get('cart.8')) !!}</span>
                                	</button>
                            	</div>
                            	</tfoot>
							</tr>
						</table>
					</div>
				</div>
				</form>
				<div class="col-lg-7 col-md-6 col-sm-6 col-xs-12">
				<div class="rs_offerdiv">
					<div class="woo_product-code">
						<form class="form-inline">
							<div class="form-group">
								<input type="text" name="code" class="form-control" value="@if(isset($_GET['code'])&&$_GET['code']!='') {!!$_GET['code']!!} @endif" placeholder="คูปองส่วนลด">
							</div>
							<button type="submit" class="rs_apply_coupenbtn rs_button rs_button_orange">{!! html_entity_decode(Lang::get('cart.9')) !!}</button>
						</form>
					</div>
				</div>
				</div>
				<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
					<div class="cart_totals">
						<div class="cart-heading rs_bottompadder20">รวม</div>
						<div class="cart-subtotal">
							<p>
								<label>{!! html_entity_decode(Lang::get('cart.12')) !!}</label> <span><?php echo Cart::instance('shop')->subtotal(); ?><small>THB</small></span>
							</p>
							@if($discount!=0)
							<p>
								@if($discount_type=='percent')
							
								<label>{!! html_entity_decode(Lang::get('cart.18')) !!}</label> <span><?php $dis_total = (str_replace(',', '', Cart::instance('shop')->total())*$discount)/100; echo number_format($dis_total,2);  ?> <small>THB</small></span>
							
								@elseif($discount_type=='price')

								<label>{!! html_entity_decode(Lang::get('cart.18')) !!}</label> <span><?php $dis_total = $discount; echo number_format($dis_total,2);  ?> <small>THB</small></span>

								@endif
							</p>
								<?php Session::put('discount', $dis_total); Session::put('discount_id', $dis_id);?>
							@endif
							<p>
								<label>{!! html_entity_decode(Lang::get('cart.13')) !!}</label> <span><?php echo number_format($sum_shipping_cost,2); ?><small>THB</small></span>
							</p>
						</div>
						<div class="order-total rs_toppadder10 rs_bottompadder10">
							<?php if(Cart::instance('shop')->subtotal()!="0.00") { ?>
							<p>
							<label>{!! html_entity_decode(Lang::get('cart.14')) !!}</label> <span><?php echo number_format((str_replace(',', '', Cart::instance('shop')->total())+$sum_shipping_cost)-$dis_total,2);  ?><small>THB</small></span>
							</p>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="rs_showmore rs_toppadder50">
						<a href="{!!url('checkout') !!}" class="rs_button rs_button_orange">{!! html_entity_decode(Lang::get('cart.15')) !!}</a>
					</div>
					<div class="rs_continuecart rs_toppadder50">
						<a href="{!!url('shop') !!}" class="rs_button rs_button_orange">{!! html_entity_decode(Lang::get('cart.16')) !!}</a>
					</div>
				</div>
				
			</div>

		</div>
	</div>
	
</div>		