<?php namespace App\Models;
use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class searchres extends Sximo  {

	use SearchableTrait;

	protected $searchable = [
        'columns' => [
            'search_res.name_th' => 10,
            'search_res.category_th' => 7,
            'search_res.products_code' => 4,
            'search_res.title_th' => 9,
        ]
    ];

	protected $table = 'search_res';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
	}

	public static function querySelect(  ){
		
		return "  SELECT search_res.* FROM search_res";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE search_res.id IS NOT NULL and active = 1";
	}

}