<script type="text/javascript">
$(document).ready(function() {


<?php if(isset($_GET['page'])=="") { $p=1; }else{ $p=$_GET['page']; } ?> 


    $(document).on('click','.pagination li a', function() {
    //do other stuff when a click happens
        $('.ajaxLoading').fadeIn( 200 ).delay( 100 ).fadeOut( 200 ).css({
            display: 'block'
        });

        $.ajax({
        url: $(this).data('url'),
        method: "GET",
        data: { },
        dataType: "html",
        success: function(data) {
                console.log(data);
                $(".{!!$modulepage!!}").html(data);

            },
        });
        $('.ajaxLoading').fadeIn( 200 ).delay( 100 ).fadeOut( 200 ).css({
            display: 'none'
        });

        
      

        return false;
    }); 
   
 // reloadDatafront('#',!!}');  
    
  var sync1 = $("#sync1");
  var sync2 = $("#sync2");
 
  sync1.owlCarousel({
    singleItem : true,
    slideSpeed : 1000,
    navigation: false,
    pagination:true,
    afterAction : syncPosition,
    responsiveRefreshRate : 200,
  });
 
  sync2.owlCarousel({
    items : 4,
    itemsDesktop      : [1199,4],
    itemsDesktopSmall     : [979,4],
    itemsTablet       : [768,4],
    itemsMobile       : [479,0],
    pagination:false,
    responsiveRefreshRate : 100,
    afterInit : function(el){
      el.find(".owl-item").eq(0).addClass("synced");
    el.find(".owl-item").addClass("itemfull");
    el.find(".owl-wrapper").addClass("itemfull");
    }
  });
 
  function syncPosition(el){
    var current = this.currentItem;
    $("#sync2")
      .find(".owl-item")
      .removeClass("synced")
      .eq(current)
      .addClass("synced")
    if($("#sync2").data("owlCarousel") !== undefined){
      center(current)
    }
  }
 
  $("#sync2").on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).data("owlItem");
    sync1.trigger("owl.goTo",number);
  });
 
  function center(number){
    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
    var num = number;
    var found = false;
    for(var i in sync2visible){
      if(num === sync2visible[i]){
        var found = true;
      }
    }
 
    if(found===false){
      if(num>sync2visible[sync2visible.length-1]){
        sync2.trigger("owl.goTo", num - sync2visible.length+2)
      }else{
        if(num - 1 === -1){
          num = 0;
        }
        sync2.trigger("owl.goTo", num);
      }
    } else if(num === sync2visible[sync2visible.length-1]){
      sync2.trigger("owl.goTo", sync2visible[1])
    } else if(num === sync2visible[0]){
      sync2.trigger("owl.goTo", num-1)
    }
    
  }
   
});
</script>