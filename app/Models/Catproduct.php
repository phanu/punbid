<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class catproduct extends Sximo  {
	
	protected $table = 'product_categories';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return " SELECT *,(SELECT COUNT(*) FROM products WHERE products.category = product_categories.id) AS product_counts FROM product_categories ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE product_categories.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
