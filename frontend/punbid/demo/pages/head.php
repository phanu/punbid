<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>gogoldrun.com</title>
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="assets/css/animate.min.css" media="all" />
<link rel="stylesheet" type="text/css" href="assets/css/font-electro.css" media="all" />
<link rel="stylesheet" type="text/css" href="assets/css/owl-carousel.css" media="all" />
<link rel="stylesheet" type="text/css" href="assets/css/style.css" media="all" />
<link rel="stylesheet" type="text/css" href="assets/css/colors/yellow.css" media="all" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,700italic,800,800italic,600italic,400italic,300italic' rel='stylesheet' type='text/css'>
<link rel="shortcut icon" href="assets/images/fav-icon.png">