<?php
	include($_SERVER['DOCUMENT_ROOT']."/demo/includes/config.php");
	
	$pid = $_POST['p'];
	$cid = $_POST['c'];
	
	$sql = mysql_query("
		SELECT
			sc_prod_sale.psid
			, sc_prod_sale.pid
			, sc_prod_sale.ps_start_price
			, sc_prod_sale.ps_end_price
			, sc_prod_sale.ps_start_date
			, sc_prod_sale.ps_end_date
		FROM
			sc_prod_sale
		WHERE sc_prod_sale.ps_status ='0'
			AND sc_prod_sale.pid ='".$pid."'
	");
	$row = mysql_fetch_assoc($sql);
	
	$newdate = date("Y-m-d H:i:s", date(strtotime("+10 seconds", strtotime($row['ps_end_date']))));
	$newprice = $row['ps_end_price'] + 0.10;
	
	//add log bid
	$arrData = array(
		'psid' => $row['psid'],
		'cid' => $cid,
		'pd_orgprice' => $row['ps_end_price'],
		'pd_newprice' => $newprice,
		'pd_orgtime' => $row['ps_end_date'],
		'pd_newtime' => $newdate,
		'pd_ip' => $_SERVER['REMOTE_ADDR'],
		'pd_date' => $today
	);
	insertSQL($arrData, "sc_prod_bid");
	
	//update psid
	mysql_query("UPDATE sc_prod_sale SET ps_end_price='".$newprice."', ps_end_date='".$newdate."', ps_udate='".$today."' WHERE psid='".$row['psid']."'");
	
	//get user
	$username = getDesc("username","cr_cust","cid='".$cid."'");
	$bidnum = countDesc("pdid","sc_prod_bid","psid='".$row['psid']."'");
	
	//return data
	$data = array(
		'p' => $pid,
		'ps' => $row['psid'],
		'pr' => $newprice,
		'ap' => '0.10',
		'd' => date("Y/m/d H:i:s", strtotime($newdate)),
		'c' => $cid,
		'n' => $bidnum,
		'u' => $username
	);
	
	echo json_encode($data);
?>