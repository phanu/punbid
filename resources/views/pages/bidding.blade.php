<?php 
  use \App\Http\Controllers\VbiddingController; 
  use \App\Http\Controllers\AutobidController; 
  use \App\Http\Controllers\BidtransController; 
  use \App\Http\Controllers\BidtransrecordsController; 
  use SocialLinks\Page;

  $data = VbiddingController::display(); 
  $row = $data['row'];  
  $lang =  Session::get('lang');
  $rowx = $data['row'];
  $row = $data['row'];
  $fields = $data['fields']; 


  if($lang=='en')
  {
    $row->name_th = isset($row->name_en)?:$row->name_th;
    $row->title_th = isset($row->title_en)?:$row->title_th;
    $row->descript_th = isset($row->descript_en)?:$row->descript_th;
  }


  //Create a Page instance with the url information
          $page = new Page([
              'url' => config('app.url').'/bidding?view='.$row->products_bid_id,
              'title' => $row->name_th,
              'text' => Lang::get('bidding.1').$row->name_th,
              'image' => config('app.url').'/uploads/products/'.$row->featured_img,
              'twitterUser' => '@punbidtheme'
          ]);

          //Use the properties to get the providers info, for example:

                          foreach ($page->html() as $tag) {
                              echo $tag."\n";
                          }
                              echo "\n";
                          foreach ($page->openGraph() as $tag) {
                              echo $tag."\n";
                          }
                              echo "\n";
                          foreach ($page->twitterCard() as $tag) {
                              echo $tag."\n";
                          }
                              echo "\n";
                          foreach ($page->schema() as $tag) {
                              echo  $tag."\n";
                          }
  
  $boots = $row->boots;

          switch ($boots):

          case 1 : break;

          case 2 :
              echo '<span class="label label-default">x2</span>';
          break;

          case 3 :
              echo '<span class="label label-pink">x3</span>';
          break;

          case 4 :
               echo '<span class="label label-warning">x4</span>';
          break;

          case 5 :
              echo ' <span class="label label-danger">x5</span>';
          break;

          case 6 :
               echo '<span class="label label-purple">x6</span>';
          break;

          case 7 :
               echo '<span class="label label-info">x7</span>';
          break;

          case 8 :
              echo '<span class="label label-success">x8</span>';
          break;

          case 9 :
              echo ' <span class="label label-primary">x9</span>';
          break;
          case 10 : 
              echo ' <span class="label label-inverse">x10</span>';
          break;
          
          default: break;
              
         endswitch;  

         $fields['featured_img']['format_as'] = $fields['gallery']['format_as'] = 'imgsmall';      

         $bidup = 0.10;
?>

 @if(Session::has('messagetext'))
    
            {!! Session::get('messagetext') !!}
     
        @endif
  <ul class="parsley-error-list" style="margin-bottom: 0px;">
    @foreach($errors->all() as $error)
      <li>{!! $error !!}</li>
    @endforeach
  </ul>   
<section class="main-container col1-layout">
    <div class="main">
      <div class="container">
        <div class="row">
          <div class="col-main">
            <div class="product-view">
              <div class="product-essential">
                <form action="#" method="post" id="product_addtocart_form">
                  <input name="form_key" value="6UbXroakyQlbfQzK" type="hidden">
                  <div class="product-img-box col-lg-4 col-sm-5 col-xs-12">
                    <div class="new-label new-top-left"> @if($row->completed=='1') {!! html_entity_decode(Lang::get('bidding.2')) !!} @else {!! html_entity_decode(Lang::get('bidding.3')) !!} @endif </div>
                    <div class="product-image">
                      <div class="product-full"> <img id="product-zoom" src="{!! asset('uploads/products') !!}{{$row->featured_img}}" data-zoom-image="{!! asset('uploads/products') !!}{{$row->featured_img}}" alt="product-image"/> </div>
                      <div class="more-views">
                        <div class="slider-items-products">
                          <div id="gallery_01" class="product-flexslider hidden-buttons product-img-thumb">
                            <div class="slider-items slider-width-col4 block-content">
                              <div class="more-views-items"> <a href="#" data-image="{!! asset('uploads/products') !!}{{$row->featured_img}}" data-zoom-image="{!! asset('uploads/products') !!}{{$row->featured_img}}"> <img id="product-zoom"  src="{!! asset('uploads/products') !!}{{$row->gallery}}" alt="product-image"/> </a></div>
                              <div class="more-views-items"> <a href="#" data-image="{!! asset('uploads/products') !!}{{$row->gallery}}" data-zoom-image="{!! asset('uploads/products') !!}{{$row->gallery}}"> <img id="product-zoom"  src="products-images/product-4.jpg" alt="product-image"/> </a></div>
                              <div class="more-views-items"> <a href="#" data-image="products-images/product-5.jpg" data-zoom-image="products-images/product-5.jpg"> <img id="product-zoom"  src="products-images/product-5.jpg" alt="product-image"/> </a></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!-- end: more-images --> 
                  </div>
                  <div class="product-shop col-lg-8 col-sm-7 col-xs-12">
                    <div class="product-next-prev"> <a class="product-next" href="#"><span></span></a> <a class="product-prev" href="#"><span></span></a> </div>
                       
                        <div class="product-name">
                      <h1>{!!$row->name_th!!}</h1>
                    </div>
                    <div class="item-info">
                            <div class="info-inner">
                               
                <div class="bid-time" data-countdown="{!!$row->bid_end!!}" style="<?php if($row->completed=="1") echo 'display:none; '; ?>font-size: 36px;
    color: #a00101; font-family: roboto-bold;">00:00:00</div>
                              <div class="item-content">
                                
                                <div class="item-price">
                                  <div class="price-box">
                                    <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> 
                                    @if($row->product_value_bid!="") 
                                    {!!number_format($row->product_value_bid,2) !!} 
                                    <?php else: $bidup = 1.1;  ?> 1.00
                                    @endif THB </span> 
                                    </p>
                                    <p class="old-price"> <span class="price-label">ราคาปกติ</span> <span class="price"> 
                  <?php echo number_format($row->price,2); ?> THB </span> </p>
                                  </div>
                                </div>
                         
                              </div>
                            </div>
                          </div>  
                          
                
                    
                    
                    <div class="short-description">
                      <h2>ประวัติการประมูล</h2>
                      <p>ทองเต็มตัว </p>
                    </div>
                    <div class="add-to-box">
                      <div class="add-to-cart" data-id="<?php echo $row->products_bid_id; ?>" data-productval="<?php echo $bidup ?>" data-boots="<?php echo $row->boots; ?>" data-customer="<?php echo Session::get('uid'); ?>">
                         
                 
                        <div class="pull-left">
                          <div class="custom pull-left">
                            <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty &gt; 0 ) result.value--;return false;" class="reduced items-count" type="button"><i class="fa fa-minus">&nbsp;</i></button>
                            <input type="text" class="input-text qty autobidcusinput" title="Qty" value="1" maxlength="12" id="qty" name="qty">
                            <button onClick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;" class="increase items-count" type="button"><i class="fa fa-plus">&nbsp;</i></button>
                            <button data-id="{!!$row->products_bid_id!!}" data-boots="{!!$row->boots!!}" class="button btn-cart autobid-button" title="Add to Cart" type="button" data-bidtype="custombid">ออโต้บิด</span></button>
                          </div>
                        </div>
                        <button onClick="productAddToCartForm.submit(this)" class="button btn-cart biddingpusher" title="Add to Cart" type="button" id="bid-<?php echo $row->products_bid_id; ?>">ประมูลเลย</span></button>
                      </div>

                      <?php 
                       $checkwlists = BidtransrecordsController::checkwlists(); 
                      ?>
                      <div class="email-addto-box">
                        <ul class="add-to-links">
                          <li> <a style="<?php if($checkwlists=="1") echo 'color: #fff; background: #392420; border: 1px #392420 solid;'; ?>" class="link-wishlist" href="#" data-id="{!!$row->products_bid_id!!}" id="addtowishlist"><span>Add to Wishlist</span></a></li>
                            </ul>
                       
                      </div>
                    </div>
                    <div class="social">
                      <ul class="link">
                        <li class="fb"><a href="{!!$page->facebook->shareUrl!!}"></a></li>
                        <li class="tw"><a href="{!!$page->twitter->shareUrl!!}"></a></li>
                        <li class="googleplus"><a href="{!!$page->plus->shareUrl!!}"></a></li>
                        <li class="pintrest"><a href="{!!$page->pinterest->shareUrl!!}"></a></li>
                        <li class="linkedin"><a href="{!!$page->linkedin->shareUrl!!}"></a></li>
                      </ul>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
      
        </div>
      </div>
    </div>
  </section>
  <!-- Main Container End --> 
  
  <!-- Related Products Slider -->
  
  <div class="container">
  
   <!-- Related Slider -->
   
  <div class="related-pro">

      <div class="slider-items-products">
        <div class="related-block">
          <div id="related-products-slider" class="product-flexslider hidden-buttons">
            <div class="home-block-inner">
              <div class="block-title">
             
                  <em>รายการที่กำลังประมูล</em>
              </div>
    <a href="grid.html" class="view_more_bnt">ดูทั้งหมด</a>  
              
            </div>
           
            <div class="slider-items slider-width-col4 products-grid block-content">
                @include('pages/auction_inbidding')
              ?>
            </div>
          </div>
        </div>
      </div>

  </div>
  <!-- End related products Slider --> 
  
 
  
    
  </div>








