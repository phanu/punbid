<div class="footer-newsletter">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-7">
			  <h5 class="newsletter-title">กรอก E-mail เพื่อรับสิทธิพิเศษมากมาย</h5>
				<span class="newsletter-marketing-text">มูลค่ามากกว่า 1,000,0000 บาท<strong></strong></span>
			</div>
			<div class="col-xs-12 col-sm-5">
				<form>
					<div class="input-group">
						<input type="text" class="form-control" placeholder="Enter your email address">
						<span class="input-group-btn">
							<button class="btn btn-secondary" type="button">Sign Up</button>
						</span>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>