<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Orders;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use App\User;
class OrdersController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'orders';
	static $per_page	= '10';
	
	public function __construct() 
	{
		parent::__construct();
		$this->model = new Orders();
		$this->modelview = new  \App\Models\Orderdetail();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'			=> 	$this->info['title'],
			'pageNote'			=>  $this->info['note'],
			'pageModule'		=> 'orders',
			'pageUrl'			=>  url('orders'),
			'return' 			=> 	self::returnUrl()	
		);		
				
	} 
	
	public function getIndex()
	{
		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
				
		$this->data['access']		= $this->access;	
		return view('orders.index',$this->data);
	}	

	public function postData( Request $request)
	{ 
		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : $this->info['setting']['orderby']); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : $this->info['setting']['ordertype']);
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = '';	
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		} 

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : $this->info['setting']['perpage'] ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('orders/data');
		
		$this->data['param']		= $params;
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		$this->data['setting'] 		= $this->info['setting'];
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('orders.table',$this->data);

	}

			
	function getUpdate(Request $request, $id = null)
	{

		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] 		=  $row;
		} else {
			$this->data['row'] 		= $this->model->getColumnTable('orders'); 
		}
		$this->data['setting'] 		= $this->info['setting'];
		$this->data['fields'] 		=  \AjaxHelpers::fieldLang($this->info['config']['forms']);
		$this->data['subform'] = $this->detailview($this->modelview ,  $this->info['config']['subform'] ,$id );
		$this->data['id'] = $id;

		return view('orders.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
			
			$this->data['id'] = $id;
			$this->data['access']		= $this->access;
			$this->data['setting'] 		= $this->info['setting'];
			$this->data['fields'] 		= \AjaxHelpers::fieldLang($this->info['config']['grid']);
			$this->data['subgrid']		= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
			return view('orders.view',$this->data);

		} else {

			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_error')
			));		
		}		
	}	


	function postCopy( Request $request)
	{
		
	    foreach(\DB::select("SHOW COLUMNS FROM orders ") as $column)
        {
			if( $column->Field != 'orderNumber')
				$columns[] = $column->Field;
        }
		if(count($request->input('ids')) >=1)
		{

			$toCopy = implode(",",$request->input('ids'));
			
					
			$sql = "INSERT INTO orders (".implode(",", $columns).") ";
			$sql .= " SELECT ".implode(",", $columns)." FROM orders WHERE orderNumber IN (".$toCopy.")";
			\DB::select($sql);
			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success')
			));		

		} else {
			return response()->json(array(
				'status'=>'success',
				'message'=> 'Please select row to copy'
			));	
		}

	
	}		

	function postSave( Request $request, $id =0)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('orders');
			
			$id = $this->model->insertRow($data , $request->input('orderNumber'));
			$this->detailviewsave( $this->modelview , $request->all() ,$this->info['config']['subform'] , $id) ;
			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success')
				));	
			
		} else {

			$message = $this->validateListError(  $validator->getMessageBag()->toArray() );
			return response()->json(array(
				'message'	=> $message,
				'status'	=> 'error'
			));	
		}	
	
	}	

	public function postDelete( Request $request)
	{

		if($this->access['is_remove'] ==0) {   
			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_restric')
			));
			die;

		}		
		// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));
			\DB::table('orderdetails')->whereIn('orderNumber',$request->input('ids'))->delete();
			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success_delete')
			));
		} else {
			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_error')
			));

		} 		

	}

	public static function display( )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Orders();
		$info = $model::makeInfo('orders');

		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']
			
		);

		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return $data;
			} 

		} else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  10000 ,
				'sort'		=> 'orderNumber' ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1 
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];	

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit']; 
			$data['pagination'] = $pagination;
			return $data;			
		}


	}

	function postSavepublic( Request $request)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('orders');		
			$id = $this->model->insertRow($data , $request->input('orderNumber'));

		$data['subform'] = $this->detailviewsave( $this->modelview , $request->all() ,$this->info['config']['subform'] , $id);
		$data['orid'] = $id;

		if($request->input('payment_type')=="spay"){
			$data['instant'] = 'spay';
		}else{
			$data['instant'] = 'shop';
		}

			if(\Session::get('discount')>0||\Session::get('discount')!="")
			{

			\DB::table('discount_to_user')->insert([
											'customerNumber' => \Session::get('uid'), 'coupon_id' => \Session::get('discount_id') , 'discount' => \Session::get('discount')
											]);
			}

			\DB::table('tb_notification')->insert([
											'userid' => \Session::get('uid'), 'title' => \Lang::get('core.orderl').$data['orid'].\Lang::get('core.ordercon'), 'noticlass' => 'icon bg-success', 'icon' => 'zmdi zmdi-plus-circle-o',
											]);

			$data['userdet'] = User::find(\Session::get('uid'));

			$to = $data['userdet']->email;
				$subject = \Lang::get('core.orderl').$data['orid'].\Lang::get('core.ordercon');
				$message = view('user.emails.thankyou', $data)->render();
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: '.CNF_APPNAME.' <'.CNF_EMAIL.'>' . "\r\n";
						mail($to, $subject, $message, $headers);

			if($request->input('payment_type')=="dbt")
			{	

				return Redirect::to('thankyou')->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success')->with('data',$data);
			}elseif($request->input('payment_type')=="credit_card")
			{
				return Redirect::to('paysbuycheckout')->with('data',$data);

			}elseif($request->input('payment_type')=="spay")
			{

				$SPAY = \DB::table('tb_users')->whereId($request->customerNumber)->value('spay');

				if($SPAY<$request->input('payment_fee'))
				{

					\DB::table('tb_notification')->insert([
											'userid' => \Session::get('uid'), 'title' => \Lang::get('core.orderl').$data['orid'].\Lang::get('core.orderalreadypay'), 'noticlass' => 'icon bg-success', 'icon' => 'zmdi zmdi-plus-circle'
											]);
					return  Redirect::back()->with('messagetext','<p class="alert alert-danger">Spay ไม่เพียงพอ</p>')->with('msgstatus','error')->withErrors($validator)->withInput();

				}else
				{
					\DB::table('orders')->where('orderNumber', '=', $id)->update(array('status' => 'process'));
					\DB::table('tb_users')->whereId($request->customerNumber)->decrement('spay', $request->input('payment_fee'));		
					return Redirect::to('thankyou')->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success')->with('data',$data);
				}
			}

		} else {

			return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	

	

function postSavecredit( Request $request)
{
		
$model  = new Orders();
// received result from paysbuy with data result,apCode,amt,fee,methos,confirm_cs
$result = $_POST["result"];
$len = strlen($result);
$payment_status = substr($result, 0,2);
$strInvoice = substr($result, 2,$len-2);
$apCode = $_POST["apCode"];
$amt = $_POST["amt"];
$fee = $_POST["fee"];
$method = $_POST["method"];
$confirm_cs = isset($_POST["confirm_cs"])?$_POST["confirm_cs"]:'';
/* status result
00=Success
99=Fail
02=Process
*/
if ($payment_status == "00") {
	if ($method == "06") {
		if ($confirm_cs == "true") {

			\DB::table('tb_notification')->insert([
											'userid' => \Session::get('uid'), 'title' => \Lang::get('core.orderl').$strInvoice.\Lang::get('core.orderalreadypay'), 'noticlass' => 'icon bg-success', 'icon' => 'zmdi zmdi-plus-circle'
											]);

			$orderdata = $model::where('orderNumber', '=', $strInvoice)->first();

			$orderdata['userdet'] = User::find(\Session::get('uid'));
			
			$to = $orderdata['userdet']->email;
				$subject = \Lang::get('core.orderl').$strInvoice.\Lang::get('core.orderalreadypay');
				$message = view('user.emails.thankyou', $orderdata)->render();
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: '.CNF_APPNAME.' <'.CNF_EMAIL.'>' . "\r\n";
						mail($to, $subject, $message, $headers);
			//success
			\DB::table('orders')->where('orderNumber', '=', $strInvoice)->update(array('status' => 'process'));
				return Redirect::to('thankyou')->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success')->with('data',$data);

			} else if ($confirm_cs == "false") {
				//fail
				return  Redirect::to('thankyou')->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
							->withErrors($validator)->withInput();
			} else {
				//process
				return  Redirect::to('thankyou')->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
							->withErrors($validator)->withInput();
		}
	} else {
				$orderdata = $model::where('orderNumber', '=', $strInvoice)->first();

				$orderdata['userdet'] = User::find(\Session::get('uid'));

				$to = $orderdata['userdet']->email;
				$subject = \Lang::get('core.orderl').$strInvoice.\Lang::get('core.orderalreadypay');
				$message = view('user.emails.thankyou', $orderdata)->render();
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: '.CNF_APPNAME.' <'.CNF_EMAIL.'>' . "\r\n";
						mail($to, $subject, $message, $headers);

				\DB::table('tb_notification')->insert([
											'userid' => \Session::get('uid'), 'title' => \Lang::get('core.orderidadd').$strInvoice.\Lang::get('core.orderalreadypay'), 'noticlass' => 'icon bg-success', 'icon' => 'zmdi zmdi-plus-circle'
											]);
				//success
				\DB::table('orders')->where('orderNumber', '=', $strInvoice)->update(array('status' => 'process'));

				return Redirect::to('thankyou')->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success')->with('data',$data);

	}
} else if ($payment_status == "99") {
				//fail
				return  Redirect::to('thankyou')->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
							->withErrors($validator)->withInput();

} else if ($payment_status == "02") {
				//process
				return  Redirect::to('thankyou')->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
							->withErrors($validator)->withInput();

} else {

	return  Redirect::to('thankyou')->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
				->withErrors($validator)->withInput();
}


			
		
	
	}	
				

}