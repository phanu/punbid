﻿<?php include('header.php'); ?>
  <div class="main-container col2-right-layout">
    <div class="main container">
      <div class="row">
        <section class="col-sm-9 wow bounceInUp animated">
        <div class="col-main">
          <div class="my-account">
            <div class="page-title">
              <h2>ซื้อ Bid Pack</h2>
            </div>
            <div class="my-wishlist">
              <div class="table-responsive">
                <form method="post" action="#/wishlist/index/update/wishlist_id/1/" id="wishlist-view-form">
                  <fieldset>
                    <input type="hidden" value="ROBdJO5tIbODPZHZ" name="form_key">
                    <table id="wishlist-table" class="clean-table linearize-table data-table">
                      <thead>
                        <tr class="first last">
                          <th class="customer-wishlist-item-image"></th>
                          <th class="customer-wishlist-item-info"></th>
                          <th class="customer-wishlist-item-quantity">จำนวน</th>
                          <th class="customer-wishlist-item-price">ราคา</th>
                          <th class="customer-wishlist-item-cart"></th>
                          <th class="customer-wishlist-item-remove"></th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr id="item_31" class="first odd" style="
    /* padding-top: 20px; */
">
                          <td class="wishlist-cell0 customer-wishlist-item-image"><a title="Softwear Women's Designer" href="#/" class="product-image"> <img width="100" alt="Softwear Women's Designer" src="products-images/bidpack.png"> </a></td>
                          <td class="wishlist-cell1 customer-wishlist-item-info"><h3 class="product-name"><a title="Softwear Women's Designer" href="#">Bid Pack x 100ea</a></h3>
                            <div class="description std">
                              <div class="inner">
โปรโมชั่น แถมฟรี 10ea</div>
                            </div>
                            </td>
                          <td data-rwd-label="Quantity" class="wishlist-cell2 customer-wishlist-item-quantity"><div class="cart-cell">
                              <div class="add-to-cart-alt">
                                <input type="text" value="1" name="qty[31]" class="input-text qty validate-not-negative-number" pattern="\d*">
                              </div>
                            </div></td>
                          <td data-rwd-label="Price" class="wishlist-cell3 customer-wishlist-item-price"><div class="cart-cell">
                              <div class="price-box"> <span id="product-price-39" class="regular-price"> <span class="price">1,000 THB</span> </span> </div>
                            </div></td>
                          
                          
                        </tr>
                        <tr id="item_33" class="odd">
                          <td class="wishlist-cell0 customer-wishlist-item-image"><a title="Bid Pack x 300ea" href="#" class="product-image"> <img width="100" alt="Bidpack" src="products-images/bidpack.png"> </a></td>
                          <td class="wishlist-cell1 customer-wishlist-item-info"><h3 class="product-name"><a title="Softwear Women's Designer" href="#">Bid Pack x 200ea</a></h3>
                            <div class="description std">
                              <div class="inner">โปรโมชั่น แถมฟรี 20ea</div>
                            </div>
                            </td>
                          <td data-rwd-label="Quantity" class="wishlist-cell2 customer-wishlist-item-quantity"><div class="cart-cell">
                              <div class="add-to-cart-alt">
                                <input type="text" value="1" name="qty[31]" class="input-text qty validate-not-negative-number" pattern="\d*">
                              </div>
                            </div></td>
                          <td data-rwd-label="Price" class="wishlist-cell3 customer-wishlist-item-price"><div class="cart-cell">
                              <div class="price-box"> <span id="product-price-39" class="regular-price"> <span class="price">2,000 THB</span> </span> </div>
                            </div></td>
                          
                          
                        </tr>
                        <tr id="item_32" class="last even">
                          <td class="wishlist-cell0 customer-wishlist-item-image"><a title="Slim Fit Casual Shirt" href="#" class="product-image"> <img width="100" alt="Slim Fit Casual Shirt" src="products-images/bidpack.png"> </a></td>
                          <td class="wishlist-cell1 customer-wishlist-item-info"><h3 class="product-name"><a title="Slim Fit Casual Shirt" href="#">Bid Pack x 300ea</a></h3>
                            <div class="description std">
                              <div class="inner">โปรโมชั่น แถมฟรี 30ea</div>
                            </div>
                            </td>
                          <td data-rwd-label="Quantity" class="wishlist-cell2 customer-wishlist-item-quantity"><div class="cart-cell">
                              <div class="add-to-cart-alt">
                                <input type="text" value="1" name="qty[32]" class="input-text qty validate-not-negative-number" pattern="\d*">
                              </div>
                            </div></td>
                          <td data-rwd-label="Price" class="wishlist-cell3 customer-wishlist-item-price"><div class="cart-cell">
                              <div class="price-box"> <span id="product-price-2" class="regular-price"> <span class="price">3,000 THB</span> </span> </div>
                            </div></td>
                          
                          
                        </tr>
                      </tbody>
                    </table>
                    <div class="buttons-set buttons-set2">
                      
                      <button class="button btn-add" onclick="addAllWItemsToCart()" title="Add All to Cart" type="button"><span>ซื้อเลย</span></button>
                      
                    </div>
                  </fieldset>
                </form>
              </div>
            </div>
            
          </div></div>
        </section>
		<aside class="col-right sidebar col-sm-3 wow bounceInUp animated">
          <div class="block block-account">
            <div class="block-title">My Account</div>
            <div class="block-content">
              <ul>
                <li><a href="#">Account Dashboard</a></li>
                <li><a href="#">Account Information</a></li>
                <li><a href="#">Address Book</a></li>
                <li><a href="#">My Orders</a></li>
                <li><a href="#">Billing Agreements</a></li>
                <li><a href="#">Recurring Profiles</a></li>
                <li><a href="#">My Product Reviews</a></li>
                <li><a href="#">My Tags</a></li>
                <li class="current"><a href="#">My Wishlist</a></li>
                <li><a href="#">My Downloadable</a></li>
                <li class="last"><a href="#">Newsletter Subscriptions</a></li>
              </ul>
            </div>
          </div>
          
        </aside>
      </div>
    </div>
  </div>
  
<?php include('footer.php'); ?>