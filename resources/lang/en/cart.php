<?php 
return array( 
"1"			=> "Added Product",  
 "2"			=> " The item has been updated",  
 "3"			=> "Product was removed",  
 "4"			=> "Empty Cart",  
 "5"			=> "Products",  
 "6"			=> "Quantity",  
 "7"			=> "Price / Unit",  
 "8"			=> "Update Cart",  
 "9"			=> "Discount Coupon",  
 "10"			=> "Agree",  
 "11"			=> "Total",  
 "12"			=> "Price",  
 "13"			=> "Delivery Price",  
 "14"			=> "Total Amount",  
 "15"			=> "Payment",  
 "16"			=> "Continue Shopping",  
 );