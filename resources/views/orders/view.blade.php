@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	
@endif	
	<div class="sbox-content" style="    display: -webkit-box;"> 
  				<div class="col-lg-12">
                    <div class="margin-bottom-50">
                        <div class="nav-tabs-horizontal">
                            <div class="tab-content padding-horizontal-10">
							<ul class="nav nav-tabs" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" href="javascript: void(0);" data-toggle="tab" data-target="#home{{ $row->orderNumber }}" role="tab">  {{ $pageTitle}} : View Detail <i class="icmn-home"></i></a>
                                        
                                    </a>
                                </li>
                            		@foreach($subgrid as $sub)
								<li class="nav-item">
                                    <a class="nav-link" href="javascript: void(0);" data-toggle="tab" data-target="#{{ str_replace(" ","_",$sub['title']) }}{{ $row->{$sub['master_key']} }}" role="tab">{{ $pageTitle}} :  {{ $sub['title'] }}
                                        <i class="icmn-database"></i>
                                    </a>
                                </li>
									@endforeach
                            </ul>
                            </div>
                        </div>
                            <div class="tab-content padding-horizontal-10">
                                <div class="tab-pane active" id="home{{ $row->orderNumber }}" role="tabpanel">
                                    <div class="table-responsive" >  
										<table class="table table-striped table-bordered" >
											<tbody>	
												
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('OrderNumber', (isset($fields['orderNumber']['language'])? $fields['orderNumber']['language'] : array())) }}</td>
													<td>{{ $row->orderNumber}} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('OrderDate', (isset($fields['orderDate']['language'])? $fields['orderDate']['language'] : array())) }}</td>
													<td>{{ $row->orderDate}} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('ShippedDate', (isset($fields['shippedDate']['language'])? $fields['shippedDate']['language'] : array())) }}</td>
													<td>{{ $row->shippedDate}} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Status', (isset($fields['status']['language'])? $fields['status']['language'] : array())) }}</td>
													<td>{{ $row->status}} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Comments', (isset($fields['comments']['language'])? $fields['comments']['language'] : array())) }}</td>
													<td>{{ $row->comments}} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('CustomerNumber', (isset($fields['customerNumber']['language'])? $fields['customerNumber']['language'] : array())) }}</td>
													<td>{{ SiteHelpers::formatLookUp($row->customerNumber,'customerNumber','1:tb_users:id:id|first_name|last_name') }} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tracking Number', (isset($fields['tracking_number']['language'])? $fields['tracking_number']['language'] : array())) }}</td>
													<td>{{ $row->tracking_number}} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Payment Type', (isset($fields['payment_type']['language'])? $fields['payment_type']['language'] : array())) }}</td>
													<td>{{ $row->payment_type}} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Payment Fee', (isset($fields['payment_fee']['language'])? $fields['payment_fee']['language'] : array())) }}</td>
													<td>{{ $row->payment_fee}} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Firstname', (isset($fields['firstname']['language'])? $fields['firstname']['language'] : array())) }}</td>
													<td>{{ $row->firstname}} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Lastname', (isset($fields['lastname']['language'])? $fields['lastname']['language'] : array())) }}</td>
													<td>{{ $row->lastname}} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Address', (isset($fields['address']['language'])? $fields['address']['language'] : array())) }}</td>
													<td>{{ $row->address}} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Postcode', (isset($fields['postcode']['language'])? $fields['postcode']['language'] : array())) }}</td>
													<td>{{ $row->postcode}} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sex', (isset($fields['sex']['language'])? $fields['sex']['language'] : array())) }}</td>
													<td>{{ $row->sex}} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Phone', (isset($fields['phone']['language'])? $fields['phone']['language'] : array())) }}</td>
													<td>{{ $row->phone}} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Province', (isset($fields['province']['language'])? $fields['province']['language'] : array())) }}</td>
													<td>{{ $row->province}} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('District', (isset($fields['district']['language'])? $fields['district']['language'] : array())) }}</td>
													<td>{{ $row->district}} </td>
													
												</tr>
											
												<tr>
													<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Subdistrict', (isset($fields['subdistrict']['language'])? $fields['subdistrict']['language'] : array())) }}</td>
													<td>{{ $row->subdistrict}} </td>
													
												</tr>
													
											</tbody>	
										</table>  		
									</div>
                                </div>
                                <div class="tab-pane" id="{{ str_replace(" ","_",$sub['title']) }}{{ $row->{$sub['master_key']} }}" role="tabpanel">
                                	<div class="table-responsive" >  
										<table class="table table-striped table-bordered" >

										</table>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>

</div>	

</div>

<script type="text/javascript">
	$(function(){
		<?php foreach($subgrid as $sub) { ?>
			$('#{{ str_replace(" ","_",$sub['title']) }}{{ $row->{$sub['master_key']} }}').load('{!! url($sub['module']."/lookup/".implode("-",$sub)."-".$row->{$sub['master_key']})!!}')
		<?php } ?>

		
	})

</script>