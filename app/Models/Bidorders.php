<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class bidorders extends Sximo  {
	
	protected $table = 'bid_orders';
	protected $primaryKey = 'orderNumber';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT bid_orders.* FROM bid_orders  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE bid_orders.orderNumber IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
