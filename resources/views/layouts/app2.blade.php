<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>{{ CNF_APPNAME }}</title>


    <link href="{{ asset('favicon.ico')}}" rel="icon" type="image/png">
    <link href="favicon.ico" rel="shortcut icon">

    <!-- HTML5 shim and Respond.js for < IE9 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
        <link href="{{ asset('sximo/js/plugins/jasny-bootstrap/css/jasny-bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{ asset('sximo/fonts/awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{ asset('sximo/js/plugins/bootstrap.summernote/summernote.css')}}" rel="stylesheet">
        <link href="{{ asset('sximo/js/plugins/datepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
        <link href="{{ asset('sximo/js/plugins/bootstrap.datetimepicker/css/bootstrap-datetimepicker.min.css')}}" rel="stylesheet">
        <link href="{{ asset('sximo/js/plugins/select2/select2.css')}}" rel="stylesheet">
        <link href="{{ asset('sximo/js/plugins/iCheck/skins/square/red.css')}}" rel="stylesheet">
        <link href="{{ asset('sximo/js/plugins/fancybox/jquery.fancybox.css') }}" rel="stylesheet">
        <link href="{{ asset('sximo/js/plugins/markitup/skins/simple/style.css') }}" rel="stylesheet">
        <link href="{{ asset('sximo/js/plugins/markitup/sets/default/style.css') }}" rel="stylesheet">
            
        <link href="{{ asset('sximo/css/animate.css')}}" rel="stylesheet">      
        <link href="{{ asset('sximo/css/icons.min.css')}}" rel="stylesheet">
        <link href="{{ asset('sximo/js/plugins/toastr/toastr.css')}}" rel="stylesheet">
    <!-- Vendors Styles -->
 
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/jquery/jquery.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/tether/dist/js/tether.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/jscrollpane/script/jquery.jscrollpane.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/spin.js/spin.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/ladda/dist/ladda.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/html5-form-validation/dist/jquery.validation.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/jquery-typeahead/dist/jquery.typeahead.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/autosize/dist/autosize.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/bootstrap-show-password/bootstrap-show-password.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/moment/min/moment.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/cleanhtmlaudioplayer/src/jquery.cleanaudioplayer.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/cleanhtmlvideoplayer/src/jquery.cleanvideoplayer.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/bootstrap-sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/summernote/dist/summernote.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/ionrangeslider/js/ion.rangeSlider.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/nestable/jquery.nestable.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/datatables/media/js/dataTables.bootstrap4.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/datatables-responsive/js/dataTables.responsive.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/editable-table/mindmup-editabletable.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/d3/d3.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/c3/c3.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/chartist/dist/chartist.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/peity/jquery.peity.min.js"></script>
    <!-- v1.0.1 -->
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/chartist-plugin-tooltip/dist/chartist-plugin-tooltip.min.js"></script>
    <!-- v1.1.1 -->
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/gsap/src/minified/TweenMax.min.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/hackertyper/hackertyper.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/vendors/jquery-countTo/jquery.countTo.js"></script>

    <!-- Clean UI Scripts -->
    <script src="{{ asset('sximo/ui/build/assets')}}/common/js/common.js"></script>
    <script src="{{ asset('sximo/ui/build/assets')}}/common/js/demo.temp.js"></script>

    <script type="text/javascript" src="{{ asset('sximo/js/plugins/jquery.cookie.js') }}"></script>         
        <script type="text/javascript" src="{{ asset('sximo/js/plugins/jquery-ui.min.js') }}"></script>             
        <script type="text/javascript" src="{{ asset('sximo/js/plugins/iCheck/icheck.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('sximo/js/plugins/select2/select2.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('sximo/js/plugins/fancybox/jquery.fancybox.js') }}"></script>
        <script type="text/javascript" src="{{ asset('sximo/js/plugins/prettify.js') }}"></script>
        <script type="text/javascript" src="{{ asset('sximo/js/plugins/parsley.js') }}"></script>
        <script type="text/javascript" src="{{ asset('sximo/js/plugins/datepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('sximo/js/plugins/switch.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('sximo/js/plugins/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

        <script type="text/javascript" src="{{ asset('sximo/js/sximo.js') }}"></script>
        <script type="text/javascript" src="{{ asset('sximo/js/plugins/jquery.form.js') }}"></script>
        <script type="text/javascript" src="{{ asset('sximo/js/plugins/jquery.jCombo.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('sximo/js/plugins/toastr/toastr.js') }}"></script>
        <script type="text/javascript" src="{{ asset('sximo/js/plugins/bootstrap.summernote/summernote.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('sximo/js/simpleclone.js') }}"></script>
        
        <script type="text/javascript" src="{{ asset('sximo/js/plugins/markitup/jquery.markitup.js') }}"></script>  
        <script type="text/javascript" src="{{ asset('sximo/js/plugins/markitup/sets/default/set.js') }}"></script> 
        <!-- AJax -->
        <link href="{{ asset('sximo/js/plugins/ajax/ajaxSximo.css')}}" rel="stylesheet"> 
        <script type="text/javascript" src="{{ asset('sximo/js/plugins/ajax/ajaxSximo.js') }}"></script>

        <!-- End Ajax -->
</head>
<body class="theme-dark mode-default menu-fixed colorful-enabled" style="padding:50px;">


            
<section class="page-content">
<div class="page-content-inner">
    @yield('content')
</div>
</section>

<div class="main-backdrop"><!-- --></div>
<div class="modal fade" id="sximo-modal" tabindex="-1" role="dialog">
<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header bg-default">
        
        <button type="button " class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Modal title</h4>
    </div>
    <div class="modal-body" id="sximo-modal-content">

    </div>

  </div>
</div>
</div>


        <script language="javascript">
        jQuery(document).ready(function($)  {
           $('.markItUp').markItUp(mySettings );
        });
        </script>

{{ Sitehelpers::showNotification() }} 
<script type="text/javascript">
jQuery(document).ready(function ($) {

    $('#sidemenu').sximMenu();
    setInterval(function(){ 
        var noteurl = $('.notif-value').attr('code'); 
        $.get('{{ url("notification/load") }}',function(data){
            $('.notif-alert').html(data.total);
            var html = '';
            $.each( data.note, function( key, val ) {
                html += '<li><a href="'+val.url+'"> <div> <i class="'+val.icon+' fa-fw"></i> '+ val.title+'  <span class="pull-right text-muted small">'+val.date+'</span></div></li>';
                html += '<li class="divider"></li>';             
            });
            html += '<li><div class="text-center link-block"><a href="'+noteurl+'/notification"><strong>View All Notification</strong> <i class="fa fa-angle-right"></i></a></div></li>';
            $('.notif-value').html(html);
        });
    }, 10000);
        
}); 
    
    
</script>
</body>
</html>