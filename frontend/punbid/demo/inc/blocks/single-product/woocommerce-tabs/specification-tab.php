<h3>ประวัติการประมูล</h3>
<table class="table">
	<tbody>
		<tr>
			<td>ผู้ประมูล</td>
			<td>ประเภทบิด</td>
		</tr>
		<tr>
			<td>จรัล มโนเพชร</td>
			<td>ออโต้ บิด</td>
		</tr>
		<tr>
			<td>ยักษ์ไม่มีเพ็ด</td>
			<td>บิดมือ</td>
		</tr>
		<tr>
			<td>อำนวย</td>
			<td>บิดมือ</td>
		</tr>
		<tr class="size-weight">
			<td>อันทะ โอราน</td>
			<td>ออโต้ บิด</td>
		</tr>
		<tr class="size-weight">
			<td>มณี</td>
			<td>ออโต้ บิด</td>
		</tr>
		<tr class="item-model-number">
			<td>โอบาม่า</td>
			<td>บิดมือ</td>
		</tr>
		<tr>
			<td>สาวน้อยปะแป้ง</td>
			<td>ออโต้ บิด</td>
		</tr>
		<tr>
			<td>เกิดมาบิดทอง</td>
			<td>บิดมือ</td>
		</tr>
		<tr>
			<td>ผู้ชนะสิบทิศ</td>
			<td>ออโต้ บิด</td>
		</tr>
		<tr>
			<td>มณี</td>
			<td>บิดมือ</td>
		</tr>
		<tr>
			<td>อำนวย</td>
			<td>ออโต้ บิด</td>
		</tr>
		<tr>
			<td>ลำพัง ไพบูลย์</td>
			<td>บิดมือ</td>
		</tr>
		<tr>
			<td>สว่าง กระจ่าง</td>
			<td>บิดมือ</td>
		</tr>
		<tr>
			<td>ผู้ชนะสิบทิศ</td>
			<td>ออโต้ บิด</td>
		</tr>
		<tr>
			<td>ประจักษ์</td>
			<td>ออโต้ บิด</td>
		</tr>
		<tr>
			<td>ยักษ์ไม่มีเพ็ด</td>
			<td>ออโต้ บิด</td>
		</tr>
		<tr>
			<td>เกิดมาบิดทอง</td>
			<td>บิดมือ</td>
		</tr>
	</tbody>
</table>			
