
@if($setting['form-method'] =='native')
	<div class="sbox">
		<div class="sbox-title">  
			<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
				<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
			</h4>
	</div>

	<div class="sbox-content"> 
@endif	
			{!! Form::open(array('url'=>'orders/save/'.SiteHelpers::encryptID($row['orderNumber']), 'class'=>'form-vertical','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'ordersFormAjax')) !!}
			<ul class="nav nav-tabs"><li class="active"><a href="#orders" data-toggle="tab">orders</a></li>
				</ul><div class="tab-content"><div class="tab-pane m-t active" id="orders"> 
				{!! Form::hidden('orderNumber', $row['orderNumber']) !!}					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> OrderDate    </label>									
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('orderDate', $row['orderDate'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> ShippedDate    </label>									
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('shippedDate', $row['shippedDate'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Status  <span class="asterix"> * </span>  </label>									
										  
					<?php $status = explode(',',$row['status']);
					$status_opt = array( 'onhold' => 'On Hold' ,  'Inprocess' => 'In Process' ,  'Shipped' => 'Shipped' ,  'Cancel' => 'Cancel' , ); ?>
					<select name='status' rows='5' required  class='select2 '  > 
						<?php 
						foreach($status_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['status'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Comments    </label>									
										  <textarea name='comments' rows='5' id='comments' class='form-control '  
				           >{{ $row['comments'] }}</textarea> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> CustomerNumber  <span class="asterix"> * </span>  </label>									
										  {!! Form::text('customerNumber', $row['customerNumber'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Tracking Number    </label>									
										  {!! Form::text('tracking_number', $row['tracking_number'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Payment Type  <span class="asterix"> * </span>  </label>									
										  {!! Form::text('payment_type', $row['payment_type'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Payment Fee  <span class="asterix"> * </span>  </label>									
										  {!! Form::text('payment_fee', $row['payment_fee'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Firstname  <span class="asterix"> * </span>  </label>									
										  <textarea name='firstname' rows='5' id='firstname' class='form-control '  
				         required  >{{ $row['firstname'] }}</textarea> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Lastname  <span class="asterix"> * </span>  </label>									
										  <textarea name='lastname' rows='5' id='lastname' class='form-control '  
				         required  >{{ $row['lastname'] }}</textarea> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Address  <span class="asterix"> * </span>  </label>									
										  <textarea name='address' rows='5' id='address' class='form-control '  
				         required  >{{ $row['address'] }}</textarea> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Phone  <span class="asterix"> * </span>  </label>									
										  <textarea name='phone' rows='5' id='phone' class='form-control '  
				         required  >{{ $row['phone'] }}</textarea> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Postcode  <span class="asterix"> * </span>  </label>									
										  <textarea name='postcode' rows='5' id='postcode' class='form-control '  
				         required  >{{ $row['postcode'] }}</textarea> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Sex  <span class="asterix"> * </span>  </label>									
										  <textarea name='sex' rows='5' id='sex' class='form-control '  
				         required  >{{ $row['sex'] }}</textarea> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Province  <span class="asterix"> * </span>  </label>									
										  <textarea name='province' rows='5' id='province' class='form-control '  
				         required  >{{ $row['province'] }}</textarea> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> District  <span class="asterix"> * </span>  </label>									
										  <textarea name='district' rows='5' id='district' class='form-control '  
				         required  >{{ $row['district'] }}</textarea> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Subdistrict  <span class="asterix"> * </span>  </label>									
										  <textarea name='subdistrict' rows='5' id='subdistrict' class='form-control '  
				         required  >{{ $row['subdistrict'] }}</textarea> 						
									  </div> 
			</div>
			
												
						
	<hr />
	<div class="clr clear"></div>
	
	<h5> order detail </h5>
	
	<div class="table-responsive">
    <table class="table table-striped ">
        <thead>
			<tr>
				@foreach ($subform['tableGrid'] as $t)
					@if($t['view'] =='1' && $t['field'] !='orderNumber')
						<th>{{ $t['label'] }}</th>
					@endif
				@endforeach
				<th></th>	
			  </tr>

        </thead>

        <tbody>
        @if(count($subform['rowData'])>=1)
            @foreach ($subform['rowData'] as $rows)
            <tr class="clone clonedInput">
									
			 @foreach ($subform['tableGrid'] as $field)
				 @if($field['view'] =='1' && $field['field'] !='orderNumber')
				 <td>					 
				 	{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] , $rows->{$field['field']}) !!}							 
				 </td>
				 @endif					 
			 
			 @endforeach
			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<input type="hidden" name="counter[]">
			 </td>
			@endforeach
			</tr> 

		@else
            <tr class="clone clonedInput">
									
			 @foreach ($subform['tableGrid'] as $field)

				 @if($field['view'] =='1' && $field['field'] !='orderNumber')
				 <td>					 
				 	{!! SiteHelpers::bulkForm($field['field'] , $subform['tableForm'] ) !!}							 
				 </td>
				 @endif					 
			 
			 @endforeach
			 <td>
			 	<a onclick=" $(this).parents('.clonedInput').remove(); return false" href="#" class="remove btn btn-xs btn-danger">-</a>
			 	<input type="hidden" name="counter[]">
			 </td>
			
			</tr> 

		
		@endif	


        </tbody>	

     </table>  
     <input type="hidden" name="enable-masterdetail" value="true">
     </div>
	<br /><br />
     
     <a href="javascript:void(0);" class="addC btn btn-xs btn-info" rel=".clone"><i class="fa fa-plus"></i> New Item</a>
     <hr />					
						
			<div style="clear:both"></div>	
							
			<div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">	
					<button type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
					<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
				</div>			
			</div> 		 
			{!! Form::close() !!}


@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

	
</div>	
			 
<script type="text/javascript">
$(document).ready(function() { 
	 
	$('.addC').relCopy({});
	$('.editor').summernote();
	$('.previewImage').fancybox();	
	$('.tips').tooltip();	
	$(".select2").select2({ width:"98%"});	
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'}); 
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});			
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("orders/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});
				
	var form = $('#ordersFormAjax'); 
	form.parsley();
	form.submit(function(){
		
		if(form.parsley('isValid') == true){			
			var options = { 
				dataType:      'json', 
				beforeSubmit :  showRequest,
				success:       showResponse  
			}  
			$(this).ajaxSubmit(options); 
			return false;
						
		} else {
			return false;
		}		
	
	});

});

function showRequest()
{
	$('.ajaxLoading').show();		
}  
function showResponse(data)  {		
	
	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		notyMessage(data.message);	
		$('#sximo-modal').modal('hide');	
	} else {
		notyMessageError(data.message);	
		$('.ajaxLoading').hide();
		return false;
	}	
}			 

</script>		 