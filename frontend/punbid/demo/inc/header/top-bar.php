<div class="top-bar">
	<div class="container">
		<nav>
			<ul id="menu-top-bar-left" class="nav nav-inline pull-left animate-dropdown flip">
				<li class="menu-item animate-dropdown"><a title="GoGoldRun.com เว็บประมูลทองออนไลน์ที่ในโลก" href="#">GoGoldRun.com เว็บประมูลทองออนไลน์ที่ในโลก</a></li>
			</ul>
		</nav>

		<nav>
			<ul id="menu-top-bar-right" class="nav nav-inline pull-right animate-dropdown flip">
				<li class="menu-item animate-dropdown"><a title="Store Locator" href="#"><i class="ec ec-map-pointer"></i>รายชื่อร้านทองในเครือ</a></li>
				<li class="menu-item animate-dropdown"><a title="Track Your Order" href="index.php?page=track-your-order"><i class="ec ec-transport"></i>ผู้ชนะประมูล</a></li>
				<li class="menu-item animate-dropdown"><a title="Shop" href="index.php?page=shop"><i class="ec ec-shopping-bag"></i>ซื้อสิทธิ์ประมูล</a></li>
				<li class="menu-item animate-dropdown"><a title="My Account" href="index.php?page=my-account"><i class="ec ec-user"></i>บัญชีของฉัน</a></li>
			</ul>
		</nav>
	</div>
</div><!-- /.top-bar -->
