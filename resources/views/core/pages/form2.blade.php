@extends('layouts.app2')
<!--srart theme style -->
		<link href="{{ asset('frontend') }}/punbidtheme/css/main.css" rel="stylesheet" type="text/css">
    @if( $row['shop'] =='1')
    <link href="{{ asset('frontend') }}/punbidtheme/css/style-shop.css" rel="stylesheet" type="text/css">
    @elseif( $row['shop'] =='2')
    <link href="{{ asset('frontend') }}/punbidtheme/css/style-bid.css" rel="stylesheet" type="text/css">
    @endif
		<!-- end theme style -->
		<link href="{{ asset('frontend') }}/punbidtheme/css/LivIconsEvo.css" rel="stylesheet" type="text/css">
		<!-- Important Owl stylesheet -->
		<link rel="stylesheet" href="{{ asset('frontend') }}/punbidtheme/owl-carousel/owl.carousel.css">
		<!-- Default Theme -->
		<link rel="stylesheet" href="{{ asset('frontend') }}/punbidtheme/owl-carousel/owl.theme.css">
		<!-- favicon links -->
		<link rel="shortcut icon" type="image/png" href="{{ asset('frontend') }}/punbidtheme/images/favicon.png">
		<link href="https://fonts.googleapis.com/css?family=Prompt:400,700&subset=thai" rel="stylesheet">
            <!-- Sweet Alert css -->
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />
         <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/css/menu.css" rel="stylesheet" type="text/css" />
         <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <!--
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/css/components.css" rel="stylesheet" type="text/css" />
        App CSS -
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/css/menu.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/css/responsive.css" rel="stylesheet" type="text/css" />-->
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/plugins/fileuploads/css/dropify.min.css" rel="stylesheet"/>
        <link href="{{ asset('frontend') }}/punbidtheme/css/custom.css" rel="stylesheet"/>
        
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ asset('frontend') }}/punbidtheme/Light/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- file uploads js -->

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
        <!-- AJax -->
        <link href="{{ asset('sximo/js/plugins/ajax/ajaxSximo.css')}}" rel="stylesheet"> 
@section('content')
  <div class="page-content row">


<div class="page-content-wrapper">
	@if(Session::has('message'))	  
		   {{ Session::get('message') }}
	@endif	
		
		<ul class="parsley-error-list">
			@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
		 {!! Form::open(array('url'=>'core/pages/save/'.$row['pageID'], 'class'=>'form-vertical row ','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

			<div class="col-sm-12 ">
				<div class="sbox">
					<div class="sbox-title">Page Content </div>	
					<div class="sbox-content">		

						<ul class="nav nav-tabs" >
						  <li class="active"><a href="#info" data-toggle="tab"> Page Content </a></li>
						  <li ><a href="#meta" data-toggle="tab"> Meta & Description </a></li>
						</ul>	

						<div class="tab-content">
						  <div class="tab-pane active m-t" id="info">
							  <div class="form-group  " >
								
								<div class="" style="background:#fff;">
								
								  <textarea name='content' rows='35' id='editor' class='form-control editor '
									 >{{ htmlentities($content) }}

							      </textarea> 
								 </div> 
							  </div> 						  

						  </div>

						  <div class="tab-pane m-t" id="meta">

					  		<div class="form-group  " >
								<label class=""> Metakey </label>
								<div class="" style="background:#fff;">
								  <textarea name='metakey' rows='5' id='metakey' class='form-control markItUp'>{{ $row['metakey'] }}</textarea> 
								 </div> 
							  </div> 

				  			<div class="form-group  " >
								<label class=""> Meta Description </label>
								<div class="" style="background:#fff;">
								  <textarea name='metadesc' rows='10' id='metadesc' class='form-control markItUp'>{{ $row['metadesc'] }}</textarea> 
								 </div> 
							  </div> 							  						  

						  </div>

						</div>  
						
	
					 </div>
				</div>	
		 	</div>		 
		 
		 <div class="col-sm-4 ">
			<div class="sbox">
				<div class="sbox-title">Page Info </div>	
				<div class="sbox-content">						
				  <div class="form-group hidethis " style="display:none;">
					<label for="ipt" class=""> PageID </label>
					
					  {!! Form::text('pageID', $row['pageID'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
					
				  </div> 					
				  <div class="form-group  " >
					<label for="ipt" > Title </label>
					
					  {!! Form::text('title', $row['title'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
					
				  </div> 					
				  <div class="form-group  " >
					<label for="ipt" > Alias </label>
					
					  {!! Form::text('alias', $row['alias'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'   )) !!} 
					 
				  </div> 					
				  <div class="form-group  " >
					<label for="ipt" > Filename </label>
					
					  <input name="filename" type="text" class="form-control" value="{{ $row['filename']}}" 
					  @if($row['pageID'] !='') readonly="1" @endif required
					  />
					
				  </div> 
				  <div class="form-group  " >
				  <label for="ipt"> Who can view this page ? </label>
					@foreach($groups as $group) 
					<label class="checkbox">					
					  <input  type='checkbox' name='group_id[{{ $group['id'] }}]'    value="{{ $group['id'] }}"
					  @if($group['access'] ==1 or $group['id'] ==1)
					  	checked
					  @endif				 
					   /> 
					  {{ $group['name'] }}
					</label>  
					@endforeach	
						  
				  </div> 
				  <div class="form-group  " >
					<label> Show for Guest ? unlogged  </label>
					<label class="checkbox"><input  type='checkbox' name='allow_guest' 
 						@if($row['allow_guest'] ==1 ) checked  @endif	
					   value="1"	/> Allow Guest ?  </lable>
				  </div>


				  <div class="form-group hidethis " style="display:none;">
					<label for="ipt" class=" control-label col-md-4 text-right"> Created </label>
					<div class="col-md-8">
					  {!! Form::text('created', $row['created'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
					 </div> 
				  </div> 					
				  <div class="form-group hidethis " style="display:none;">
					<label for="ipt" > Updated </label>
			
					  {!! Form::text('updated', $row['updated'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
					
				  </div>	
	
				  <div class="form-group  " >
					<label> Status </label>
					<label class="radio">					
					  <input  type='radio' name='status'  value="enable" required
					  @if( $row['status'] =='enable')  	checked	  @endif				  
					   /> 
					  Enable
					</label> 
					<label class="radio">					
					  <input  type='radio' name='status'  value="disabled" required
					   @if( $row['status'] =='disabled')  	checked	  @endif				  
					   /> 
					  Disabled
					</label> 					 
				  </div> 

				  <div class="form-group  " >
					<label> Template </label>
					<label class="radio">					
					  <input  type='radio' name='template'  value="frontend" required
					  @if( $row['template'] =='frontend')  	checked	  @endif				  
					   /> 
					  Frontend
					</label> 
					<label class="radio">					
					  <input  type='radio' name='template'  value="backend" required
					   @if( $row['template'] =='backend')  	checked	  @endif				  
					   /> 
					  Backend
					</label> 					 
				  </div> 	

				  <div class="form-group  " >
					<label> Slide ? </label>
					<label class="checkbox"><input  type='checkbox' name='slide' 
 						@if($row['slide'] ==1 ) checked  @endif	
					   value="1"	/> Yes  
					</lable>					 
				  </div> 	

				  <div class="form-group  " >
					<label> Shop template ? </label>
					 <label class="radio">					
					  <input  type='radio' name='shop'  value="0" required
					  @if( $row['shop'] =='0')  	checked	  @endif				  
					   /> 
					  No
					</label> 
					<label class="radio">					
					  <input  type='radio' name='shop'  value="1" required
					   @if( $row['shop'] =='1')  	checked	  @endif				  
					   /> 
					  Shop
					</label> 	
					  <label class="radio">					
					  <input  type='radio' name='shop'  value="2" required
					   @if( $row['shop'] =='2')  	checked	  @endif				  
					   /> 
					  Bidshop
					</label> 	
				  </div> 

										

				  <div class="form-group  " >
					<label> Set As Homepage ? </label>
					<label class="checkbox"><input  type='checkbox' name='default' 
 						@if($row['default'] ==1 ) checked  @endif	
					   value="1"	/> Yes  
					</lable>					 

				  </div> 				  			  
				  
			  <div class="form-group">
				
				<button type="submit" class="btn btn-primary ">  Submit </button>
				<a href="{{ url('core/pages')}}" class="btn btn-info"> Cancel </a>
				 
		
			  </div> 
			  </div>
			  </div>				  				  
				  		
			</div>

		 {!! Form::close() !!}
	</div>
</div>	

<style type="text/css">
.note-editor .note-editable { height:1500px;}
</style>			 	 
@stop