<!DOCTYPE html>
<html lang="{!! CNF_LANG !!}">
<head>
<meta http-equiv="x-ua-compatible" content="ie=edge">
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="keywords" content="{!! $pageMetakey !!}" />
<meta name="description" content="{!! $pageMetadesc !!}" />
<meta name="Author" content="punbid" />
<!-- Favicons Icon -->
<link rel="icon" href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico" type="image/x-icon" />
<title>{!! CNF_APPNAME !!} | {!! $pageTitle!!}</title>

<!-- Mobile Specific -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<!-- CSS Style -->
<link rel="stylesheet" type="text/css" href="{!! asset('frontend/punbid/') !!}/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="{!! asset('frontend/punbid/') !!}/css/font-awesome.css" media="all">
<link rel="stylesheet" type="text/css" href="{!! asset('frontend/punbid/') !!}/css/simple-line-icons.css" media="all">
<link rel="stylesheet" type="text/css" href="{!! asset('frontend/punbid/') !!}/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="{!! asset('frontend/punbid/') !!}/css/owl.theme.css">
<link rel="stylesheet" type="text/css" href="{!! asset('frontend/punbid/') !!}/css/jquery.bxslider.css">
<link rel="stylesheet" type="text/css" href="{!! asset('frontend/punbid/') !!}/css/jquery.mobile-menu.css">
<link rel="stylesheet" type="text/css" href="{!! asset('frontend/punbid/') !!}/css/style.css" media="all">
<link rel="stylesheet" type="text/css" href="{!! asset('frontend/punbid/') !!}/css/revslider.css">
<!-- Google Fonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:700,600,800,400' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Raleway:400,300,600,500,700,800' rel='stylesheet' type='text/css'>
<!-- Sweet Alert css -->
<link href="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/bootstrap-sweetalert/sweet-alert.css" rel="stylesheet" type="text/css" />

<link href="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/fileuploads/css/dropify.min.css" rel="stylesheet"/>
<link href="{!! asset('frontend') !!}/punbidtheme/css/custom.css" rel="stylesheet"/>
<link href="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
<link href="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
<link href="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
<link href="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- AJax -->
<link href="{!! asset('sximo/js/plugins/ajax/ajaxSximo.css') !!}" rel="stylesheet"> 
<script type="text/javascript" src="{!! asset('sximo/js/plugins/ajax/ajaxSximo.js') !!}"></script>

</head>
@if(Auth::check())
                    <?php
                    $users_details = \App\Http\Controllers\UserscustomController::display(Session::get('uid'));
                    $userdet = $users_details['row'];
                    ?>

                    <?php
                     $countwlists = \App\Http\Controllers\BidtransrecordsController::countwlists();
                    ?>
@endif
<body class="cms-index-index cms-home-page">
<div id="page"> 

@include('layouts/default/topheader')
@include('layouts/default/slide')

<div class="content-page">
    <div class="container"> 
		<?php $modulepage = str_replace('.', '', $pages);    $pos = strpos($pages, 'pages'); ?>
		@if($pos !== false)
		@include($pages)
		@else
		@yield('content') 
		@endif
    </div>
</div>
@include('layouts/default/footerbidsection')
@include('layouts/default/jsscript')