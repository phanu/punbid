<?php namespace App\Models;

use Nicolaslopezj\Searchable\SearchableTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class productdetail extends Sximo  {

	use SearchableTrait;

	protected $searchable = [
        'columns' => [
            'v_products.name_th_1' => 10,
            'v_products.category_th' => 9,
            'v_products.title_th' => 8,
            'v_products.descript_th' => 7,
            'v_products.price' => 6,
            'v_products.sale_price' => 6,
            'v_products.color_th' => 5,
            'v_products.products_code' => 4,
            'v_products.tag' => 3
        ],
    ];

	protected $table = 'v_products';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(){
		
		return "  SELECT v_products.* FROM v_products  ";
	}	

	public static function queryWhere(){
		
		return "  WHERE v_products.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
