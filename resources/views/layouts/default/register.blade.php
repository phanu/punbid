<div class="modal fade rs_mypopup rs_signup" id="signup_popup" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header" style="padding:0px;">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                <div class="rs_popup_list">
                    <ul>
                      <li>
                        <div class="rs_popup_list_img"><img src="{!! asset('frontend') !!}/punbidtheme/images/af_1.jpg" alt="" class="img-responsive"></div>
                        <div class="rs_popup_list_data">
                          <h5>{!! html_entity_decode(Lang::get('register.1')) !!}</h5>
                          <p>{!! html_entity_decode(Lang::get('register.2')) !!}</p>
                        </div>
                      </li>
                      <li>
                        <div class="rs_popup_list_img"><img src="{!! asset('frontend') !!}/punbidtheme/images/af_2.jpg" alt="" class="img-responsive"></div>
                        <div class="rs_popup_list_data">
                          <h5>{!! html_entity_decode(Lang::get('register.3')) !!}</h5>
                          <p>{!! html_entity_decode(Lang::get('register.4')) !!}</p>
                        </div>
                      </li>
                      <li>
                        <div class="rs_popup_list_img"><img src="{!! asset('frontend') !!}/punbidtheme/images/af_3.jpg" alt="" class="img-responsive"></div>
                        <div class="rs_popup_list_data">
                          <h5>{!! html_entity_decode(Lang::get('register.5')) !!}</h5>
                          <p>{!! html_entity_decode(Lang::get('register.6')) !!}</p>
                        </div>
                      </li>
                      <li>
                        <div class="rs_popup_list_img"><img src="{!! asset('frontend') !!}/punbidtheme/images/af_6.jpg" alt="" class="img-responsive"></div>
                        <div class="rs_popup_list_data">
                          <h5>{!! html_entity_decode(Lang::get('register.7')) !!}</h5>
                          <p>{!! html_entity_decode(Lang::get('register.8')) !!}</p>
                        </div>
                      </li>
                    </ul>
                  </div>
                  <div class="rs_popup_form" >
                    <div class="rs_popup_form_header">
                      <h4 class="text-uppercase">{!! html_entity_decode(Lang::get('register.9')) !!}</h4>
                    </div>
                    <form action="{!! url('user/create') !!}" method="post">
                      <div class="form-group"> 
                      <input type="text" name="firstname" id="rname" class="form-control" placeholder="{!! html_entity_decode(Lang::get('register.10')) !!}" style="width: 50%;float: left;" required> 
                      <input type="text" name="lastname" id="rlname" class="form-control" placeholder="{!! html_entity_decode(Lang::get('register.11')) !!}" style="width: 50%;float: left;" required> 
                      </div>
                      <div class="form-group" style="color: #333;font-size: 12px; text-align:left;" > 
                        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 no-padding">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding"  id="rdate" >
                                {!! html_entity_decode(Lang::get('register.12')) !!}
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-padding">
                              <select size="1" class="rs-custom-select" name="day" required>
                                         <option value="">DD</option>
                                         <option value="01">01</option>
                                         <option value="02">02</option>
                                         <option value="03">03</option>
                                         <option value="04">04</option>
                                         <option value="05">05</option>
                                         <option value="06">06</option>
                                         <option value="07">07</option>
                                         <option value="08">08</option>
                                         <option value="09">09</option>
                                         <option value="10">10</option>
                                         <option value="11">11</option>
                                         <option value="12">12</option>
                                         <option value="13">13</option>
                                         <option value="14">14</option>
                                         <option value="15">15</option>
                                         <option value="16">16</option>
                                         <option value="17">17</option>
                                         <option value="18">18</option>
                                         <option value="19">19</option>
                                         <option value="20">20</option>
                                         <option value="21">21</option>
                                         <option value="22">22</option>
                                         <option value="23">23</option>
                                         <option value="24">24</option>
                                         <option value="25">25</option>
                                         <option value="26">26</option>
                                         <option value="27">27</option>
                                         <option value="28">28</option>
                                         <option value="29">29</option>
                                         <option value="30">30</option>
                                         <option value="31">31</option>
                                 </select>
                              </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-padding">
                                <select size="1" class="rs-custom-select" name="month" required>
                                    <option value="">MM</option>
                                    <option value="01">มกราคม</option>
                                    <option value="02">กุมภาพันธ์</option>
                                    <option value="03">มีนาคม</option>
                                    <option value="04">เมษายน</option>
                                    <option value="05">พฤษภาคม</option>
                                    <option value="06">มิถุนายน</option>
                                    <option value="07">กรกฎาคม</option>
                                    <option value="08">สิงหาคม</option>
                                    <option value="09">กันยายน</option>
                                    <option value="10">ตุลาคม</option>
                                    <option value="11">พฤศจิกายน</option>
                                    <option value="12">ธันวาคม</option>
                                </select>
              </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-padding">
            <select name="year" class="rs-custom-select" required>
            <option value="">YYYY</option>
            <?php $i=2009; ?>
            @while($i>1906)
            <option value="{!!$i!!}">{!!$i!!}</option>
            <?php $i--; ?>
            @endwhile
            </select> 
              </div>
              </div>
                        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12 no-padding"> 
                           {!! html_entity_decode(Lang::get('register.13')) !!}  
                            <select size="1" id="rsex" class="rs-custom-select" name="sex" required>
                                            <option value="0">{!! html_entity_decode(Lang::get('register.14')) !!}</option>
                                            <option value="1">{!! html_entity_decode(Lang::get('register.15')) !!}</option>
                            </select>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                      <div class="form-group"> <input type="text" class="form-control" name="phone" placeholder="{!! html_entity_decode(Lang::get('register.16')) !!}" required> </div>
                      </div>
                      <div class="rs_popup_form_header"> <!--<img src="images/popup_logo.png" class="img-responsive" alt="">-->
                      <h4 class="text-uppercase">{!! html_entity_decode(Lang::get('register.17')) !!}</h4>
                      </div>
                      <div class="form-group"> <input type="text" name="email" class="form-control" placeholder="{!! html_entity_decode(Lang::get('register.18')) !!}" required> </div>

                      <div class="form-group"> <input type="text" name="username" id="rusername" class="form-control" placeholder="{!! html_entity_decode(Lang::get('register.19')) !!}" required> </div>
                      <div class="form-group"> 
                      <input type="password" class="form-control" name="password" id="rpassword" placeholder="{!! html_entity_decode(Lang::get('register.20')) !!}" style="width: 50%;float: left;" required> 
                      <input type="password" class="form-control" name="password_confirmation" placeholder="{!! html_entity_decode(Lang::get('register.21')) !!}" style="width: 50%;float: left;" required>
                      </div>
                      <div class="form-group"> 
                        <input type="text" name="friend" class="form-control" placeholder="{!! html_entity_decode(Lang::get('register.30')) !!}" >
                      </div>
                      <div class="form-group">
                        <div class="checkbox checkbox-primary"> <input id="checkbox1" type="checkbox" value="1" name="checkbox" required>
                          <label for="checkbox1">{!! html_entity_decode(Lang::get('register.22')) !!}</label>
                        </div>
                      </div>
                      <div class="form-group">
                        <button type="submit" id="rsub" class="rs_button rs_button_orange">{!! html_entity_decode(Lang::get('register.23')) !!}</button>
                        <h4 style="margin: 20px 0px 0px 0px;">{!! html_entity_decode(Lang::get('register.24')) !!}</h4>
                      </div>
                    </form>
                    <div class="form-group has-feedback text-center">
              <p class="text-muted text-center"><b> {!! html_entity_decode(Lang::get('register.29')) !!} </b>    </p>
              <div style="padding:15px 0; float:left; width:100%;">
                <a href="{!! URL::to('user/socialize/facebook') !!}" class="btn btn-primary"><i class="icon-facebook" id="ridfacebook"></i> {!! html_entity_decode(Lang::get('register.25')) !!} </a>
                <a href="{!! URL::to('user/socialize/google') !!}" class="btn btn-danger"><i class="icon-google"></i> {!! html_entity_decode(Lang::get('register.26')) !!} </a>
                <!--<a href="{!! URL::to('user/socialize/twitter') !!}" class="btn btn-info"><i class="icon-twitter"></i> Twitter </a>-->
              </div>
              <p style="margin:0px;">
                     {!! html_entity_decode(Lang::get('register.27')) !!} <a href="{!! url('user/login') !!}">{!! html_entity_decode(Lang::get('register.28')) !!} <i class="fa fa-angle-double-right"></i></a>
                       </p>
                    </div>
                    </div>
                </div>
              </div>
            </div>
          </div>