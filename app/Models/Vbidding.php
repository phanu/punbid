<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class vbidding extends Sximo  {
	
	protected $table = 'v_bid_relations';
	protected $primaryKey = 'products_bid_id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT v_bid_relations.* FROM v_bid_relations  ";
	}	

	public static function queryWhere(  ){
		
		return " Where v_bid_relations.bid_start is not null and v_bid_relations.bid_end is not null ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
