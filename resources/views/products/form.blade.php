
@if($setting['form-method'] =='native')
	<div class="sbox">
		<div class="sbox-title">  
			<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small>
				<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{{ $pageModule }}')"><i class="fa fa fa-times"></i></a>
			</h4>
	</div>

	<div class="sbox-content"> 
@endif	
			{!! Form::open(array('url'=>'products/save/'.SiteHelpers::encryptID($row['id']), 'class'=>'form-vertical','files' => true , 'parsley-validate'=>'','novalidate'=>' ','id'=> 'productsFormAjax')) !!}
			<div class="col-md-6">
						<fieldset><legend> รายละเอียดสินค้า</legend>
									
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> รหัสสินค้า  <span class="asterix"> * </span>  </label>									
										  {!! Form::text('products_code', $row['products_code'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> ชื่อสินค้า  <span class="asterix"> * </span>  </label>									
										  {!! Form::text('name_th', $row['name_th'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> ชื่อสินค้า En    </label>									
										  {!! Form::text('name_en', $row['name_en'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> รูปสินค้า    </label>									
										  <input  type='file' name='featured_img' id='featured_img' @if($row['featured_img'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['featured_img'],'/uploads/products/') !!}
						
						</div>					
					 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> รูปอัลบั้มสินค้า    </label>									
										  
					<a href="javascript:void(0)" class="btn btn-xs btn-primary pull-right" onclick="addMoreFiles('gallery')"><i class="fa fa-plus"></i></a>
					<div class="galleryUpl">	
					 	<input  type='file' name='gallery[]'  />			
					</div>
					<ul class="uploadedLists " >
					<?php $cr= 0; 
					$row['gallery'] = explode(",",$row['gallery']);
					?>
					@foreach($row['gallery'] as $files)
						@if(file_exists('./uploads/products/'.$files) && $files !='')
						<li id="cr-<?php echo $cr;?>" class="">							
							<a href="{{ url('/uploads/products//'.$files) }}" target="_blank" >{{ $files }}</a> 
							<span class="pull-right removeMultiFiles" rel="cr-<?php echo $cr;?>" url="/uploads/products/{{$files}}">
							<i class="fa fa-trash-o  btn btn-xs btn-danger"></i></span>
							<input type="hidden" name="currgallery[]" value="{{ $files }}"/>
							<?php ++$cr;?>
						</li>
						@endif
					
					@endforeach
					</ul>
					 						
									  </div> {!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> ราคา  <span class="asterix"> * </span>  </label>									
										  {!! Form::text('price', $row['price'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> ราคาโปรโมชั่น    </label>									
										  {!! Form::text('sale_price', $row['sale_price'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> หมวดหมู่  <span class="asterix"> * </span>  </label>									
										  <select name='category' rows='5' id='category' class='select2 ' required  ></select> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> แบรนด์  <span class="asterix"> * </span>  </label>									
										  <select name='brand' rows='5' id='brand' class='select2 ' required  ></select> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> ตั้งเป็นสินค้าแนะนำ    </label>									
										  <?php $featured = explode(",",$row['featured']); ?>
					 <label class='checked checkbox-inline'>   
					<input type='checkbox' name='featured[]' value ='1'   class='' 
					@if(in_array('1',$featured))checked @endif 
					 />  </label>  						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> เปิดใช้งาน    </label>									
										  <?php $active = explode(",",$row['active']); ?>
					 <label class='checked checkbox-inline'>   
					<input type='checkbox' name='active[]' value ='1'   class='' 
					@if(in_array('1',$active))checked @endif 
					 />  </label>  						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> ค่าขนส่ง  <span class="asterix"> * </span>  </label>									
										  {!! Form::text('shiping_cost', $row['shiping_cost'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Tags    </label>									
										  {!! Form::text('tag', $row['tag'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 						
									  </div> </fieldset>
			</div>
			
			<div class="col-md-6">
						<fieldset><legend> ข้อมูลสินค้า</legend>
									
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> คำอธิบายสินค้า  <span class="asterix"> * </span>  </label>									
										  <textarea name='title_th' rows='5' id='title_th' class='form-control '  
				         required  >{{ $row['title_th'] }}</textarea> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> รายละเอียดสินค้า  <span class="asterix"> * </span>  </label>									
										  <textarea name='descript_th' rows='5' id='editor' class='form-control editor '  
						required >{{ $row['descript_th'] }}</textarea> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> คำอธิบายสินค้า En    </label>									
										  <textarea name='title_en' rows='5' id='title_en' class='form-control '  
				           >{{ $row['title_en'] }}</textarea> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> รายละเอียดสินค้า En    </label>									
										  <textarea name='descript_en' rows='5' id='editor' class='form-control editor '  
						 >{{ $row['descript_en'] }}</textarea> 						
									  </div> </fieldset>
			</div>
			
												
								
						
			<div style="clear:both"></div>	
							
			<div class="form-group">
				<label class="col-sm-4 text-right">&nbsp;</label>
				<div class="col-sm-8">	
					<button type="submit" class="btn btn-primary btn-sm "><i class="icon-checkmark-circle2"></i>  {{ Lang::get('core.sb_save') }} </button>
					<button type="button" onclick="ajaxViewClose('#{{ $pageModule }}')" class="btn btn-success btn-sm"><i class="icon-cancel-circle2 "></i>  {{ Lang::get('core.sb_cancel') }} </button>
				</div>			
			</div> 		 
			{!! Form::close() !!}


@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

	
</div>	
			 
<script type="text/javascript">
$(document).ready(function() { 
	
		$("#category").jCombo("{!! url('products/comboselect?filter=product_categories:id:name_th') !!}",
		{  selected_value : '{{ $row["category"] }}' });
		
		$("#brand").jCombo("{!! url('products/comboselect?filter=brand:id:name_th') !!}",
		{  selected_value : '{{ $row["brand"] }}' });
		 
	
	$('.editor').summernote();
	$('.previewImage').fancybox();	
	$('.tips').tooltip();	
	$(".select2").select2({ width:"98%"});	
	$('.date').datepicker({format:'yyyy-mm-dd',autoClose:true})
	$('.datetime').datetimepicker({format: 'yyyy-mm-dd hh:ii:ss'}); 
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});			
		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("products/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});
				
	var form = $('#productsFormAjax'); 
	form.parsley();
	form.submit(function(){
		
		if(form.parsley('isValid') == true){			
			var options = { 
				dataType:      'json', 
				beforeSubmit :  showRequest,
				success:       showResponse  
			}  
			$(this).ajaxSubmit(options); 
			return false;
						
		} else {
			return false;
		}		
	
	});

});

function showRequest()
{
	$('.ajaxLoading').show();		
}  
function showResponse(data)  {		
	
	if(data.status == 'success')
	{
		ajaxViewClose('#{{ $pageModule }}');
		ajaxFilter('#{{ $pageModule }}','{{ $pageUrl }}/data');
		notyMessage(data.message);	
		$('#sximo-modal').modal('hide');	
	} else {
		notyMessageError(data.message);	
		$('.ajaxLoading').hide();
		return false;
	}	
}			 

</script>		 