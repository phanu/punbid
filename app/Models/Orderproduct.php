<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class orderproduct extends Sximo  {
	
	protected $table = 'v_orderdetails_pro';
	protected $primaryKey = '';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT v_orderdetails_pro.* FROM v_orderdetails_pro  ";
	}	

	public static function queryWhere(  ){
		
		return " where v_orderdetails_pro.ordernumber != 0 ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
