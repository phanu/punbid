<div class="woocommerce columns-3">
						<?php //require 'inc/components/product-item.php'; ?>
						<?php
							$productImage = array(
								1 => array('product_image' => 'assets/images/products/6.jpg'),
								2 => array('product_image' => 'assets/images/products/5.jpg'),
								3 => array('product_image' => 'assets/images/products/4.jpg'),
								4 => array('product_image' => 'assets/images/products/3.jpg'),
								5 => array('product_image' => 'assets/images/products/2.jpg'),
								6 => array('product_image' => 'assets/images/products/1.jpg'),
								7 => array('product_image' => 'assets/images/products/3.jpg')
								);
							shuffle($productImage );

						?>
						<ul class="products columns-<?php echo $column; ?>">
							<?php 
								$sql = mysql_query("
									SELECT
										sc_prod_sale.psid
										, sc_prod_sale.pid
										, sc_prod_sale.ps_start_price
										, sc_prod_sale.ps_end_price
										, sc_prod_sale.ps_start_date
										, sc_prod_sale.ps_end_date
										, sc_prod.prod_title
										, sc_prod.prod_sku
										, sc_prod.prod_price
										, sc_prod_sale.ps_status
									FROM
										sc_prod_sale
										INNER JOIN sc_prod 
											ON (sc_prod_sale.pid = sc_prod.pid)
									WHERE (sc_prod_sale.ps_status ='0')
									ORDER BY sc_prod_sale.ps_end_date ASC
								");
								$i=0;
								while($row = mysql_fetch_assoc($sql)){
							?>
							<?php
								if ( empty( $loop ) ) {
									$loop = 0;
								}
								$loop++;
								/*$classes = '';
								if ( 0 === ( $loop - 1 ) % $column || 1 === $column ) {
									$classes = 'first';
								}
								if ( 0 === $loop % $column ) {
									$classes = 'last';
								}*/
								$classes = "";
								if($i==0){
									$classes = "first";
								}
								if($i==2){
									$classes = "last";
								}
								
								$pid = $row['pid'];
								$time = date("Y/m/d H:i:s", strtotime($row['ps_end_date']));
								$sprice = $row['ps_start_price'];
								$eprice = $row['ps_end_price'];
								$price = $row['prod_price'];
								
								//numbid
								$bidnum = countDesc("pdid","sc_prod_bid","psid='".$row['psid']."'");
								
								//get cus
								if($bidnum > 0){
									$cust = getDescArr("cr_cust.username,cr_cust.cid","sc_prod_bid,cr_cust","sc_prod_bid.cid=cr_cust.cid AND sc_prod_bid.psid='".$row['psid']."'", "sc_prod_bid.pdid DESC", "1");
									$bidcus = $cust['cid'];
								}else{
									$bidcus = 0;
								}
							?>

							<li class="product <?php echo $classes; ?>">
							
									<div class="product-inner">
										<span class="loop-product-categories"><a href="index.php?page=product-category" rel="tag"><?=$row['prod_sku']?></a></span>
										<a href="index.php?page=single-product">
											<h3><?=$row['prod_title']?></h3>
											<div class="product-thumbnail">
												<img data-echo="<?php echo $productImage[$i]['product_image'] ?>" src="assets/images/blank.gif" alt="">
											</div>
										</a>

										<div class="price-add-to-cart">
											<span class="price">
												<span class="electro-price">
													<ins><span class="amount"><span class="amount-bid"><?=$eprice?></span></span></ins>
													<del><span class="amount"><?=$price?></span></del>
												</span>
											</span>
											<!--<a rel="nofollow" href="index.php?page=single-product" class="button add_to_cart_button">Add to cart</a>-->
										</div><!-- /.price-add-to-cart -->

										<!--<div class="hover-area">
											<div class="action-buttons">

												<a href="#" rel="nofollow" class="add_to_wishlist">
													Wishlist</a>

													<a href="#" class="add-to-compare-link">Compare</a>
												</div>
											</div>
										</div>-->
										<div class="deal-countdown-timer" id="prod_<?=$pid?>"
											data-bidtime="<?=$time?>"
											data-bidnum="<?=$bidnum?>"
											data-bidprice="<?=$eprice?>"
											data-bidpid="<?=$i?>"
											data-bidcus="<?=$bidcus?>">
											<div class="marketing-text text-xs-center">จบภายใน</div>
											<div id="deal-countdown" class="countdown">	
												<!--<span class="days"><span class="value">01</span><b>Days</b></span> 
												<span class="hours"><span class="value">22</span><b>Hours</b></span> 
												<span class="minutes"><span class="value">20</span><b>Mins</b></span> 
												<span class="seconds"><span class="value">35</span><b>Secs</b></span>--> 
											</div>
											<span class="deal-end-date" style="display:none"></span>
											<div style="width:100%; text-align:center; margin-top:10px; font-size: 1.362em; font-weight:600" class="user-bid">
												<?=($bidnum>0) ? $cust['username'] : "ให้ท่านคนแรก"?>
											</div>
											<div style="width:100%; text-align:center; margin-top:10px; display:" class="btn-bit">
												<button type="button" class="btn btn-primary" id="btn-bid" data-pid="<?=$pid?>">BID NOW</button>
											</div>
										</div>
										<!-- /.product-inner -->
									</div>
								</li>
							<?php $i++; } ?>
						</ul>
					</div>