<!-- Datatables-->
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/dataTables.bootstrap.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/dataTables.buttons.min.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/buttons.bootstrap.min.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/jszip.min.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/pdfmake.min.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/vfs_fonts.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/buttons.html5.min.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/buttons.print.min.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/dataTables.responsive.min.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/datatables/dataTables.scroller.min.js"></script>
<script type="text/javascript" src="{!! asset('frontend') !!}/punbidtheme/Light/assets/plugins/parsleyjs/dist/parsley.min.js"></script>
        <!-- Datatable init js -->
        <script src="{!! asset('frontend') !!}/punbidtheme/Light/assets/pages/datatables.init.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.table-striped').DataTable({ order: [ 0, 'desc' ] });
    $('form').parsley();
                    // select the file input (using a id would be faster)
                    $('#cavatar').change(function() { 
                        // select the form and submit
                        $('#userpicform').submit(); 
                    });


   $.datepicker.setDefaults( $.datepicker.regional[ "th" ] );
 jQuery('#datepicker').datepicker({ dateFormat: 'dd/mm/yy', changeYear: true, changeMonth: true });
 jQuery('#datepickerpay').datepicker({ dateFormat: 'dd/mm/yy', changeYear: true, changeMonth: true });

            TableManageButtons.init();


            $('#calculatebid').on("click", function() { 


                  var amount = jQuery('#calbid').val();

                  var cal;
                  if(jQuery('#calbid').val()>=1000&&jQuery('#calbid').val()<=5000)
                   {
                      cal= 0.9;
                   }else if(jQuery('#calbid').val()>5000&&jQuery('#calbid').val()<=10000){
                      cal= 0.7;
                   }else if(jQuery('#calbid').val()>10000&&jQuery('#calbid').val()<=20000){
                      cal= 0.5;
                   }else if(jQuery('#calbid').val()>20000){
                      cal= 0.3;
                   }

                  jQuery('#withdrawamount').val(((amount*2)*cal)-25);

                    return false;

                  });

            } );
            


        </script>