<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class gallery extends Sximo  {
	
	protected $table = 'product_gallery';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT product_gallery.* FROM product_gallery  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE product_gallery.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
