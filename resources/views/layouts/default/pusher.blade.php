<script type="text/javascript">
$(document).ready(function() {

@if(Auth::check())

$(document).on('click','.getitdone', function() {
   var pbidid = $(this).data('id');
   $.ajax({
                            url: "{!! url('bidtransrecords/checkyouwin') !!}",
                            method: "POST",
                            data: { pbidid : pbidid },
                            dataType: "html",
                            success: function(ress) {
                                    var message = jQuery.parseJSON(ress);
                                    if(message.winner==='win')
                                    {
                                        swal({
                                            title: "สิ้นสุดการประมูล",
                                            text: "ขอแสดงความยินดี คุณเป็นผู้ชนะการประมูลสินค้า",
                                            type: "success",
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "ยืนยันและรับสิทธิ",
                                            closeOnConfirm: false
                                        }, function (isConfirm) {
                                            if (isConfirm) {
                                                swal({
                                                        title: "ยืนยันและรับสิทธิ",
                                                        text: "ขอแสดงความยินดี คุณเป็นผู้ชนะการประมูลสินค้า กรุณารับสิทธิภายใน 24 ชม. ค่ะ",
                                                        type: "success",
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "ไปหน้า checkout",
                                                        closeOnConfirm: false
                                                    }, function (isConfirm) {
                                                        if (isConfirm) {
                                                          window.location.href = "{!!url('winnercheckout?pwin=') !!}"+pbidid;
                                                        }
                                                    });
                                            } 
                                        });
                                    }
                                    else if(message.winner==='lose')
                                    {
                                        swal({
                                            title: "สิ้นสุดการประมูล",
                                            text: "\nคุณแพ้การประมูล แต่ยังไม่จบ! \n เลือกรับ Bid Return เพื่อสู้ต่อ หรือ สะสม S-Pay เพื่อซื้อสินค้าใน punbidthemeshop",
                                            type: "info",
                                            showCancelButton: true,
                                            confirmButtonColor: "#438afe",
                                            confirmButtonText: "Spay",
                                            cancelButtonColor: "#fe5722",
                                            cancelButtonText: "Bid",
                                            closeOnConfirm: false,
                                            closeOnCancel: false
                                        }, function (isConfirm) {
                                            if (isConfirm) {

                                                swal({
                                                        title: "เลือกรับ Spay",
                                                        text: "คุณได้รับ Spay เพื่อซื้อสินค้าใน punbidthemeshop",
                                                        type: "success",
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "กดเพื่อรับ Spay คืน",
                                                        closeOnConfirm: true
                                                    }, function (isConfirm) {

                                                           $.ajax
                                                                ({ 
                                                                    url: "{!! url('bidtransrecords/returnbidandspay') !!}",
                                                                    method: "POST",
                                                                    data: {"pbidid": pbidid, "selector" : "spay" },
                                                                    type: 'html',
                                                                    success: function(result)
                                                                    {
                                                                       var retbid = jQuery.parseJSON(result);
                                                                       //console.log(retbid);
                                                                       if(retbid.message==="success")
                                                                       {
                                              var cvaluebid = parseFloat(retbid.current_bid);  
                                              $( "#current_spay" ).html(commaSeparateNumber(cvaluebid));
                                               $("#bidremind"+pbidid).hide();
$('.notification-box').saveusernotify('คุณได้รับ '+ retbid.spaygain +' S-Pay','icon bg-success','zmdi zmdi-plus-circle');
                                                                          

                                                                       }else if(retbid.message==="fail")
                                                                       {
                                                                          swal({
                                                                            title: 'ไม่สามารถทำรายการได้',
                                                                            confirmButtonText: 'ตกลง',
                                                                            text:'กรุณาลองอีกครั้งในหน้าข้อมูลส่วนตัว'},
                                                                            function (isConfirm) {
                                                                              if (isConfirm) {
                                                                                
                                                                              }
                                                                            })
                                                                       }
                                                                       
                                                                    }

                                                                });
                                                           // window.location.href = "{!!url('user/register') !!}";
                                                        
                                                    });

                                            } else {

                                                swal({
                                                        title: "เลือกรับ Bid",
                                                        text: "คุณได้รับ Bid คืน 100%",
                                                        type: "success",
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "กดเพื่อรับ Bid คืน",
                                                        closeOnConfirm: true
                                                    }, function (isConfirm) {

                                                           $.ajax
                                                                ({ 
                                                                    url: "{!! url('bidtransrecords/returnbidandspay') !!}",
                                                                    method: "POST",
                                                                    data: {"pbidid": pbidid, "selector" : "bid" },
                                                                    type: 'html',
                                                                    success: function(result)
                                                                    {
                                                                       var retbid = jQuery.parseJSON(result);
                                                                       //console.log(retbid);
                                                                       if(retbid.message==="success")
                                                                       {
                                                                          var cvaluebid = parseFloat(retbid.current_bid);  
                                                                          $( "#current_bid" ).html(commaSeparateNumber(cvaluebid));
                                                                           $("#bidremind"+pbidid).hide();
$('.notification-box').saveusernotify('คุณได้รับ '+ retbid.bidgain +' Bid','icon bg-success','zmdi zmdi-plus-circle');

                                                                       }else if(retbid.message==="fail")
                                                                       {
                                                                          swal({
                                                                            title: 'ไม่สามารถทำรายการได้',
                                                                            confirmButtonText: 'ตกลง',
                                                                            text:'กรุณาลองอีกครั้งในหน้าข้อมูลส่วนตัว'},
                                                                            function (isConfirm) {
                                                                              if (isConfirm) {
                                                                                
                                                                              }
                                                                            })
                                                                       }
                                                                       
                                                                    }

                                                                });
                                                           // window.location.href = "{!!url('user/register') !!}";
                                                        
                                                    });
                                            }
                                        });
                                    }
                                }
                            });
  


   return false;

});


                          
@if(isset($userdet)&&$userdet->bid>0)
$('.biddingpusher').click(function(){

 var cid = $(this).parent().data('id');

 var productval = $(this).parent().data('productval');

 var boots = $(this).parent().data('boots');

 var customer = $(this).parent().data('customer');
    
    $.ajax
    ({ 
        url: "{!! url('bidtransrecords/savepublic') !!}",
        data: {"customerNumber": customer, "products_bid_id" : cid, "bid" : boots, "product_value" : productval },
        type: 'post',
        success: function(result)
        {
           var res = jQuery.parseJSON(result);
           //console.log(res);
           if(res.mess==="success")
           {

              if(typeof res.clickdisable !== "undefined")
              {
                swal({
                  title: "แจ้งเตือนจากระบบ?",
                  text: "คุณไม่สามารถแข่งกับตัวเองได้!",
                  type: "warning",
                  showCancelButton: false,
                  confirmButtonClass: 'btn-warning waves-effect waves-light',
                  confirmButtonText: 'ตกลง'
                });

                return false;
              }

              var cvaluebid = parseFloat(res.current_bid);  
              $( "#current_bid" ).html(commaSeparateNumber(cvaluebid));

              var product_value_bid = parseFloat(res.product_value_bid);
             
                
           }else if(res.mess==="nobidegn"){

                                        swal({
                                            title: "ไม่สามารถประมูลได้",
                                            text: "Bid ของคุณหมดกรุณาเติม Bid",
                                            type: "error",
                                            showCancelButton: true,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "ซื้อ Bid เลย",
                                            cancelButtonText: "ยังไม่ซื้อ",
                                            closeOnConfirm: true,
                                            closeOnCancel: false
                                        }, function (isConfirm) {
                                            if (isConfirm) {
                                                window.location.href = "{!!url('bidcheckout') !!}";
                                            }
                                        });

           }else if(res.mess==="fail")
           {
                                        swal({
                                          title: 'ไม่สามารถทำรายการได้',
                                          confirmButtonText: 'ตกลง',
                                          text:'กรุณาลองอีกครั้ง'},
                                          function (isConfirm) {
                                            if (isConfirm) {

                                            }
                                          });
          }
        }
    });
});
@else

$('.biddingpusher').click(function(){
swal({
      title: 'ไม่สามารถประมูลได้',
      confirmButtonText: 'ซื้อบิดเลย',
      text:'Bid ของคุณหมดกรุณาเติม Bid',
      showCancelButton: true,
      type:'error'},
      function (isConfirm) {
        if (isConfirm) {
          window.location.href = "{!!url('bidpackhome') !!}";
        }
      })
    

});
@endif


@if($pages=="pages.bidding")

$(document).on('click','#addtowishlist', function() {
  var id = $(this).data('id');
  $.ajax({
        url: "{!! url('bidtransrecords/addwlists') !!}",
        method: "POST",
        data: { wid : id },
        dataType: "html",
        success: function(data) {

                //console.log(data);
                var data = jQuery.parseJSON(data);
                if(data.message==="success")
                {
                    $( ".fa-heart-o" ).css({ 'color': "red" });
                    $( "#current_wlists" ).html(data.rewishlists);

                }else if(data.message==="delsuccess")
                {

                     $( ".fa-heart-o" ).css({ 'color': "" });
                     $( "#current_wlists" ).html(data.rewishlists);                

                }else if(data.message==="fail")
                {
                                          swal({
                                          title: 'ไม่สามารถทำรายการได้',
                                          confirmButtonText: 'ตกลง',
                                          type: "error",
                                          text:'กรุณา login เข้าสู่ระบบ'},
                                          function (isConfirm) {
                                            if (isConfirm) {

                                            }
                                          });
                }

            },
      });
});

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;
    var pusher = new Pusher('8b5cebf10f40a2986078', {
      cluster: 'ap1',
      encrypted: true
    });

    var channel = pusher.subscribe('bidmain');
    channel.bind('bidding_{!!$_REQUEST['view']!!}', function(data) {
      if(typeof data.bidnewtime !== "undefined")
      {
        $('.bid-time').countdown(new Date(data.bidnewtime.date) ).on('finish.countdown', function(data) {  
          $('.bid-end-date').html('ปิดประมูล');
          $('.endhide').hide();
          });
      }

                    if(data.bidcompleted) 
                    {
                        $('.bid-end-date').html('ปิดประมูล');
                        $('.endhide').hide();

                        @if(Auth::check())
                          $.ajax({
                            url: "{!! url('bidtransrecords/checkyouwin') !!}",
                            method: "POST",
                            data: { pbidid : data.pbidid },
                            dataType: "html",
                            success: function(ress) {
                                    var message = jQuery.parseJSON(ress);
                                    if(message.winner==='win')
                                    {
                                        swal({
                                            title: "สิ้นสุดการประมูล",
                                            text: "ขอแสดงความยินดี คุณเป็นผู้ชนะการประมูลสินค้า",
                                            type: "success",
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "ยืนยันและรับสิทธิ",
                                            closeOnConfirm: false
                                        }, function (isConfirm) {
                                            if (isConfirm) {
                                                swal({
                                                        title: "ยืนยันและรับสิทธิ",
                                                        text: "ขอแสดงความยินดี คุณเป็นผู้ชนะการประมูลสินค้า กรุณารับสิทธิภายใน 24 ชม. ค่ะ",
                                                        type: "success",
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "ไปหน้า checkout",
                                                        closeOnConfirm: false
                                                    }, function (isConfirm) {
                                                        if (isConfirm) {
                                                          window.location.href = "{!!url('winnercheckout?pwin=') !!}"+message.pbidid;
                                                        }
                                                    });
                                            } 
                                        });
                                    }
                                    else if(message.winner==='lose')
                                    {
                                        swal({
                                            title: "สิ้นสุดการประมูล",
                                            text: "\nคุณแพ้การประมูล แต่ยังไม่จบ! \n เลือกรับ Bid Return เพื่อสู้ต่อ หรือ สะสม S-Pay เพื่อซื้อสินค้าใน punbidthemeshop",
                                            type: "info",
                                            showCancelButton: true,
                                            confirmButtonColor: "#438afe",
                                            confirmButtonText: "Spay",
                                            cancelButtonColor: "#fe5722",
                                            cancelButtonText: "Bid",
                                            closeOnConfirm: false,
                                            closeOnCancel: false
                                        }, function (isConfirm) {
                                            if (isConfirm) {

                                                swal({
                                                        title: "เลือกรับ Spay",
                                                        text: "คุณได้รับ Spay เพื่อซื้อสินค้าใน punbidthemeshop",
                                                        type: "success",
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "กดเพื่อรับ Spay คืน",
                                                        closeOnConfirm: true
                                                    }, function (isConfirm) {
                                                           $.ajax
                                                                ({ 
                                                                    url: "{!! url('bidtransrecords/returnbidandspay') !!}",
                                                                    method: "POST",
                                                                    data: {"pbidid": message.pbidid, "selector" : "spay" },
                                                                    type: 'html',
                                                                    success: function(result)
                                                                    {
                                                                       var retbid = jQuery.parseJSON(result);
                                                                       //console.log(retbid);
                                                                       if(retbid.message==="success")
                                                                       {
                                                                          var cvaluebid = parseFloat(retbid.current_bid);  
                                                                          $( "#current_spay" ).html(commaSeparateNumber(cvaluebid));
$('.notification-box').saveusernotify('คุณได้รับ '+ retbid.spaygain +' S-Pay','icon bg-success','zmdi zmdi-plus-circle');

                                                                       }else if(retbid.message==="fail")
                                                                       {
                                                                          swal({
                                                                            title: 'ไม่สามารถทำรายการได้',
                                                                            confirmButtonText: 'ตกลง',
                                                                            text:'กรุณาลองอีกครั้งในหน้าข้อมูลส่วนตัว'},
                                                                            function (isConfirm) {
                                                                              if (isConfirm) {
                                                                                
                                                                              }
                                                                            })
                                                                       }
                                                                       
                                                                    }

                                                                });
                                                           // window.location.href = "{!!url('user/register') !!}";
                                                        
                                                    });

                                            } else {

                                                swal({
                                                        title: "เลือกรับ Bid",
                                                        text: "คุณได้รับ Bid คืน 100%",
                                                        type: "success",
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "กดเพื่อรับ Bid คืน",
                                                        closeOnConfirm: true
                                                    }, function (isConfirm) {

                                                           $.ajax
                                                                ({ 
                                                                    url: "{!! url('bidtransrecords/returnbidandspay') !!}",
                                                                    method: "POST",
                                                                    data: {"pbidid": message.pbidid, "selector" : "bid" },
                                                                    type: 'html',
                                                                    success: function(result)
                                                                    {
                                                                       var retbid = jQuery.parseJSON(result);
                                                                       //console.log(retbid);
                                                                       if(retbid.message==="success")
                                                                       {
                                                                          var cvaluebid = parseFloat(retbid.current_bid);  
                                                                          $( "#current_bid" ).html(commaSeparateNumber(cvaluebid));
$('.notification-box').saveusernotify('คุณได้รับ '+ retbid.bidgain +' Bid','icon bg-success','zmdi zmdi-plus-circle');
                                                                       }else if(retbid.message==="fail")
                                                                       {
                                                                          swal({
                                                                            title: 'ไม่สามารถทำรายการได้',
                                                                            confirmButtonText: 'ตกลง',
                                                                            text:'กรุณาลองอีกครั้งในหน้าข้อมูลส่วนตัว'},
                                                                            function (isConfirm) {
                                                                              if (isConfirm) {
                                                                                
                                                                              }
                                                                            })
                                                                       }
                                                                       
                                                                    }

                                                                });
                                                           // window.location.href = "{!!url('user/register') !!}";
                                                        
                                                    });
                                            }
                                        });
                                    }
                                }
                          });
                        @endif 
                        
                        $( "#product_value_bid" ).html( data.product_value_bid );
                        $( "#xuser_bidding_{!!$_REQUEST['view']!!}" ).html( data.biduser );

                    }else{

                        var typebid = '';
                        $( "#product_value_bid" ).html( data.product_value_bid );

                      
                        if(data.bidd.bid_type===1){  typebid = 'auto'; }else{ typebid = 'manual'; } 
                        $( "#bidd" ).after('<div class="row no-margin"> <div class="col-md-4"> <p class="text-muted">'+data.bidd.product_value_bid+'</p> </div> <div class="col-md-4"> <p class="text-muted">'+data.bidd.username+'</p> </div> <div class="col-md-4"> <p class="text-muted">'+typebid+'</p> </div> </div>');

                        if(($('.cauto').length) >7)
                        {
                          $("#removebt")
                            .prev('div.row')
                            .remove()
                            .end();
                        }

                        $( "#xuser_bidding_{!!$_REQUEST['view']!!}" ).html( data.biduser );
                    }
      
    });

@endif

@if($pages=="pages.home"||$pages=="pages.auction")


    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('8b5cebf10f40a2986078', {
      cluster: 'ap1',
      encrypted: true
    });

        var channel = pusher.subscribe('bidmain');
        var channelall = pusher.subscribe('bidmain');
          channelall.bind_all(function(chnal) {
                
                channel.bind(chnal, function(data) {

                    if(typeof data.bidnewtime !== "undefined")
                    {
                        $('#bid-time_'+chnal).countdown(new Date(data.bidnewtime.date) ).on('finish.countdown', function(data) {  
                        $('.bid-end-date_'+chnal).html('ปิดประมูล');
                        $('.endhide_'+chnal).hide();
                      });
                    }

                    if(data.bidcompleted) 
                    {
                        $('.bid-end-date').html('ปิดประมูล');
                        $('.endhide').hide();

                        @if(Auth::check())
                          $.ajax({
                            url: "{!! url('bidtransrecords/checkyouwin') !!}",
                            method: "POST",
                            data: { pbidid : data.pbidid },
                            dataType: "html",
                            success: function(ress) {
                                    var message = jQuery.parseJSON(ress);
                                    if(message.winner==='win')
                                    {
                                        swal({
                                            title: "สิ้นสุดการประมูล",
                                            text: "ขอแสดงความยินดี คุณเป็นผู้ชนะการประมูลสินค้า",
                                            type: "success",
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "ยืนยันและรับสิทธิ",
                                            closeOnConfirm: false
                                        }, function (isConfirm) {
                                            if (isConfirm) {
                                                swal({
                                                        title: "ยืนยันและรับสิทธิ",
                                                        text: "ขอแสดงความยินดี คุณเป็นผู้ชนะการประมูลสินค้า กรุณารับสิทธิภายใน 24 ชม. ค่ะ",
                                                        type: "success",
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "ไปหน้า checkout",
                                                        closeOnConfirm: false
                                                    }, function (isConfirm) {
                                                        if (isConfirm) {
                                                          window.location.href = "{!!url('winnercheckout?pwin=') !!}"+data.pbidid;
                                                          $('.notification-box').saveusernotify('คุณชนะการประมูล!','icon bg-success','zmdi zmdi-star-circle');
                                                        }
                                                    });
                                            } 
                                        });
                                    }
                                    else if(message.winner==='lose')
                                    {
                                        swal({
                                            title: "สิ้นสุดการประมูล",
                                            text: "\nคุณแพ้การประมูล แต่ยังไม่จบ! \n เลือกรับ Bid Return เพื่อสู้ต่อ หรือ สะสม S-Pay เพื่อซื้อสินค้าใน punbidthemeshop",
                                            type: "info",
                                            showCancelButton: true,
                                            confirmButtonColor: "#438afe",
                                            confirmButtonText: "Spay",
                                            cancelButtonColor: "#fe5722",
                                            cancelButtonText: "Bid",
                                            closeOnConfirm: false,
                                            closeOnCancel: false
                                        }, function (isConfirm) {
                                            if (isConfirm) {

                                                swal({
                                                        title: "เลือกรับ Spay",
                                                        text: "คุณได้รับ Spay เพื่อซื้อสินค้าใน punbidthemeshop",
                                                        type: "success",
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "กดเพื่อรับ Spay คืน",
                                                        closeOnConfirm: true
                                                    }, function (isConfirm) {

                                                           $.ajax
                                                                ({ 
                                                                    url: "{!! url('bidtransrecords/returnbidandspay') !!}",
                                                                    method: "POST",
                                                                    data: {"pbidid": message.pbidid, "selector" : "spay" },
                                                                    type: 'html',
                                                                    success: function(result)
                                                                    {
                                                                       var retbid = jQuery.parseJSON(result);
                                                                       //console.log(retbid);
                                                                       if(retbid.message==="success")
                                                                       {
                                                                          var cvaluebid = parseFloat(retbid.current_bid);  
                                                                          $( "#current_spay" ).html(commaSeparateNumber(cvaluebid));
$('.notification-box').saveusernotify('คุณได้รับ '+ retbid.spaygain +' S-Pay','icon bg-success','zmdi zmdi-plus-circle');

                                                                       }else if(retbid.message==="fail")
                                                                       {
                                                                          swal({
                                                                            title: 'ไม่สามารถทำรายการได้',
                                                                            confirmButtonText: 'ตกลง',
                                                                            text:'กรุณาลองอีกครั้งในหน้าข้อมูลส่วนตัว'},
                                                                            function (isConfirm) {
                                                                              if (isConfirm) {
                                                                                
                                                                              }
                                                                            })
                                                                       }
                                                                       
                                                                    }

                                                                });
                                                           // window.location.href = "{!!url('user/register') !!}";
                                                        
                                                    });

                                            } else {

                                                swal({
                                                        title: "เลือกรับ Bid",
                                                        text: "คุณได้รับ Bid คืน 100%",
                                                        type: "success",
                                                        confirmButtonColor: "#DD6B55",
                                                        confirmButtonText: "กดเพื่อรับ Bid คืน",
                                                        closeOnConfirm: true
                                                    }, function (isConfirm) {

                                                           $.ajax
                                                                ({ 
                                                                    url: "{!! url('bidtransrecords/returnbidandspay') !!}",
                                                                    method: "POST",
                                                                    data: {"pbidid": message.pbidid, "selector" : "bid" },
                                                                    type: 'html',
                                                                    success: function(result)
                                                                    {
                                                                       var retbid = jQuery.parseJSON(result);
                                                                       //console.log(retbid);
                                                                       if(retbid.message==="success")
                                                                       {
                                                                          var cvaluebid = parseFloat(retbid.current_bid);  
                                                                          $( "#current_bid" ).html(commaSeparateNumber(cvaluebid));
$('.notification-box').saveusernotify('คุณได้รับ '+ retbid.bidgain +' Bid','icon bg-success','zmdi zmdi-plus-circle');
                                                                       }else if(retbid.message==="fail")
                                                                       {
                                                                          swal({
                                                                            title: 'ไม่สามารถทำรายการได้',
                                                                            confirmButtonText: 'ตกลง',
                                                                            text:'กรุณาลองอีกครั้งในหน้าข้อมูลส่วนตัว'},
                                                                            function (isConfirm) {
                                                                              if (isConfirm) {
                                                                                
                                                                              }
                                                                            })
                                                                       }
                                                                       
                                                                    }

                                                                });
                                                           // window.location.href = "{!!url('user/register') !!}";
                                                        
                                                    });
                                            }
                                        });
                                    }
                                }
                            });
                        @endif
                    }
                    //console.log(data.bidnewtime.date);
                    $( "#product_value_bid_"+chnal ).html( data.product_value_bid );
                    $( "#xuser_"+chnal ).html( data.biduser );
                });

          });

     $('.bid-time').each(function() {
                  var $this = $(this), finalDate = $(this).data('countdown'); 
                  var vid = $(this).data('vid');
                  var h = '';
                  $this.countdown(finalDate, function(event) {
                  $this.html(event.strftime('%H:%M:%S'));
                  }).on('finish.countdown', function(data) {  
                        $('.bid-end-date_bidding_'+vid).html('ปิดประมูล');
                        $('.endhide_bidding_'+vid).hide();

                  if($this.context.innerText==="00:00:00"){
                    $('.bid-end-date_bidding_'+vid).html('ปิดประมูล');
                    $('.endhide_bidding_'+vid).hide();
                  }
                }); 

     });


@endif

});
</script>
