<nav class="navbar navbar-primary navbar-full yamm">
	<div class="container">
		<div class="clearfix">
			<button class="navbar-toggler hidden-sm-up pull-right flip" type="button" data-toggle="collapse" data-target="#header-v3">
				&#9776;
			</button>
		</div>

		<div class="collapse navbar-toggleable-xs" id="header-v3">
			<ul class="nav navbar-nav">
				 <li class="menu-item
                animate-dropdown"><a title="ประมูลทั้งหมด" href="product-category.html">ประมูลทั้งหมด</a></li>
                <li class="menu-item"><a title="ทองคำแท่ง" href="product-category.html">ทองคำแท่ง</a></li>
                <li class="menu-item"><a title="สร้อยทอง" href="product-category.html">สร้อยทอง</a></li>
                <li class="menu-item"><a title="กำไล " href="product-category.html">กำไล</a></li>
                <li class="menu-item"><a title="กรอบพระของเด็ก" href="product-category.html">กรอบพระของเด็ก</a></li>
                <li class="menu-item"><a title="พระแผง" href="product-category.html">พระแผง</a></li>
                <li class="menu-item"><a title="แหวนหัวพลอย" href="product-category.html">แหวนหัวพลอย</a></li>
             
			</ul>				
		</div><!-- /.collpase -->
	</div><!-- /.-container -->
</nav><!-- /.navbar -->