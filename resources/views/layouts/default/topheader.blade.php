   {{--*/ $menus = SiteHelpers::menus('top') /*--}}
  <!-- Header -->
  <header>
    <div class="header-container">
      
      <div class="container">
        <div class="row">
          
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 logo-block"> 
            <!-- Header Logo -->
            <div class="logo"> <a title="gogoldrun" href="{{url('')}}"><img alt="gogoldrun" src="{!! asset('frontend/punbid/') !!}/images/logo.png"> </a> </div>
            <!-- End Header Logo --> 
          </div>
          
        </div>
      </div>
    </div>
  </header>
  <!-- end header --> 
  
  <!-- Navigation -->
  
  <nav>
    <div class="container">
      <div class="mm-toggle-wrap">
      <div class="search-box">
              <form action="cat" method="POST" id="search_mini_form" name="Categories">
                <input type="text" placeholder="Search entire store here..." value="Search" maxlength="70" name="search" id="search">
                <button type="button" class="search-btn-bg"><span class="glyphicon glyphicon-search"></span>&nbsp;</button>
              </form>
            </div>
      </div>
      
      <div class="nav-inner"> 
        <!-- BEGIN NAV -->
        <ul id="nav" class="hidden-xs">
                    @foreach ($menus as $menu)
                        <li @if(Request::segment(1) == $menu['module']) class="level2 nav-10-3" @endif>
                          <a 
                          @if($menu['menu_type'] =='external')
                            href="{!! URL::to($menu['url']) !!}" 
                          @else
                            href="{!! URL::to($menu['module']) !!}<?php if(Session::get('lang')=='en'&&$menu['module']!='home'&&$menu['module']!='shop'&&$menu['module']!='bidpackhome')echo '-en';?>" 
                          @endif
                          >
                          <span>
                          @if(Session::get('lang')=='th'||Session::get('lang')==''||empty(Session::get('lang')))
                            {!! $menu['menu_lang']['title']['th'] !!}
                          @else
                            {!! $menu['menu_name'] !!}
                          @endif 
                          </span>
                          </a>
                        </li>
                    @endforeach
        </ul>
        <!--nav--> 
        </div>
    </div>
  </nav>
  <!-- end nav -->