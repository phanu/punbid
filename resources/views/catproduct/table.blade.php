<?php usort($tableGrid, "SiteHelpers::_sort"); ?>
<section class="panel">
        <div class="panel-body">
        		<h3><i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{{ $pageNote }}</small></h3>
                <div class="sbox-title"> 
				
				
					<div class="sbox-tools" >
						<a href="javascript:void(0)" class="btn btn-xs btn-white tips" title="Clear Search" onclick="reloadData('#{{ $pageModule }}','catproduct/data?search=')"><i class="fa fa-trash-o"></i> Clear Search </a>
						<a href="javascript:void(0)" class="btn btn-xs btn-white tips" title="Reload Data" onclick="reloadData('#{{ $pageModule }}','catproduct/data?return={{ $return }}')"><i class="fa fa-refresh"></i></a>
						@if(Session::get('gid') ==1)
						<a href="{{ url('sximo/module/config/'.$pageModule) }}" class="btn btn-xs btn-white tips" title=" {{ Lang::get('core.btn_config') }}" ><i class="fa fa-cog"></i></a>
						@endif 
					</div>
				</div>
				<div class="col-lg-12">
					@include( $pageModule.'/toolbar')

	 				{!! (isset($search_map) ? $search_map : '') !!}

	 			</div>
<?php echo Form::open(array('url'=>'catproduct/delete/', 'class'=>'form-horizontal' ,'id' =>'SximoTable'  ,'data-parsley-validate'=>'' )) ;?>
                    <div class="margin-bottom-50">
                    @if(count($rowData)>=1)
                        <table class="table table-hover nowrap example1" id="{{ $pageModule }}Table" width="100%">
                            <thead>
                            <tr>
								<th class="{sorter: 'digit'}"> No </th>
								<th> <input type="checkbox" class="checkall" /></th>		
											
								<?php foreach ($tableGrid as $t) :
									if($t['view'] =='1'):
										$limited = isset($t['limited']) ? $t['limited'] :'';
										if(SiteHelpers::filterColumn($limited ))
										{
											echo '<th>'.\SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())).'</th>';				
										} 
									endif;
								endforeach; ?>
								<th><?php echo Lang::get('core.btn_action') ;?></th>
							</tr>
                            </thead>
                            <tfoot>
                            <tr>
								<th> No </th>
								<th> <input type="checkbox" class="checkall" /></th>		
											
								<?php foreach ($tableGrid as $t) :
									if($t['view'] =='1'):
										$limited = isset($t['limited']) ? $t['limited'] :'';
										if(SiteHelpers::filterColumn($limited ))
										{
											echo '<th>'.\SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())).'</th>';				
										} 
									endif;
								endforeach; ?>
								<th><?php echo Lang::get('core.btn_action') ;?></th>
							</tr>
                            </tfoot>
                            <tbody>
        	@if($access['is_add'] =='1' && $setting['inline']=='true')
			<tr id="form-0" >
				<td> # </td>
				<td> </td>
				@if($setting['view-method']=='expand') <td> </td> @endif
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')
					<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
						<td data-form="{{ $t['field'] }}" data-form-type="{{ AjaxHelpers::inlineFormType($t['field'],$tableForm)}}">
							{!! SiteHelpers::transForm($t['field'] , $tableForm) !!}								
						</td>
						@endif
					@endif
				@endforeach
				<td width="100" class="text-right">
					<button onclick="saved('form-0')" class="btn btn-primary btn-xs" type="button"><i class="icon-checkmark-circle2"></i></button>
				</td>
			  </tr>	 
			  @endif        
			
           		<?php foreach ($rowData as $row) : 
           			  $id = $row->id;
           		?>
                <tr class="editable" id="form-{{ $row->id }}">
					<td class="number"> <?php echo ++$i;?>  </td>
					<td ><input type="checkbox" class="ids" name="ids[]" value="<?php echo $row->id ;?>" />  </td>					
					@if($setting['view-method']=='expand')
					<td><a href="javascript:void(0)" class="expandable" rel="#row-{{ $row->id }}" data-url="{{ url('catproduct/show/'.$id) }}"><i class="fa fa-plus " ></i></a></td>								
					@endif			
					 <?php foreach ($tableGrid as $field) :
					 	if($field['view'] =='1') : 
							$value = SiteHelpers::formatRows($row->{$field['field']}, $field , $row);
						 	?>
						 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
						 	@if(SiteHelpers::filterColumn($limited ))
								 <td align="<?php echo $field['align'];?>" data-values="{{ $row->{$field['field']} }}" data-field="{{ $field['field'] }}" data-format="{{ htmlentities($value) }}">				
                                                                @if($field['field'] =='completed')
					 						{!! $row->completed == 1 ? '<span class="label label-success">ปิดประมูลแล้ว</span>' : '<span class="label label-warning">ยังไม่ปิด</span>'  !!}
					 				 @elseif($field['field'] =='active')
{!! $row->active == 1 ? '<span class="label label-success">เปิด</span>' : '<span class="label label-warning">ปิด</span>'  !!}
@elseif($field['field'] =='featured')
{!! $row->active == 1 ? '<span class="label label-success">แนะนำ</span>' : '<span class="label label-warning">ปกติ</span>'  !!}
                                      @else
									{!! $value !!}					
									@endif		 
								 </td>
							@endif	
						 <?php endif;					 
						endforeach; 
					  ?>
				 <td data-values="action" data-key="<?php echo $row->id ;?>" width="100" class="text-right">
					{!! AjaxHelpers::buttonAction('catproduct',$access,$id ,$setting) !!}
					{!! AjaxHelpers::buttonActionInline($row->id,'id') !!}		
				</td>			 
                </tr>
                @if($setting['view-method']=='expand')
                <tr style="display:none" class="expanded" id="row-{{ $row->id }}">
                	<td class="number"></td>
                	<td></td>
                	<td></td>
                	<td colspan="{{ $colspan}}" class="data"></td>
                	<td></td>
                </tr>
                @endif				
            <?php endforeach;?>
              
        </tbody>
                        </table>
                        @else

	<div style="margin:100px 0; text-align:center;">
	
		<p> No Record Found </p>
	</div>
	
	@endif		
                    </div>
                    	<?php echo Form::close() ;?>
	@include('ajaxfooter')
                </div>
            </div>
</section>


	


	

	</div>
</div>	
	
	@if($setting['inline'] =='true') @include('sximo.module.utility.inlinegrid') @endif
<script>
$(document).ready(function() {

	$('.tips').tooltip();	
	$('input[type="checkbox"],input[type="radio"]').iCheck({
		checkboxClass: 'icheckbox_square-red',
		radioClass: 'iradio_square-red',
	});	
	$('#{{ $pageModule }}Table .checkall').on('ifChecked',function(){
		$('#{{ $pageModule }}Table input[type="checkbox"]').iCheck('check');
	});
	$('#{{ $pageModule }}Table .checkall').on('ifUnchecked',function(){
		$('#{{ $pageModule }}Table input[type="checkbox"]').iCheck('uncheck');
	});	
	
	$('#{{ $pageModule }}Paginate .pagination li a').click(function() {
		var url = $(this).attr('href');
		reloadData('#{{ $pageModule }}',url);		
		return false ;
	});

	<?php if($setting['view-method'] =='expand') :
			echo AjaxHelpers::htmlExpandGrid();
		endif;
	 ?>	
});		
</script>	
<style>
.table th.right { text-align:right !important;}
.table th.center { text-align:center !important;}

</style>
<!-- Page Scripts -->
<script>
    $(function(){

        $('.example1').DataTable({
            responsive: true
        });

        
    });
</script>
<!-- End Page Scripts -->

