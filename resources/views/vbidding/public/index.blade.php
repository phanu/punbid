<?php use SocialLinks\Page; ?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rs_toppadder30">
<div class="row">
<h3>ปิดประมูลแล้ว</h3>
  <?php foreach ($rowData as $row) { ?>

  <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 rs_toppadder10">

    <a href="{{url('bidding?view='.$row->products_bid_id)}}">
      <h4 class="widget-title widget-title-bid">
        <?php echo $row->name_th; ?>           
        <img src="{{url('')}}/frontend/punbidtheme/images/winnerbid.svg" class="img-toph4bidend" alt="">
      </h4>
    </a>
    <aside class="widget widget_authors m-bottom">
      <div class="bid-product-img">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <a href="{{url('bidding?view='.$row->products_bid_id)}}">
          <img src="{{url('')}}/uploads/products/{{$row->featured_img}}" class="img-responsive" alt="">
          </a>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
          <div class="line_orange">ปิดประมูล {{number_format($row->product_value_bid,2)}}THB</div>  
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding-right">
        <?php 
               $price_dis = $row->price - $row->product_value_bid;
               
               $percent = floor(($price_dis/$row->price)*100);

        ?>
        <div class="bid-userwintop">ราคา <span>{{number_format($row->price,2)}}</span>THB (ถูกลง <span>{{number_format($percent)}}%</span>)</div>

        <div class="bid-userwin">
                <img src="http://www.punbid.com/frontend/punbidtheme/images/userwin.svg">
                <x id="xuser_bidding_34">คุณ {{$row->username}} เป็นผู้ชนะ</x></div>
        </div>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
          <div class="line_yellow">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">ปิดประมูล</div>
          <!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-padding">จำนวนบิดที่ใช้</div>-->
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">ประหยัดไป</div>
          <div class="line_blue">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding"><?php echo date('d-m-Y',strtotime($row->bid_end)); ?></div>
            <!--<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 no-padding">{{number_format($row->bid_total)}} Bid</div>-->
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding">{{number_format($price_dis,2)}}</div>
          </div>
        </div>
<?php
  //Create a Page instance with the url information
          $page = new Page([
              'url' => config('app.url').'/bidding?view='.$row->products_bid_id,
              'title' => $row->name_th,
              'text' => 'ประมูล '.$row->name_th,
              'image' => config('app.url').'/uploads/products/'.$row->featured_img,
              'twitterUser' => '@punbidtheme'
          ]);

          //Use the properties to get the providers info, for example:
                          foreach ($page->html() as $tag) {
                              echo $tag."\n";
                          }
                              echo "\n";
                          foreach ($page->openGraph() as $tag) {
                              echo $tag."\n";
                          }
                              echo "\n";
                          foreach ($page->twitterCard() as $tag) {
                              echo $tag."\n";
                          }
                              echo "\n";
                          foreach ($page->schema() as $tag) {
                              echo  $tag."\n";
                          }
?>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
              <ul class="auctions_shares">
              <li><a href="{{$page->facebook->shareUrl}}" class="clr_1" ><i class="fa fa-facebook-square fa-3"></i></a></li>
              <li><a href="{{$page->twitter->shareUrl}}" class="clr_2"><i class="fa fa-twitter-square fa-3"></i></a></li>
              <li><a href="{{$page->pinterest->shareUrl}}" class="clr_4"><i class="fa fa-pinterest fa-3"></i></a></li>
              <li><a href="{{$page->plus->shareUrl}}" class="clr_5"><i class="fa fa-google-plus-square fa-3"></i></a></li>
              </ul>
          </div>

          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                   ชวนเพื่อนมาเล่นรับ Bid ฟรี 5 Bid
          </div>
      </div>
    </aside>
  </div>
  <?php } ?>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div class="row">
            <div class="rs_pagination rs_toppadder40 rs_bottompadder40 text-right">
              {!! $pagination->render() !!}
            </div>
          </div>
        </div>
</div>
</div>