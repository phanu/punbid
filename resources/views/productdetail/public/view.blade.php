<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array())) }}</td>
						<td>{{ $row->id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Category Th', (isset($fields['category_th']['language'])? $fields['category_th']['language'] : array())) }}</td>
						<td>{{ $row->category_th}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Category En', (isset($fields['category_en']['language'])? $fields['category_en']['language'] : array())) }}</td>
						<td>{{ $row->category_en}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Name Th 1', (isset($fields['name_th_1']['language'])? $fields['name_th_1']['language'] : array())) }}</td>
						<td>{{ $row->name_th_1}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Name En 1', (isset($fields['name_en_1']['language'])? $fields['name_en_1']['language'] : array())) }}</td>
						<td>{{ $row->name_en_1}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Title Th', (isset($fields['title_th']['language'])? $fields['title_th']['language'] : array())) }}</td>
						<td>{{ $row->title_th}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Title En', (isset($fields['title_en']['language'])? $fields['title_en']['language'] : array())) }}</td>
						<td>{{ $row->title_en}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Descript Th', (isset($fields['descript_th']['language'])? $fields['descript_th']['language'] : array())) }}</td>
						<td>{{ $row->descript_th}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Descript En', (isset($fields['descript_en']['language'])? $fields['descript_en']['language'] : array())) }}</td>
						<td>{{ $row->descript_en}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Stock Items', (isset($fields['stock_items']['language'])? $fields['stock_items']['language'] : array())) }}</td>
						<td>{{ $row->stock_items}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Price', (isset($fields['price']['language'])? $fields['price']['language'] : array())) }}</td>
						<td>{{ $row->price}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sale Price', (isset($fields['sale_price']['language'])? $fields['sale_price']['language'] : array())) }}</td>
						<td>{{ $row->sale_price}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Color Th', (isset($fields['color_th']['language'])? $fields['color_th']['language'] : array())) }}</td>
						<td>{{ $row->color_th}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Color En', (isset($fields['color_en']['language'])? $fields['color_en']['language'] : array())) }}</td>
						<td>{{ $row->color_en}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Color Id', (isset($fields['color_id']['language'])? $fields['color_id']['language'] : array())) }}</td>
						<td>{{ $row->color_id}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Create Date', (isset($fields['create_date']['language'])? $fields['create_date']['language'] : array())) }}</td>
						<td>{{ $row->create_date}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Featured', (isset($fields['featured']['language'])? $fields['featured']['language'] : array())) }}</td>
						<td>{{ $row->featured}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Brand', (isset($fields['brand']['language'])? $fields['brand']['language'] : array())) }}</td>
						<td>{{ $row->brand}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Featured Img', (isset($fields['featured_img']['language'])? $fields['featured_img']['language'] : array())) }}</td>
						<td>{!! SiteHelpers::formatRows($row->featured_img,$fields['featured_img'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Gallery', (isset($fields['gallery']['language'])? $fields['gallery']['language'] : array())) }}</td>
						<td>{!! SiteHelpers::formatRows($row->gallery,$fields['gallery'],$row ) !!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Active', (isset($fields['active']['language'])? $fields['active']['language'] : array())) }}</td>
						<td>{{ $row->active}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Products Code', (isset($fields['products_code']['language'])? $fields['products_code']['language'] : array())) }}</td>
						<td>{{ $row->products_code}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Tag', (isset($fields['tag']['language'])? $fields['tag']['language'] : array())) }}</td>
						<td>{{ $row->tag}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Shiping Cost', (isset($fields['shiping_cost']['language'])? $fields['shiping_cost']['language'] : array())) }}</td>
						<td>{{ $row->shiping_cost}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sold', (isset($fields['sold']['language'])? $fields['sold']['language'] : array())) }}</td>
						<td>{{ $row->sold}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	