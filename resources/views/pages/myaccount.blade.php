						<?php
                        $users_details = \App\Http\Controllers\UserscustomController::display(Session::get('uid'));
                        $userdet = $users_details['row'];
                        $avatarbig = SiteHelpers::avatar( 120 , 1);
                        $_GET['v'] = isset($_GET['v'])?$_GET['v']:'';


                        ?>

 
<div class="rs_graybg rs_toppadder10 rs_bottompadder10">
		<div class="container">
			<div class="row">
                @if(Session::has('message'))
                {!! Session::get('message') !!} 
                    <ul class="parsley-error-list">
                        @foreach($errors->all() as $error)
                        <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                @endif
            </div>
            <div class="row">

                <div class="col-md-3">
                    <!-- user sidebar -->
                    @include('pages/myaccount/usersidebar')
                </div>
                <div class="col-md-9">
                    <!-- tab content -->
                    <div class="tab-content">
                        <div class="tab-pane @if($_GET['v']=='') active @endif>" id="account">
                            <div class="card-box card-tabs">
                                @include('pages/myaccount/usercontent')
                            </div>
                        </div>
                        <div class="tab-pane @if($_GET['v']=='wishlist') active @endif>" id="wishlist">
                            <div class="card-box card-tabs">
                                @include('pages/auction')
                            </div>
                        </div>
                        <div class="tab-pane @if($_GET['v']=='ordershop') active @endif>" id="ordershop">
                            <div class="card-box card-tabs">
                                @include('pages/myaccount/ordershop')
                            </div>
                        </div>
                        <div class="tab-pane @if($_GET['v']=='transactions') active @endif>" id="transactions">
                            <div class="card-box card-tabs">
                                @include('pages/myaccount/transactions')
                            </div>
                        </div>
                        <div class="tab-pane @if($_GET['v']=='informpayment') active @endif>" id="informpayment">
                            <div class="card-box card-tabs">
                                @include('pages/myaccount/informpayment')
                            </div>
                        </div>
                        <div class="tab-pane @if($_GET['v']=='withdraw') active @endif>" id="withdraw">
                            <div class="card-box card-tabs">
                                @include('pages/myaccount/withdraw')
                            </div>
                        </div>
                        <div class="tab-pane @if($_GET['v']=='bidremain') active @endif>" id="bidremain">
                            <div class="card-box card-tabs">
                                @include('pages/myaccount/bidremain')
                            </div>
                        </div>
                        <div class="tab-pane @if($_GET['v']=='bonus') active @endif>" id="bonus">
                            <div class="card-box card-tabs">
                                @include('pages/myaccount/bonus')
                            </div>
                        </div>
                    </div>
                </div> 
            </div>
        </div>
</div>


