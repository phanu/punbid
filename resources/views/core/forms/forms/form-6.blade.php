{!! Form::open(array('url'=>'home/proccess/6', 'id'=>'formconfiguration','class'=>'tg-themeform' ,'files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}
@if(Session::has('message'))	  
		{!! Session::get('message') !!}
@endif	
<fieldset>
<ul class="parsley-error-list">
	@foreach($errors->all() as $error)
		<li>{{ $error }}</li>
	@endforeach
</ul>

<div class="form-group  " >
				{!! Form::text('fullname','',array('class'=>'form-control', 'placeholder'=>'ชื่อ-นามสกุล', 'required'=>'true'  )) !!}
		</div>

		<div class="form-group  " >
					
				{!! Form::text('phone','',array('class'=>'form-control', 'placeholder'=>'เบอร์โทรศัพท์', 'required'=>'true'  )) !!}
		</div>

		<div class="form-group  " >
					
				{!! Form::text('email','',array('class'=>'form-control', 'placeholder'=>'อีเมล', 'required'=>'true', 'parsley-type'=>'email'   )) !!}
		</div>

		<div class="form-group  " >
				<textarea name='address' rows='5' id='address' class='form-control '  placeholder="ที่อยู่"
				         required  ></textarea>
		</div>

		
			
				<button type="submit" class="tg-btn"><span>สมัครเลย</span></button>
		
</fieldset>
{!! Form::close() !!}

<link href="{{ asset('sximo/js/plugins/iCheck/skins/square/red.css')}}" rel="stylesheet">
<script type="text/javascript" src="{{ asset('sximo/js/plugins/iCheck/icheck.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('input[type="checkbox"],input[type="radio"]').iCheck({
			checkboxClass: 'icheckbox_square-red',
			radioClass: 'iradio_square-red',
		});	
	});

	
</script>