<div class="woocommerce-tabs wc-tabs-wrapper">
	<ul class="tabs wc-tabs">
		
		<li class="accessories_tab"> <a href="#tab-accessories">ซื้อสิทธิ์การประมูล</a> </li>
                    <li class="description_tab"> <a href="#tab-description">เทคนิคการประมูล</a> </li>
                    <li class="specification_tab"> <a href="#tab-specification">ประวัติการประมูล</a> </li>
                    <li class="reviews_tab active"> <a href="#tab-reviews">ผู้ชนะประมูล</a></li>
	</ul>

	<div class="tab-content">
		<div class="panel entry-content wc-tab" id="tab-accessories" style="display:block;">
			<?php require_once 'inc/blocks/single-product/woocommerce-tabs/accessories-tab.php'; ?>
		</div>

		<div class="panel entry-content wc-tab" id="tab-description" style="display:none;">
			<?php require_once 'inc/blocks/single-product/woocommerce-tabs/description-tab.php'; ?>
		</div>

		<div class="panel entry-content wc-tab" id="tab-specification" style="display:none;">
			<?php require_once 'inc/blocks/single-product/woocommerce-tabs/specification-tab.php'; ?>
		</div><!-- /.panel -->

		<div class="panel entry-content wc-tab" id="tab-reviews" style="display:none;">
			<?php require_once 'inc/blocks/single-product/woocommerce-tabs/reviews-tab.php'; ?>
		</div><!-- /.panel -->
	</div>
</div>



