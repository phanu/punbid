<nav class="top-menu">
    <div class="menu-icon-container hidden-md-up">
        <div class="animate-menu-button left-menu-toggle">
            <div><!-- --></div>
        </div>
    </div>
    <div class="menu">
    	@if(Auth::user()->group_id == 1)
        <div class="menu-user-block">
            <div class="dropdown dropdown-avatar">
                <a href="javascript: void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                    <span class="avatar" href="javascript:void(0);">
                       {!! SiteHelpers::avatar( 40 ) !!}
                    </span>

                </a>
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="" role="menu">
                    <a class="dropdown-item" href="{{ URL::to('user/profile')}}"><i class="dropdown-icon icmn-user"></i> {{ Lang::get('core.m_profile') }}</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ URL::to('dashboard')}}"><i class="dropdown-icon icmn-circle-right"></i> {{ Lang::get('core.m_dashboard') }}</a>
                    <a class="dropdown-item" href="{{ URL::to('')}}"><i class="dropdown-icon icmn-circle-right"></i> Main Site </a>
                    <a class="dropdown-item" href="{{ URL::to('core/elfinder')}}"><i class="dropdown-icon icmn-circle-right"></i> File Manager</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="{{ URL::to('user/logout')}}"><i class="dropdown-icon icmn-exit"></i> {{ Lang::get('core.m_logout') }}</a>
                </ul>
            </div>
        </div>
        @endif
        <div class="menu-info-block">
            <div class="left">
                <div class="header-buttons">
                     <div class="left margin-right-20 hidden-md-down ">
	                @if(isset($pageTitle) && isset($pageNote))
			        
			             {{ $pageTitle }} : <small> {{ $pageNote }}</small>    
			        
			        @endif
            		</div>
                    @if(CNF_MULTILANG ==1)
					<div class="dropdown">

						<?php 
						$flag ='en';
						$langname = 'English'; 
						foreach(SiteHelpers::langOption() as $lang):
							if($lang['folder'] == Session::get('lang') or $lang['folder'] == 'CNF_LANG') {
								$flag = $lang['folder'];
								$langname = $lang['name']; 
							}
							
						endforeach;?>

						<a href="javascript: void(0);" class="dropdown-toggle dropdown-inline-button" data-toggle="dropdown" data-hover="dropdown" aria-expanded="false">
                            <img class="flag-lang" src="{{ asset('sximo/images/flags/'.$flag.'.png') }}" width="16" height="12" alt="lang" />
                            <span class="hidden-lg-down">{{ strtoupper($flag) }}</span>
                            
                        </a>
                         <ul class="dropdown-menu" aria-labelledby="" role="menu">

                         	@foreach(SiteHelpers::langOption() as $lang)
									<a class="dropdown-item" href="{{ URL::to('home/lang/'.$lang['folder'])}}"><img class="flag-lang" src="{{ asset('sximo/images/flags/'. $lang['folder'].'.png')}}" width="16" height="11" alt="lang" /> {{  $lang['name'] }}</a>
							@endforeach	
                            
                        </ul>
						
					</div>	
					@endif
                    <div class="dropdown">
                        <a href="javascript: void(0);" class="dropdown-toggle dropdown-inline-button" data-toggle="dropdown" aria-expanded="false">
                            <i class="dropdown-inline-button-icon icmn-database"></i>
                            <span class="hidden-lg-down">Dashboards</span>
                           
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="" role="menu">
                            

                    <a class="dropdown-item" href="{{ URL::to('sximo/config')}}"><i class="dropdown-icon icmn-circle-right"></i> {{ Lang::get('core.m_setting') }}</a>
					<a class="dropdown-item" href="{{ URL::to('core/users')}}"><i class="dropdown-icon icmn-circle-rightdropdown-icon icmn-circle-right"></i> {{ Lang::get('core.m_users') }} &  {{ Lang::get('core.m_groups') }} </a>
					<a class="dropdown-item" href="{{ URL::to('core/users/blast')}}"><i class="dropdown-icon icmn-circle-right"></i> {{ Lang::get('core.m_blastemail') }} </a>	
					<a class="dropdown-item" href="{{ URL::to('core/logs')}}"><i class="dropdown-icon icmn-circle-right"></i> {{ Lang::get('core.m_logs') }}</a>	
					<li class="divider">
					<a class="dropdown-item" href="{{ URL::to('core/pages')}}"><i class="dropdown-icon icmn-circle-right"></i> {{ Lang::get('core.m_pagecms')}}</a>
					
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="{{ URL::to('sximo/module')}}"><i class="dropdown-icon icmn-circle-right"></i> {{ Lang::get('core.m_codebuilder') }}</a>
					<a class="dropdown-item" href="{{ URL::to('sximo/code')}}"><i class="dropdown-icon icmn-circle-right"></i>  <span class="text-danger">Source Code Editor</span>  </a>
					<a class="dropdown-item" href="{{ URL::to('sximo/tables')}}"><i class="dropdown-icon icmn-circle-right"></i> Database Tables </a>
					<a class="dropdown-item" href="{{ URL::to('sximo/menu')}}"><i class="dropdown-icon icmn-circle-right"></i> {{ Lang::get('core.m_menu') }}</a>	
					<div class="dropdown-divider"></div>
					<a class="dropdown-item" href="{{ URL::to('core/template/changelog')}}"><i class="dropdown-icon icmn-circle-right"></i> Changelog </a>
					<a class="dropdown-item" href="{{ URL::to('core/template')}}"><i class="dropdown-icon icmn-circle-right"></i> Template Guide </a>
					<a class="dropdown-item" href="http://sximobuilder.com/faqs" target="_blank"><i class="dropdown-icon icmn-circle-right"></i> Online Documentation </a>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="left hidden-md-down">
                <div class="example-top-menu-chart">
                    <span class="title">Income:</span>
                    <span class="chart" id="topMenuChart">1,3,2,0,3,1,2,3,5,2</span>
                    <span class="count">14,452 THB</span>

                    <!-- Top Menu Chart Script -->
                    <script>
                        $(function () {

                            var topMenuChart = $("#topMenuChart").peity("bar", {
                                fill: ['#01a8fe'],
                                height: 22,
                                width: 44
                            });

                            setInterval(function() {
                                var random = Math.round(Math.random() * 10);
                                var values = topMenuChart.text().split(",");
                                values.shift();
                                values.push(random);
                                topMenuChart.text(values.join(",")).change()
                            }, 1000);

                        });
                    </script>
                    <!-- Top Menu Chart Script -->
                </div>
                </div>
               
            <!--<div class="right hidden-md-down margin-left-20">
                <div class="search-block">
                    <div class="form-input-icon form-input-icon-right">
                        <i class="icmn-search"></i>
                        <input type="text" class="form-control form-control-sm form-control-rounded" placeholder="Search...">
                        <button type="submit" class="search-block-submit "></button>
                    </div>
                </div>
            </div>-->
            <div class="right example-buy-btn">
                <a href="{{ URL::to('user/profile')}}" target="_blank" class="btn btn-success-outline btn-rounded">
                    {{ Session::get('fid')}}
                </a>
            </div>
        </div>
    </div>
</nav>