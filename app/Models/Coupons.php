<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class coupons extends Sximo  {
	
	protected $table = 'coupons';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT coupons.* FROM coupons  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE coupons.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
