

		 {!! Form::open(array('url'=>'orders/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> orders</legend>
				{!! Form::hidden('orderNumber', $row['orderNumber']) !!}					
									  <div class="form-group  " >
										<label for="OrderDate" class=" control-label col-md-4 text-left"> OrderDate </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('orderDate', $row['orderDate'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="ShippedDate" class=" control-label col-md-4 text-left"> ShippedDate </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('shippedDate', $row['shippedDate'],array('class'=>'form-control date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $status = explode(',',$row['status']);
					$status_opt = array( 'onhold' => 'On Hold' ,  'Inprocess' => 'In Process' ,  'Shipped' => 'Shipped' ,  'Cancel' => 'Cancel' , ); ?>
					<select name='status' rows='5' required  class='select2 '  > 
						<?php 
						foreach($status_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['status'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Comments" class=" control-label col-md-4 text-left"> Comments </label>
										<div class="col-md-6">
										  <textarea name='comments' rows='5' id='comments' class='form-control '  
				           >{{ $row['comments'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="CustomerNumber" class=" control-label col-md-4 text-left"> CustomerNumber <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('customerNumber', $row['customerNumber'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Tracking Number" class=" control-label col-md-4 text-left"> Tracking Number </label>
										<div class="col-md-6">
										  {!! Form::text('tracking_number', $row['tracking_number'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Payment Type" class=" control-label col-md-4 text-left"> Payment Type <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('payment_type', $row['payment_type'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Payment Fee" class=" control-label col-md-4 text-left"> Payment Fee <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('payment_fee', $row['payment_fee'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Firstname" class=" control-label col-md-4 text-left"> Firstname <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='firstname' rows='5' id='firstname' class='form-control '  
				         required  >{{ $row['firstname'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Lastname" class=" control-label col-md-4 text-left"> Lastname <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='lastname' rows='5' id='lastname' class='form-control '  
				         required  >{{ $row['lastname'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Address" class=" control-label col-md-4 text-left"> Address <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='address' rows='5' id='address' class='form-control '  
				         required  >{{ $row['address'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Phone" class=" control-label col-md-4 text-left"> Phone <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='phone' rows='5' id='phone' class='form-control '  
				         required  >{{ $row['phone'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Postcode" class=" control-label col-md-4 text-left"> Postcode <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='postcode' rows='5' id='postcode' class='form-control '  
				         required  >{{ $row['postcode'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Sex" class=" control-label col-md-4 text-left"> Sex <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='sex' rows='5' id='sex' class='form-control '  
				         required  >{{ $row['sex'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Province" class=" control-label col-md-4 text-left"> Province <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='province' rows='5' id='province' class='form-control '  
				         required  >{{ $row['province'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="District" class=" control-label col-md-4 text-left"> District <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='district' rows='5' id='district' class='form-control '  
				         required  >{{ $row['district'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Subdistrict" class=" control-label col-md-4 text-left"> Subdistrict <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='subdistrict' rows='5' id='subdistrict' class='form-control '  
				         required  >{{ $row['subdistrict'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
