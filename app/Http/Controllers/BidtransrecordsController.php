<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Bidtransrecords;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use Illuminate\Support\Facades\App;
use Carbon\Carbon;
use App\Http\Controllers\BidtransController;
use App\Jobs\SendProductsBid;
class BidtransrecordsController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'bidtransrecords';
	static $per_page	= '10';

	public function __construct()
	{
		
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->model = new Bidtransrecords();
		
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'bidtransrecords',
			'return'	=> self::returnUrl()
			
		);
		
		\App::setLocale(CNF_LANG);
		if (defined('CNF_MULTILANG') && CNF_MULTILANG == '1') {

		$lang = (\Session::get('lang') != "" ? \Session::get('lang') : CNF_LANG);
		\App::setLocale($lang);
		}  


		
	}

	public function getIndex( Request $request )
	{

		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');

		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : 'id'); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : 'asc');
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = '';	
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		} 

		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : static::$per_page ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('bidtransrecords');
		
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		$this->data['fields'] =  \AjaxHelpers::fieldLang($this->info['config']['grid']);
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('bidtransrecords.index',$this->data);
	}	



	function getUpdate(Request $request, $id = null)
	{
	
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] =  $row;
		} else {
			$this->data['row'] = $this->model->getColumnTable('bids_trans'); 
		}
		$this->data['fields'] =  \AjaxHelpers::fieldLang($this->info['config']['forms']);
		
		$this->data['id'] = $id;
		return view('bidtransrecords.form',$this->data);
	}	

	public function getShow( Request $request, $id = null)
	{

		if($this->access['is_detail'] ==0) 
		return Redirect::to('dashboard')
			->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
			$this->data['fields'] 		=  \SiteHelpers::fieldLang($this->info['config']['grid']);
			$this->data['id'] = $id;
			$this->data['access']		= $this->access;
			$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
			$this->data['fields'] =  \AjaxHelpers::fieldLang($this->info['config']['grid']);
			return view('bidtransrecords.view',$this->data);
		} else {
			return Redirect::to('bidtransrecords')->with('messagetext','Record Not Found !')->with('msgstatus','error');					
		}
	}

	function postCopy( Request $request)
	{	
	    foreach(\DB::select("SHOW COLUMNS FROM bids_trans ") as $column)
        {
			if( $column->Field != 'id')
				$columns[] = $column->Field;
        }
		
		if(count($request->input('ids')) >=1)
		{
			$toCopy = implode(",",$request->input('ids'));
			$sql = "INSERT INTO bids_trans (".implode(",", $columns).") ";
			$sql .= " SELECT ".implode(",", $columns)." FROM bids_trans WHERE id IN (".$toCopy.")";
			\DB::select($sql);

			return Redirect::to('bidtransrecords')->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
		} else {
		
			return Redirect::to('bidtransrecords')->with('messagetext','Please select row to copy')->with('msgstatus','error');
		}	
		
	}		

	/*function postSave( Request $request)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('tb_bidtransrecords');
				
			$id = $this->model->insertRow($data , $request->input('id'));
			
			if(!is_null($request->input('apply')))
			{
				$return = 'bidtransrecords/update/'.$id.'?return='.self::returnUrl();
			} else {
				$return = 'bidtransrecords?return='.self::returnUrl();
			}

			// Insert logs into database
			if($request->input('id') =='')
			{
				\SiteHelpers::auditTrail( $request , 'New Data with ID '.$id.' Has been Inserted !');
			} else {
				\SiteHelpers::auditTrail($request ,'Data with ID '.$id.' Has been Updated !');
			}

			return Redirect::to($return)->with('messagetext',\Lang::get('core.note_success'))->with('msgstatus','success');
			
		} else {

			return Redirect::to('bidtransrecords/update/'. $request->input('id'))->with('messagetext',\Lang::get('core.note_error'))->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}	*/
	
	public function postAddwlists( Request $request )
	{
		$yourid = \Session::get('uid');

		if(is_null($yourid)){

			$arr = array('message' => 'false');
			return json_encode($arr);
		}
		
		$rules = array(
			'wid' => 'required'		
		);
		$validator = Validator::make($request->all(), $rules);	

		if ($validator->passes()) {

			$wishlists_have = \DB::table('wishlists')->where('customerNumber', '=',$yourid )->where('products_bid_id', '=', $request->wid )->value('id');
				
			if(is_null($wishlists_have))
			{
				\DB::table('wishlists')->insert([
											'customerNumber' => $yourid, 'products_bid_id' => $request->wid
											]);

				$wishlists_have = \DB::table('wishlists')->where('customerNumber', '=',$yourid )->count();
				$arr = array('message' => 'success', 'rewishlists' => '('.$wishlists_have.')');
				
			}else
			{
				\DB::table('wishlists')->where('id', '=', $wishlists_have)->delete();
				$wishlists_have = \DB::table('wishlists')->where('customerNumber', '=',$yourid )->count();
				$arr = array('message' => 'delsuccess', 'rewishlists' => '('.$wishlists_have.')');
			}

			return json_encode($arr);
			
		} else {

			$arr = array('message' => 'false');
			return json_encode($arr);
		}	
	
	}

	public static function checkwlists( $pid = null )
	{
		$yourid = \Session::get('uid');
		$id = $_GET['view'];

		if(is_null($yourid)||is_null($id)){

			return 0;
		}

			$wishlists_have = \DB::table('wishlists')->where('customerNumber', '=',$yourid )->where('products_bid_id', '=', $id )->value('id');

		
				
			if(is_null($wishlists_have))
			{
				return 0;
			}else
			{
				return 1;
			}

	}

	public static function countwlists( $pid = null )
	{
		$yourid = \Session::get('uid');

		if(is_null($yourid)){

			return false;
		}else{

			$wishlists_have = \DB::table('wishlists')->where('customerNumber', '=',$yourid )->count();
				
			return $wishlists_have;
		}

	}

	public function postDelete( Request $request)
	{
		
		if($this->access['is_remove'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
		// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));
			
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
			// redirect
			return Redirect::to('bidtransrecords')
        		->with('messagetext', \Lang::get('core.note_success_delete'))->with('msgstatus','success'); 
	
		} else {
			return Redirect::to('bidtransrecords')
        		->with('messagetext','No Item Deleted')->with('msgstatus','error');				
		}

	}	

	public static function display( )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Bidtransrecords();
		$info = $model::makeInfo('bidtransrecords');

		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']
			
		);

		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('bidtransrecords.public.view',$data);
			} 

		} else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> 'id' ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1 
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];	

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit']; 
			$data['pagination'] = $pagination;
			return view('bidtransrecords.public.index',$data);			
		}


	}

	function postSavepublic( Request $request)
	{

		$rules = array(
			'customerNumber' => 'required'		
		);	
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {

			$yourid = \Session::get('uid');

			$data = $this->validatePost('bids_trans');

			$bidid = $request->input('products_bid_id');

			$setcomp = 0;
			$enddate = \DB::table('products_bid')->where('products_bid_id', '=',$bidid )->value('bid_end');	
			$maxval = $this->model->select('product_value','customerNumber')->where('products_bid_id', '=', $bidid)->orderBy('id','desc')->first();

			if(isset($maxval->customerNumber)&&$maxval->customerNumber==$yourid)
			{
					$arr = array('mess' => 'success','clickdisable'=>'bid-'.$bidid, 'pid' => $bidid);
					return json_encode($arr);
			}

			if(is_null($maxval))
			{
				$maxval = 1;
			}else
			{
				$maxval = $maxval->product_value;
			}

			$newmaxval=0.1+$maxval;

			$dt = Carbon::now();

			$dt2 = Carbon::parse($enddate);

			//$bidnewtime = 0;
			if($dt<$dt2)
			{
					$bidtotal = \DB::table('tb_users')->whereId($yourid)->value('bid');

					if(($bidtotal-$request->input('bid'))<0)
					{

						$arr = array('mess' => 'nobidegn');

						return json_encode($arr);

					}else{


						
						//$last_bidtran = $this->model->insertRow($data, $request->input('id'));
						
						$inserted = \DB::statement("INSERT INTO bids_trans (products_bid_id, customerNumber, bid, product_value, bid_type, curr_bids)
												SELECT * FROM (SELECT ".$bidid." as products_bid_id, ".$yourid." 
												as customerNumber, ".$request->input('bid')." as bid, ".$newmaxval." as product_value, 0 as bid_type, ".$bidtotal." as curr_bids ) AS tmp
												WHERE NOT EXISTS (
												    SELECT b1.id from bids_trans b1 join 
												    (
												    	SELECT products_bid_id, customerNumber, product_value FROM bids_trans 
												        WHERE id = (select MAX(id) from bids_trans 
												        where products_bid_id = ".$bidid.")
													) b2 on b1.products_bid_id = b2.products_bid_id 
												    and b1.customerNumber = b2.customerNumber
												WHERE b1.products_bid_id = ".$bidid." 
												and b1.customerNumber = ".$yourid.")");


												if($inserted==1)
												{
													$diff = $dt->diffInseconds($dt2);
													if($diff<=15)
													{
														$bidnewtime = $dt->addSeconds(15);
															\DB::table('products_bid')->where('products_bid_id', '=', $bidid)->update(array('bid_end' => $bidnewtime));
													}
													\DB::table('tb_users')->whereId($yourid)->decrement('bid', $request->input('bid'));	
													//sleep(1);
													//$this->setcroncomplete($pid);
													//sleep(1);
												}
					}

			}else
			{    
				$arr = array('mess' => 'success','clickdisable'=>'bid-'.$bidid);

				return json_encode($arr);
			}

			//broadcast chanel 
			//$ch = 'bid_'.$bidid;
			/*$ch = 'bidmain';
      	
      $bidtransrow = \DB::table('v_bid_tran')->select('username','product_value_bid','bid_type')->where('products_bid_id', '=', $bidid)->first();

			//$html = '';

            /*foreach ($bidtransrow as $biduser) { 
             	$html = array ( $grep => array ( "product_value_bid" => $biduser->product_value_bid, "username" => $biduser->username, "bid_type" => $biduser->bid_type) );
                $grep++;
             }*/

            //broadcast
			/*$pusher = App::make('pusher');

			if(isset($bidnewtime))
			{
				 $pusher->trigger( $ch,'bidding_'.$bidid, array('bidcompleted' => $setcomp, 'bidd' => $bidtransrow,
				 	'product_value_bid' => number_format($bidtransrow->product_value_bid,2),'biduser' => $bidtransrow->username, 'bidnewtime' => $bidnewtime, 'pbidid' => $bidid));

 			}else{
				 $pusher->trigger( $ch,'bidding_'.$bidid, array('bidcompleted' => $setcomp,'bidd' => $bidtransrow,'biduser' => $bidtransrow->username,'product_value_bid' => number_format($bidtransrow->product_value_bid,2),'pbidid' => $bidid));	
 			}*/

    		//send profile to server
    		$youruser = User::find($yourid);

    		$arr = array('mess' => 'success','current_bid' => $youruser->bid, 'pid' => $bidid);

			return json_encode($arr);

		} else {

			$arr = array('mess' => 'fail', 'pid' => $bidid);

			return json_encode($arr);
		}
	
	}	

	/**
     * Send a job products to a given user.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
	public function getProductsbidcron()
	{
		$cronprod = \DB::select('SELECT products_bid_id FROM `products_bid` WHERE completed = "0" and active = "1"');

		if(!empty($cronprod))
		{	
				foreach ($cronprod as $row) {

					$job = (new SendProductsBid($row->products_bid_id));

					$this->dispatch($job);

				}
		}
	}


	public static function croncom($pid = null)
	{

		
		$autobid_incom = \DB::select('SELECT products_bid_id,bid_end,boots,complete_val,botwin FROM `products_bid` WHERE DATE_ADD(bid_end, INTERVAL -16 second) <= now() and completed = "0" and active = "1" and products_bid_id = '.$pid);		


		$model  = new Bidtransrecords();

			if(!empty($autobid_incom))
			{	
				
				foreach ($autobid_incom as $row) {

					
					$autobid_users = \DB::select('SELECT customerNumber,bid_remain,boots,id FROM `autobid` WHERE products_bid_id = '.$row->products_bid_id.' and bid_remain > 0 order by id asc');

					if(!empty($autobid_users))
						{	

						foreach ($autobid_users as $autobid_row) {

								$setcomp = 0;
								$data['customerNumber']= $yourid = $autobid_row->customerNumber;

								$data['products_bid_id']= $bidid = $row->products_bid_id;

								$data['bid']= $boots = $row->boots;

								$aid =  $autobid_row->id;


								$bid_remain = $autobid_row->bid_remain;

								$enddate = $row->bid_end;

								$product_value_inc = 0.1;

								$maxval = $model->select('product_value','customerNumber')->where('products_bid_id', '=', $bidid)->orderBy('product_value','desc')->first();

								$data['bid_type'] = 1;


								if(isset($maxval->customerNumber)&&$maxval->customerNumber==$yourid)
								{
									//$cc_val = $maxval->customerNumber;
								}else
								{
									//$cc_val = $maxval->customerNumber;
									if(is_null($maxval))
									{
										$maxval = 1; 
									}else
									{
										$maxval = $maxval->product_value;
									}



										$data['product_value']=$newmaxval=$product_value_inc=$product_value_inc+$maxval;
										
											if(($bid_remain-$boots)<0)
											{

											}else{

													sleep(1);
													$bidtotal = \DB::table('tb_users')->whereId($yourid)->value('bid');
													$inserted = \DB::statement("INSERT INTO bids_trans (products_bid_id, customerNumber, bid, product_value, bid_type, curr_bids)
													SELECT * FROM (SELECT ".$bidid." as products_bid_id, ".$yourid." 
													as customerNumber, ".$boots." as bid, ".$newmaxval." as product_value, 1 as bid_type, ".$bidtotal." as curr_bids) AS tmp
													WHERE NOT EXISTS (
													    SELECT b1.id from bids_trans b1 join 
													    (
													    	SELECT products_bid_id, customerNumber, product_value FROM bids_trans 
													        WHERE id = (select MAX(id) from bids_trans 
													        where products_bid_id = ".$bidid.")
														) b2 on b1.products_bid_id = b2.products_bid_id 
													    and b1.customerNumber = b2.customerNumber
													WHERE b1.products_bid_id = ".$bidid." 
													and b1.customerNumber = ".$yourid.")");

													if($inserted==1)
													{
														
														\DB::table('autobid')->whereId($aid)->decrement('bid_remain', $boots);

														
															$dt = Carbon::now();
															$bidnewtime = $dt->addSeconds(15);
																\DB::table('products_bid')->where('products_bid_id', '=', $bidid)
																	->update(array('bid_end' => $bidnewtime));

															sleep(2);
													}
											}
														//sleep(1);
														//
													

													//broadcast chanel 
													/*//$ch = 'bid_'.$bidid;
													$ch = 'bidmain';
										      		
													$bidtransrow = \DB::table('v_bid_tran')->select('username','product_value_bid','bid_type')->where('products_bid_id', '=', $bidid)->first();

										            //broadcast
													/*$pusher = App::make('pusher');

										 			if(isset($bidnewtime))
													{

														 $pusher->trigger( $ch,'bidding_'.$bidid, array('bidcompleted' => $setcomp, 'bidd' => $bidtransrow,'product_value_bid' => number_format($bidtransrow->product_value_bid,2),
														 	'biduser' => $bidtransrow->username, 'bidnewtime' => $bidnewtime, 'pbidid' => $bidid));

										 			}else{

														 $pusher->trigger( $ch,'bidding_'.$bidid, array('bidcompleted' => $setcomp,'bidd' => $bidtransrow,'biduser' => $bidtransrow->username,'product_value_bid' => number_format($bidtransrow->product_value_bid,2),'pbidid' => $bidid));	
										 			
										 			}*/

													

										

										
							 		}
							}

						}

						$pbv = 0;
						$bot = 0;
						//$product_value_bid = \DB::table('v_bid_tran_new')->where('products_bid_id', '=',$row->products_bid_id)->value('product_value_bid');
						$product_value_bid = \DB::table('v_bid_tran_new')->select('product_value_bid','customerNumber')->where('products_bid_id', '=', $row->products_bid_id)->orderBy('id','desc')->first();


						if(!is_null($product_value_bid))
						{
							$pbv = $product_value_bid->product_value_bid;
							$bot = $product_value_bid->customerNumber;
						}

						$dt = Carbon::now();

						$dt2 = Carbon::parse($row->bid_end);

						$diff = $dt->diffInseconds($dt2);
						

						if($row->complete_val>$pbv&&$diff<8||$row->botwin==1&&$diff<6||$pbv==0)
						{
							if($row->botwin==1){
									$bot = User::find($bot);
								}

								if($row->complete_val>$pbv||$pbv==0||(!is_null($bot)&&$bot->group_id!=4))
								{

									$bot = \DB::table('tb_users')->where('group_id', '=', 4)->orderByRaw("RAND()")->first();

									$yourid = $bot->id;

									$boots = $row->boots;

									$bidid = $row->products_bid_id;
									$product_value_inc = 0.1;
									$setcomp = 0;
									$enddate = \DB::table('products_bid')->where('products_bid_id', '=',$bidid )->value('bid_end');	
									$maxval = $model->select('product_value','customerNumber')->where('products_bid_id', '=', $bidid)->orderBy('product_value','desc')->first();

									if(isset($maxval->customerNumber)&&$maxval->customerNumber==$yourid)
									{	
											$arr = array('mess' => 'success','clickdisable'=>'bid-'.$bidid);
											return json_encode($arr);
									}

									if(is_null($maxval))
									{	
										$maxval = 1; 
									}else
									{	
										$maxval = $maxval->product_value;
									}

									$newmaxval=$product_value_inc=$product_value_inc+$maxval;
												//$last_bidtran = $this->model->insertRow($data, $request->input('id'));
												
												$inserted = \DB::statement("INSERT INTO bids_trans (products_bid_id, customerNumber, bid, product_value, bid_type)
																		SELECT * FROM (SELECT ".$bidid." as products_bid_id, ".$yourid." 
																		as customerNumber, ".$row->boots." as bid, ".$newmaxval." as product_value, 0 as bid_type) AS tmp
																		WHERE NOT EXISTS (
																		    SELECT b1.id from bids_trans b1 join 
																		    (
																		    	SELECT products_bid_id, customerNumber, product_value FROM bids_trans 
																		        WHERE id = (select MAX(id) from bids_trans 
																		        where products_bid_id = ".$bidid.")
																			) b2 on b1.products_bid_id = b2.products_bid_id 
																		    and b1.customerNumber = b2.customerNumber
																		WHERE b1.products_bid_id = ".$bidid." 
																		and b1.customerNumber = ".$yourid.")");


																		if($inserted==1)
																		{
																			$dt = Carbon::now();
																			$bidnewtime = $dt->addSeconds(15);
																			\DB::table('products_bid')->where('products_bid_id', '=', $bidid)->update(array('bid_end' => $bidnewtime));
																			sleep(1);
																			
																		}
									
								}
						}
						
						
							
				$probid_completed = \DB::select('SELECT products_bid_id,bid_end,boots,complete_val,botwin,boots FROM `products_bid` WHERE bid_end < now() and completed = "0" and active = "1" and products_bid_id = '.$pid);

				if(!is_null($probid_completed))
				{
					
					foreach ($probid_completed as $row) {
							$product_value_bid = \DB::table('v_bid_tran_new')->select('product_value_bid','customerNumber')->where('products_bid_id', '=', $row->products_bid_id)->orderBy('id','desc')->first();

							if($row->botwin==0&&$row->complete_val<=$pbv){
								//$cwinner = \DB::table('v_bid_tran_new')->where('products_bid_id', '=',$row->products_bid_id )->value('customerNumber');

								//\DB::table('products_bid')->where('products_bid_id', '=', $row->products_bid_id)->update(array('completed' => '1', 'winner_id' => $cwinner));
								
											$all_users = \DB::table('bids_trans')->distinct()->where('products_bid_id', '=', $row->products_bid_id)->lists('customerNumber');

												foreach ($all_users as $user) {
												//SELECT DISTINCT customerNumber FROM `bids_trans` where products_bid_id = 16
												$sumbid = \DB::table('v_bid_tran')->where('customerNumber', '=', $user)->where('products_bid_id', '=', $row->products_bid_id)->sum('bid_product');

														if($cwinner==$user)
														{

															/*\DB::table('bid_to_spay')->insert([
															'customerNumber' => $user, 'products_bid_id' => $row->products_bid_id, 
															'bid_total' => $sumbid, 'spay_gain' => $sumbid*0.8, 'bid_gain' => $sumbid, 'get' => '3'
															]);*/

															\DB::table('bid_to_spay')->insert([
															'customerNumber' => $user, 'products_bid_id' => $row->products_bid_id, 
															'bid_total' => $sumbid, 'spay_gain' => $sumbid*0.8, 'bid_gain' => $sumbid, 'get' => '0'
															]);

														}else
														{
															\DB::table('bid_to_spay')->insert([
															'customerNumber' => $user, 'products_bid_id' => $row->products_bid_id, 
															'bid_total' => $sumbid, 'spay_gain' => $sumbid*0.8, 'bid_gain' => $sumbid, 'get' => '0'
															]);
														}
												}

													$setcomp = 1;
													$ch = 'bidmain';
													//broadcast
													$pusher = App::make('pusher');
													$pusher->trigger( $ch,'bidding_'.$row->products_bid_id, array('bidcompleted' => $setcomp, 'pbidid' => $row->products_bid_id));

							}elseif($row->botwin==1||$row->complete_val>$pbv)
							{
								$cwinner = \DB::table('v_bid_tran_new')->where('products_bid_id', '=',$row->products_bid_id )->value('customerNumber');
								$bot = User::find($cwinner);
								if(!is_null($bot)&&$bot->group_id==4)
								{

											\DB::table('products_bid')->where('products_bid_id', '=', $row->products_bid_id)->update(array('completed' => '1', 'winner_id' => $cwinner));
								
											$all_users = \DB::table('bids_trans')->distinct()->where('products_bid_id', '=', $row->products_bid_id)
												->lists('customerNumber');

												foreach ($all_users as $user) {
												//SELECT DISTINCT customerNumber FROM `bids_trans` where products_bid_id = 16
													$sumbid = \DB::table('v_bid_tran')->where('customerNumber', '=', $user)->where('products_bid_id', '=', $row->products_bid_id)->sum('bid_product');

														if($cwinner==$user)
														{
															/*\DB::table('bid_to_spay')->insert([
															'customerNumber' => $user, 'products_bid_id' => $row->products_bid_id, 
															'bid_total' => $sumbid, 'spay_gain' => $sumbid*0.8, 'bid_gain' => $sumbid, 'get' => '3'
															]);*/
														}else
														{
															\DB::table('bid_to_spay')->insert([
															'customerNumber' => $user, 'products_bid_id' => $row->products_bid_id, 
															'bid_total' => $sumbid, 'spay_gain' => $sumbid*0.8, 'bid_gain' => $sumbid, 'get' => '0'
															]);
														}
												}



													$setcomp = 1;
													$ch = 'bidmain';
													//broadcast
													$pusher = App::make('pusher');
													$pusher->trigger( $ch,'bidding_'.$row->products_bid_id, array('bidcompleted' => $setcomp, 'pbidid' => $row->products_bid_id));

										

								}else{
									$bot = \DB::table('tb_users')->where('group_id', '=', 4)->orderByRaw("RAND()")->first();

									$yourid = $bot->id;

									$bidid = $row->products_bid_id;
									$product_value_inc = 0.1;
									$setcomp = 0;
									$enddate = \DB::table('products_bid')->where('products_bid_id', '=',$bidid )->value('bid_end');	
									$maxval = $model->select('product_value','customerNumber')->where('products_bid_id', '=', $bidid)->orderBy('product_value','desc')->first();


									
									if(isset($maxval->customerNumber)&&$maxval->customerNumber==$yourid)
									{	

											$arr = array('mess' => 'success','clickdisable'=>'bid-'.$bidid);
											return json_encode($arr);
									}

									if(is_null($maxval))
									{	
										$maxval = 1; 
									}else
									{	
										$maxval = $maxval->product_value;
									}

									$newmaxval=$product_value_inc=$product_value_inc+$maxval;

									$dt = Carbon::now();
									
									$bidnewtime = $dt->addSeconds(15);
									\DB::table('products_bid')->where('products_bid_id', '=', $bidid)->update(array('bid_end' => $bidnewtime));

												//$last_bidtran = $this->model->insertRow($data, $request->input('id'));
												
												$inserted = \DB::statement("INSERT INTO bids_trans (products_bid_id, customerNumber, bid, product_value, bid_type)
																		SELECT * FROM (SELECT ".$bidid." as products_bid_id, ".$yourid." 
																		as customerNumber, ".$row->boots." as bid, ".$newmaxval." as product_value, 0 as bid_type) AS tmp
																		WHERE NOT EXISTS (
																		    SELECT b1.id from bids_trans b1 join 
																		    (
																		    	SELECT products_bid_id, customerNumber, product_value FROM bids_trans 
																		        WHERE id = (select MAX(id) from bids_trans 
																		        where products_bid_id = ".$bidid.")
																			) b2 on b1.products_bid_id = b2.products_bid_id 
																		    and b1.customerNumber = b2.customerNumber
																		WHERE b1.products_bid_id = ".$bidid." 
																		and b1.customerNumber = ".$yourid.")");


																		if($inserted==1)
																		{
																			
																			
																			//sleep(1);
																		}
								}
										

									
							}

					}

				}
						/*$cc_val = 0;
						$goloop = 0;

						
						if($row->complete_val>0){

							$getsumprofit = \DB::select('SELECT sum(bid) as totalbid FROM `bids_trans` WHERE products_bid_id = '.$row->products_bid_id.' and customerNumber NOT BETWEEN 16 AND 39');
							if(count($getsumprofit)>0)
							{
								if($getsumprofit->totalbid<$row->complete_val)
								{
									$goloop = 1;
								}
							}
						}

						if($row->botwin==1){ 
							
						$getcus = \DB::table('bids_trans')->select('customerNumber')->where('products_bid_id', '=', $row->products_bid_id)->orderBy('id','desc')->first();

							if(count($getcus)>0){
								if(($getcus->customerNumber < 16) || ($getcus->customerNumber> 39))
								{
									$goloop = 1;
								}
							}

						}*/
					//add bot  //if not set total default price every last 2-5 sec
						

				}

			}


		


			

	}	

	

	function getBidcom(Request $request, $id = null)
	{
		$bidstatus = \DB::select('SELECT  products_bid_id as pid, bid_end, product_value_bid as v, username as u, completed as bc  FROM `v_bid_relations` WHERE bid_end < now() and completed = "0" and active = "1" order by bid_end DESC');

		return response()->json([
			    'c' => $bidstatus
			]);
	}

	function getBidstatus(Request $request, $id = null)
	{
		$bidstatus = \DB::select('SELECT  products_bid_id as pid, bid_end as t, product_value_bid as v, username as u, customerNumber as ui FROM `v_bid_relations` WHERE bid_end > now() and completed = "0" and active = "1" order by bid_end DESC');
		
		return response()->json([
			    'b' => $bidstatus
			]);
	}

	function getTransactable(){

                  
                $bidtrans = BidtransController::display(); 
                $bidtransrow = $bidtrans['rowData'];  
                $html = '';
                  foreach ($bidtransrow as $biduser) { 
                  $html .= '<div class="row no-margin cauto">
                    <div class="col-md-4">
                      <p class="text-muted">'.$biduser->product_value_bid.'</p>
                    </div>
                    <div class="col-md-4">
                      <p class="text-muted">'.$biduser->username.'</p>
                    </div>
                    <div class="col-md-4">
                      <p class="text-muted">';
                    if($biduser->bid_type=="1"){
                  		$html .= 'auto'; 
               		}else{
                     	$html .= 'manual'; 
                    }
                    $html .='</p>
                    </div>
                  </div>';
                }

                return $html;
            
            //broadcast
			/*$pusher = App::make('pusher');

			if(isset($bidnewtime))
			{
				 $pusher->trigger( $ch,'bidding_'.$bidid, array('bidcompleted' => $setcomp, 'bidd' => $bidtransrow,
				 	'product_value_bid' => number_format($bidtransrow->product_value_bid,2),'biduser' => $bidtransrow->username, 'bidnewtime' => $bidnewtime, 'pbidid' => $bidid));

 			}else{
				 $pusher->trigger( $ch,'bidding_'.$bidid, array('bidcompleted' => $setcomp,'bidd' => $bidtransrow,'biduser' => $bidtransrow->username,'product_value_bid' => number_format($bidtransrow->product_value_bid,2),'pbidid' => $bidid));	
 			}*/
	}

	public static function frontcheckyouwin($pbidid=null)
	{
		//echo $pbidid;
		if($pbidid != null){

			$yourid = \Session::get('uid');

			//$customer_bid_end = \DB::select('SELECT winner_id FROM `customer_bid_end` WHERE completed = "1" and products_bid_id = '.$pbidid);

				$customer_bid_end = \DB::table('customer_bid_end')->where('products_bid_id', '=',$pbidid )->first();

				if(is_null($customer_bid_end))
				{
					
					return  'nowin';

				}elseif($customer_bid_end->winner_id==$yourid)
				{
					$getcheck = \DB::table('bid_to_spay')->where('customerNumber', '=',$yourid)->where('products_bid_id', '=', $pbidid)->value('get');

					if($getcheck=="4"||$getcheck==4)
					{
						return  'justget';
						
					}else
					{	
						$product_value = \DB::table('v_bid_relations')->where('products_bid_id', '=',$pbidid )->first();
						return $product_value;
					}

				}else
				{
					return  'lose';
				}

			}	
	}

	function postCheckyouwin( Request $request)
	{
		
		$rules = array();
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {	
		 					
		
		//echo $pbidid;
		if($request->pbidid != null){

			$yourid = \Session::get('uid');

			//$customer_bid_end = \DB::select('SELECT winner_id FROM `customer_bid_end` WHERE completed = "1" and products_bid_id = '.$pbidid);

				$customer_bid_end = \DB::table('customer_bid_end')->where('products_bid_id', '=',$request->pbidid )->where('customerNumber', '=',$yourid )->value('winner_id');

				if($customer_bid_end=="")
				{
					$arr = array('winner' => 'no');
					echo json_encode($arr);

				}elseif($customer_bid_end==$yourid)
				{
					$getcheck = \DB::table('bid_to_spay')->where('customerNumber', '=',$yourid)->where('products_bid_id', '=', $request->pbidid)->value('get');

					if($getcheck=="4"||$getcheck==4)
					{
						
						$arr = array('winner' => 'justget');
					}else
					{
						
						$arr = array('winner' => 'win', 'pbidid' => $request->pbidid);
					}

					
					echo json_encode($arr);
				}else
				{

					$arr = array('winner' => 'lose', 'pbidid' => $request->pbidid);
					echo json_encode($arr);

				}

			}	
		}
	}



	function postReturnbidandspay( Request $request)
	{
		
		$rules = array();
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {	
		 					

		if($request->pbidid != null){ //SELECT sum(bid_product) FROM `v_bid_tran` where customerNumber = 8 ORDER BY `id` DESC

				$yourid = \Session::get('uid');

				$checkbidtospay = \DB::table('bid_to_spay')->select('id','spay_gain','bid_gain','get')
				->where('customerNumber', '=', $yourid)
				->where('products_bid_id', '=', $request->pbidid)->first();

				if(isset($checkbidtospay)&&$checkbidtospay->get=="0")
				{

					if($request->selector=="bid")
					{
						\DB::table('bid_to_spay')->where('id', '=', $checkbidtospay->id)
							->update(array('get' => '2'));

						\DB::table('tb_users')->whereId($yourid)->increment('bid', $checkbidtospay->bid_gain);	


						$youruser = User::find($yourid);

	    				$arr = array('message' => 'success','current_bid' => $youruser->bid,'bidgain' => $checkbidtospay->bid_gain);

					}elseif($request->selector=="spay")
					{
						\DB::table('bid_to_spay')->where('id', '=', $checkbidtospay->id)
									->update(array('get' => '1'));

						\DB::table('tb_users')->whereId($yourid)->increment('spay', $checkbidtospay->spay_gain);

						$youruser = User::find($yourid);

	    				$arr = array('message' => 'success','current_bid' => $youruser->spay,'spaygain' => $checkbidtospay->spay_gain);
						
					}

					return json_encode($arr);

				}else{

					$arr = array('message' => 'fail');
					return json_encode($arr);

				}
			
			
			}	
		}
	}


	function postAutobidsave( Request $request)
	{
		
		$rules = array();
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {	
		 											
			if($request->products_bid_id != null){

				$yourid = \Session::get('uid');

						$youruser = User::find($yourid);

						$userbid = $youruser->bid;

						$boots = $request->boots;

						$addbid = $request->addbid;

						$addbid = $addbid*$boots;
						
						if(($userbid-$addbid) > 0)
						{
								
							$checkautobid = \DB::table('autobid')->select('id','bid_remain')->where('customerNumber', '=', $yourid)->where('products_bid_id', '=', $request->products_bid_id)->first();

												if(empty($checkautobid))
												{

													\DB::table('autobid')->insert([
														'customerNumber' => $yourid, 'products_bid_id' => $request->products_bid_id, 
														'bid_remain' => $addbid, 'boots' => $boots
														]);
													
													\DB::table('tb_users')->whereId($yourid)->decrement('bid', $addbid);

												}else
												{
													
													\DB::table('autobid')->whereId($checkautobid->id)
													->increment('bid_remain', $addbid);
													
													\DB::table('tb_users')->whereId($yourid)->decrement('bid', $addbid);
												}


							$youruser = User::find($yourid);

							$autobidlist = \DB::table('autobid')->where('customerNumber', '=', $yourid)->where('products_bid_id', '=', $request->products_bid_id)->get();

		    				$arr = array('message' => 'success','current_bid' => $youruser->bid, 'autobidlist' => $autobidlist[0]);

		    				return json_encode($arr);
							
						}else
						{
							$arr = array('message' => 'fail');
							return json_encode($arr);
						}



			}elseif($request->delid != null)
			{
				$yourid = \Session::get('uid');

						$idtodel = $request->delid;

							$checkautobid = \DB::table('autobid')->select('id','bid_remain')->where('id', '=', $idtodel)->where('customerNumber', '=', $yourid)->first();

						
												if(empty($checkautobid))
												{
													$arr = array('message' => 'fail');
													return json_encode($arr);

												}else
												{


													/*\DB::table('autobid')->whereId($checkautobid->id)
													->decrement('bid_remain', $checkautobid->bid_remain);*/

													\DB::table('autobid')->where('id', '=', $idtodel)->delete();
													\DB::table('tb_users')->whereId($yourid)->increment('bid', $checkautobid->bid_remain);

												}


							$youruser = User::find($yourid);

							$autobidlist = \DB::table('autobid')->where('customerNumber', '=', $yourid)->where('products_bid_id', '=', $request->products_bid_id)->get();

		    				$arr = array('message' => 'success','current_bid' => $youruser->bid);

		    				return json_encode($arr);
			}
		}
	}


//



	
}