<div class="rs_graybg rs_toppadder100 rs_bottompadder100">
		<div class="container">
			<div class="row">
				<div class="rs_faq_content">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="rs_faq_content_heading rs_green_heading rs_bottompadder30">
							<h3>คำถามที่พบบ่อย</h3>
							<div><span><i class="fa fa-heart"></i></span></div>
						</div>
<ul>
<li><a href="#" id="howtologin">ขั้นตอนการ login</a></li>
<li><a href="#" id="howtoregister">ขั้นตอนการสมัครสมาชิก</a></li>
<li><a href="#" id="howtobidpack">ขั้นตอนการซื้อ bid pack</a></li>
</ul>
						<p>ทั้งหมดนี้คือคำถามที่พบบ่อยภายในเว็บไซต์ Gogoldrun.com ที่ทางเว็บไซต์รวบรวมเอามาให้สมาชิกทุกท่านได้ไขข้อข้องใจ
						ด้วยการให้คำตอบที่ชัดเจน หรือถ้ามีคำถามที่ต้องการคำตอบที่มากกว่านี้ ก็สามารถที่จะเข้าไปทิ้งคำถามของคุณไว้ได้ที่หัวข้อ 
						Contact Us พร้อมกรอกข้อมูลให้ครบถ้วนและชัดเจน ทาง Gogoldrun.com ก็จะทำการตอบคำถามที่ต้องการกลับไปทางอีเมล์ของคุณอย่างรวดเร็วที่สุด</p>
						<div class="rs_faq_questions">
							<div id="accordion">
							  <h3>Gogoldrun.com?</h3><div><div>Gogoldrun.com? คือเว็บไซต์ที่เปิดโอกาสให้ผู้ที่ชื่นชอบการประมูลสินค้าออนไลน์ ได้เข้ามาทำการประมูลสิ่งของที่ต้องการอย่างโปร่งใส พร้อมรับความสนุกสนานที่สร้างความคุ้มค่าในการประมูลหรือที่เรียกว่า Bid ไปแต่ละครั้ง และยังเป็นเว็บไซต์ประมูลสินค้าประเภททองคำรูปพรรณเจ้าแรกของประเทศไทยอีกด้วย จึงถือว่าเป็นเว็บไซต์ประมูลที่น่าสนใจมากอีกเว็บไซต์หนึ่ง โดยการประมูลนี้มีการดำเนินงานผ่านบริษัท โกโกลด์รัน จำกัด ที่จัดทำการประมูลภายในเว็บไซต์ Gogoldrun.com และสามารถรับสินค้าได้ที่ร้านทองใกล้บ้านท่านอีกด้วย จึงมั่นใจได้ว่า ท่านจะได้รับสินค้าจริง และได้รับทองรูปพรรณที่ได้มาตรฐาน จาก สคบ. อีกด้วย</div></div><h3>เงื่อนไขในการสมัครสมาชิก?</h3><div><div>- ต้องมีอายุ 18 ปีขึ้นไป </div><div>- สมาชิก 1 คน สามารถที่จะมี 1 Account เท่านั้น </div><div>- ชื่อสมาชิกจะต้องไม่มีคำที่สื่อความหมายถึงทีมงาน Gogoldrun เช่น webmaster, admin </div><div>- ผู้ที่สมัครสมาชิกต้องใช้ Username เป็นภาษาอังกฤษเท่านั้น </div><div>- ผู้ที่สมัครสมาชิกต้องกรอกข้อมูลที่เป็นจริงให้ครบโดยเฉพาะอีเมล์ (ใช้ยืนยันในการสมัครสมาชิก) และเพื่อสิทธิประโยชน์ของท่านในการเข้าร่วมกิจกรรมต่างๆในอนาคต </div><div>- เข้าประมูลด้วยความสุภาพเท่านั้น คำพูดที่ก้าวล้ำล่วงเกินไปยังบุคคลอื่น หรือการพูดไปในทางที่ผิดต่อกฎหมาย ทางเว็บไซต์ Gogoldrun จะขอทำการระงับไอดีของผู้เข้าประมูลผู้นั้นโดยทันที เมื่อเข้าใจและยอมรับต่อข้อกำหนดต่าง ๆ ของทางเว็บไซต์ก็สามารถที่จะเข้ามาทำการประมูลหลังการยืนยันตนเสร็จเรียบร้อยได้เลยทันที</div></div><h3>กติกาในการประมูลมีความยุติธรรมมากน้อยขนาดไหน?</h3><div>กติกาของทาง gogoldrun มีความเป็นกลางและมีความยุติธรรมมากที่สุด ซึ่งทางเว็บไซต์มีข้อกำหนดในการห้ามไม่ให้พนักงานและครอบครัวเข้ามาร่วมประมูลภายในเว็บไซต์โดยเด็ดขาด พร้อมด้วยการตรวจสอบวิธีการโกงในรูปแบบต่าง ๆ ของผู้ที่เข้ามาประมูล ซึ่งถ้ามีการกระทำความผิดในแง่สมรู้ร่วมคิดแล้วทาง gogoldrun ตรวจเจอก็จะทำการตัดสิทธิ์ของผู้ที่กำลังประมูลอยู่ทันที โดยจะทำการเตือนผ่านอีเมล์ก่อน 1 ครั้ง แต่ถ้ายังมีการกระทำความผิดซ้ำก็จะถูกระงับสิทธิ์การใช้บริการโดยทันที</div>
							  <h3>รูปแบบค่าใช้จ่ายในการประมูลเป็นอย่างไร?</h3><div>การประมูลของ gogoldrun จะเริ่มต้นเพียง 1 THBในทุกการประมูล ซึ่งผู้ที่เข้ามาร่วมประมูลจะมีการเคาะที่ 1 Bid ต่อ 10 THBเท่านั้น ซึ่งถ้าต้องการที่จะร่วมประมูลก็สามารถที่จะซื้อเป็น Bid Pack ที่มีราคาเริ่มต้นที่ 250 THB ที่มีให้ 25 Bid โดยการซื้อคือการโอนเงินผ่านทางธนาคาร Paysbay ที่ให้ความสะดวกรวดเร็วในการซื้อเป็นอย่างมาก</div>
							   <h3>เมื่อชนะการประมูลจะลูกค้าสามารถรับสินค้าได้อย่างไร?</h3><div><div> แบ่งออกเป็น 2 วิธี ซึ่งลูกค้าสามารถเลือกอย่างใดอย่างหนึ่งได้</div><div>1. บริษัท โกโกลด์รัน จะจัดส่งทาง EMS และแนบใบรับประกันจากร้านทอง ที่ได้รับการรับรองจาก ส.ค.บ. หรือหน่วยงานคุ้มครองสิทธิผู้บริโภค เพื่อเป็นการการัณตีคุณภาพทองรูปพรรณ</div><div>2. ลูกค้าสามารถนำใบรับทองที่บริษัท โกโกลด์รัน จำกัด ออกให้ไปรับทองได้ที่ ร้านทองภายในเครือข่ายของบริษัทโกโกลด์รัน ซึ่งกระจ่ายตามเขตต่างๆ ภายในกรุงเทพ และปริมณฑล </div><div>หมายเหตุ ทุกร้านที่อยู่ภายในเครือข่ายของบริษัทโกโกลด์รัน จำกัด ได้ผ่านการตรวจสอบคุณภาพทองคำ จากหน่วยงาน สคบ. ทุกร้าน การันตีโดยบริษัทโกโกลด์รัน จำกัด</div></div><h3>วิธีการประมูลยากหรือไม่?</h3><div><div>วิธีการประมูลของ gogoldrun มีรูปแบบที่ชัดเจนและง่ายดาย ผู้ที่มี User สามารถที่จะเข้ามาทำการประมูลได้ตลอดเวลาจนกว่าจะจบการประมูล เมื่อคลิ๊ก Bid 1 ครั้ง ราคาจะเพิ่มขึ้นที่ 0.10 THB โดยที่ตัวเวลาจะกลับไปที่ 15 วินาที ซึ่งถ้ามีผู้ประมูลแข่งขันทำการเคาะ Bid อีกครั้ง เวลาก็จะกลับไปสู่ 15 วินาทีเท่าเดิม รวมไปถึงมีระบบ Autobid ที่ตนเองจะสามารถกำหนดใส่จำนวนBID และให้ระบบบิดอัตโนมัติ ซึ่งช่วยให้สะดวกมากยิ่งขึ้น และถ้าใครสามารถที่จะเคาะเป็นคนสุดท้ายได้ก่อนจะได้รับสินค้าที่ตนเองต้องการกลับบ้านไป ถือว่าเป็นวิธีที่ง่ายและมีความยุติธรรมสูงอีกด้วย</div></div><div>
							  </div>
							</div>
				</div>
			</div>
		</div>
	</div>
	</div></div>

							      

							      

							      