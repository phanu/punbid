

		 {!! Form::open(array('url'=>'products/savepublic', 'class'=>'form-vertical','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-6">
						<fieldset><legend> รายละเอียดสินค้า</legend>
									
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> รหัสสินค้า  <span class="asterix"> * </span>  </label>									
										  {!! Form::text('products_code', $row['products_code'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> ชื่อสินค้า  <span class="asterix"> * </span>  </label>									
										  {!! Form::text('name_th', $row['name_th'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> ชื่อสินค้า En    </label>									
										  {!! Form::text('name_en', $row['name_en'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> รูปสินค้า    </label>									
										  <input  type='file' name='featured_img' id='featured_img' @if($row['featured_img'] =='') class='required' @endif style='width:150px !important;'  />
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['featured_img'],'/uploads/products/') !!}
						
						</div>					
					 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> รูปอัลบั้มสินค้า    </label>									
										  
					<a href="javascript:void(0)" class="btn btn-xs btn-primary pull-right" onclick="addMoreFiles('gallery')"><i class="fa fa-plus"></i></a>
					<div class="galleryUpl">	
					 	<input  type='file' name='gallery[]'  />			
					</div>
					<ul class="uploadedLists " >
					<?php $cr= 0; 
					$row['gallery'] = explode(",",$row['gallery']);
					?>
					@foreach($row['gallery'] as $files)
						@if(file_exists('./uploads/products/'.$files) && $files !='')
						<li id="cr-<?php echo $cr;?>" class="">							
							<a href="{{ url('/uploads/products//'.$files) }}" target="_blank" >{{ $files }}</a> 
							<span class="pull-right removeMultiFiles" rel="cr-<?php echo $cr;?>" url="/uploads/products/{{$files}}">
							<i class="fa fa-trash-o  btn btn-xs btn-danger"></i></span>
							<input type="hidden" name="currgallery[]" value="{{ $files }}"/>
							<?php ++$cr;?>
						</li>
						@endif
					
					@endforeach
					</ul>
					 						
									  </div> {!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> ราคา  <span class="asterix"> * </span>  </label>									
										  {!! Form::text('price', $row['price'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> ราคาโปรโมชั่น    </label>									
										  {!! Form::text('sale_price', $row['sale_price'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> หมวดหมู่  <span class="asterix"> * </span>  </label>									
										  <select name='category' rows='5' id='category' class='select2 ' required  ></select> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> แบรนด์  <span class="asterix"> * </span>  </label>									
										  <select name='brand' rows='5' id='brand' class='select2 ' required  ></select> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> ตั้งเป็นสินค้าแนะนำ    </label>									
										  <?php $featured = explode(",",$row['featured']); ?>
					 <label class='checked checkbox-inline'>   
					<input type='checkbox' name='featured[]' value ='1'   class='' 
					@if(in_array('1',$featured))checked @endif 
					 />  </label>  						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> เปิดใช้งาน    </label>									
										  <?php $active = explode(",",$row['active']); ?>
					 <label class='checked checkbox-inline'>   
					<input type='checkbox' name='active[]' value ='1'   class='' 
					@if(in_array('1',$active))checked @endif 
					 />  </label>  						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> ค่าขนส่ง  <span class="asterix"> * </span>  </label>									
										  {!! Form::text('shiping_cost', $row['shiping_cost'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!} 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> Tags    </label>									
										  {!! Form::text('tag', $row['tag'],array('class'=>'form-control', 'placeholder'=>'',   )) !!} 						
									  </div> </fieldset>
			</div>
			
			<div class="col-md-6">
						<fieldset><legend> ข้อมูลสินค้า</legend>
									
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> คำอธิบายสินค้า  <span class="asterix"> * </span>  </label>									
										  <textarea name='title_th' rows='5' id='title_th' class='form-control '  
				         required  >{{ $row['title_th'] }}</textarea> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> รายละเอียดสินค้า  <span class="asterix"> * </span>  </label>									
										  <textarea name='descript_th' rows='5' id='editor' class='form-control editor '  
						required >{{ $row['descript_th'] }}</textarea> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> คำอธิบายสินค้า En    </label>									
										  <textarea name='title_en' rows='5' id='title_en' class='form-control '  
				           >{{ $row['title_en'] }}</textarea> 						
									  </div> 					
									  <div class="form-group  " >
										<label for="ipt" class=" control-label "> รายละเอียดสินค้า En    </label>									
										  <textarea name='descript_en' rows='5' id='editor' class='form-control editor '  
						 >{{ $row['descript_en'] }}</textarea> 						
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		$("#category").jCombo("{!! url('products/comboselect?filter=product_categories:id:name_th') !!}",
		{  selected_value : '{{ $row["category"] }}' });
		
		$("#brand").jCombo("{!! url('products/comboselect?filter=brand:id:name_th') !!}",
		{  selected_value : '{{ $row["brand"] }}' });
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
