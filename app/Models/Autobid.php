<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class autobid extends Sximo  {
	
	protected $table = 'autobid';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT autobid.* FROM autobid  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE autobid.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
