<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use Socialize;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use Auth;
use Carbon\Carbon;

class UserController extends Controller {
	
	protected $layout = "layouts.main";

	public function __construct() {
		parent::__construct();
	
		$this->data = array(
			'pageTitle'			=>  'Sibeza',
			'pageNote'			=>  '',
			'pageMetakey'		=> '',	
			'pageMetadesc'		=> '',
			'shop'				=> 0,
			'slide'				=> 0,
			'pages'				=> '',
			'return' 			=> 	self::returnUrl()	
		);	
	} 

	public function getRegister() {
        
		if(CNF_REGIST =='false') :    
			if(\Auth::check()):
				 return Redirect::to('')->with('message',\SiteHelpers::alert('success','Youre already login'));
			else:
				 return Redirect::to('user/login');
			  endif;
			  
		else :
				
				return view('user.register',$this->data);  
		 endif ; 
           
	

	}

	public function postCreate( Request $request) {
	
		$rules = array(
			'username'=>'required|alpha_num|unique:tb_users',
			'firstname'=>'required|alpha_num|min:2',
			'lastname'=>'required|alpha_num|min:2',
			'email'=>'required|email|unique:tb_users',
			'password'=>'required|between:6,12|confirmed',
			'password_confirmation'=>'required|between:6,12',
			'phone'=>'required|alpha_num|min:9',
			);	
		if(CNF_RECAPTCHA =='true') $rules['recaptcha_response_field'] = 'required|recaptcha';
				
		$validator = Validator::make($request->all(), $rules);

		if ($validator->passes()) {
			$code = rand(10000,10000000);
			
			$authen = new User;
			$authen->username = $request->input('username');
			$authen->first_name = $request->input('firstname');
			$authen->last_name = $request->input('lastname');
			$authen->email = trim($request->input('email'));
			$authen->activation = $code;
			$authen->group_id = 3;
			$authen->password = \Hash::make($request->input('password'));
			if(CNF_ACTIVATION == 'auto') { $authen->active = '1'; } else { $authen->active = '0'; }
			$authen->save();
			
			$data = array(
				'username'	=> $request->input('username') ,
				'firstname'	=> $request->input('firstname') ,
				'lastname'	=> $request->input('lastname') ,
				'email'		=> $request->input('email') ,
				'password'	=> $request->input('password'),
				'friend' => $request->input('friend'),
				'phone'	=> $request->input('phone') ,
				'birth_date' => $request->input('year').'-'.$request->input('month').'-'.$request->input('day') ,
				'sex'	=> $request->input('sex'),
				'code'		=> $code
				
			);

			if(CNF_ACTIVATION == 'confirmation')
			{ 

				$to = $request->input('email');
				$subject = "[ " .CNF_APPNAME." ] REGISTRATION "; 

			
				if(defined('CNF_MAIL') && CNF_MAIL =='swift')
				{ 
					Mail::send('user.emails.registration', $data, function ($message) {
			    		$message->to($to)->subject($subject);
			    	});	

				}  else {
		
					$message = view('user.emails.registration', $data);
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: '.CNF_APPNAME.' <'.CNF_EMAIL.'>' . "\r\n";
						mail($to, $subject, $message, $headers);	
				}

				$message = "Thanks for registering! . Please check your inbox and follow activation link";
								
			} elseif(CNF_ACTIVATION=='manual') {
				$message = "Thanks for registering! . We will validate you account before your account active";
			} else {
   			 	$message = "Thanks for registering! . Your account is active now ";         
			
			}	


			return Redirect::to('user/login')->with('message',\SiteHelpers::alert('success',$message));
		} else {
			return Redirect::to('user/register')->with('message',\SiteHelpers::alert('error','The following errors occurred')
			)->withErrors($validator)->withInput();
		}
	}

	public function postCreatesocial($usersocial) {

	

		//if(CNF_RECAPTCHA =='true') $rules['recaptcha_response_field'] = 'required|recaptcha';
		if(!is_null($usersocial)){




			$authen = new User;
			if(!is_null($usersocial->name))
			{
				$authen->username = str_replace(' ', '', $usersocial->name);
			}
			if(!is_null($usersocial->email))
			{
				$authen->email = trim($usersocial->email);
			}else
			{
				return Redirect::to('user/register')->with('message',\SiteHelpers::alert('error','The following errors occurred')
				)->withErrors($validator)->withInput();
			}
			if(!is_null($usersocial->avatar))
			{
				$authen->avatar = $usersocial->avatar;
			}
			
			$authen->group_id = 3;
			$authen->active = '1';
			//$authen->password = \Hash::make($request->input('password'));
			//if(CNF_ACTIVATION == 'auto') { $authen->active = '1'; } else { $authen->active = '0'; }
			$authen->save();
			
			
			/*if(CNF_ACTIVATION == 'confirmation')
			{ 

				$to = $request->input('email');
				$subject = "[ " .CNF_APPNAME." ] REGISTRATION "; 

			
				if(defined('CNF_MAIL') && CNF_MAIL =='swift')
				{ 
					Mail::send('user.emails.registration', $data, function ($message) {
			    		$message->to($to)->subject($subject);
			    	});	

				}  else {
		
					$message = view('user.emails.registration', $data);
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: '.CNF_APPNAME.' <'.CNF_EMAIL.'>' . "\r\n";
						mail($to, $subject, $message, $headers);	
				}

				$message = "Thanks for registering! . Please check your inbox and follow activation link";
								
			} elseif(CNF_ACTIVATION=='manual') {
				$message = "Thanks for registering! . We will validate you account before your account active";
			} else {*/
   			 	$message = "Thanks for registering! . Your account is active now ";         
				return Redirect::to('user/login')->with('message',\SiteHelpers::alert('success',$message));
			}else{
				return Redirect::to('user/register')->with('message',\SiteHelpers::alert('error','The following errors occurred')
				)->withErrors($validator)->withInput();
			}
	}
	
	public function getActivation( Request $request  )
	{
		$num = $request->input('code');
		if($num =='')
			return Redirect::to('user/login')->with('message',\SiteHelpers::alert('error','Invalid Code Activation!'));
		
		$user =  User::where('activation','=',$num)->first();
		if (count($user) >=1)
		{
			\DB::table('tb_users')->where('activation', $num )->update(array('active' => 1,'activation'=>''));

			if(!is_sull($user->friend))
			{
				$friend = \DB::table('tb_users')->where('username', $user->friend)->value('id');

				\DB::table('tb_users')->where('id', $friend)->increment('bid', 5);

				\DB::table('tb_notification')->insert([
											'userid' => $friend, 'title' => 'คุณได้รับ 5 Bid จากการแนะนำเพื่อน', 'noticlass' => 'icon bg-success', 'icon' => 'zmdi zmdi-sign-in',
											]);
			}

			return Redirect::to('user/login')->with('message',\SiteHelpers::alert('success','Your account is active now!'));
			
		} else {
			return Redirect::to('user/login')->with('message',\SiteHelpers::alert('error','Invalid Code Activation!'));
		}
		
		
	
	}

	public function getLogin() {
	
		if(\Auth::check())
		{
			return Redirect::to('')->with('message',\SiteHelpers::alert('success','Youre already login'));

			\DB::table('tb_notification')->insert([
											'userid' => \Session::get('uid'), 'title' => \Lang::get('core.logging'), 'noticlass' => 'icon bg-success', 'icon' => 'zmdi zmdi-sign-in',
											]);

		} else {
			$this->data['socialize'] =  config('services');
			return View('user.login',$this->data);
			
		}	
	}

	public function postSignin( Request $request) {
		
		$rules = array(
			'username'=>'required',
			'password'=>'required',
		);		
		if(CNF_RECAPTCHA =='true') $rules['captcha'] = 'required|captcha';
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes()) {	

			$remember = (!is_null($request->get('remember')) ? 'true' : 'false' );
			
			if (\Auth::attempt(array('username'=>$request->input('username'), 'password'=> $request->input('password') ), $remember )||\Auth::attempt(array('email'=>$request->input('username'), 'password'=> $request->input('password') ), $remember )) {
				if(\Auth::check())
				{
					$row = User::find(\Auth::user()->id); 
	
					if($row->active =='0')
					{
						// inactive 
						if($request->ajax() == true )
						{
							return response()->json(['status' => 'error', 'message' => 'Your Account is not active']);
						} else {
							\Auth::logout();
							return Redirect::to('user/login')->with('message', \SiteHelpers::alert('error','Your Account is not active'));
						}
						
					} else if($row->active=='2')
					{

						if($request->ajax() == true )
						{
							return response()->json(['status' => 'error', 'message' => 'Your Account is BLocked']);
						} else {
							// BLocked users
							\Auth::logout();
							return Redirect::to('user/login')->with('message', \SiteHelpers::alert('error','Your Account is BLocked'));
						}
					} else {
						\DB::table('tb_users')->where('id', '=',$row->id )->update(array('last_login' => date("Y-m-d H:i:s")));
						\Session::put('uid', $row->id);
						\Session::put('gid', $row->group_id);
						\Session::put('eid', $row->email);
						\Session::put('userid', $row->username);
						\Session::put('ll', $row->last_login);
						\Session::put('fid', $row->first_name.' '. $row->last_name);
						\Session::put('uname', $row->first_name);
						\Session::put('ulastname', $row->last_name);
						if(\Session::get('lang') =='')
						{
							\Session::put('lang', CNF_LANG);	
						}

						\DB::table('tb_notification')->insert([
											'userid' => \Session::get('uid'), 'title' => \Lang::get('core.logging'), 'noticlass' => 'icon bg-success', 'icon' => 'zmdi zmdi-sign-in',
											]);

						if($request->ajax() == true )
						{
							if(CNF_FRONT =='false') :
								return response()->json(['status' => 'success', 'url' => url('dashboard')]);					
							else :
								return response()->json(['status' => 'success', 'url' => url('')]);
							endif;	

						} else {

							if(CNF_FRONT =='false') :
								return Redirect::to('dashboard');						
							else :
								return Redirect::to('');
							endif;	

						}
											
					}			
					
				}			
				
			} else {

				if($request->ajax() == true )
				{
					return response()->json(['status' => 'error', 'message' => 'Your username/password combination was incorrect']);
				} else {

					return Redirect::to('user/login')
						->with('message', \SiteHelpers::alert('error','Your username/password combination was incorrect'))
						->withInput();					
				}


			}
		} else {

				if($request->ajax() == true)
				{
					return response()->json(['status' => 'error', 'message' => 'The following  errors occurred']);
				} else {

					return Redirect::to('user/login')
						->with('message', \SiteHelpers::alert('error','The following  errors occurred'))
						->withErrors($validator)->withInput();
				}	
		

		}	
	}

	public function getProfile() {
		
		if(!\Auth::check()) return redirect('user/login');
		
		
		$info =	User::find(\Auth::user()->id);
		$this->data = array(
			'pageTitle'	=> 'My Profile',
			'pageNote'	=> 'View Detail My Info',
			'info'		=> $info,
		);
		return view('user.profile',$this->data);
	}
	
	public function postSaveprofile( Request $request)
	{
		if(!\Auth::check()) return Redirect::to('user/login');
		$rules = array(
			'first_name'=>'required|alpha_num|min:2',
			'last_name'=>'required|alpha_num|min:2',
			);	
			
		if($request->input('email') != \Session::get('eid'))
		{
			$rules['email'] = 'required|email|unique:tb_users';
		}	

		if(!is_null(Input::file('avatar'))) $rules['avatar'] = 'mimes:jpg,jpeg,png,gif,bmp';

				
		$validator = Validator::make($request->all(), $rules);

		if ($validator->passes()) {
			
			if(!is_null(Input::file('avatar')))
			{
				$file = $request->file('avatar'); 
				$destinationPath = './uploads/users/';
				$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension(); //if you need extension of the file
				$rand = rand(1000,100000000);
				$newfilename = strtotime(date('Y-m-d H:i:s')).'-'.$rand.'x'.\Session::get('uid').'.'.$extension;
				$uploadSuccess = $request->file('avatar')->move($destinationPath, $newfilename);				 
				if( $uploadSuccess ) {
				    $data['avatar'] = $newfilename; 
				} 
			}		
			
			$user = User::find(\Session::get('uid'));
			$user->first_name 	= $request->input('first_name');
			$user->last_name 	= $request->input('last_name');
			$user->email 		= $request->input('email');
			if(isset( $data['avatar']))  $user->avatar  = $newfilename; 			
			$user->save();

			$newUser = User::find(\Session::get('uid'));

			\Session::put('fid',$newUser->first_name.' '.$newUser->last_name);

			\DB::table('tb_notification')->insert([
											'userid' => \Session::get('uid'), 'title' => \Lang::get('core.userprofileupdate'), 'noticlass' => 'icon bg-warning', 'icon' => 'zmdi zmdi-edit'
											]);
			return Redirect::to('user/profile')->with('messagetext','Profile has been saved!')->with('msgstatus','success');
		} else {
			return Redirect::to('user/profile')->with('messagetext','The following errors occurred')->with('msgstatus','error')
			->withErrors($validator)->withInput();
		}	
	
	}
	
	public function postSaveprofilefront( Request $request)
	{
		if(!\Auth::check()) return Redirect::to('user/login');
		$rules = array(
			'first_name'=>'required|alpha_num|min:2',
			'last_name'=>'required|alpha_num|min:2',
			);	
			
		if($request->input('email') != \Session::get('eid'))
		{
			$rules['email'] = 'required|email|unique:tb_users';
		}	

		if(!is_null(Input::file('avatar'))) $rules['avatar'] = 'mimes:jpg,jpeg,png,gif,bmp';

				
		$validator = Validator::make($request->all(), $rules);

		if ($validator->passes()) {
			
			if(!is_null(Input::file('avatar')))
			{
				$file = $request->file('avatar'); 
				$destinationPath = './uploads/users/';
				$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension(); //if you need extension of the file
				$rand = rand(1000,100000000);
				$newfilename = strtotime(date('Y-m-d H:i:s')).'-'.$rand.'x'.\Session::get('uid').'.'.$extension;
				$uploadSuccess = $request->file('avatar')->move($destinationPath, $newfilename);				 
				if( $uploadSuccess ) {
				    $data['avatar'] = $newfilename; 
				} 
				
			}		
			


			$user = User::find(\Session::get('uid'));
			$user->first_name 	= $request->input('first_name');
			$user->last_name 	= $request->input('last_name');
			$user->email 		= $request->input('email');
			$user->birth_date	= Carbon::createFromFormat('d/m/Y', $request->input('birth_date'));	
			$user->sex	= $request->input('sex');
			$user->phone	= $request->input('phone');
			$user->address	= $request->input('address');
			$user->province	= $request->input('province');
			$user->district	= $request->input('district');
			$user->subdistrict	= $request->input('subdistrict');
			$user->postcode = $request->input('postcode');
			if(isset( $data['avatar']))  $user->avatar  = $newfilename; 			
			$user->save();

			$newUser = User::find(\Session::get('uid'));

			\Session::put('fid',$newUser->first_name.' '.$newUser->last_name);

			\DB::table('tb_notification')->insert([
											'userid' => \Session::get('uid'), 'title' => \Lang::get('core.userprofileupdate'), 'noticlass' => 'icon bg-warning', 'icon' => 'zmdi zmdi-edit'
											]);
		
			return Redirect::back()->with('message', \SiteHelpers::alert('success','Profile has been saved!'));

		} else {
			return Redirect::back()->with('message','The following errors occurred')->with('message', \SiteHelpers::alert('error','The following errors occurred')
			)->withErrors($validator)->withInput();
		}	
	
	}

	public function postSaveprofilepic( Request $request)
	{
		if(!\Auth::check()) return Redirect::to('user/login');

		if(!is_null(Input::file('avatar'))) $rules['avatar'] = 'mimes:jpg,jpeg,png,gif,bmp';
				
		$validator = Validator::make($request->all(), $rules);

		if ($validator->passes()) {
			
			if(!is_null(Input::file('avatar')))
			{
				$file = $request->file('avatar'); 
				$destinationPath = './uploads/users/';
				$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension(); //if you need extension of the file
				$rand = rand(1000,100000000);
				$newfilename = strtotime(date('Y-m-d H:i:s')).'-'.$rand.'x'.\Session::get('uid').'.'.$extension;
				$uploadSuccess = $request->file('avatar')->move($destinationPath, $newfilename);				 
				if( $uploadSuccess ) {
				    $data['avatar'] = $newfilename; 
				} 
				
			}		

			$user = User::find(\Session::get('uid'));
			if(isset( $data['avatar']))  $user->avatar  = $newfilename; 			
			$user->save();

			

			\DB::table('tb_notification')->insert([
											'userid' => \Session::get('uid'), 'title' => \Lang::get('core.userprofileupdate'), 'noticlass' => 'icon bg-warning', 'icon' => 'zmdi zmdi-edit'
											]);
		
			return Redirect::back()->with('message', \SiteHelpers::alert('success','Profile has been saved!'));

		} else {
			return Redirect::back()->with('message','The following errors occurred')->with('message', \SiteHelpers::alert('error','The following errors occurred')
			)->withErrors($validator)->withInput();
		}	
	
	}

	public function postSaveprofileverify( Request $request)
	{
		if(!\Auth::check()) return Redirect::to('user/login');
		if(!is_null(Input::file('pic_personid'))) $rules['pic_personid'] = 'mimes:jpg,jpeg,png,gif,bmp';
		if(!is_null(Input::file('pic_bookbank'))) $rules['pic_bookbank'] = 'mimes:jpg,jpeg,png,gif,bmp';

		$validator = Validator::make($request->all(), $rules);

		if ($validator->passes()) {
			
			if(!is_null(Input::file('pic_personid')))
			{
				$file = $request->file('pic_personid'); 
				$destinationPath = './uploads/users/';
				$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension(); //if you need extension of the file
				$rand = rand(1000,100000000);
				$newfilename = strtotime(date('Y-m-d H:i:s')).'-'.$rand.'x'.\Session::get('uid').'personalid.'.$extension;
				$uploadSuccess = $request->file('pic_personid')->move($destinationPath, $newfilename);				 
				if( $uploadSuccess ) {
				    $data['pic_personid'] = $newfilename; 
				} 
				
			}		

			if(!is_null(Input::file('pic_bookbank')))
			{
				$file = $request->file('pic_bookbank'); 
				$destinationPath = './uploads/users/';
				$filename = $file->getClientOriginalName();
				$extension = $file->getClientOriginalExtension(); //if you need extension of the file
				$rand = rand(1000,100000000);
				$newfilename2 = strtotime(date('Y-m-d H:i:s')).'-'.$rand.'x'.\Session::get('uid').'book.'.$extension;
				$uploadSuccess = $request->file('pic_bookbank')->move($destinationPath, $newfilename2);				 
				if( $uploadSuccess ) {
				    $data['pic_bookbank'] = $newfilename2; 
				} 
				
			}		
			
			$user = User::find(\Session::get('uid'));
			if(isset( $data['pic_personid']))  $user->pic_personid  = $newfilename; 		
			if(isset( $data['pic_bookbank']))  $user->pic_bookbank  = $newfilename2; 	
			$user->verified = "inform";
			$user->save();

			$newUser = User::find(\Session::get('uid'));
			\Session::put('fid',$newUser->first_name.' '.$newUser->last_name);
		
			return Redirect::back()->with('message', \SiteHelpers::alert('success','send verification to admin already!'));

		} else {
			return Redirect::back()->with('message','The following errors occurred')->with('message', \SiteHelpers::alert('error','The following errors occurred')
			)->withErrors($validator)->withInput();
		}	
	
	}

	public function postSavepasswordfront( Request $request)
	{
		$rules = array(
			'password'=>'required|between:6,12|confirmed',
			'password_confirmation'=>'required|between:6,12'
			);		
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			$user = User::find(\Session::get('uid'));
			$user->password = \Hash::make($request->input('password'));
			$user->save();

			\DB::table('tb_notification')->insert([
											'userid' => \Session::get('uid'), 'title' => \Lang::get('core.userprofileupdate'), 'noticlass' => 'icon bg-warning', 'icon' => 'zmdi zmdi-edit'
											]);

			return Redirect::back()->with('message', \SiteHelpers::alert('success','Password has been saved!'));	
		} else {
			return Redirect::back()->with('message','The following errors occurred')->with('message', \SiteHelpers::alert('error','The following errors occurred')
			)->withErrors($validator)->withInput();
		}	
	
	}

	public function postSavepassword( Request $request)
	{
		$rules = array(
			'password'=>'required|between:6,12|confirmed',
			'password_confirmation'=>'required|between:6,12'
			);		
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			$user = User::find(\Session::get('uid'));
			$user->password = \Hash::make($request->input('password'));
			$user->save();

			\DB::table('tb_notification')->insert([
											'userid' => \Session::get('uid'), 'title' => \Lang::get('core.userprofileupdate'), 'noticlass' => 'icon bg-warning', 'icon' => 'zmdi zmdi-edit'
											]);
			return Redirect::to('user/profile')->with('success', \SiteHelpers::alert('success','Password has been saved!'));
		} else {
			return Redirect::to('user/profile')->with('error', \SiteHelpers::alert('error','The following errors occurred')
			)->withErrors($validator)->withInput();
		}	
	
	}		
	
	public function getReminder()
	{
	
		return view('user.remind');
	}	

	public function postRequest( Request $request)
	{

		$rules = array(
			'credit_email'=>'required|email'
		);	
		
		$validator = Validator::make(Input::all(), $rules);
		if ($validator->passes()) {	
	
			$user =  User::where('email','=',$request->input('credit_email'));
			if($user->count() >=1)
			{
				$user = $user->get();
				$user = $user[0];
				$data = array('token'=>$request->input('_token'));	
				$to = $request->input('credit_email');
				$subject = "[ " .CNF_APPNAME." ] REQUEST PASSWORD RESET "; 	

				if(defined('CNF_MAIL') && CNF_MAIL =='swift')
				{ 
					Mail::send('user.emails.auth.reminder', $data, function ($message) {
			    		$message->to($to)->subject($subject);
			    	});	

				}  else {

							
					$message = view('user.emails.auth.reminder', $data);
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: '.CNF_APPNAME.' <'.CNF_EMAIL.'>' . "\r\n";
						mail($to, $subject, $message, $headers);	
				}					
			
				
				$affectedRows = User::where('email', '=',$user->email)
								->update(array('reminder' => $request->input('_token')));
								
				return Redirect::to('user/login')->with('message', \SiteHelpers::alert('success','Please check your email'));	
				
			} else {
				return Redirect::to('user/login?reset&forgot=pass')->with('message', \SiteHelpers::alert('error','Cant find email address'));
			}

		}  else {
			return Redirect::to('user/login?reset&forgot=pass')->with('message', \SiteHelpers::alert('error','The following errors occurred')
			)->withErrors($validator)->withInput();
		}	 
	}	
	
	public function getReset( $token = '')
	{
		if(\Auth::check()) return Redirect::to('dashboard');

		$user = User::where('reminder','=',$token);
		if($user->count() >=1)
		{
			$this->data['verCode']= $token;
			return view('user.remind',$this->data);

		} else {
			return Redirect::to('user/login')->with('message', \SiteHelpers::alert('error','Cant find your reset code'));
		}
		
	}	
	
	public function postDoreset( Request $request , $token = '')
	{
		$rules = array(
			'password'=>'required|alpha_num|between:6,12|confirmed',
			'password_confirmation'=>'required|alpha_num|between:6,12'
			);		
		$validator = Validator::make($request->all(), $rules);
		if ($validator->passes()) {
			
			$user =  User::where('reminder','=',$token);
			if($user->count() >=1)
			{
				$data = $user->get();
				$user = User::find($data[0]->id);
				$user->reminder = '';
				$user->password = \Hash::make($request->input('password'));
				$user->save();			
			}
			\DB::table('tb_notification')->insert([
											'userid' => \Session::get('uid'), 'title' => \Lang::get('core.userprofileupdate'), 'noticlass' => 'icon bg-warning', 'icon' => 'zmdi zmdi-edit'
											]);
			return Redirect::to('user/login')->with('message',\SiteHelpers::alert('success','Password has been saved!'));
		} else {
			return Redirect::to('user/reset/'.$token)->with('message', \SiteHelpers::alert('error','The following errors occurred')
			)->withErrors($validator)->withInput();
		}	
	
	}	

	public function getLogout() {
		$yourid = \Session::get('uid');
		$currentLang = \Session::get('lang');
		\DB::table('tb_notification')->insert([
											'userid' => $yourid, 'title' => \Lang::get('core.logginout'), 'noticlass' => 'icon bg-danger', 'icon' => 'zmdi zmdi-sign-in'
											]);
		\Auth::logout();
		\Session::flush();
		\Session::put('lang', $currentLang);
		
		return Redirect::to('')->with('message', \SiteHelpers::alert('info','Your are now logged out!'));
	}

	function getSocialize( $social )
	{
		return Socialize::with($social)->redirect();
	}

	function getAutosocial( $social )
	{
		$usersocial = Socialize::with($social)->user();
		$user =  User::where('email',$usersocial->email)->first();
		return self::autoSignin($user,$usersocial);
	}


	function autoSignin($user,$usersocial)
	{

		if(is_null($user)){

		  return self::postCreatesocial($usersocial);

		  return Redirect::to('user/login')
				->with('message', \SiteHelpers::alert('error','You have not registered yet '))
				->withInput();
		} else{

		    Auth::login($user);
			if(Auth::check())
			{
				$row = User::find(\Auth::user()->id); 

				if($row->active =='0')
				{
					// inactive 
					Auth::logout();
					return Redirect::to('user/login')->with('message', \SiteHelpers::alert('error','Your Account is not active'));

				} else if($row->active=='2')
				{
					// BLocked users
					Auth::logout();
					return Redirect::to('user/login')->with('message', \SiteHelpers::alert('error','Your Account is BLocked'));
				} else {
					\Session::put('uid', $row->id);
					\Session::put('gid', $row->group_id);
					\Session::put('eid', $row->group_email);
					\Session::put('userid', $row->username);
					\Session::put('fid', $row->first_name.' '. $row->last_name);	

					\DB::table('tb_notification')->insert([
											'userid' => \Session::get('uid'), 'title' => \Lang::get('core.logging'), 'noticlass' => 'icon bg-success', 'icon' => 'zmdi zmdi-sign-in',
											]);
					if(CNF_FRONT =='false') :
						return Redirect::to('dashboard');						
					else :
						return Redirect::to('');
					endif;					
							
				}
				
				
			}
		}

	}
	
}