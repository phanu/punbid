<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {!! $pageTitle !!} <small> {!! $pageNote !!} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array())) !!}</td>
						<td>{!! $row->id!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Coupon Name', (isset($fields['coupon_name']['language'])? $fields['coupon_name']['language'] : array())) !!}</td>
						<td>{!! $row->coupon_name!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Coupon Code', (isset($fields['coupon_code']['language'])? $fields['coupon_code']['language'] : array())) !!}</td>
						<td>{!! $row->coupon_code!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Discount Type', (isset($fields['discount_type']['language'])? $fields['discount_type']['language'] : array())) !!}</td>
						<td>{!! $row->discount_type!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Discount Num', (isset($fields['discount_num']['language'])? $fields['discount_num']['language'] : array())) !!}</td>
						<td>{!! $row->discount_num!!} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	