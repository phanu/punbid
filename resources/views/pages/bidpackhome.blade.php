<?php use \App\Http\Controllers\BidpackController; 
$data = BidpackController::display(); 

$row = $data['rowData'];


?>

<div class="rs_transprantbg rs_toppadder10 rs_bottompadder50">
<div class="container">
	<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 rs_bottompadder20">
				<div class="rs_hot_coupon_heading">
					<h3 id="buybid" name="buybid">{!! html_entity_decode(Lang::get('bidpackhome.1')) !!}</h3>
					<span><i class="fa fa-heart"></i></span>
				</div>
			</div>
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="woocommerce_wrapper">
					<div class="row">
						<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 rs_toppadder30 mix mix-all ratings" data-value="1" style="display: inline-block;" data-bound="">
                                    <div class="rs_product_div">
                                    <?php foreach ($row as $value) : if($value->bid==0) : ?>
                                        <form method="post" action="{!!url('bidcheckout') !!}">
                                        <div class="rs_product_img">
                                        <img src="{!! asset('frontend') !!}/punbidtheme/images/bidpack/200.svg" class="img-responsive">
                                        <div class="rs_overlay">
                                            <div class="rs_overlay_inner">
                                                <ul>
                                                    <li>
                                                    <button type="submit" href="{!!url('bidcheckout') !!}" class="animated slideInDown"><i class="fa fa-shopping-cart"></i>
                                                    </button>
                                                    </li>

                                                </ul>
                                       
                                            </div>
                                        </div>
                                        <div class="rs_product_detail">
                                            <h4>{!! html_entity_decode(Lang::get('bidpackhome.2')) !!}</a></h4>
                                        </div>
                                        </div>

                                        <p><span>{!! html_entity_decode(Lang::get('bidpackhome.3')) !!}</span></p>
    								
                                        <div class="rs_product_div_footer">
                                        
                                        <input type="text" name="bids" class="form-control" value="" placeholder="{!! html_entity_decode(Lang::get('bidpackhome.5')) !!}">
                                        <div class="rs_btn_div">
                                            <button type="submit" class="rs_button rs_button_orange buybid_width">
                                            <i class="fa fa-shopping-cart"></i> {!! html_entity_decode(Lang::get('bidpackhome.5')) !!}</a>
                                   
                                            <input type="hidden" name="qty" value="1">
                                            <input type="hidden" name="name" value="{!!$value->name!!} : ">
                                            <input type="hidden" name="pid" value="{!!$value->id!!}">
                                            <input type="hidden" name="paytype" value="bidpack">
                                            <input type="hidden" name="url" value="{!!url('') !!}/uploads/bidpacks/{!!$value->img!!}">
                                            <input type="hidden" name="price" value="0"> 
                                        </div>
                                        </div>
                                        </form>
                                        <?php endif; endforeach; ?>
                                    </div>
                                    </div>

                    <?php foreach ($row as $value) : if($value->bid>0) : ?>
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 rs_toppadder30 mix mix-all ratings" data-value="1" style="display: inline-block;" data-bound="">
                    <form method="post" action="{!!url('bidcheckout') !!}">
                                    <div class="rs_product_div">
                                        
                                        <div class="rs_product_img">
                                        <img src="{!!url('') !!}/uploads/bidpacks/{!!$value->img!!}" class="img-responsive" >
                                      
                                        
                                        </div>
                                        
                                        <div class="rs_product_detail">
                                            <h4>{!!$value->name!!} : {!!$value->bid!!} Bid</h4>
                                        </div>
    								<div class="rs_product_div_footer">
                                        <p><span>{!!$value->price!!} THB</span></p>
                                           <div class="rs_incre_decre_btns">
        										<button type="button" class="btn_small count_minus" id="breakDecrease">
        										  <i class="fa fa-minus"></i>
        										</button>
        										<button type="button" class="btn_big count_val" id="breakValue" disabled="">1</button>
        										<button type="button" class="btn_small count_plus" id="breakIncrease">
        										  <i class="fa fa-plus"></i>
        										</button>
                                                <input type="hidden" class="quantity_rows" name="qty" value="1">
									       </div>
                                        <div class="rs_btn_div">
                                            <input type="hidden" name="price" value="{!!$value->price!!}">
                                            <input type="hidden" name="name" value="{!!$value->name!!} : {!!$value->bid!!} Bid">
                                            <input type="hidden" name="pid" value="{!!$value->id!!}">
                                            <input type="hidden" name="paytype" value="bidpack">
                                            <input type="hidden" name="url" value="{!!url('') !!}/uploads/bidpacks/{!!$value->img!!}">
                                            <button type="submit" class="rs_button rs_button_orange buybid_width">
                                             <input type="hidden" name="bids" id="bids" value="{!!$value->bid!!}"> 
                                            <i class="fa fa-shopping-cart"></i> {!! html_entity_decode(Lang::get('bidpackhome.4')) !!}</a>
                                        </div>
                                    </div>
                                        
                                    </div>
                    </form>
                    </div>
                   <?php endif; endforeach; ?>
						
						
					</div>	
				</div>	
			</div>
			
		</div>
</div>
</div>