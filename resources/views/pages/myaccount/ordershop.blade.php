
                                    <ul class="nav nav-pills pull-right myheadbidbg">
                                        <li class="active">
                                            <a href="#myorderbid" data-toggle="tab" aria-expanded="true">{!! html_entity_decode(Lang::get('myacc-ordershop.1')) !!}</a>
                                        </li>
                                        <li class="">
                                            <a href="#myordershop" data-toggle="tab" aria-expanded="false">{!! html_entity_decode(Lang::get('myacc-ordershop.2')) !!}</a>
                                        </li>
                                    </ul>
                                    <h4 class="header-title m-b-30 myheadbid">{!! html_entity_decode(Lang::get('myacc-ordershop.3')) !!}</h4>

                                    <div class="tab-content">
                                        <div id="myorderbid" class="tab-pane fade active in">
                                            <div class="row">
                                                <div class="col-md-12">
                                                
                                                <?php use \App\Http\Controllers\BidordersController; 
                                                $data = BidordersController::display(); 
                                                ?>
                                                   <table id="datatable" class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                        @foreach ($data['tableGrid'] as $t)
                                                            @if($t['view'] =='1')               
                                                                <?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
                                                                @if(SiteHelpers::filterColumn($limited ))
                                                                
                                                                    <th><?php echo \SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())); ?></th>          
                                                                @endif 
                                                            @endif
                                                        @endforeach
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @foreach ($data['rowData'] as $row)
                                                            <tr>                                    
                                                             @foreach ($data['tableGrid'] as $field)
                                                                 @if($field['view'] =='1')
                                                                    <?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
                                                                    @if(SiteHelpers::filterColumn($limited ))
                                                                     <td>                    
                                                                        <p>{!! SiteHelpers::formatRows($row->{$field['field']},$field,$row) !!}</p>                      
                                                                     </td>
                                                                    @endif  
                                                                 @endif                  
                                                             @endforeach              
                                                            </tr>
                                                        @endforeach
                                                    </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="myordershop" class="tab-pane fade in">
                                            <div class="row">
                                                <div class="col-md-12">
                                                <?php use \App\Http\Controllers\OrdersController; 
                                                $data = OrdersController::display(); ?>
                                                   <table id="datatable" class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                        @foreach ($data['tableGrid'] as $t)
                                                            @if($t['view'] =='1')               
                                                                <?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
                                                                @if(SiteHelpers::filterColumn($limited ))
                                                                
                                                                    <th><?php echo \SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())); ?></th>          
                                                                @endif 
                                                            @endif
                                                        @endforeach
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @foreach ($data['rowData'] as $row)
                                                            <tr>                                    
                                                             @foreach ($data['tableGrid'] as $field)
                                                                 @if($field['view'] =='1')
                                                                    <?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
                                                                    @if(SiteHelpers::filterColumn($limited ))
                                                                     <td>                    
                                                                        <p>{!! SiteHelpers::formatRows($row->{$field['field']},$field,$row) !!}</p>                      
                                                                     </td>
                                                                    @endif  
                                                                 @endif                  
                                                             @endforeach              
                                                            </tr>
                                                        @endforeach
                                                    </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                     </div>
