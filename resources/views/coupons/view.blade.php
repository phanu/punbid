@if($setting['view-method'] =='native')
<div class="sbox">
	<div class="sbox-title">  
		<h4> <i class="fa fa-table"></i> <?php echo $pageTitle ;?> <small>{!! $pageNote !!}</small>
			<a href="javascript:void(0)" class="collapse-close pull-right btn btn-xs btn-danger" onclick="ajaxViewClose('#{!! $pageModule !!}')">
			<i class="fa fa fa-times"></i></a>
		</h4>
	 </div>

	<div class="sbox-content"> 
@endif	

		<table class="table table-striped table-bordered" >
			<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Id', (isset($fields['id']['language'])? $fields['id']['language'] : array())) !!}</td>
						<td>{!! $row->id!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Coupon Name', (isset($fields['coupon_name']['language'])? $fields['coupon_name']['language'] : array())) !!}</td>
						<td>{!! $row->coupon_name!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Coupon Code', (isset($fields['coupon_code']['language'])? $fields['coupon_code']['language'] : array())) !!}</td>
						<td>{!! $row->coupon_code!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Discount Num', (isset($fields['discount_num']['language'])? $fields['discount_num']['language'] : array())) !!}</td>
						<td>{!! $row->discount_num!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Discount Type', (isset($fields['discount_type']['language'])? $fields['discount_type']['language'] : array())) !!}</td>
						<td>{!! $row->discount_type!!} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{!! SiteHelpers::activeLang('Active', (isset($fields['active']['language'])? $fields['active']['language'] : array())) !!}</td>
						<td>{!! SiteHelpers::formatRows($row->active,$fields['active'],$row ) !!} </td>
						
					</tr>
				
			</tbody>	
		</table>  
			
		 	

@if($setting['form-method'] =='native')
	</div>	
</div>	
@endif	

<script>
$(document).ready(function(){

});
</script>	