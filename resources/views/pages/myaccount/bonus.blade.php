
                                    <ul class="nav nav-pills pull-right myheadbidbg">
                                        <li class="active">
                                            <a href="#mybonusbid" data-toggle="tab" aria-expanded="true">{!! html_entity_decode(Lang::get('myacc-bonus.1')) !!}</a>
                                        </li>
                                        <li class="">
                                            <a href="#mybonusspay" data-toggle="tab" aria-expanded="false">{!! html_entity_decode(Lang::get('myacc-bonus.2')) !!}</a>
                                        </li>
                                        
                                    </ul>
                                    <h4 class="header-title m-b-30">{!! html_entity_decode(Lang::get('myacc-bonus.3')) !!}</h4>

                                    <div class="tab-content">
                                        <div id="mybonusbid" class="tab-pane active fade in">
                                            <div class="row">
                                                <div class="col-md-12">
                                                
                                                   <?php use \App\Http\Controllers\BidremindController; 
                                                    $data = BidremindController::display("bid"); 
                                                   
                                                    
                                                    ?>

                                                   <table id="datatable" class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                        @foreach ($data['tableGrid'] as $t)
                                                            @if($t['view'] =='1')               
                                                                <?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
                                                                @if(SiteHelpers::filterColumn($limited ))
                                                                
                                                                    <th><?php echo \SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())); ?></th>          
                                                                @endif 
                                                            @endif
                                                        @endforeach
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @foreach ($data['rowData'] as $row)
                                                            <tr>                                    
                                                             @foreach ($data['tableGrid'] as $field)
                                                                 @if($field['view'] =='1')
                                                                    <?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
                                                                    @if(SiteHelpers::filterColumn($limited ))
                                                                     <td>          
                                                                        <p>{!! SiteHelpers::formatRows($row->{$field['field']},$field,$row) !!}</p> 
                                                                     </td>
                                                                    @endif  
                                                                 @endif                  
                                                             @endforeach              
                                                            </tr>
                                                        @endforeach
                                                    </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="mybonusspay" class="tab-pane fade in">
                                            <div class="row">
                                                <div class="col-md-12">
                                                
                                                <?php 
                                                    $data = BidremindController::display("spay"); ?>

                                                   <table id="datatable" class="table table-striped">
                                                    <thead>
                                                        <tr>
                                                        @foreach ($data['tableGrid'] as $t)
                                                            @if($t['view'] =='1')               
                                                                <?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
                                                                @if(SiteHelpers::filterColumn($limited ))
                                                                
                                                                    <th><?php echo \SiteHelpers::activeLang($t['label'],(isset($t['language'])? $t['language'] : array())); ?></th>          
                                                                @endif 
                                                            @endif
                                                        @endforeach
                                                        </tr>
                                                    </thead>

                                                    <tbody>
                                                        @foreach ($data['rowData'] as $row)
                                                            <tr>                                    
                                                             @foreach ($data['tableGrid'] as $field)
                                                                 @if($field['view'] =='1')
                                                                    <?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
                                                                    @if(SiteHelpers::filterColumn($limited ))
                                                                     <td>
                                                                                 
                                                                        <p>{!! SiteHelpers::formatRows($row->{$field['field']},$field,$row) !!}</p> 
                                                                        
                                                                     </td>
                                                                    @endif  
                                                                 @endif                  
                                                             @endforeach              
                                                            </tr>
                                                        @endforeach
                                                    </tbody>

                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                        
