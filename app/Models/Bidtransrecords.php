<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class bidtransrecords extends Sximo  {
	
	protected $table = 'bids_trans';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT bids_trans.* FROM bids_trans  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE bids_trans.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
