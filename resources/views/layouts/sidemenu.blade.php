      <?php $sidebar = SiteHelpers::menus('sidebar') ;?>
<nav class="left-menu" left-menu>
    <div class="logo-container">
    	<a  href="{{ URL::to('dashboard')}}"  class="logo">
		 	@if(file_exists(public_path().'/sximo/images/'.CNF_LOGO) && CNF_LOGO !='')
		 	<img src="{{ asset('sximo/images/'.CNF_LOGO)}}" alt="{{ CNF_APPNAME }}" />
		 	<img class="logo-inverse" src="{{ asset('sximo/images/'.CNF_LOGO)}}" alt="{{ CNF_APPNAME }}" />
		 	@else
			<img src="{{ asset('sximo/images/logo.png')}}" alt="{{ CNF_APPNAME }}"  />
			<img class="logo-inverse" src="{{ asset('sximo/images/logo.png')}}" alt="{{ CNF_APPNAME }}" />
			@endif
		 </a>
    </div>
    <div class="left-menu-inner scroll-pane">
        <ul class="left-menu-list left-menu-list-root list-unstyled">
            <li class="left-menu-list-submenu" style="display: none;">
                <a class="left-menu-link" href="javascript: void(0);">
                    <i class="left-menu-link-icon icmn-cog util-spin-delayed-pseudo"><!-- --></i>
                    <span class="menu-top-hidden">Theme</span> Settings
                </a>
                <ul class="left-menu-list list-unstyled">
                    <li>    
                        <div class="left-menu-item">
                            <div class="left-menu-block">
                                <div class="left-menu-block-item">
	                                </div>
                                <div class="left-menu-block-item">
                                    <span class="font-weight-600">Theme Style:</span>
                                </div>
                                <div class="left-menu-block-item" id="options-theme">
                                    <div class="btn-group btn-group-justified" data-toggle="buttons">
                                        <div class="btn-group">
                                            <label class="btn btn-default active">
                                                <input type="radio" name="options-theme" value="theme-default" autocomplete="off" checked=""> Light
                                            </label>
                                        </div>
                                        <div class="btn-group">
                                            <label class="btn btn-default">
                                                <input type="radio" name="options-theme" value="theme-dark" autocomplete="off"> Dark
                                            </label>
                                        </div>
                                    </div>
                                    <div class="btn-group btn-group-justified" data-toggle="buttons">
                                        <div class="btn-group">
                                            <label class="btn btn-default">
                                                <input type="radio" name="options-theme" value="theme-green" autocomplete="off"> Green
                                            </label>
                                        </div>
                                        <div class="btn-group">
                                            <label class="btn btn-default">
                                                <input type="radio" name="options-theme" value="theme-blue" autocomplete="off"> Blue
                                            </label>
                                        </div>
                                    </div>
                                    <div class="btn-group btn-group-justified" data-toggle="buttons">
                                        <div class="btn-group">
                                            <label class="btn btn-default">
                                                <input type="radio" name="options-theme" value="theme-red" autocomplete="off"> Red
                                            </label>
                                        </div>
                                        <div class="btn-group">
                                            <label class="btn btn-default">
                                                <input type="radio" name="options-theme" value="theme-orange" autocomplete="off"> Orange
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="left-menu-block-item">
                                    <span class="font-weight-600">Fixed Top Menu</sup>:</span>
                                </div>
                                <div class="left-menu-block-item" id="options-menu">
                                    <div class="btn-group btn-group-justified" data-toggle="buttons">
                                        <div class="btn-group">
                                            <label class="btn btn-default active">
                                                <input type="radio" name="options-menu" value="menu-fixed" checked=""> On
                                            </label>
                                        </div>
                                        <div class="btn-group">
                                            <label class="btn btn-default">
                                                <input type="radio" name="options-menu" value="menu-static"> Off
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="left-menu-block-item">
                                    <span class="font-weight-600">Super Clean Mode:</span>
                                </div>
                                <div class="left-menu-block-item" id="options-mode">
                                    <div class="btn-group btn-group-justified" data-toggle="buttons">
                                        <div class="btn-group">
                                            <label class="btn btn-default active">
                                                <input type="radio" name="options-mode" value="mode-superclean" checked=""> On
                                            </label>
                                        </div>
                                        <div class="btn-group">
                                            <label class="btn btn-default">
                                                <input type="radio" name="options-mode" value="mode-default"> Off
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="left-menu-block-item">
                                    <span class="font-weight-600">Colorful Menu:</span>
                                </div>
                                <div class="left-menu-block-item" id="options-colorful">
                                    <div class="btn-group btn-group-justified" data-toggle="buttons">
                                        <div class="btn-group">
                                            <label class="btn btn-default active">
                                                <input type="radio" name="options-colorful" value="colorful-enabled" checked=""> On
                                            </label>
                                        </div>
                                        <div class="btn-group">
                                            <label class="btn btn-default">
                                                <input type="radio" name="options-colorful" value="colorful-disabled"> Off
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
            <li class="left-menu-list-active">
                <a class="left-menu-link" href="{{url('dashboard')}}">
                    <i class="left-menu-link-icon icmn-home2"><!-- --></i>
                    <span class="menu-top-hidden">Dashboard</span> 
                </a>
            </li>
     

            <li class="left-menu-list-separator"><!-- --></li>

            @foreach ($sidebar as $menu)
            
            @if($menu['module'] =='separator')
             <li class="left-menu-list-separator"> <!-- --></li>
            @else

             <li @if(Request::segment(1) == $menu['module']) 
                class="left-menu-list-active" 
            @elseif(count($menu['childs']) > 0 )
                class="left-menu-list-submenu left-menu-list-opened" 
            @else
                class="left-menu-link" 
            @endif>
                <a 
                @if($menu['menu_type'] =='external')
                        href="{{ $menu['url'] }}" 
                    @else
                        href="{{ URL::to($menu['module'])}}" 
                    @endif              
                
                    class="left-menu-link" >
                    <i class="left-menu-link-icon {{$menu['menu_icons']}}"><!-- --></i>
                    @if(CNF_MULTILANG ==1 && isset($menu['menu_lang']['title'][Session::get('lang')]))
                        {{ $menu['menu_lang']['title'][Session::get('lang')] }}
                    @else
                        {{$menu['menu_name']}}
                    @endif         

                </a> 

                @endif  


                @if(count($menu['childs']) > 0)
                     <ul class="left-menu-list list-unstyled">
                        @foreach ($menu['childs'] as $menu2)
                         <li>
                            <a 
                                class="left-menu-link" 
                                @if($menu2['menu_type'] =='external')
                                    href="{{ $menu2['url']}}" 
                                @else
                                    href="{{ URL::to($menu2['module'])}}"  
                                @endif                                  
                            >
                            @if(CNF_MULTILANG ==1 && isset($menu2['menu_lang']['title'][Session::get('lang')]))
                                {{ $menu2['menu_lang']['title'][Session::get('lang')] }}
                            @else
                                {{$menu2['menu_name']}}
                            @endif  
                            </a> 
                                                 
                          </li>                           
                        @endforeach
                    </ul>
                @endif
            </li>
        @endforeach
           
            
        </ul>
    </div>
</nav>
	  
