                    <div class="card-box widget-user">
                        <div class="user-box">
                            <div class="user-img">
                        {!! Form::open(array('url'=>'user/saveprofilepic', 'class'=>'' ,'files' => true, 'id' => 'userpicform' )) !!}
                                <input type="file" class="dropify" name="avatar" id="cavatar" data-default-file="{!!$avatarbig!!}" />
                        {!! Form::close() !!}
                                <div class="user-status online"><i class="zmdi zmdi-dot-circle"></i></div>
                            </div>
                                <h4 class="header-title-side">{!!$userdet->username!!}</h4>
                    <div class="col-md-12 col-xs-12">
                      <div class="col-md-6  col-xs-6 no-padding">  <h5 class="header-title-side"><span class="livicon-evo vertical-adjust" data-options="name: legal.svg; style: Filled; size: 25px; tryToSharpen: true;"></span>  
                                            {!!number_format($userdet->bid) !!}</h5>
                                            </div>
                      <div class="col-md-6  col-xs-6 no-padding"> <h5 class="header-title-side">
                      <span class="livicon-evo vertical-adjust" data-options="name: coins.svg; style: Filled; size: 25px; tryToSharpen: true;"></span>  
                                             {!!number_format($userdet->spay) !!}</h5></div>
                    </div>
                            <ul class="list-inline">
                                <li>
                                    <a href="#account" data-toggle="tab">
                                        <i class="zmdi zmdi-settings"></i>
                                    </a>
                                </li>

                                <li>
                                    <a href="{!! URL::to('user/logout') !!}" class="text-custom">
                                        <i class="zmdi zmdi-power"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="card-box nopadding-left-right">
                        <div id="sidebar-menu">
                                <ul>
                                    <li>
                                        <a href="#account" data-toggle="tab" class="waves-effect @if($_GET['v']=='') subdrop @endif"><span
                                                        class="livicon-evo vertical-adjust" 
                                                        data-options="name: user.svg; style: Filled; size: 32px; tryToSharpen: true;">
                                                </span>{!! html_entity_decode(Lang::get('myacc-sidebar.1')) !!}</a>
                                    </li>
                                    <li>
                                        <a href="#wishlist" data-toggle="tab" class="waves-effect 
                                        @if($_GET['v']=='wishlist') subdrop @endif">
                                                <span
                                                class="livicon-evo vertical-adjust"
                                                data-options="name: heart.svg; style: Filled; size: 32px; tryToSharpen: true;">
                                        </span>{!! html_entity_decode(Lang::get('myacc-sidebar.2')) !!}</a>
                                      </li>
                                      <li><a href="#ordershop"  class="waves-effect" data-toggle="tab"><span
                                                class="livicon-evo vertical-adjust"
                                                data-options="name: shoppingcart.svg; style: Filled; size: 32px; tryToSharpen: true;">
                                        </span>{!! html_entity_decode(Lang::get('myacc-sidebar.3')) !!}</a></li>
                                      <li><a href="#transactions" data-toggle="tab"  class="waves-effect"><span
                                                class="livicon-evo vertical-adjust"
                                                data-options="name: legal.svg; style: Filled; size: 32px; tryToSharpen: true;">
                                        </span>{!! html_entity_decode(Lang::get('myacc-sidebar.4')) !!}</a></li>
                                      <li><a href="#informpayment" data-toggle="tab"  class="waves-effect"><span
                                                class="livicon-evo vertical-adjust"
                                                data-options="name: credit-card-out.svg; style: Filled; size: 32px; tryToSharpen: true;">
                                        </span>{!! html_entity_decode(Lang::get('myacc-sidebar.5')) !!}</a></li>
                                      <li><a href="#withdraw" data-toggle="tab"  class="waves-effect"><span
                                                class="livicon-evo vertical-adjust"
                                                data-options="name: credit-card-in.svg; style: Filled; size: 32px; tryToSharpen: true;">
                                        </span>{!! html_entity_decode(Lang::get('myacc-sidebar.6')) !!}</a></li>
                                      <li><a href="#bidremain" data-toggle="tab"  class="waves-effect"><span
                                                class="livicon-evo vertical-adjust"
                                                data-options="name: touch.svg; style: Filled; size: 32px; tryToSharpen: true;">
                                        </span>{!! html_entity_decode(Lang::get('myacc-sidebar.7')) !!}</a></li>
                                    
                                      <li><a href="#bonus" data-toggle="tab"  class="waves-effect"><span
                                                class="livicon-evo vertical-adjust"
                                                data-options="name: coins.svg; style: Filled; size: 32px; tryToSharpen: true;">
                                        </span>{!! html_entity_decode(Lang::get('myacc-sidebar.8')) !!}</a></li>
                                      
                                    </ul>
                        <div class="clearfix"></div>
                        </div>
                    </div>