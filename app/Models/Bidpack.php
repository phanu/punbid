<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class bidpack extends Sximo  {
	
	protected $table = 'bidpack';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT bidpack.* FROM bidpack  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE bidpack.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
