<script type="text/javascript">
$(document).ready(function() {

$.fn.digits = function(){ 
    return this.each(function(){ 
        $(this).text( $(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,") ); 
    })
}

window.commaSeparateNumber = function(val) {
        while (/(\d+)(\d{3})/.test(val.toString())) {
            val = val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
        }
        return val;
}

@if($pages=="pages.myaccount"||$pages=="pages.checkout"||$pages=="pages.winnercheckout")
$(".select2").select2();
    $.thprovinceset({
    /*กำหนด id ของ Dropdown*/
    eProvinceID: "#xprovince",
    eDistrictID: "#xdistrict",
    eSubDistrictID: "#xsubdistrict",
    eZipcode: "#xzipcode", 
    /*กำหนดค่าให้ Dropdown*/
    SelectProvince : "{!!$userdet->province!!}", /*10 = กรุงเทพฯ*/
    SelectDistrict: "{!!$userdet->district!!}", 
    SelectSubDistrict: "{!!$userdet->subdistrict!!}",
  });

    $("#select2-chosen-1").text($( "#xprovince option:selected" ).text());
    $("#select2-chosen-2").text($( "#xdistrict option:selected" ).text());
    $("#select2-chosen-3").text($( "#xsubdistrict option:selected" ).text());

@endif


    $('.dropify').dropify({
                messages: {
                    'default': '{!! html_entity_decode(Lang::get('script-text.default')) !!}',
                    'replace': '{!! html_entity_decode(Lang::get('script-text.replace')) !!}',
                    'remove': '{!! html_entity_decode(Lang::get('script-text.remove')) !!}',
                    'error': '{!! html_entity_decode(Lang::get('script-text.error')) !!}'
                },
                error: {
                    'fileSize': '{!! html_entity_decode(Lang::get('script-text.fileSize')) !!}'

                }
            });
    $(document).on('click','.pagination li a', function() {
    //do other stuff when a click happens
        $('.ajaxLoading').fadeIn( 200 ).delay( 100 ).fadeOut( 200 ).css({
            display: 'block'
        });

        $.ajax({
        url: $(this).data('url'),
        method: "GET",
        data: { },
        dataType: "html",
        success: function(data) {
                $(".{!!$modulepage!!}").html(data);
            },
        });
        $('.ajaxLoading').fadeIn( 200 ).delay( 100 ).fadeOut( 200 ).css({
            display: 'none'
        });

        return false;
    }); 

    $("input[name$='payment_type']").on("click",function () {
      
      var sum = 0;
      var test = $(this).val();
      $(".payment_box").hide('slow');
      $(".payment_box[data-period='" + test + "']").show('slow');
        if(test==="paypal")
        { 
          var value =  parseFloat($("#numtotal").val());

          var value1 = value*0.042;

          var value2 = value1 + value;

          $("#payment_fee").val(value1.toFixed(2));

          $("#amount").val(value2.toFixed(2));
          
          $("#setpaymentfee").text(commaSeparateNumber(value1.toFixed(2))+' THB');

          $("#checkouttotal").text(commaSeparateNumber(value2.toFixed(2))+' THB');          


        }else if(test==="credit_card")
        {
          var value =  parseFloat($("#numtotal").val());

          var value1 = value*0.042;

          var value2 = value1 + value;

          $("#payment_fee").val(value1.toFixed(2));

          $("#amount").val(value2.toFixed(2));
          
          $("#setpaymentfee").text(commaSeparateNumber(value1.toFixed(2))+' THB');

          $("#checkouttotal").text(commaSeparateNumber(value2.toFixed(2))+' THB');      

        }else if(test==="dbt")
        {
          var value =  parseFloat($("#numtotal").val());

          var value1 = value*0;

          var value2 = value1 + value;

          $("#payment_fee").val(value1.toFixed(2));

          $("#amount").val(value2.toFixed(2));
          
          $("#setpaymentfee").text(commaSeparateNumber(value1.toFixed(2))+' THB');

          $("#checkouttotal").text(commaSeparateNumber(value2.toFixed(2))+' THB');      
        } 

      });


    var sync1 = $("#sync1");
  var sync2 = $("#sync2");
 
  sync1.owlCarousel({
    singleItem : true,
    slideSpeed : 1000,
    navigation: false,
    pagination:true,
    afterAction : syncPosition,
    responsiveRefreshRate : 200,
  });
 
  sync2.owlCarousel({
    items : 4,
    itemsDesktop      : [1199,4],
    itemsDesktopSmall     : [979,4],
    itemsTablet       : [768,4],
    itemsMobile       : [479,0],
    pagination:false,
    responsiveRefreshRate : 100,
    afterInit : function(el){
      el.find(".owl-item").eq(0).addClass("synced");
    el.find(".owl-item").addClass("itemfull");
    el.find(".owl-wrapper").addClass("itemfull");
    }
  });
 
  function syncPosition(el){
    var current = this.currentItem;
    $("#sync2")
      .find(".owl-item")
      .removeClass("synced")
      .eq(current)
      .addClass("synced")
    if($("#sync2").data("owlCarousel") !== undefined){
      center(current)
    }
  }
 
  $("#sync2").on("click", ".owl-item", function(e){
    e.preventDefault();
    var number = $(this).data("owlItem");
    sync1.trigger("owl.goTo",number);
  });
 
  function center(number){
    var sync2visible = sync2.data("owlCarousel").owl.visibleItems;
    var num = number;
    var found = false;
    for(var i in sync2visible){
      if(num === sync2visible[i]){
        var found = true;
      }
    }
 
    if(found===false){
      if(num>sync2visible[sync2visible.length-1]){
        sync2.trigger("owl.goTo", num - sync2visible.length+2)
      }else{
        if(num - 1 === -1){
          num = 0;
        }
        sync2.trigger("owl.goTo", num);
      }
    } else if(num === sync2visible[sync2visible.length-1]){
      sync2.trigger("owl.goTo", sync2visible[1])
    } else if(num === sync2visible[0]){
      sync2.trigger("owl.goTo", num-1)
    }
    
  }

  
/*                  var $this = $(this), finalDate = $(this).data('countdown'); 
                  var vid = $(this).data('vid');
                  $this.countdown(finalDate, function(event) {
                  $this.html(event.strftime('%H:%M:%S'));
                  }).on('finish.countdown', function(data) {  
                        $('.bid-end-date').html('{!! html_entity_decode(Lang::get('script-text.closebid')) !!}');
                        $('.endhide').hide();
                      });
                }); */

$('.deletecartshop').on("click", function() {
      $.ajax({
        url: "{!! url('cart') !!}",
        method: "POST",
        data: { cartshopid : this.id , cartaction : "delete"},
        dataType: "html",
        success: function(data) {
                location.href = location.href  + "?message=del"

                $('.notification-box').saveusernotify('{!! html_entity_decode(Lang::get('script-text.cartdelete')) !!}','icon bg-danger','zmdi zmdi-neg-1');
            },
      });
    });
});

</script>