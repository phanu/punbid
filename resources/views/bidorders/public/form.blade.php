

		 {!! Form::open(array('url'=>'bidorders/savepublic', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> bid orders</legend>
				{!! Form::hidden('orderNumber', $row['orderNumber']) !!}					
									  <div class="form-group  " >
										<label for="OrderDate" class=" control-label col-md-4 text-left"> OrderDate </label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('orderDate', $row['orderDate'],array('class'=>'form-control datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $status = explode(',',$row['status']);
					$status_opt = array( 'onhold' => 'On hold' ,  'Inprocess' => 'In process' ,  'Shipped' => 'Shipped' ,  'Cancel' => 'Cancel' , ); ?>
					<select name='status' rows='5' required  class='select2 '  > 
						<?php 
						foreach($status_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['status'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Comments" class=" control-label col-md-4 text-left"> Comments </label>
										<div class="col-md-6">
										  <textarea name='comments' rows='5' id='comments' class='form-control '  
				           >{{ $row['comments'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="CustomerNumber" class=" control-label col-md-4 text-left"> CustomerNumber <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('customerNumber', $row['customerNumber'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Payment Type" class=" control-label col-md-4 text-left"> Payment Type <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('payment_type', $row['payment_type'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true'  )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Bidpack Id" class=" control-label col-md-4 text-left"> Bidpack Id <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('bidpack_id', $row['bidpack_id'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Qty" class=" control-label col-md-4 text-left"> Qty <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('qty', $row['qty'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Price" class=" control-label col-md-4 text-left"> Price <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  {!! Form::text('price', $row['price'],array('class'=>'form-control', 'placeholder'=>'', 'required'=>'true', 'parsley-type'=>'number'   )) !!} 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Payment Fee" class=" control-label col-md-4 text-left"> Payment Fee <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='payment_fee' rows='5' id='payment_fee' class='form-control '  
				         required  >{{ $row['payment_fee'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Bids" class=" control-label col-md-4 text-left"> Bids </label>
										<div class="col-md-6">
										  <textarea name='bids' rows='5' id='bids' class='form-control '  
				           >{{ $row['bids'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
