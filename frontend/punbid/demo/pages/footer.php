<section class="brands-carousel">
	<h2 class="sr-only">Brands Carousel</h2>
	<div class="container">
		<div id="owl-brands" class="owl-brands owl-carousel unicase-owl-carousel owl-outer-nav">
			<div class="item"> <a href="#">
				<figure>
					<figcaption class="text-overlay">
						<div class="info">
							<h4>Acer</h4>
						</div>
						<!-- /.info --> 
					</figcaption>
					<img src="assets/images/blank.gif" data-echo="assets/images/brands/1.png" class="img-responsive" alt=""> </figure>
				</a> </div>
			<!-- /.item -->
			
			<div class="item"> <a href="#">
				<figure>
					<figcaption class="text-overlay">
						<div class="info">
							<h4>Apple</h4>
						</div>
						<!-- /.info --> 
					</figcaption>
					<img src="assets/images/blank.gif" data-echo="assets/images/brands/2.png" class="img-responsive" alt=""> </figure>
				</a> </div>
			<!-- /.item -->
			
			<div class="item"> <a href="#">
				<figure>
					<figcaption class="text-overlay">
						<div class="info">
							<h4>Asus</h4>
						</div>
						<!-- /.info --> 
					</figcaption>
					<img src="assets/images/blank.gif" data-echo="assets/images/brands/3.png" class="img-responsive" alt=""> </figure>
				</a> </div>
			<!-- /.item -->
			
			<div class="item"> <a href="#">
				<figure>
					<figcaption class="text-overlay">
						<div class="info">
							<h4>Dell</h4>
						</div>
						<!-- /.info --> 
					</figcaption>
					<img src="assets/images/blank.gif" data-echo="assets/images/brands/4.png" class="img-responsive" alt=""> </figure>
				</a> </div>
			<!-- /.item -->
			
			<div class="item"> <a href="#">
				<figure>
					<figcaption class="text-overlay">
						<div class="info">
							<h4>Gionee</h4>
						</div>
						<!-- /.info --> 
					</figcaption>
					<img src="assets/images/blank.gif" data-echo="assets/images/brands/5.png" class="img-responsive" alt=""> </figure>
				</a> </div>
			<!-- /.item -->
			
			<div class="item"> <a href="#">
				<figure>
					<figcaption class="text-overlay">
						<div class="info">
							<h4>HP</h4>
						</div>
						<!-- /.info --> 
					</figcaption>
					<img src="assets/images/blank.gif" data-echo="assets/images/brands/6.png" class="img-responsive" alt=""> </figure>
				</a> </div>
			<!-- /.item -->
			
			<div class="item"> <a href="#">
				<figure>
					<figcaption class="text-overlay">
						<div class="info">
							<h4>HTC</h4>
						</div>
						<!-- /.info --> 
					</figcaption>
					<img src="assets/images/blank.gif" data-echo="assets/images/brands/3.png" class="img-responsive" alt=""> </figure>
				</a> </div>
			<!-- /.item -->
			
			<div class="item"> <a href="#">
				<figure>
					<figcaption class="text-overlay">
						<div class="info">
							<h4>IBM</h4>
						</div>
						<!-- /.info --> 
					</figcaption>
					<img src="assets/images/blank.gif" data-echo="assets/images/brands/5.png" class="img-responsive" alt=""> </figure>
				</a> </div>
			<!-- /.item -->
			
			<div class="item"> <a href="#">
				<figure>
					<figcaption class="text-overlay">
						<div class="info">
							<h4>Lenova</h4>
						</div>
						<!-- /.info --> 
					</figcaption>
					<img src="assets/images/blank.gif" data-echo="assets/images/brands/2.png" class="img-responsive" alt=""> </figure>
				</a> </div>
			<!-- /.item -->
			
			<div class="item"> <a href="#">
				<figure>
					<figcaption class="text-overlay">
						<div class="info">
							<h4>LG</h4>
						</div>
						<!-- /.info --> 
					</figcaption>
					<img src="assets/images/blank.gif" data-echo="assets/images/brands/1.png" class="img-responsive" alt=""> </figure>
				</a> </div>
			<!-- /.item -->
			
			<div class="item"> <a href="#">
				<figure>
					<figcaption class="text-overlay">
						<div class="info">
							<h4>Micromax</h4>
						</div>
						<!-- /.info --> 
					</figcaption>
					<img src="assets/images/blank.gif" data-echo="assets/images/brands/6.png" class="img-responsive" alt=""> </figure>
				</a> </div>
			<!-- /.item -->
			
			<div class="item"> <a href="#">
				<figure>
					<figcaption class="text-overlay">
						<div class="info">
							<h4>Microsoft</h4>
						</div>
						<!-- /.info --> 
					</figcaption>
					<img src="assets/images/blank.gif" data-echo="assets/images/brands/4.png" class="img-responsive" alt=""> </figure>
				</a> </div>
			<!-- /.item --> 
			
		</div>
		<!-- /.owl-carousel --> 
		
	</div>
</section>
<footer id="colophon" class="site-footer">
	<div class="footer-widgets">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4 col-xs-12">
					<aside class="widget clearfix">
						<div class="body">
							<h4 class="widget-title">Featured Products</h4>
							<ul class="product_list_widget">
								<li> <a href="single-product.html" title="Tablet Thin EliteBook  Revolve 810 G6"> <img class="wp-post-image" data-echo="assets/images/footer/1.jpg" src="assets/images/blank.gif" alt=""> <span class="product-title">Tablet Thin EliteBook  Revolve 810 G6</span> </a> <span class="electro-price"><span class="amount">&#36;1,300.00</span></span> </li>
								<li> <a href="single-product.html" title="Smartphone 6S 128GB LTE"> <img class="wp-post-image" data-echo="assets/images/footer/2.jpg" src="assets/images/blank.gif" alt=""><span class="product-title">Smartphone 6S 128GB LTE</span> </a> <span class="electro-price"><span class="amount">&#36;780.00</span></span> </li>
								<li> <a href="single-product.html" title="Smartphone 6S 64GB LTE"> <img class="wp-post-image" data-echo="assets/images/footer/3.jpg" src="assets/images/blank.gif" alt=""> <span class="product-title">Smartphone 6S 64GB LTE</span> </a> <span class="electro-price"><span class="amount">&#36;1,215.00</span></span> </li>
							</ul>
						</div>
					</aside>
				</div>
				<div class="col-lg-4 col-md-4 col-xs-12">
					<aside class="widget clearfix">
						<div class="body">
							<h4 class="widget-title">Onsale Products</h4>
							<ul class="product_list_widget">
								<li> <a href="single-product.html" title="Notebook Black Spire V Nitro  VN7-591G"> <img class="wp-post-image" data-echo="assets/images/footer/3.jpg" src="assets/images/blank.gif" alt=""> <span class="product-title">Notebook Black Spire V Nitro  VN7-591G</span> </a> <span class="electro-price"><ins><span class="amount">&#36;1,999.00</span></ins> <del><span class="amount">&#36;2,299.00</span></del></span> </li>
								<li> <a href="single-product.html" title="Tablet Red EliteBook  Revolve 810 G2"> <img class="wp-post-image" data-echo="assets/images/footer/4.jpg" src="assets/images/blank.gif" alt=""> <span class="product-title">Tablet Red EliteBook  Revolve 810 G2</span> </a> <span class="electro-price"><ins><span class="amount">&#36;1,999.00</span></ins> <del><span class="amount">&#36;2,299.00</span></del></span> </li>
								<li> <a href="single-product.html" title="Widescreen 4K SUHD TV"> <img class="wp-post-image" data-echo="assets/images/footer/5.jpg" src="assets/images/blank.gif" alt=""> <span class="product-title">Widescreen 4K SUHD TV</span> </a> <span class="electro-price"><ins><span class="amount">&#36;2,999.00</span></ins> <del><span class="amount">&#36;3,299.00</span></del></span> </li>
							</ul>
						</div>
					</aside>
				</div>
				<div class="col-lg-4 col-md-4 col-xs-12">
					<aside class="widget clearfix">
						<div class="body">
							<h4 class="widget-title">Top Rated Products</h4>
							<ul class="product_list_widget">
								<li> <a href="single-product.html" title="Notebook Black Spire V Nitro  VN7-591G"> <img class="wp-post-image" data-echo="assets/images/footer/6.jpg" src="assets/images/blank.gif" alt=""> <span class="product-title">Notebook Black Spire V Nitro  VN7-591G</span> </a>
									<div class="star-rating" title="Rated 5 out of 5"><span style="width:100%"><strong class="rating">5</strong> out of 5</span></div>
									<span class="electro-price"><ins><span class="amount">&#36;1,999.00</span></ins> <del><span class="amount">&#36;2,299.00</span></del></span> </li>
								<li> <a href="single-product.html" title="Apple MacBook Pro MF841HN/A 13-inch Laptop"> <img class="wp-post-image" data-echo="assets/images/footer/7.jpg" src="assets/images/blank.gif" alt=""> <span class="product-title">Apple MacBook Pro MF841HN/A 13-inch Laptop</span> </a>
									<div class="star-rating" title="Rated 5 out of 5"><span style="width:100%"><strong class="rating">5</strong> out of 5</span></div>
									<span class="electro-price"><span class="amount">&#36;1,800.00</span></span> </li>
								<li> <a href="single-product.html" title="Tablet White EliteBook Revolve  810 G2"> <img class="wp-post-image" data-echo="assets/images/footer/2.jpg" src="assets/images/blank.gif" alt=""> <span class="product-title">Tablet White EliteBook Revolve  810 G2</span> </a>
									<div class="star-rating" title="Rated 5 out of 5"><span style="width:100%"><strong class="rating">5</strong> out of 5</span></div>
									<span class="electro-price"><span class="amount">&#36;1,999.00</span></span> </li>
							</ul>
						</div>
					</aside>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-newsletter">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-7">
					<h5 class="newsletter-title">ชวนเพื่อมาบิด</h5>
					<span class="newsletter-marketing-text">รับบิดฟรีสูงสุด<strong>100 BID</strong></span> </div>
				<div class="col-xs-12 col-sm-5">
					<form>
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Enter your email address">
							<span class="input-group-btn">
							<button class="btn btn-secondary" type="button">ชวนเลย</button>
							</span> </div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom-widgets">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-7 col-md-push-5">
					<div class="columns">
						<aside id="nav_menu-2" class="widget clearfix widget_nav_menu">
							<div class="body">
								<h4 class="widget-title">Find It Fast</h4>
								<div class="menu-footer-menu-1-container">
									<ul id="menu-footer-menu-1" class="menu">
										<li class="menu-item"><a href="single-product.html">ทองคำแท่ง</a></li>
										<li class="menu-item"><a href="single-product.html">ทองคำแท่ง</a></li>
										<li class="menu-item"><a href="single-product.html">ทองคำแท่ง</a></li>
										<li class="menu-item"><a href="single-product.html">ทองคำแท่ง</a></li>
										<li class="menu-item"><a href="single-product.html">ทองคำแท่ง</a></li>
										<li class="menu-item"><a href="single-product.html">ทองคำแท่ง</a></li>
										<li class="menu-item"><a href="single-product.html">ทองคำแท่ง</a></li>
									</ul>
								</div>
							</div>
						</aside>
					</div>
					<!-- /.columns -->
					
					<div class="columns">
						<aside id="nav_menu-3" class="widget clearfix widget_nav_menu">
							<div class="body">
								<h4 class="widget-title">&nbsp;</h4>
								<div class="menu-footer-menu-2-container">
									<ul id="menu-footer-menu-2" class="menu">
										<li class="menu-item"><a href="single-product.html">แหวนทอง</a></li>
										<li class="menu-item "><a href="single-product.html">กรอบพระ</a></li>
										<li  class="menu-item menu-item-type-taxonomy menu-item-object-product_cat menu-item-2742"><a href="single-product.html">สร้อยทอง</a></li>
										<li  class="menu-item "><a href="single-product.html">ของเด็ก</a></li>
									</ul>
								</div>
							</div>
						</aside>
					</div>
					<!-- /.columns -->
					
					<div class="columns">
						<aside id="nav_menu-4" class="widget clearfix widget_nav_menu">
							<div class="body">
								<h4 class="widget-title">Customer Care</h4>
								<div class="menu-footer-menu-3-container">
									<ul id="menu-footer-menu-3" class="menu">
										<li class="menu-item"><a href="single-product.html">บัญชีของฉัน</a></li>
										<li class="menu-item"><a href="single-product.html">การประมูลของฉัน</a></li>
										<li class="menu-item"><a href="single-product.html">AUTO BID</a></li>
										<li class="menu-item"><a href="single-product.html">วิธีการประมูล</a></li>
										<li class="menu-item"><a href="single-product.html">ข้อตกลงและเงื่อนไข</a></li>
										<li class="menu-item"><a href="single-product.html">คำถามที่พบบ่อย</a></li>
										<li class="menu-item"><a href="hsingle-product.html">ช่วยเหลือ</a></li>
									</ul>
								</div>
							</div>
						</aside>
					</div>
					<!-- /.columns --> 
					
				</div>
				<!-- /.col -->
				
				<div class="footer-contact col-xs-12 col-sm-12 col-md-5 col-md-pull-7">
					<div class="footer-logo"> <img alt="logo" src="assets/images/logo.svg" width="175.748px" height="42.52px"/> </div>
					<!-- /.footer-contact -->
					
					<div class="footer-call-us">
						<div class="media"> <span class="media-left call-us-icon media-middle"><i class="ec ec-support"></i></span>
							<div class="media-body"> <span class="call-us-text">Call us 24/7!</span> <span class="call-us-number">098 252 8986</span> </div>
						</div>
					</div>
					<!-- /.footer-call-us -->
					
					<div class="footer-address"> <strong class="footer-address-title">Contact Info</strong>
						<address>
						gogoldrun.com
						</address>
					</div>
					<!-- /.footer-address -->
					
					<div class="footer-social-icons">
						<ul class="social-icons list-unstyled">
							<li><a class="fa fa-facebook" href="http://themeforest.net/user/shaikrilwan/portfolio"></a></li>
							<li><a class="fa fa-twitter" href="http://themeforest.net/user/shaikrilwan/portfolio"></a></li>
							<li><a class="fa fa-pinterest" href="http://themeforest.net/user/shaikrilwan/portfolio"></a></li>
							<li><a class="fa fa-linkedin" href="http://themeforest.net/user/shaikrilwan/portfolio"></a></li>
							<li><a class="fa fa-google-plus" href="http://themeforest.net/user/shaikrilwan/portfolio"></a></li>
							<li><a class="fa fa-tumblr" href="http://themeforest.net/user/shaikrilwan/portfolio"></a></li>
							<li><a class="fa fa-instagram" href="http://themeforest.net/user/shaikrilwan/portfolio"></a></li>
							<li><a class="fa fa-youtube" href="http://themeforest.net/user/shaikrilwan/portfolio"></a></li>
							<li><a class="fa fa-rss" href="#"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="copyright-bar">
		<div class="container">
			<div class="pull-left flip copyright">&copy; <a href="http://demo2.transvelo.in/html/electro/">GoGoRun</a> - All Rights Reserved</div>
			<div class="pull-right flip payment">
				<div class="footer-payment-logo">
					<ul class="cash-card card-inline">
						<li class="card-item"><img src="assets/images/footer/payment-icon/1.png" alt="" width="52"></li>
						<li class="card-item"><img src="assets/images/footer/payment-icon/2.png" alt="" width="52"></li>
						<li class="card-item"><img src="assets/images/footer/payment-icon/3.png" alt="" width="52"></li>
						<li class="card-item"><img src="assets/images/footer/payment-icon/4.png" alt="" width="52"></li>
						<li class="card-item"><img src="assets/images/footer/payment-icon/5.png" alt="" width="52"></li>
					</ul>
				</div>
				<!-- /.payment-methods --> 
			</div>
		</div>
		<!-- /.container --> 
	</div>
	<!-- /.copyright-bar --> 
</footer>