<?php 

use \App\Http\Controllers\VbiddingController; 
if($pages=="pages.myaccount"){
  $data = VbiddingController::displaywlist(); 
}else
{
  $data = VbiddingController::getDisplayedspacial();
}

$rowData = $data['rowData'];

$lang =  Session::get('lang');

?>


  <?php foreach ($rowData as $row) { ?>
<li class="item">
                        <div class="item-inner">
                          <div class="item-img">
                          <div class="item-img-info"> <a class="product-image" title="<?php echo $row->name_th; ?>" href="{!!url('bidding?view='.$row->products_bid_id) !!}"> 
                            <img alt="<?php echo $row->name_th; ?>" src="{!!url('') !!}/uploads/products/{!!$row->featured_img!!}"> </a>
                              <div class="new-label new-top-left">ปิดประมูล</div>
                              <div class="box-hover">
                                <ul class="add-to-links">
                                  <li><a class="link-wishlist" href="wishlist.php">ซื้อบิด</a> </li>
                                </ul>
                              </div>                             
                           </div>
                          </div>
                          <div class="item-info">
                            <div class="info-inner">
                              <div class="item-title"> <a title="<?php echo $row->name_th; ?>" href="{!!url('bidding?view='.$row->products_bid_id) !!}"> <?php echo $row->name_th; ?> </a> </div>
                              
                <div class="bid-time" id="bid-time_bidding_{!!$row->products_bid_id!!}" data-vid="{!!$row->products_bid_id!!}" data-countdown="<?php echo $row->bid_end; ?>"></div>
                  <div class="bid-end-date_bidding_{!!$row->products_bid_id!!}" style="color:#a00101;">
              
                    <?php 

                      if($row->completed=="1")
                          {
                              
                                  echo Lang::get('auction.2');
                                
                          }else
                          {


                              if(date('Y-m-d',strtotime($row->bid_end))==date('Y-m-d'))
                              {  echo Lang::get('auction.3');  }
                              else
                              { 

                                $datetime1 = new DateTime(date('Y-m-d',strtotime($row->bid_end)));
                                $datetime2 = new DateTime(date('Y-m-d'));
                                $dateinterval = $datetime2->diff($datetime1);

                                $lackdate =  $dateinterval->format('%d');

                                echo Lang::get('auction.4').$lackdate.Lang::get('auction.5');

                              }
                          }
                     ?>      
                  </div>
                              <div class="item-content">
                                
                                <div class="item-price">
                                  <div class="price-box">
                  <p class="special-price"> <span class="price-label">Special Price</span> 

                  <span class="price"> 
                    <label id="product_value_bid_bidding_{!!$row->products_bid_id!!}">
                    @if($row->product_value_bid!="") {!!number_format($row->product_value_bid,2) !!} 
                    <?php else: $bidup = 1.1; ?> 1.00
                    @endif
                    </label> THB 
                  </span> </p>
                                    <p class="old-price"> <span class="price-label">ราคาปกติ</span> <span class="price"> 
                  <?php echo number_format($row->price); ?> THB </span> </p>
                                  </div>
                                </div>
                                <div class="action">
                                  <button class="button btn-cart" type="button" title="" data-original-title=""><span>ประมูลเลย</span> </button> <button class="button2 btn-cart" type="button" title="" data-original-title=""><span>ออโต้บิด</span> </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
</li>
<?php } ?>