<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class orderswinner extends Sximo  {
	
	protected $table = 'orderswinner';
	protected $primaryKey = 'orderNumber';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT orderswinner.* FROM orderswinner  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE orderswinner.orderNumber IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
