<?php

/* * *************************************************
  COMMON FUNCTIONS
 * ************************************************* */

/* * *************************
  COMMON: Data Mining
 * ************************* */
// Halt page and display error with details
function error($error, $file, $line, $extras = false) {
    global $settings, $l;
    $settings['debug'] = true; // SHOULD CREATE GLOBAL SETTINGS?

    $html = '
			<style type="text/css">
			#error {
			FILTER:alpha(opacity=75); OPACITY:.75;
			-KHTML-OPACITY:.75; -MOZ-OPACITY:.75;
			POSITION:fixed; TOP:0px; RIGHT:0px; Z-INDEX:9999;
			COLOR:#fe9b00; BACKGROUND-COLOR:black; PADDING:5px 10px;
			FONT-SIZE:14px; FONT-FAMILY:Courier;
			-WEBKIT-BORDER-RADIUS:7px;
			-MOZ-BORDER-RADIUS:7px;
			BORDER-RADIUS:7px;
			MARGIN:10px;
			LETTER-SPACING: normal; LINE-HEIGHT: normal;}
			#error DT, #error DD {
			MARGIN: 0px; PADDING: 0px;
			LINE-HEIGHT: 15px;}
			#error DT {
			DISPLAY:BLOCK; FONT-WEIGHT:bold;}
			#error DD {
			MARGIN-LEFT:45px;}
			#error OL, #error LI {
			MARGIN:0px; PADDING:0px; LIST-STYLE-POSITION:inside}
			</style>';

    if ($settings['debug']) {
        $html .= '
			<dl id="error">
			<dt>Error:</dt><dd>' . $error . '</dd>
			<dt>File:</dt><dd>' . $file . '</dd>
			<dt>Line:</dt><dd>' . $line . '</dd>';

        if (is_array($extras) || $extras) {
            $html .= "\n\t\t" . '<dt>Other:</dt>';
            if (is_array($extras) && count($extras)) {
                $html .= "\n\t\t\t" . '<dd>' . "\n\t\t\t\t" . '<ol>';
                foreach ($extras as $key => $value) {
                    $html .= "\n\t\t\t\t\t" . '<li><strong>' . $key . '</strong>: <var>' . $value . '</var></li>';
                }
                $html .= "\n\t\t\t\t" . '</ol>' . "\n\t\t\t" . '</dd>';
            }
            if (!is_array($extras) && $extras) {
                $html .= '<dd>' . $extras . '</dd>';
            }
        }

        $html .= '
</dl>';
    } else {
        $html .= '
<p id="error">' . $l['ERROR'] . '</p>';
    }

    die($html);
}

//CLEANS VALUES BEFORE BEING PASSED TO THE SQL
function clean($value)
{
	// Stripslashes
	if (get_magic_quotes_gpc())  {
		$value = stripslashes($value);
	}
	// Quote if not a number
	if (!is_numeric($value)) {
		$value = stripslashes(str_replace('\r\n', '', mysql_real_escape_string($value)));
	}
	
	return trim($value);
}

//GET THE CONTENT BETWEEN TWO DEFINED VARS
function GetBetween($content, $start, $end) {
    $r = explode($start, $content);
    if (isset($r[1])) {
        $r = explode($end, $r[1]);
        return $r[0];
    }
    return '';
}

function sentence_case($string) { 
    $sentences = preg_split('/([.?!]+)/', $string, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE); 
    $new_string = ''; 
    foreach ($sentences as $key => $sentence) { 
        $new_string .= ($key & 1) == 0? 
            ucfirst(strtolower(trim($sentence))) : 
            $sentence.' '; 
    } 
    return trim($new_string); 
} 

// Return name of file from complete filename
function filename($file) {
    $name = substr($file, 0, strrpos($file, '.'));
    return ($name) ? $name : $file;
}

// Return extension of file from complete filename
function fileext($file) {
    return substr($file, strrpos($file, '.'));
}

// Truncate text
function truncate($text, $numChar, $append = '') {
    if (strlen($text) > $numChar) {
        $text = substr($text, 0, $numChar - strlen($append));
        $text .= $append;
    }
    return $text;
}

// Duplicate a database record
// Source: http://www.epigroove.com/posts/79/how_to_duplicate_a_record_in_mysql_using_php
function mysql_duplicate_row($table, $search_arr, $new_id_col, $exceptions = array(), $appends = array()) {
    // prepare exceptions, if any;
    if (!is_array($exceptions)) {
        $exceptions = ($exceptions) ? array($exceptions => '') : array();
    }
    // prepare appends, if any;
    if (!is_array($appends)) {
        $appends = (!count($appends)) ? array() : $appends;
    }

    // build query to search for original record
    $query = "SELECT * FROM $table WHERE ";
    foreach ($search_arr as $key => $value) {
        $query .= '`' . $key . '` = "' . str_replace('"', '\"', $value) . '" AND ';
    }
    $query = substr($query, 0, strlen($query) - 5); # lop off the extra trailing ADD
    // load the original record into an array
    $result = mysql_query($query) or error(mysql_error(), __FILE__, __LINE__);
    $original_record = mysql_fetch_assoc($result);

    if ($original_record) {
        // insert the new record and get the new auto_increment id
        mysql_query("INSERT INTO $table () VALUES ()") or error(mysql_error(), __FILE__, __LINE__);
        $new_id = mysql_insert_id();

        // generate the query to update the new record with the previous values
        $query = "UPDATE $table SET ";
        foreach ($original_record as $key => $value) {
            // we musn't duplicate the id field
            if ($key != $new_id_col) {
                // if key is an exception, use the exception value
                if (array_key_exists($key, $exceptions)) {
                    $value = $exceptions[$key];
                }
                // if key has an append and value is not blank
                if (array_key_exists($key, $appends) && $value) {
                    $value = $value . $appends[$key];
                }
                // build an insert into the query
                $query .= '`' . $key . '` = "' . str_replace('"', '\"', $value) . '", ';
            }
        }
        $query = substr($query, 0, strlen($query) - 2); # lop off the extra trailing comma
        $query .= " WHERE $new_id_col=$new_id";
        mysql_query($query) or error(mysql_error(), __FILE__, __LINE__, $query);

        // return the new id
        return $new_id;
    }
    return false;
}

/* * *************************
  COMMON: Parse & Format
 * ************************* */

// Get the indent of INPUT following a SAMP
// Explanation: Search global.css for 'samp'
function get_indent($text) {
    if ($text)
        return (strlen($text) * 7 + 10 + 2) . 'px';
    else
        return false;
}

// Format user input for HTML output
function format_user_input($input) {
    $allowed_tags = array('b', 'strong', 'i', 'em', 'code', 'pre');
    $search = array("\t");
    $replace = array("<span></span>");
    foreach ($allowed_tags as $tag) {
        $search[] = '{' . $tag . '}';
        $replace[] = '<' . $tag . '>';
        $search[] = '{/' . $tag . '}';
        $replace[] = '</' . $tag . '>';
    }

    $output = $input;
    $output = htmlspecialchars($output);
    $output = nl2br($output);
    $output = str_replace($search, $replace, $output);

    return $output;
}

// Revert URL-Safe Filename to Visually-Appealing String
function filenametostr($filename) {
    $string = htmlspecialchars_decode($filename);

    $search = array('-', '_');
    $replace = array(' ', ' ');
    $string = str_replace($search, $replace, $string);

    $string = ucwords($string);

    return $string;
}

/* * *************************************************
  SPECIAL CLASSES
 * ************************************************* */

/* PDF SEARCH CLASS */

class pdf_search {

    // Just one private variable.
    // It holds the document.
    var $_buffer;

    // Constructor. Takes the pdf document as only parameter
    function pdf_search($buffer) {
        $this->_buffer = $buffer;
    }

    // This function returns the next line from the document.
    // If a stream follows, it is deflated into readable text.
    function nextline() {
        $pos = strpos($this->_buffer, "\r");
        if ($pos === false) {
            return false;
        }
        $line = substr($this->_buffer, 0, $pos);
        $this->_buffer = substr($this->_buffer, $pos + 1);
        if ($line == "stream") {
            $endpos = strpos($this->_buffer, "endstream");
            $stream = substr($this->_buffer, 1, $endpos - 1);
            $stream = @gzuncompress($stream);
            $this->_buffer = $stream . substr($this->_buffer, $endpos + 9);
        }
        return $line;
    }

    // This function returns the next line in the document that is printable text.
    // We need it so we can search in just that portion.
    function textline() {
        $line = $this->nextline();
        if ($line === false) {
            return false;
        }
        if (preg_match("/[^\\\\]\\((.+)[^\\\\]\\)/", $line, $match)) {
            $line = preg_replace("/\\\\(\d+)/e", "chr(0\\1);", $match[1]);
            return stripslashes($line);
        }
        return $this->textline();
    }

    // This function returns true or false, indicating whether the document contains
    // the text that is passed in $str.
    function textfound($str) {
        while (($line = $this->textline()) !== false) {
            if (preg_match("/$str/i", $line) != 0) {
                return true;
            }
        }
        return false;
    }

}

class enCodePass {
    ###############Gen Key Password#################

    var $hash_key = 'fdfdsfdsafdsa4654fsdafdsf65456231fsdafd687/7998789fdsa5646asdfasdfsa';  //  รหัสพิเศษ ที่จะเอาไปใส่ร่วมกับ encode ให้เปลี่ยนไปในแต่ละเว็บ

    // encode
    function encrypted($string) {
        $key = $this->hash_key;
        return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, md5(md5($key))));
    }

    // decode
    function decrypted($encrypted) {
        $key = $this->hash_key;
        return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($encrypted), MCRYPT_MODE_CBC, md5(md5($key))), "\0");
    }

}

/* * ******************
  DB Template
 * ******************* */

function getDesc($field, $table, $where, $debug = "") {
    $sql = "SELECT $field FROM $table WHERE $where";
	if ($debug == "") {
		$query = mysql_query($sql) or error(mysql_error(), __FILE__, __LINE__, $query);
		$num = mysql_num_rows($query);
		if ($num > 0) {
			$row = mysql_fetch_assoc($query);		
			return $row["$field"];
		} else {
			return "";
		}
	}else{
		return $sql;
	}
}

function countDesc($field, $table, $where, $debug = "") {
    $sql = "SELECT $field FROM $table WHERE $where";
    if ($debug == "") {
        $query = mysql_query("SELECT $field FROM $table WHERE $where") or error(mysql_error(), __FILE__, __LINE__, $query);
        $num = mysql_num_rows($query);
        if ($num > 0) {
            return $num;
        } else {
            return "0";
        }
    } else {
        return $sql;
    }
}

function countGetDesc($field, $table, $where) {
    $query = mysql_query("SELECT $field FROM $table WHERE $where") or error(mysql_error(), __FILE__, __LINE__, $query);
    $row = mysql_fetch_assoc($query);
    $num = mysql_num_rows($query);
    $arr = array('num' => $num, 'val' => $row["$field"]);
    if ($num > 0) {
        return $arr;
    } else {
        return array('num' => '0');
    }
}

function getSum($field, $table, $where = '', $group = '') {
    if ($where != "") {
        $where = " WHERE $where";
    } else {
        $where = "";
    }
    if ($group != "") {
        $group = " GROUP BY $group";
    } else {
        $group = "";
    }
    $query = mysql_query("SELECT SUM($field) AS sum FROM " . $table . $where . $group) or error(mysql_error(), __FILE__, __LINE__, $query);
    $row = mysql_fetch_assoc($query);
    $num = mysql_num_rows($query);
    if ($num > 0) {
        return $row["sum"];
    } else {
        return "&nbsp;";
    }
}

function getDescArr($field, $table, $where = '', $order = '', $limit = '', $debug = '') {
    if ($where != "") {
        $where = " WHERE $where";
    } else {
        $where = "";
    }
    if ($order != "") {
        $order = " ORDER BY $order";
    } else {
        $order = "";
    }
    if ($limit != "") {
        $limit = " LIMIT $limit";
    } else {
        $limit = "";
    }
	$sql = "SELECT $field FROM " . $table . $where . $order . $limit;
	if($debug == ""){
		$query = mysql_query($sql);
		$num = mysql_num_rows($query);
		if ($num > 0) {
			$row = mysql_fetch_assoc($query);		
			return $row;
		} else {
			return "&nbsp;";
		}
	}else{
		return $sql;
	}
}

function insertSQL($arrData = "", $table = "", $debug = "") {
    $myFieldName = "";
    $myFieldValue = "";

    if ($arrData == "")
        $arrData = $data;
    if ($table == "")
        $table = $myTable;

    foreach ($arrData as $key => $value) {
        $myFieldName .= $key . " , ";
        if (is_array($value)) {
            foreach ($value as $value2) {
                if ($value2 == "NOW()") {
                    $myFieldValue .= "" . $value2 . " , ";
                } else {
                    $myFieldValue .= "\"" . $value2 . "\" , ";
                }
            }
        } else {
            if ($value == "NOW()") {
                $myFieldValue .= "" . $value . " , ";
            } else {
                $myFieldValue .= "\"" . $value . "\" , ";
            }
        }
    }
    unset($data, $myTable);
    $qrs = "INSERT INTO " . $table . " ( " . substr($myFieldName, 0, $myFieldName - 2) . " ) VALUES ( " . substr($myFieldValue, 0, $myFieldValue - 2) . " ) ";
    if ($debug == "") {
        $rs = mysql_query($qrs);
        if ($rs) {
            return mysql_insert_id();
        } else {
            return "Error Query : " . $qrs;
        }
    } else {
        return $qrs;
    }
    unset($myFieldName);
    unset($myFieldValue);
}

function updateSQL($arrData = "", $arrKey = "", $table = "", $debug = '') {

    $myTableName = $table;

    if ($arrData == "")
        $arrData = $data;
    if ($arrKey == "")
        $arrKey = $myWhere;
    if ($table == "")
        $table = $myTable;

    foreach ($arrData as $key => $value) {
        if ($value == "NOW()") {
            $myUpdate .= $key . " = " . $value . " , ";
        } else {
            $myUpdate .= $key . " = \"" . $value . "\" , ";
        }
    }

    foreach ($arrKey as $key => $value) {
        if (count($arrKey) == 1) {
            $myWhereCauseKeyID .= $key . " = \"" . $value . "\"";
        } else {
            $myWhereCauseKeyID .= $key . " = \"" . $value . "\" AND ";
        }
    }


    $sql = "UPDATE " . $table . " SET " . substr($myUpdate, 0, -2) . " WHERE " . $myWhereCauseKeyID . " ";
    if ($debug == "") {
        mysql_query($sql) or error(mysql_error(), __FILE__, __LINE__, $query);
    } else {
        return $sql;
    }
    unset($myUpdate);
    unset($myWhereCauseKeyID);
    unset($data, $myWhere, $myTable);
}

function optionSelect($val, $text, $table, $where, $select = '') {
	if($where != ''){
		$where = "WHERE $where";
	}
	$sql = "SELECT * FROM $table $where";
    $query = mysql_query($sql) or error(mysql_error(), __FILE__, __LINE__, $query);
    $num = mysql_num_rows($query);
    if ($num > 0) {
    	$opt = '';
		while($row = mysql_fetch_assoc($query)){
			$selected = '';
			if($select != '' && $select == $row["$val"]){
				$selected = 'selected="selected"';
			}
			$opt .= '<option value="'.$row["$val"].'" '.$selected.'>'.$row["$text"].'</option>';
		}
	    return $opt;
    } else {
        return "&nbsp;";
    }
}

function getWidget($ref){
	global $ln;
	$widget = getDescArr("cms_widget_ln.wg_content,cms_widget.wg_status","cms_widget, cms_widget_ln","cms_widget.wg_id=cms_widget_ln.wg_id AND cms_widget_ln.wg_ref='$ref' AND cms_widget_ln.ln='$ln'");
	if($widget['wg_status']==96){
		return $widget['wg_content'];
	}else{
		return "";
	}
	return $widget;
}

function getGroup($field,$id){
	global $ln;
	return getDesc("$field","cms_group,cms_group_ln","cms_group.grp_id=cms_group_ln.grp_id AND cms_group.grp_id='$id' AND cms_group_ln.ln='".$ln."'");
}

function mysql_escape_gpc($dirty)
{
    if (ini_get('magic_quotes_gpc'))
    {
        return $dirty;
    }
    else
    {
        return mysql_real_escape_string($dirty);
    }
}

function getGroupEst($field,$id){
	global $ln;
	return getDesc($field,"est_group,est_group_ln","est_group.grp_id=est_group_ln.grp_id AND est_group_ln.ln='$ln' AND est_group.grp_id='$id'");
}
function getTypeEst($field,$id){
	global $ln;
	return getDesc($field,"est_type,est_type_ln","est_type.typ_id=est_type_ln.typ_id AND est_type_ln.ln='$ln' AND est_type.typ_id='$id'");
}
/* * ******************
  END DB Template
 * ******************* */

/* * ******************
  PAGE NAVIGATOR
 * ******************* */

function page_navigator($before_p, $plus_p, $total, $total_p, $chk_page, $url, $Prev, $Next) {
    global $urlquery_str;
    $pPrev = $chk_page - 1;
    $pPrev = ($pPrev >= 0) ? $pPrev : 0;
    $pNext = $chk_page + 1;
    $pNext = ($pNext >= $total_p) ? $total_p - 1 : $pNext;
    $lt_page = $total_p - 4;
    if ($chk_page > 0) {

        echo "<a  href='" . $url . "&s_page=$pPrev&urlquery_str=" . $urlquery_str . "' class='naviPN'>$Prev</a>";
    }
    if ($total_p >= 11) {
        if ($chk_page >= 4) {
            echo "<a $nClass href='" . $url . "&s_page=0&urlquery_str=" . $urlquery_str . "'>1</a><a class='SpaceC'>. . .</a>";
        }
        if ($chk_page < 4) {
            for ($i = 0; $i < $total_p; $i++) {
                $nClass = ($chk_page == $i) ? "class='selectPage'" : "";
                if ($i <= 4) {
                    echo "<a $nClass href='" . $url . "&s_page=$i&urlquery_str=" . $urlquery_str . "'>" . intval($i + 1) . "</a> ";
                }
                if ($i == $total_p - 1) {
                    echo "<a class='SpaceC'>. . .</a><a $nClass href='" . $url . "&s_page=$i&urlquery_str=" . $urlquery_str . "'>" . intval($i + 1) . "</a> ";
                }
            }
        }
        if ($chk_page >= 4 && $chk_page < $lt_page) {
            $st_page = $chk_page - 3;
            for ($i = 1; $i <= 5; $i++) {
                $nClass = ($chk_page == ($st_page + $i)) ? "class='selectPage'" : "";
                echo "<a $nClass href='" . $url . "&s_page=" . intval($st_page + $i) . "'>" . intval($st_page + $i + 1) . "</a> ";
            }
            for ($i = 0; $i < $total_p; $i++) {
                if ($i == $total_p - 1) {
                    $nClass = ($chk_page == $i) ? "class='selectPage'" : "";
                    echo "<a class='SpaceC'>. . .</a><a $nClass href='" . $url . "&s_page=$i&urlquery_str=" . $urlquery_str . "'>" . intval($i + 1) . "</a> ";
                }
            }
        }
        if ($chk_page >= $lt_page) {
            for ($i = 0; $i <= 4; $i++) {
                $nClass = ($chk_page == ($lt_page + $i - 1)) ? "class='selectPage'" : "";
                echo "<a $nClass href='" . $url . "&s_page=" . intval($lt_page + $i - 1) . "'>" . intval($lt_page + $i) . "</a> ";
            }
        }
    } else {
        for ($i = 0; $i < $total_p; $i++) {
            $nClass = ($chk_page == $i) ? "class='selectPage'" : "";
            echo "<a href='" . $url . "&s_page=$i&urlquery_str=" . $urlquery_str . "' $nClass  >" . intval($i + 1) . "</a> ";
        }
    }
    if ($chk_page < $total_p - 1) {
        echo "<a href='" . $url . "&s_page=$pNext&urlquery_str=" . $urlquery_str . "'  class='naviPN'>$Next</a>";
    }
}

function page_navigator_boostarp($before_p, $plus_p, $total, $total_p, $chk_page, $url, $Prev, $Next) {
    $pPrev = $chk_page - 1;
    $pPrev = ($pPrev >= 0) ? $pPrev : 0;
    $pNext = $chk_page + 1;
    $pNext = ($pNext >= $total_p) ? $total_p - 1 : $pNext;
    $lt_page = $total_p - 4;
    if ($chk_page > 0) {

        echo "<li><a  href='" . $url . "&s_page=$pPrev' class='naviPN' aria-label='$Prev'><span aria-hidden='true'>&laquo;</span></a></li>";
    }
    if ($total_p >= 11) {
        if ($chk_page >= 4) {
            echo "<li><a $nClass href='" . $url . "&s_page=0'>1</a></li><li><a class='SpaceC'>. . .</a></li>";
        }
        if ($chk_page < 4) {
            for ($i = 0; $i < $total_p; $i++) {
                $nClass = ($chk_page == $i) ? "class='selectPage'" : "";
                if ($i <= 4) {
                    echo "<li><a $nClass href='" . $url . "&s_page=$i'>" . intval($i + 1) . "</a></li>";
                }
                if ($i == $total_p - 1) {
                    echo "<li><a class='SpaceC'>. . .</a></li><li><a $nClass href='" . $url . "&s_page=$i'>" . intval($i + 1) . "</a></li>";
                }
            }
        }
        if ($chk_page >= 4 && $chk_page < $lt_page) {
            $st_page = $chk_page - 3;
            for ($i = 1; $i <= 5; $i++) {
                $nClass = ($chk_page == ($st_page + $i)) ? "class='selectPage'" : "";
                echo "<li><a $nClass href='" . $url . "&s_page=" . intval($st_page + $i) . "'>" . intval($st_page + $i + 1) . "</a></li>";
            }
            for ($i = 0; $i < $total_p; $i++) {
                if ($i == $total_p - 1) {
                    $nClass = ($chk_page == $i) ? "class='selectPage'" : "";
                    echo "<li><a class='SpaceC'>. . .</a></li><li><a $nClass href='" . $url . "&s_page=$i'>" . intval($i + 1) . "</a></li>";
                }
            }
        }
        if ($chk_page >= $lt_page) {
            for ($i = 0; $i <= 4; $i++) {
                $nClass = ($chk_page == ($lt_page + $i - 1)) ? "class='selectPage'" : "";
                echo "<li><a $nClass href='" . $url . "&s_page=" . intval($lt_page + $i - 1) . "'>" . intval($lt_page + $i) . "</a></li>";
            }
        }
    } else {
        for ($i = 0; $i < $total_p; $i++) {
            $nClass = ($chk_page == $i) ? "class='selectPage'" : "";
            echo "<li><a href='" . $url . "&s_page=$i' $nClass  >" . intval($i + 1) . "</a></li>";
        }
    }
    if ($chk_page < $total_p - 1) {
        echo "<li><a href='" . $url . "&s_page=$pNext'  class='naviPN' aria-label='$Next'><span aria-hidden='true'>&raquo;</span></a></li>";
    }
}

/* * ******************
  END PAGE NAVIGATOR
 * ******************* */

/***********************************************************************
			SHIPPING CAL
***********************************************************************/
function ShipCal($carrier,$weight,$country_id,$soid='',$shpid=''){
	$tbl='cr_so';	
	$extraprice = 0;
	/*if($shpid != ""){
		$getRes = getDesc("so_shp_regis","cr_so_shp","so_shp_id='$shpid'");
		if($getRes == 1){
			$extraprice += 70;
		}
	}*/
	
	if ($soid!=''){
		//GET SHIP PRICE ID FROM SOID
		$priceid = getDesc("so_shp_price_id",$tbl,"soid='$soid'");
	}else{
		$ratre = getDescArr("priceid","cr_shp_price","","priceid DESC","0,1");
		$priceid = $ratre['priceid'];
	}
	//GET USD AND FULE
	
	$price = getDescArr("*","cr_shp_price","priceid='$priceid'","","");
	$saledate = getDesc("so_saledate","cr_so","soid='$soid'");
	$ckeff_date = "";
	if($soid != ""){
		$ckeff_date = " AND eff_date <= '$saledate'";
	}else{
		$ckeff_date = " AND eff_date <= '".date("Y-m-d")."'";
	}
	
	//GET ZONE CUSTOMER
	$country = getDescArr("*","cr_countries","country_id='$country_id'","","");
	
	if($carrier == 1){ //DHL
		
		//SELECT RATE DHL
		if ($weight<20.5){
			$operator=">=";
			$order="ASC";
		}else{
			$operator="<=";
			$order="DESC";
		}
		
		
		$sql_rate = mysql_query("SELECT zone".$country['zone_ex'].", every FROM cr_shp_rate WHERE weight".$operator."'$weight' AND expid='1' ORDER BY weight ".$order."");
		$num_rate = mysql_num_rows($sql_rate);	
		if($num_rate > 0){		
			$rate = mysql_fetch_assoc($sql_rate);	
			$ratezone = $rate['zone'.$country['zone_ex']];
			if($rate['every'] != 1){			
				$sumthb = ($ratezone * $price['fule'] * 1.07) + $extraprice;
				$sumusd = $sumthb / $price['usd_rate'];
			}else{
				$sumthb = (($ratezone * $weight) * $price['fule'] * 1.07) + $extraprice;
				$sumusd = $sumthb / $price['usd_rate'];
			}
		}else{
			$sql_air = mysql_query("SELECT zone".$country['zone_air']." FROM cr_shp_rate WHERE expid='1' AND every != '1' ORDER BY zone".$country['zone_air']." ASC");
			$rate_air = mysql_fetch_assoc($sql_air);
			$ratezone = $rate_air['zone'.$country['zone_ex']];
			$sumthb = ($ratezone * $price['fule'] * 1.07) + $extraprice;
			$sumusd = $sumthb / $price['usd_rate'];
		}
	}elseif($carrier == 2){
		//SELECT RATE
		//echo "SELECT zone".$country['zone_gm'].", every FROM cr_shp_rate WHERE weight>='$weight' AND expid='2' AND status = '1' ORDER BY weight ASC";
		//echo "SELECT zone".$country['zone_gm'].", every, plus_rate FROM cr_shp_rate WHERE weight>='$weight' AND expid='2' AND status = '1' AND eff_date <= '$saledate' ORDER BY weight ASC, eff_date DESC LIMIT 1";
		$sql_rate = mysql_query("SELECT zone".$country['zone_gm'].", every, plus_rate FROM cr_shp_rate WHERE weight>='$weight' AND expid='2' AND status = '1' $ckeff_date ORDER BY weight ASC, eff_date DESC LIMIT 1");
		$num_rate = mysql_num_rows($sql_rate);	
		if($num_rate > 0){	
			$rate = mysql_fetch_assoc($sql_rate);	
			if($shpid != ""){
				$getRes = getDesc("so_shp_regis","cr_so_shp","so_shp_id='$shpid'");
				if($getRes == 1){
					$extraprice += $rate['plus_rate'];
				}
			}
			
			$ratezone = $rate['zone'.$country['zone_gm']];	
			if($rate['every'] != 1){			
				$sumthb = ($ratezone * 1.07) + $extraprice;
				$sumusd = $sumthb / $price['usd_rate'];
			}else{
				$sumthb = ($ratezone * $weight * 1.07) + $extraprice;
				$sumusd = $sumthb / $price['usd_rate'];
			}
		}else{
			$sql_air = mysql_query("SELECT zone".$country['zone_gm'].", plus_rate FROM cr_shp_rate WHERE expid='2' AND every != '1' AND status = '1' ORDER BY zone".$country['zone_gm']." ASC");
			$rate_air = mysql_fetch_assoc($sql_air);
				
			if($shpid != ""){
				$getRes = getDesc("so_shp_regis","cr_so_shp","so_shp_id='$shpid'");
				if($getRes == 1){
					$extraprice += $rate_air['plus_rate'];
				}
			}
			
			$ratezone = $rate_air['zone'.$country['zone_gm']];
			$sumthb = ($ratezone * 1.07) + $extraprice;
			$sumusd = $sumthb / $price['usd_rate'];

		}
	}elseif($carrier == 4){
		//SELECT RATE
		$sql_rate = mysql_query("SELECT zone".$country['zone_air'].", every FROM cr_shp_rate WHERE weight<='$weight' AND expid='4' ORDER BY rateid DESC LIMIT 0,2");
		$num_rate = mysql_num_rows($sql_rate);	
		if($num_rate > 0){		
			while($rate = mysql_fetch_assoc($sql_rate)){
				$ratesm[] = $rate['zone'.$country['zone_air']];
				$every[] = $rate['every'];
			}			
			$ratezone = $ratesm[0];
			$ratemax = $ratesm[1];
			if($every[0] != 1){			
				$sumthb = ($ratezone * 1.07) + $extraprice;
				$sumusd = $sumthb / $price['usd_rate'];
			}else{
				$sumthb = ((($ratezone * (($weight - 20) * 2)) + $ratemax) * 1.07) + $extraprice;
				$sumusd = $sumthb / $price['usd_rate'];
			}
		}else{
			$sql_air = mysql_query("SELECT zone".$country['zone_air']." FROM cr_shp_rate WHERE expid='4' AND every != '1' ORDER BY zone".$country['zone_air']." ASC");
			$rate_air = mysql_fetch_assoc($sql_air);
			$ratezone = $rate_air['zone'.$country['zone_air']];
			$sumthb = ($ratezone * 1.07) + $extraprice;
			$sumusd = $sumthb / $price['usd_rate'];
		}
	}
	
	$arrData = array(
		'ratethb' => str_replace(",","",number_format($sumthb,2)),
		'rateusd' => str_replace(",","",number_format($sumusd,2)),
		'debug' => $ratezone
	);
	
	return $arrData;
}

/* * ******************
  FUNCTIONS
 * ******************* */
 
function full_url() {
	$s = empty($_SERVER["HTTPS"]) ? '' : ($_SERVER["HTTPS"] == "on") ? "s" : "";
	$sp = strtolower($_SERVER["SERVER_PROTOCOL"]);
	$protocol = substr($sp, 0, strpos($sp, "/")) . $s;
	$port = ($_SERVER["SERVER_PORT"] == "80") ? "" : (":" . $_SERVER["SERVER_PORT"]);
	return $protocol . "://" . $_SERVER['SERVER_NAME'] . $port . rawurldecode($_SERVER['REQUEST_URI']);
}  

function to_upper($name) {
    $name = ucwords(strtolower($name));
    $arr = explode('-', $name);
    $name = array();
    foreach ($arr as $v) {
        $name[] = ucfirst($v);
    }
    $name = implode('-', $name);
    return $name;
}

function randomAlphaNum($length = 8){ 
	$rangeMin = pow(36, $length-1); //smallest number to give length digits in base 36 
	$rangeMax = pow(36, $length)-1; //largest number to give length digits in base 36 
	$base10Rand = mt_rand($rangeMin, $rangeMax); //get the random number 
	$newRand = base_convert($base10Rand, 10, 36); //convert it 
	
	return $newRand; //spit it out	
}

function get_code($seed_length = 8) {
	$seed = "ABCDEFGHJKLMNPQRSTUVWXYZ234567892345678923456789";
	$str = '';
	srand((double) microtime() * 1000000);
	for ($i = 0; $i < $seed_length; $i++) {
		$str .= substr($seed, rand() % 48, 1);
	}
	return $str;
}
	
function short_text($str, $limit) {
    $charset = 'UTF-8';
    if (mb_strlen($str, $charset) > $limit) {
        return $string = mb_substr($str, 0, $limit, $charset) . '...';
        //return mb_strlen($str, $charset).' > '.$limit;
    } else {
        return $str;
    }
}

function seotitle($raw) {
    $raw = @preg_replace('#[^-ก-๙a-zA-Z0-9]#u', '-', $raw);
    $raw = @ereg_replace("-+", "-", $raw);
    if (substr($raw, 0, 1) == '-')
        $raw = substr($raw, 1);
    $raw = trim($raw);
    $raw = trim($raw, "-");
    return strtolower(trim($raw));
}

//clean the user's input
function cleanInput($value, $link = '') {
    //if the variable is an array, recurse into it
    if (is_array($value)) {
        //for each element in the array...
        foreach ($value as $key => $val) {
            //...clean the content of each variable in the array
            $value[$key] = clean($val);
            //$value[$key] = cleanInput($val);
        }
        //return clean array
        return $value;
    } else {
        return mysql_real_escape_string(strip_tags(trim($value)), $link);
    }
}

function cleanOutput($value, $link = '') {
    //if the variable is an array, recurse into it
    if (is_array($value)) {
        //for each element in the array...
        foreach ($value as $key => $val) {
            //...clean the content of each variable in the array
            $value[$key] = htmlspecialchars($val, ENT_QUOTES, "UTF-8");
        }
        //return clean array
        return $value;
    } else {
        return htmlspecialchars($value);
    }
}

function fetchAssoc($query) {
    $row = mysql_fetch_assoc($query);
    return cleanOutput($row);
}

//GET ENUM VALUES
function getEnum($table, $field) {
    $sql = "
        SELECT COLUMN_TYPE 
        FROM INFORMATION_SCHEMA.COLUMNS
        WHERE TABLE_NAME = '" . mysql_real_escape_string($table) . "' 
            AND COLUMN_NAME = '" . mysql_real_escape_string($field) . "'
		";
    $result = mysql_query($sql) or die(mysql_error());
    $row = mysql_fetch_array($result);
    $enum_list = explode(",", str_replace("'", "", substr($row['COLUMN_TYPE'], 5, (strlen($row['COLUMN_TYPE']) - 6))));
    return $enum_list;
}

function getExtension($str) {
    $i = strrpos($str, ".");
    if (!$i) {
        return "";
    }
    $l = strlen($str) - $i;
    $ext = substr($str, $i + 1, $l);
    return strtolower($ext);
}

$thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");   
$thai_month_arr=array(   
    "0"=>"",   
    "1"=>"มกราคม",   
    "2"=>"กุมภาพันธ์",   
    "3"=>"มีนาคม",   
    "4"=>"เมษายน",   
    "5"=>"พฤษภาคม",   
    "6"=>"มิถุนายน",    
    "7"=>"กรกฎาคม",   
    "8"=>"สิงหาคม",   
    "9"=>"กันยายน",   
    "10"=>"ตุลาคม",   
    "11"=>"พฤศจิกายน",   
    "12"=>"ธันวาคม"                     
);   
$thai_month_arr_short=array(   
    "0"=>"",   
    "1"=>"ม.ค.",   
    "2"=>"ก.พ.",   
    "3"=>"มี.ค.",   
    "4"=>"เม.ย.",   
    "5"=>"พ.ค.",   
    "6"=>"มิ.ย.",    
    "7"=>"ก.ค.",   
    "8"=>"ส.ค.",   
    "9"=>"ก.ย.",   
    "10"=>"ต.ค.",   
    "11"=>"พ.ย.",   
    "12"=>"ธ.ค."                     
);   
function thai_date_and_time($time){   // 19 ธันวาคม 2556 เวลา 10:10:43
    global $thai_day_arr,$thai_month_arr;   
    $thai_date_return.=date("j",strtotime($time));   
    $thai_date_return.=" ".$thai_month_arr[date("n",strtotime($time))];   
    $thai_date_return.= " ".(date("Y",strtotime($time))+543);   
	$thai_date_return.= " เวลา ".date("H:i:s",strtotime($time));
    return $thai_date_return;   
} 
function thai_date_and_time_short($time){   // 19  ธ.ค. 2556 10:10:4
    global $thai_day_arr,$thai_month_arr_short;   
    $thai_date_return.=date("j",strtotime($time));   
    $thai_date_return.="&nbsp;&nbsp;".$thai_month_arr_short[date("n",strtotime($time))];   
    $thai_date_return.= " ".(date("Y",strtotime($time))+543);   
	$thai_date_return.= " ".date("H:i",strtotime($time));
    return $thai_date_return;   
} 
function thai_date_short($time){   // 19  ธ.ค. 2556
    global $thai_day_arr,$thai_month_arr_short;   
    $thai_date_return.=date("j",strtotime($time));   
    $thai_date_return.="&nbsp;&nbsp;".$thai_month_arr_short[date("n",strtotime($time))];   
    $thai_date_return.= " ".(date("Y",strtotime($time))+543);   
    return $thai_date_return;   
}
function thai_date_fullmonth($time){   // 19 ธันวาคม 2556
    global $thai_day_arr,$thai_month_arr;   
    $thai_date_return.=date("j",strtotime($time));   
    $thai_date_return.=" ".$thai_month_arr[date("n",strtotime($time))];   
    $thai_date_return.= " ".(date("Y",strtotime($time))+543);   
    return $thai_date_return;   
} 
function thai_date_short_number($time){   // 19-12-56
    global $thai_day_arr,$thai_month_arr;   
    $thai_date_return.=date("d",strtotime($time));   
    $thai_date_return.="-".date("m",strtotime($time));   
    $thai_date_return.= "-".substr((date("Y",strtotime($time))+543),-2);   
    return $thai_date_return;   
}
function thai_date_fullday($time){   // 19 ธันวาคม 2556
    global $thai_day_arr,$thai_month_arr;   
    $thai_date_return.=$thai_day_arr[date("w",strtotime($time))];
	$thai_date_return.=" ที่ ".date("j",strtotime($time));   
    $thai_date_return.=" ".$thai_month_arr[date("n",strtotime($time))];   
    $thai_date_return.= " ".(date("Y",strtotime($time))+543);   
    return $thai_date_return;   
}
function thai_date_fullday_time($time){   // 19 ธันวาคม 2556
    global $thai_day_arr,$thai_month_arr;   
    $thai_date_return.=$thai_day_arr[date("w",strtotime($time))];
	$thai_date_return.=" ที่ ".date("j",strtotime($time));   
    $thai_date_return.=" ".$thai_month_arr[date("n",strtotime($time))];   
    $thai_date_return.= " ".(date("Y",strtotime($time))+543);   
	$thai_date_return.= " เวลา ".date("H:i",strtotime($time));
    return $thai_date_return;   
} 

function ShowDateLongTime($myDate) {
    $myTimeArray = explode(" ", $myDate);
    $myTime = explode(":", $myTimeArray[1]);
    $myDateArray = explode("-", $myDate);
    $myDay = sprintf("%d", $myDateArray[2]);
    switch ($myDateArray[1]) {
        case "01" : $myMonth = "มกราคม";
            break;
        case "02" : $myMonth = "กุมภาพันธ์";
            break;
        case "03" : $myMonth = "มีนาคม";
            break;
        case "04" : $myMonth = "เมษายน";
            break;
        case "05" : $myMonth = "พฤษภาคม";
            break;
        case "06" : $myMonth = "มิถุนายน";
            break;
        case "07" : $myMonth = "กรกฎาคม";
            break;
        case "08" : $myMonth = "สิงหาคม";
            break;
        case "09" : $myMonth = "กันยายน";
            break;
        case "10" : $myMonth = "ตุลาคม";
            break;
        case "11" : $myMonth = "พฤศจิกายน";
            break;
        case "12" : $myMonth = "ธันวาคม";
            break;
    }
    $myYear = sprintf("%d", $myDateArray[0]) + 543;
    return($myDay . " " . $myMonth . " " . $myYear . ", " . $myTime[0] . ":" . $myTime[1]);
}

function ShowDateLong($myDate) {
    $myTimeArray = explode(" ", $myDate);
    $myTime = explode(":", $myTimeArray[1]);
    $myDateArray = explode("-", $myDate);
    $myDay = sprintf("%d", $myDateArray[2]);
    switch ($myDateArray[1]) {
        case "01" : $myMonth = "มกราคม";
            break;
        case "02" : $myMonth = "กุมภาพันธ์";
            break;
        case "03" : $myMonth = "มีนาคม";
            break;
        case "04" : $myMonth = "เมษายน";
            break;
        case "05" : $myMonth = "พฤษภาคม";
            break;
        case "06" : $myMonth = "มิถุนายน";
            break;
        case "07" : $myMonth = "กรกฎาคม";
            break;
        case "08" : $myMonth = "สิงหาคม";
            break;
        case "09" : $myMonth = "กันยายน";
            break;
        case "10" : $myMonth = "ตุลาคม";
            break;
        case "11" : $myMonth = "พฤศจิกายน";
            break;
        case "12" : $myMonth = "ธันวาคม";
            break;
    }
    $myYear = sprintf("%d", $myDateArray[0]) + 543;
    return($myDay . " " . $myMonth . " " . $myYear);
}

function ShowDateLongThDay($myDate) {
    $myDateArray = explode("-", $myDate);
    $myDay = sprintf("%d", $myDateArray[2]);
    switch ($myDateArray[1]) {
        case "01" : $myMonth = "มกราคม";
            break;
        case "02" : $myMonth = "กุมภาพันธ์";
            break;
        case "03" : $myMonth = "มีนาคม";
            break;
        case "04" : $myMonth = "เมษายน";
            break;
        case "05" : $myMonth = "พฤษภาคม";
            break;
        case "06" : $myMonth = "มิถุนายน";
            break;
        case "07" : $myMonth = "กรกฎาคม";
            break;
        case "08" : $myMonth = "สิงหาคม";
            break;
        case "09" : $myMonth = "กันยายน";
            break;
        case "10" : $myMonth = "ตุลาคม";
            break;
        case "11" : $myMonth = "พฤศจิกายน";
            break;
        case "12" : $myMonth = "ธันวาคม";
            break;
    }
    $myYear = sprintf("%d", $myDateArray[0]) + 543;
    return($myDay . " เดือน " . $myMonth . " พ.ศ. " . $myYear);
}

function ShowMount($mount) {
    switch ($mount) {
        case "01" : $myMonth = "มกราคม";
            break;
        case "02" : $myMonth = "กุมภาพันธ์";
            break;
        case "03" : $myMonth = "มีนาคม";
            break;
        case "04" : $myMonth = "เมษายน";
            break;
        case "05" : $myMonth = "พฤษภาคม";
            break;
        case "06" : $myMonth = "มิถุนายน";
            break;
        case "07" : $myMonth = "กรกฎาคม";
            break;
        case "08" : $myMonth = "สิงหาคม";
            break;
        case "09" : $myMonth = "กันยายน";
            break;
        case "10" : $myMonth = "ตุลาคม";
            break;
        case "11" : $myMonth = "พฤศจิกายน";
            break;
        case "12" : $myMonth = "ธันวาคม";
            break;
    }
    return($myMonth);
}

function ShowDateShortTime($myDate) {
    $myTimeArray = explode(" ", $myDate);
    $myTime = explode(":", $myTimeArray[1]);
    $myDateArray = explode("-", $myDate);
    $myDay = sprintf("%d", $myDateArray[2]);
    switch ($myDateArray[1]) {
        case "01" : $myMonth = "ม.ค.";
            break;
        case "02" : $myMonth = "ก.พ.";
            break;
        case "03" : $myMonth = "มี.ค.";
            break;
        case "04" : $myMonth = "เม.ย.";
            break;
        case "05" : $myMonth = "พ.ค";
            break;
        case "06" : $myMonth = "มิ.ย";
            break;
        case "07" : $myMonth = "ก.ค";
            break;
        case "08" : $myMonth = "ส.ค.";
            break;
        case "09" : $myMonth = "ก.ย.";
            break;
        case "10" : $myMonth = "ต.ค.";
            break;
        case "11" : $myMonth = "พ.ย";
            break;
        case "12" : $myMonth = "ธ.ค.";
            break;
    }
    $myYear = sprintf("%d", $myDateArray[0]) + 543;
    return($myDay . " " . $myMonth . " " . $myYear . ", " . $myTime[0] . ":" . $myTime[1]);
}

function ShowDateShort($myDate) {
    $myTimeArray = explode(" ", $myDate);
    $myTime = explode(":", $myTimeArray[1]);
    $myDateArray = explode("-", $myDate);
    $myDay = sprintf("%d", $myDateArray[2]);
    switch ($myDateArray[1]) {
        case "01" : $myMonth = "ม.ค.";
            break;
        case "02" : $myMonth = "ก.พ.";
            break;
        case "03" : $myMonth = "มี.ค.";
            break;
        case "04" : $myMonth = "เม.ย.";
            break;
        case "05" : $myMonth = "พ.ค";
            break;
        case "06" : $myMonth = "มิ.ย";
            break;
        case "07" : $myMonth = "ก.ค";
            break;
        case "08" : $myMonth = "ส.ค.";
            break;
        case "09" : $myMonth = "ก.ย.";
            break;
        case "10" : $myMonth = "ต.ค.";
            break;
        case "11" : $myMonth = "พ.ย";
            break;
        case "12" : $myMonth = "ธ.ค.";
            break;
    }
    $myYear = sprintf("%d", $myDateArray[0]) + 543;
    return($myDay . " " . $myMonth . " " . $myYear);
}

function convert($str) {
    return iconv("Windows-1252", "UTF-8", $str);
}

function convertNumtoTh($number) {
    $txtnum1 = array('ศูนย์', 'หนึ่ง', 'สอง', 'สาม', 'สี่', 'ห้า', 'หก', 'เจ็ด', 'แปด', 'เก้า', 'สิบ');
    $txtnum2 = array('', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน', 'สิบ', 'ร้อย', 'พัน', 'หมื่น', 'แสน', 'ล้าน');
    $number = str_replace(",", "", $number);
    $number = str_replace(" ", "", $number);
    $number = str_replace("บาท", "", $number);
    $number = explode(".", $number);
    if (sizeof($number) > 2) {
        return 'ทศนิยมหลายตัวนะจ๊ะ';
        exit;
    }
    $strlen = strlen($number[0]);
    $substr = substr($number[0], -2, 1);
    $convert = '';
    for ($i = 0; $i < $strlen; $i++) {
        $n = substr($number[0], $i, 1);
        if ($n != 0) {
            if ($i == ($strlen - 1) AND $n == 1) {
                if ($substr != "0") {
                    $convert .= 'เอ็ด';
                } else {
                    $convert .= 'หนึ่ง';
                }
            } elseif ($i == ($strlen - 2) AND $n == 2) {
                $convert .= 'ยี่';
            } elseif ($i == ($strlen - 8) AND $n == 2 AND $i == 0) {
                $convert .= 'ยี่';
            } elseif ($i == ($strlen - 2) AND $n == 1) {
                $convert .= '';
            } else {
                $convert .= $txtnum1[$n];
            }
            $convert .= $txtnum2[$strlen - $i - 1];
        }
    }

    $convert .= 'บาท';
    if ($number[1] == '0' OR $number[1] == '00' OR
            $number[1] == '') {
        $convert .= 'ถ้วน';
    } else {
        $strlen = strlen($number[1]);
        for ($i = 0; $i < $strlen; $i++) {
            $n = substr($number[1], $i, 1);
            if ($n != 0) {
                if ($i == ($strlen - 1) AND $n == 1) {
                    $convert .= 'เอ็ด';
                } elseif ($i == ($strlen - 2) AND $n == 2) {
                    $convert .= 'ยี่';
                } elseif ($i == ($strlen - 2) AND $n == 1) {
                    $convert .= '';
                } else {
                    $convert .= $txtnum1[$n];
                }
                $convert .= $txtnum2[$strlen - $i - 1];
            }
        }
        $convert .= 'สตางค์';
    }
    return $convert;
}

function padZero($input, $digit) {
    return str_pad($input, $digit, "0", STR_PAD_LEFT);
}

function sentSMS($massage,$phone = "0830561165"){
	$url = "http://www.sbuysms.com/api.php"; 
	$user = "phiopan"; 
	$pass = "026924385"; 
	$phone = $phone; 
	$message = $massage; 
	 
	$param = "command=send&username=$user&password=$pass&msisdn=$phone&message=$message"; 
	$agent = "Mozilla/5.0 (Windows; U; Windows NT 5.0; en-US; rv:1.4) Gecko/20030624 Netscape/7.1 (ax)"; 
	 
	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_USERAGENT, $agent); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
	curl_setopt($ch, CURLOPT_POSTFIELDS, $param); 
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15); 
	$result = curl_exec($ch); 
	curl_close ($ch);
}

function rand12($length = 1) {
	$characters = '12';
	$charactersLength = strlen($characters);
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, $charactersLength - 1)];
	}
	return $randomString;
}

function getImgCache($src,$dim){
	global $docimgcrop;
	return $docimgcrop.$src.",".$dim;
}
/* * ******************
  END FUNCTIONS
 * ******************* */

/**************************************/
/************** FUNCTION GLOBAL
/**************************************/
/*************************************/
/************** END FUNCTION GLOBAL
/*************************************/
?>