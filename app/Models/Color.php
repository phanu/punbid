<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class color extends Sximo  {
	
	protected $table = 'product_color';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT product_color.* FROM product_color  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE product_color.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
