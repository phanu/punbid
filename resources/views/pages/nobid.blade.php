	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="woo-cart-table rs_bottompadder30">
						<table class="table">
							<tbody><tr>
								<th></th>
								<th>สินค้า</th>
								<th>จำนวน</th>
								<th>ราคา</th>
								<th></th>
							</tr>
							<tr>
								<td></td>
								<td>
									<img src="images/bidpack/bidpack-06.svg" alt="">
									<p>Bid Pack 1,000</p>
								</td>
								<td class="text-center">
									<div class="rs_incre_decre_btns">
										<button type="button" class="btn_small count_minus" id="breakDecrease">
										  <i class="fa fa-minus"></i>
										</button>
										<button type="button" class="btn_big count_val" id="breakValue" disabled="">1</button>
										<button type="button" class="btn_small count_plus" id="breakIncrease">
										  <i class="fa fa-plus"></i>
										</button>
									</div>
								</td>
								<td class="text-center"><span>2,000<small></small>THB</span>
								</td>
								<td class="text-center"><a href="#"><i class="fa fa-times"></i></a>
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<img src="images/bidpack/bidpack-01.svg" alt="">
									<p>Bid Pack 25</p>
								</td>
								<td class="text-center">
									<div class="rs_incre_decre_btns">
										<button type="button" class="btn_small count_minus" id="breakDecrease">
										  <i class="fa fa-minus"></i>
										</button>
										<button type="button" class="btn_big count_val" id="breakValue" disabled="">1</button>
										<button type="button" class="btn_small count_plus" id="breakIncrease">
										  <i class="fa fa-plus"></i>
										</button>
									</div>
								</td>
								<td class="text-center"><span>50<small>THB</small></span>
								</td>
								<td class="text-center"><a href="#"><i class="fa fa-times"></i></a>
								</td>
							</tr>
						</tbody></table>
					</div>
				</div>
				<div class="col-lg6 col-md-6 col-sm-12 col-xs-12">
				<div class="rs_offerdiv">
					<div class="woo_product-code">
						<form class="form-inline">
							<div class="form-group">
								<input type="text" name="code" class="form-control" value="" placeholder="ใส่คูปองลดราคา">
							</div>
							<button class="rs_apply_coupenbtn rs_button rs_button_orange">ตกลง</button>
						</form>
					</div>
				</div>
				</div>
				<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
					<div class="cart_totals">
						<div class="cart-heading rs_bottompadder20">รวม</div>
						<div class="cart-subtotal">
							<p>
								<label>ราคา</label> <span>2,050<small>THB</small></span>
							</p>
							<p>
								<label>คูปองลดราคา</label> <span>0<small>THB</small></span>
							</p>
						</div>
						<div class="order-total rs_toppadder10 rs_bottompadder10">
							<p>
								<label>ราคาทั้งหมด</label> <span>2,050<small>THB</small></span>
							</p>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="rs_showmore rs_toppadder50">
						<a href="bidcheckout.php" class="rs_button rs_button_orange">ชำระเงิน</a>
					</div>
					<div class="rs_continuecart rs_toppadder50">
						<a href="shop.php" class="rs_button rs_button_orange">เลือกซื้อสินค้าต่อ</a>
					</div>
				</div>