<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class winnerordersdetail extends Sximo  {
	
	protected $table = 'orderdetailswinner';
	protected $primaryKey = 'orderDetailId';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT orderdetailswinner.* FROM orderdetailswinner  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE orderdetailswinner.orderDetailId IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
