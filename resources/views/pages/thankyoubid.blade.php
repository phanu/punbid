<?php 



if(Session::get('data')!=null)
{
    $data = Session::get('data');
}else
{
echo '<div class="alert alert-danger  fade in block-inner">
            <i class="icon-cancel-circle"></i> '.Lang::get('thankyou.1').' </div>';
            exit();
}

?>


<div marginwidth="0" marginheight="0" style="min-height:100%!important;margin:0;padding:0;width:100%!important;background-color:#e5eaec">
    <center>
   
    @if(Session::has('messagetext'))
    
       {!! Session::get('messagetext') !!}
     
  @endif
  <ul class="parsley-error-list">
    @foreach($errors->all() as $error)
      <li>{!! $error !!}</li>
    @endforeach
  </ul>   
        <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" style="border-collapse:collapse;height:100%!important;margin:0;padding:0;width:100%!important;background-color:#e5eaec"><tbody><tr>
            <td align="center" valign="top" style="height:100%!important;margin:0;padding:0;width:100%!important;border-top:0">
                
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse">
                    <tbody><tr>
                        <td align="center" valign="top">
                            
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;background-color:#e5eaec;border-top:0;border-bottom:0"><tbody><tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse"><tbody><tr>
                                        <td valign="top" style="padding-top:60px; text-align:right;">
                                        <a href="{!! url('/') !!}">
                                            <img src="{!! asset('frontend') !!}/punbidtheme/images/bidloei.svg">
                                        </a>
                                        </td>
                                    </tr></tbody></table>
                                </td>                     
                                
                            </tr></tbody></table>

                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse"><tbody><tr>
                                <td align="center" valign="top">
                                    <table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse"><tbody><tr>
                                        <td valign="top" style="padding:0 0 30px 0;background-color:#ffffff;background-image:url('{!! asset('frontend') !!}/punbidtheme/images/emailbg.png');background-position:left top;background-repeat:no-repeat">



                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse"><tbody><tr>
                                                <td valign="top">

                                                  
                                                   <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse"><tbody><tr>
                                                    <td width="150px" height="136px">
                                                      
                                                    </td>

                                                    
                                                    <td valign="middle" style="padding-left:30px;padding-right:17px;padding-bottom:20px;padding-top:30px;text-align:right;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%">
                                                      
                                                        
                                                      
                                                    </td>
                                                    
                                                </tr></tbody></table>
                                            </td>
                                            
                                        </tr></tbody></table>
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse"><tbody><tr>
                                            <td valign="top">

                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse"><tbody><tr>
                                                    <td valign="top" style="padding-top:20px;padding-bottom:20px;padding-right:47px;padding-left:47px;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%;text-align:left">
                                                        <h1 style="margin:0;padding:0;color:#000;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:22px;font-weight:300;line-height:110%">{!! html_entity_decode(Lang::get('thankyou.2')) !!}</h1> 
                                                        <p style="margin:1em 0;padding:0;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%;text-align:left">{!! html_entity_decode(Lang::get('thankyou.3')) !!} {!!$userdet->first_name!!} {!!$userdet->last_name!!}{!! html_entity_decode(Lang::get('thankyou.4')) !!}</p>
                                                            <h2 style="margin:0;padding:0;color:#313131;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:28px;font-weight:300;line-height:125%">{!! html_entity_decode(Lang::get('thankyou.5')) !!} #{!!$data['orid']!!} {!! html_entity_decode(Lang::get('thankyou.6')) !!} <time datetime="2016-08-05T05:58:15+00:00">{!! date("d/m/Y") !!}</time>
                                                            </h2>
                                                            
                                                        </td>
                                                    </tr></tbody></table>
                                                </td>
                                            </tr></tbody></table>
                                            <table border="0" cellpadding="0" cellspacing="0" width="506" align="center" style="border-collapse:collapse"><tbody><tr>
                                                <td valign="top">

                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse"><tbody>
                                                        <tr>
                                                            <td scope="col" style="text-align:left;border:none;border-bottom:2px solid #ccc;padding:14px 12px 12px 12px;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%"><strong>{!! html_entity_decode(Lang::get('thankyou.7')) !!}</strong></td>
                                                            <td scope="col" style="text-align:left;border:none;border-bottom:2px solid #ccc;border-left:1px solid #ccc;padding:14px 12px 12px 12px;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%"><strong>{!! html_entity_decode(Lang::get('thankyou.8')) !!}</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:left;border:none;border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding:14px 12px 12px 12px;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%">
                                                                {!! html_entity_decode(Lang::get('thankyou.16')) !!} {!!$data['bids']!!} 
                                                            </td>
                                                            <td style="text-align:left;border:none;border-left:1px solid #ccc;border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding:14px 12px 12px 12px;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%;white-space:nowrap">
                                                            <span>{!! number_format($data['price'],2) !!} THB</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td scope="row" style="text-align:left;border:none;border-top:2px solid #ccc;border-bottom:1px solid #ccc;padding:14px 12px 12px 12px;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%"><strong>{!! html_entity_decode(Lang::get('thankyou.9')) !!}</strong></td>
                                                            <td style="text-align:left;border:none;border-left:1px solid #ccc;border-top:2px solid #ccc;border-bottom:1px solid #ccc;padding:14px 12px 12px 12px;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%;white-space:nowrap"><span>{!! number_format($data['price'],2) !!} THB</span></td>
                                                        </tr>
                                                        <tr>
                                                            <td scope="row" style="text-align:left;border:none;border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding:14px 12px 12px 12px;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%"><strong>{!! html_entity_decode(Lang::get('thankyou.10')) !!}</strong></td>
                                                            <td style="text-align:left;border:none;border-left:1px solid #ccc;border-top:1px solid #ccc;border-bottom:1px solid #ccc;padding:14px 12px 12px 12px;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%">{!!$data['payment_type']!!}</td>
                                                        </tr>
                                                    </tbody></table>
                                                </td>
                                            </tr></tbody></table>
                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse"><tbody>
                                              <?php /*  <tr>
                                                    <td valign="top" style="padding-top:20px;padding-right:47px;padding-bottom:10px;padding-left:47px;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%;text-align:left">
                                                        <h2 style="margin:0;padding:0;color:#313131;display:block;font-family:&quot;Helvetica Neue&quot;,Helvetica,Arial,sans-serif;font-size:28px;font-weight:300;line-height:125%">Billing Information</h2>
                                                        <p style="margin:1em 0;padding:0;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%;text-align:left">
                                                            {!!$data['firstname']!!} {!!$data['lastname']!!}<br>
                                                            {!!$data['address']!!} {!!$data['district']!!} {!!$data['subdistrict']!!}<br>{!!$data['province']!!}<br>
                                                            {!!$data['postcode']!!}<br>
                                                            {!!$data['phone']!!}<!--<a href="mailto:info@graphicbuffet.co.th" style="word-wrap:break-word;color:#057dce;text-decoration:none" target="_blank">info@graphicbuffet.co.th</a>--><br></p>


                                                        </td>
                                                    </tr>*/ ?>  
                                                    <tr>
                                                        <td valign="top" style="padding-top:10px;padding-right:47px;padding-bottom:10px;padding-left:47px;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%;text-align:left">
                                                            <p style="margin:1em 0;padding:0;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%;text-align:left">{!! html_entity_decode(Lang::get('thankyou.13')) !!}</p>

                                                            </td>
                                                        </tr>
                                                    </tbody></table>
                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse"><tbody><tr>
                                                        <td valign="top">

                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse"><tbody><tr>
                                                                <td valign="top" style="padding-top:10px;padding-right:47px;padding-bottom:10px;padding-left:47px;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%;text-align:left">
                                                                    <img src="{!! asset('frontend') !!}/punbidtheme/images/punbidthemeblack.svg" width="159" height="61" style="display:block;border:0;outline:none;text-decoration:none;min-height:auto!important" class="CToWUd"><p style="margin:1em 0;padding:0;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:14px;line-height:150%;text-align:left">{!! html_entity_decode(Lang::get('thankyou.14')) !!}</a></p>


                                                                </td>
                                                            </tr></tbody></table>
                                                        </td>
                                                    </tr></tbody></table>
                                                </td>
                                            </tr></tbody></table>
                                        </td>
                                    </tr></tbody></table>

                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse;border-top:0;border-bottom:0"><tbody><tr>
                                        <td align="center" valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse:collapse"><tbody><tr>
                                                <td valign="top" style="padding-top:40px;padding-bottom:30px;background:url(&quot;https://ci5.googleusercontent.com/proxy/LLe1K94ZDtcVb4a1tMjOyeDq-yM-Cs9ibR9i_1uc-F6nDgBPLJnDGa1IuDpjjuc8oCtJONtvHTC8vM_vw3pkZ1H1XtQj8KGy3YiIs1nsGjTDvKkRTwzHC5k4B8OHC_AKzp8jDPAy7Eq-fh3GZERX6y89RW_-fRuE_4De6Ng=s0-d-e1-ft#https://gallery.mailchimp.com/11b8c330837b0ae3a70c0356c/images/193eed7a-b087-4e40-8498-baaccc76abdb.png&quot;) 0 0 no-repeat">


                                                    <table border="0" cellpadding="0" cellspacing="0" width="506" align="center" style="border-collapse:collapse"><tbody><tr>
                                                        <td valign="top">

                                                            <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse:collapse"><tbody><tr>
                                                                <td valign="top" style="padding-top:10px;padding-right:47px;padding-bottom:10px;padding-left:47px;color:#313131;font-family:Helvetica,Arial,sans-serif;font-weight:300;font-size:11px;line-height:125%;text-align:center">
                                                                  
                                                                  {!! html_entity_decode(Lang::get('thankyou.15')) !!}
                                                                   
                                                               </td>
                                                           </tr></tbody></table>
                                                       </td>
                                                   </tr></tbody></table>
                                               </td>
                                           </tr></tbody></table>
                                       </td>
                                   </tr></tbody></table>

                               </td>
                           </tr>
                       </tbody></table>

                   </td>
               </tr></tbody></table>
           </center>
           <img src="https://ci3.googleusercontent.com/proxy/KWifZJBOQm7-Zt8p3v8ZL6c4zcLy6m5IiZG_rRLdc-oX0mGzIaJkjRNhy8OUvZ32eA28t4hu20AT0RtrLdCpCUId_e0f5GTb8sIpKCr5XbGWRldiNC2PPf7lZyrrX9cOyjX9VRZ9WAGDZw=s0-d-e1-ft#http://mandrillapp.com/track/open.php?u=30785385&amp;id=9866723a59be4f73aee776fba1a94755" height="1" width="1" class="CToWUd"></div>
<?php Cart::instance('shop')->destroy();  ?>
<script type="text/javascript">
    var sumbit = <?php echo $data['bids']*$data['qty']; ?>
</script>
