<?php namespace App\Http\Controllers;

use App\Http\Controllers\controller;
use App\Models\Bidorders;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect; 
use \App\Http\Controllers\UserscustomController; 
use App\User;
class BidordersController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'bidorders';
	static $per_page	= '10';
	
	public function __construct() 
	{
		parent::__construct();
		$this->model = new Bidorders();
		$this->info = $this->model->makeInfo( $this->module);
		$this->access = $this->model->validAccess($this->info['id']);
		$this->data = array(
			'pageTitle'			=> 	$this->info['title'],
			'pageNote'			=>  $this->info['note'],
			'pageModule'		=> 'bidorders',
			'pageUrl'			=>  url('bidorders'),
			'return' 			=> 	self::returnUrl()	
		);		
	} 

	public function getIndex()
	{
		if($this->access['is_view'] ==0) 
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
				
		$this->data['access']		= $this->access;	
		return view('bidorders.index',$this->data);
	}	

	public function postData( Request $request)
	{ 
		$sort = (!is_null($request->input('sort')) ? $request->input('sort') : $this->info['setting']['orderby']); 
		$order = (!is_null($request->input('order')) ? $request->input('order') : $this->info['setting']['ordertype']);
		// End Filter sort and order for query 
		// Filter Search for query		
		$filter = '';	
		if(!is_null($request->input('search')))
		{
			$search = 	$this->buildSearch('maps');
			$filter = $search['param'];
			$this->data['search_map'] = $search['maps'];
		}
		
		$page = $request->input('page', 1);
		$params = array(
			'page'		=> $page ,
			'limit'		=> (!is_null($request->input('rows')) ? filter_var($request->input('rows'),FILTER_VALIDATE_INT) : $this->info['setting']['perpage'] ) ,
			'sort'		=> $sort ,
			'order'		=> $order,
			'params'	=> $filter,
			'global'	=> (isset($this->access['is_global']) ? $this->access['is_global'] : 0 )
		);
		// Get Query 
		$results = $this->model->getRows( $params );		
		
		// Build pagination setting
		$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
		$pagination = new Paginator($results['rows'], $results['total'], $params['limit']);	
		$pagination->setPath('bidorders/data');
		
		$this->data['param']		= $params;
		$this->data['rowData']		= $results['rows'];
		// Build Pagination 
		$this->data['pagination']	= $pagination;
		// Build pager number and append current param GET
		$this->data['pager'] 		= $this->injectPaginate();	
		// Row grid Number 
		$this->data['i']			= ($page * $params['limit'])- $params['limit']; 
		// Grid Configuration 
		$this->data['tableGrid'] 	= $this->info['config']['grid'];
		$this->data['tableForm'] 	= $this->info['config']['forms'];
		$this->data['colspan'] 		= \SiteHelpers::viewColSpan($this->info['config']['grid']);		
		// Group users permission
		$this->data['access']		= $this->access;
		// Detail from master if any
		$this->data['setting'] 		= $this->info['setting'];
		
		// Master detail link if any 
		$this->data['subgrid']	= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array()); 
		// Render into template
		return view('bidorders.table',$this->data);
	}

			
	function getUpdate(Request $request, $id = null)
	{
		if($id =='')
		{
			if($this->access['is_add'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}	
		
		if($id !='')
		{
			if($this->access['is_edit'] ==0 )
			return Redirect::to('dashboard')->with('messagetext',\Lang::get('core.note_restric'))->with('msgstatus','error');
		}				
				
		$row = $this->model->find($id);
		if($row)
		{
			$this->data['row'] 		=  $row;
		} else {
			$this->data['row'] 		= $this->model->getColumnTable('bid_orders'); 
		}
		$this->data['setting'] 		= $this->info['setting'];
		$this->data['fields'] 		=  \AjaxHelpers::fieldLang($this->info['config']['forms']);
		
		$this->data['id'] = $id;

		return view('bidorders.form',$this->data);
	}	

	public function getShow( $id = null)
	{
	
		if($this->access['is_detail'] ==0) 
			return Redirect::to('dashboard')
				->with('messagetext', \Lang::get('core.note_restric'))->with('msgstatus','error');
					
		$row = $this->model->getRow($id);
		if($row)
		{
			$this->data['row'] =  $row;
			
			$this->data['id'] = $id;
			$this->data['access']		= $this->access;
			$this->data['setting'] 		= $this->info['setting'];
			$this->data['fields'] 		= \AjaxHelpers::fieldLang($this->info['config']['grid']);
			$this->data['subgrid']		= (isset($this->info['config']['subgrid']) ? $this->info['config']['subgrid'] : array());
			return view('bidorders.view',$this->data);

		} else {

			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_error')
			));		
		}		
	}	


	function postCopy( Request $request)
	{
		
	    foreach(\DB::select("SHOW COLUMNS FROM bid_orders ") as $column)
        {
			if( $column->Field != 'orderNumber')
				$columns[] = $column->Field;
        }
		if(count($request->input('ids')) >=1)
		{

			$toCopy = implode(",",$request->input('ids'));
			
					
			$sql = "INSERT INTO bid_orders (".implode(",", $columns).") ";
			$sql .= " SELECT ".implode(",", $columns)." FROM bid_orders WHERE orderNumber IN (".$toCopy.")";
			\DB::select($sql);
			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success')
			));		

		} else {
			return response()->json(array(
				'status'=>'success',
				'message'=> 'Please select row to copy'
			));	
		}

	
	}		

	function postSave( Request $request, $id =0)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('bid_orders');
			
			$id = $this->model->insertRow($data , $request->input('orderNumber'));
			
			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success')
				));	
			
		} else {

			$message = $this->validateListError(  $validator->getMessageBag()->toArray() );
			return response()->json(array(
				'message'	=> $message,
				'status'	=> 'error'
			));	
		}	
	
	}	

	public function postDelete( Request $request)
	{

		if($this->access['is_remove'] ==0) {   
			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_restric')
			));
			die;

		}		
		// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));
			
			return response()->json(array(
				'status'=>'success',
				'message'=> \Lang::get('core.note_success_delete')
			));
		} else {
			return response()->json(array(
				'status'=>'error',
				'message'=> \Lang::get('core.note_error')
			));

		} 		

	}

	public static function display( )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Bidorders();
		$info = $model::makeInfo('bidorders');

		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']
			
		);

		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return $datas;
			} 

		} else {

			$yourid = \Session::get('uid');

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10000 ) ,
				'sort'		=> 'orderNumber' ,
				'order'		=> 'desc',
				'params'	=> ' and customerNumber = '.$yourid,
				'global'	=> 1 
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];	

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit']; 
			$data['pagination'] = $pagination;
			return $data;			
		}


	}

	function postSavepublic( Request $request)
	{
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost('bid_orders');		

			$data['payment_fee'] = $request->input('payment_fee') + $request->input('numtotal');
			$id = $this->model->insertRow($data , $request->input('orderNumber'));
			$data['orid'] = $id;
/*
			use \App\Http\Controllers\UserscustomController; 
$sumbid = $userdet->bid+($data['bids']*$data['qty']); 

$userdet_updated = new UserscustomController;
$userdet_updated->postSavepublic(Session::get('uid'),$sumbid);
*/	
			\DB::table('tb_notification')->insert([
											'userid' => \Session::get('uid'), 'title' => \Lang::get('core.orderl').$data['orid'].\Lang::get('core.ordercon'), 'noticlass' => 'icon bg-success', 'icon' => 'zmdi zmdi-plus-circle-o',
											]);

			$userdet = User::find(\Session::get('uid'));

			$data['first_name'] = $userdet->first_name;
			$data['last_name'] = $userdet->last_name;
			$to = $userdet->email;
				$subject = \Lang::get('core.orderl').$data['orid'].\Lang::get('core.ordercon');
				$message = view('user.emails.thankyoubid', $data)->render();
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: '.CNF_APPNAME.' <'.CNF_EMAIL.'>' . "\r\n";
						mail($to, $subject, $message, $headers);

			if($request->input('payment_type')=="dbt")
			{
				
				return Redirect::to('thankyoubid')->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success')->with('data',$data);

			}elseif($request->input('payment_type')=="credit_card")
			{
				return Redirect::to('paysbuybidcheckout')->with('data',$data);
			}

		} else {

			return  Redirect::back()->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
			->withErrors($validator)->withInput();

		}	
	
	}	

function postSavecredit( Request $request)
{
$model  = new Bidorders();
// received result from paysbuy with data result,apCode,amt,fee,methos,confirm_cs
$result = $_POST["result"];
$len = strlen($result);
$payment_status = substr($result, 0,2);
$strInvoice = substr($result, 2,$len-2);
$apCode = $_POST["apCode"];
$amt = $_POST["amt"];
$fee = $_POST["fee"];
$method = $_POST["method"];
$confirm_cs = isset($_POST["confirm_cs"])?$_POST["confirm_cs"]:'';
/* status result
00=Success
99=Fail
02=Process
*/
if ($payment_status == "00") {
	if ($method == "06") {
		if ($confirm_cs == "true") {
			
			//success
			$bidorders = $model::where('orderNumber', '=', $strInvoice)->first();
			\DB::table('tb_users')->whereId($bidorders->customerNumber)->increment('bid', $bidorders->bids*$bidorders->qty);
			\DB::table('bid_orders')->where('orderNumber', '=', $strInvoice)->update(array('status' => 'success'));
			\DB::table('tb_notification')->insert([
											'userid' => $bidorders->customerNumber, 'title' => \Lang::get('core.youhadgot').$bidorders->bids*$bidorders->qty.' Bid', 'noticlass' => 'icon bg-success', 'icon' => 'zmdi zmdi-plus-circle'
											]);

			$$data['userdet'] = User::find(\Session::get('uid'));

			$to = $data['userdet']->email;
				$subject = \Lang::get('core.orderl').$strInvoice.\Lang::get('core.orderalreadypay');
				$message = view('user.emails.thankyoubid', $bidorders)->render();
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: '.CNF_APPNAME.' <'.CNF_EMAIL.'>' . "\r\n";
						mail($to, $subject, $message, $headers);
			

				return Redirect::to('thankyoubid')->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success')->with('data',$bidorders);

			} else if ($confirm_cs == "false") {
				//fail
				return  Redirect::to('thankyoubid')->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
							->withErrors($validator)->withInput();
			} else {
				//process
				return  Redirect::to('thankyoubid')->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
							->withErrors($validator)->withInput();
		}
	} else {

			//success
			$bidorders = $model::where('orderNumber', '=', $strInvoice)->first();
			\DB::table('tb_users')->whereId($bidorders->customerNumber)->increment('bid', $bidorders->bids*$bidorders->qty);
			\DB::table('bid_orders')->where('orderNumber', '=', $strInvoice)->update(array('status' => 'success'));
			\DB::table('tb_notification')->insert([
											'userid' => $bidorders->customerNumber, 'title' => \Lang::get('core.youhadgot').$bidorders->bids*$bidorders->qty.' Bid', 'noticlass' => 'icon bg-success', 'icon' => 'zmdi zmdi-plus-circle'
											]);

			$data['userdet'] = User::find(\Session::get('uid'));

			$to = $data['userdet']->email;
				$subject = \Lang::get('core.orderl').$strInvoice.\Lang::get('core.orderalreadypay');
				$message = view('user.emails.thankyoubid', $bidorders)->render();
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					$headers .= 'From: '.CNF_APPNAME.' <'.CNF_EMAIL.'>' . "\r\n";
						mail($to, $subject, $message, $headers);

						
				return Redirect::to('thankyoubid')->with('messagetext','<p class="alert alert-success">'.\Lang::get('core.note_success').'</p>')->with('msgstatus','success')->with('data',$bidorders);

	}
} else if ($payment_status == "99") {
				//fail
				return  Redirect::to('thankyoubid')->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
							->withErrors($validator)->withInput();

} else if ($payment_status == "02") {
				//process
				return  Redirect::to('thankyoubid')->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
							->withErrors($validator)->withInput();

} else {

	return  Redirect::to('thankyoubid')->with('messagetext','<p class="alert alert-danger">'.\Lang::get('core.note_error').'</p>')->with('msgstatus','error')
				->withErrors($validator)->withInput();
}


			
		
	
	}	
				
}