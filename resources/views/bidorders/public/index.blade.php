<div class="rs_purchase_table">    
    <table class="table">
        <thead>
			<tr>
				@foreach ($tableGrid as $t)
					@if($t['view'] =='1')				
						<?php $limited = isset($t['limited']) ? $t['limited'] :''; ?>
						@if(SiteHelpers::filterColumn($limited ))
						
							<th>{{ $t['label'] }}</th>			
						@endif 
					@endif
				@endforeach
				<th>

				</th>
				
			  </tr>
        </thead>

        <tbody>        						
            @foreach ($rowData as $row)
                <tr>									
				 @foreach ($tableGrid as $field)
					 @if($field['view'] =='1')
					 	<?php $limited = isset($field['limited']) ? $field['limited'] :''; ?>
					 	@if(SiteHelpers::filterColumn($limited ))
						 <td>					 
						 	<p>{!! SiteHelpers::formatRows($row->{$field['field']},$field,$row) !!}</p>						 
						 </td>
						@endif	
					 @endif					 
				 @endforeach
				 <td>
					<!--<a href="?view={{ $row->orderNumber }}" oncl> <i class="fa fa-search"></i></a>-->			
					
				</td>				 
                </tr>
				
            @endforeach
              
        </tbody>        

    </table>  
<div class="text-center"> {!! $pagination->render() !!}</div>
</div>  
